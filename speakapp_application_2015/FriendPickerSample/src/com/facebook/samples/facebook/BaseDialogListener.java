package com.facebook.samples.facebook;



import com.facebook.samples.facebook.Facebook.DialogListener;

import android.util.Log;


/**
 * Skeleton base class for RequestListeners, providing default error handling.
 * Applications should handle these error conditions.
 * 
 */
public abstract class BaseDialogListener implements DialogListener {

	private final String TAG = this.getClass().getName();

	public void onFacebookError(FacebookError e) {
		Log.e("FacebookError", ""+e.getMessage());
	}

	public void onError(DialogError e) {
		Log.e("DialogError", ""+e.getMessage());
	}

	public void onCancel() {
	}

}
