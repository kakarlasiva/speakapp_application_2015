package com.facebook.samples.fbconnect;

public class FBConnectJSONConst {
	public static final String  DATA = "data";
	public static final String  AID = "aid";
	public static final String  NAME="name";
	public static final String  COVER_OBJ_ID="cover_object_id";
	public static final String  PHOTO_COUNT="photo_count";
	public static final String  OBJ_ID="object_id";
	public static final String  PHOTOS_ME="Photos of Me";
	public static final String  LOGGED_AS="Logged in as ";
	public static final String  ON_DESTROY= "onDestroy";
	public static final String  ALBUM_ID="album-id";
	public static final String  YES="Yes";
	public static final String  NO="No";
	public static final String  CHECK_OUT="Checkout";
	public static final String  PRINT="Print";
	public static final String  IMAGES="images";
	public static final String  SOURCE="source"; 
	public static final String  SRC_SMALL="src_small";
	public static final String  LOADING="Loading...";
	public static final String  OK="OK";
	public static final String  TRUE="true";
	public static final String  CANCEL="Cancel";
	public static final String  GET="GET";
	public static final String  TOTAL_COUNT="TOTAL_COUNT";
	public static final String  PROFILE_PICTURES="Profile pictures";
	public static final String  ACCESS_TOKEN="";
	public static final String  ME="me";
	public static final String  ERROR="error";
	public static final String  ERROR_TYPE="error_type";
	public static final String  ACCESS_DENIED="access_denied";
}
