package com.facebook.samples.fbconnect;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.facebook.samples.friendpicker.R;
import com.facebook.samples.friendpicker.UrlImageLoader;

public class MyFBFriendAdapter extends ArrayAdapter<FbFriend> implements SectionIndexer {
	private List<FbFriend> stringArray;
	private Context context;
	private Activity activity;
	private UrlImageLoader fBConnectImageLoader;

	public MyFBFriendAdapter(Context _context,int resID, List<FbFriend> arr,
			UrlImageLoader fbFriendImageLoader) {
		super(_context, resID, arr);
		stringArray = arr;
		context = _context;
		activity = (Activity)context;
		fBConnectImageLoader = fbFriendImageLoader;
	}

	public int getCount() {
		return stringArray.size();
	}

	public FbFriend getItem(int arg0) {
		return stringArray.get(arg0);
	}

	public long getItemId(int arg0) {
		return 0;
	}

	public View getView(int position, View v, ViewGroup parent) {
		LayoutInflater inflate = ((Activity) context).getLayoutInflater();
		View view = (View) inflate.inflate(R.layout.layout_row,parent,false);
		LinearLayout header = (LinearLayout) view.findViewById(R.id.section);
		String label = stringArray.get(position).getName();
		String number = stringArray.get(position).getObjectID();
		String url = stringArray.get(position).getUrl();
		char firstChar = label.toUpperCase().charAt(0);
		if (position == 0) {
			setSection(header, label);
		} else {
			String preLabel = stringArray.get(position - 1).getName();
			char preFirstChar = preLabel.toUpperCase().charAt(0);
			if (firstChar != preFirstChar) {
				setSection(header, label);
			} else {
				header.setVisibility(View.GONE);
			}
		}
		TextView textView = (TextView) view.findViewById(R.id.textView);
		textView.setText(label);
		TextView textView2 = (TextView) view.findViewById(R.id.textView2);
		textView2.setText(number);
		ImageView imgViw = (ImageView) view.findViewById(R.id.imageID);
		imgViw.setTag(stringArray
				.get(position).getUrl());
		if (null != stringArray.get(position).getUrl()) {
			fBConnectImageLoader.DisplayImage(url, activity, imgViw);
		}
		return view;
	}

	private void setSection(LinearLayout header, String label) {
		TextView text = new TextView(context);
		header.setBackgroundColor(0xffaabbcc);
		text.setTextColor(Color.WHITE);
		text.setText(label.substring(0, 1).toUpperCase());
		text.setTextSize(20);
		text.setPadding(5, 0, 0, 0);
		text.setGravity(Gravity.CENTER_VERTICAL);
		header.addView(text);
	}

	public int getPositionForSection(int section) {
		if (section == 35) {
			return 0;
		}
		for (int i = 0; i < stringArray.size(); i++) {
			String l = stringArray.get(i).getName();
			char firstChar = l.toUpperCase().charAt(0);
			if (firstChar == section) {
				return i;
			}
		}
		return -1;
	}

	public int getSectionForPosition(int arg0) {
		return 0;
	}

	public Object[] getSections() {
		return null;
	}
}