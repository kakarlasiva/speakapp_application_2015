
package com.facebook.samples.friendpicker;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;


import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.Process;
import android.os.StatFs;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


public class Common {

    private static final String TAG = Common.class.getSimpleName();

    public static final boolean DEBUG = true;

    public static Resources resourcesBundle;

    public static boolean isbackPressed = false;



    static Dialog mOverlayDialog;

    public static final boolean isInternetAvailable(Context ctx) {
        boolean lRetVal = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) ctx
            .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                NetworkInfo nInfo = cm.getActiveNetworkInfo();
                if (null != nInfo) {
                    lRetVal = nInfo.isConnectedOrConnecting();
                }
            }
        } catch (Exception e) {
            return lRetVal;
        }

        return lRetVal;
    }

    public static void showToast(Context context, String Mesg) {
        Toast.makeText(context, Mesg, Toast.LENGTH_LONG).show();
    }

    public static boolean checkForFreeSpace() {
        long FIVE_MB = 5;
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
        long megAvailable = bytesAvailable / 1048576;// MB conversion
        if (Common.DEBUG) {

            Log.d("Common", "bytesAvailable:" + megAvailable + " MB");
        }
        return (megAvailable > FIVE_MB);
    }


    /**
     * @param tag can be null
     * @param message can be null
     * @param ex can be null
     * @param app can't be null
     */



    public static String getAppVersion(Application app) {
        Resources res = app.getResources();
      /*  final String version = String.format(res.getString(R.string.version), res
                .getString(R.string.version_number));*/
        return "";
    }

    private String get() {
        return Build.VERSION.RELEASE;
    }
    public static void internetAlertMsg(Context ctx) {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(ctx);
        alertbox.setTitle(R.string.alert_InternetConnection_title);
        alertbox.setMessage(R.string.alert_InternetConnection);
        alertbox.setPositiveButton("OK", null);
        alertbox.show();
    }

    public static void ReferralLog(int pScreenStrID, String pEvents,
            HashMap<String, String> eProps, Application pApplication) {
        if (Common.DEBUG) {
            Log.d("ReferralLog--->Omniture__>", eProps.toString());
        }
    }

    private static String getErrorPageVar(String errType, String pageName) {
        String s = errType + " - " + pageName + " | Error | Android";
        return errType + " - " + pageName + " | Error | Android";
    }

    public static List<Header> fetchSessionCookiesHeaders(HttpResponse response) {
        Header[] array = response.getHeaders("Set-Cookie");
        printCookies(array);
        return ((array != null) ? Arrays.asList(array) : null);
    }

    private static void printCookies(Header[] hdrs){
        Log.i(TAG,"getting headers after response------------------");
        for(int i=0;i<hdrs.length;i++){
            Log.i(TAG,""+hdrs[i]);
        }
    }

    public static Bitmap getBitmapFromURL(final String pUrl) {
        HttpURLConnection conn;
        InputStream is;
        Bitmap bitmap = null;
        try {
            conn = (HttpURLConnection) new URL(pUrl).openConnection();
            is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static Drawable loadImageFromUrl(String url) {
        InputStream inputStream;
        try {
            inputStream = new URL(url).openStream();

            if (inputStream == null)
                return null;

        } catch (IOException e) {
            return null;
        }
        return Drawable.createFromStream(inputStream, "src");
    }

    public static boolean hasSDCardMounted() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        try {
            if ((Build.MANUFACTURER).equalsIgnoreCase("HTC")
                    && (Build.DEVICE).equalsIgnoreCase("inc")) {
                return true;
            }
        } catch (Exception pEx) {
            return false;
        }
        return false;
    }

    public static boolean isAirplaneModeOn(Context ctx) {
        boolean isModeOn;
        isModeOn = Settings.System.getInt(ctx.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        return isModeOn;
    }



    /*
     * public static void beinformAlertMsg(final Activity activity, String
     * title, String Message) { final SharedPreferences prefs =
     * activity.getSharedPreferences("ALERTS", 0); AlertDialog.Builder alertbox
     * = new AlertDialog.Builder(activity); alertbox.setTitle(title);
     * alertbox.setMessage(Message); alertbox.setIcon(Color.TRANSPARENT);
     * alertbox.setPositiveButton("OK", new OnClickListener() {
     * @Override public void onClick(DialogInterface dialog, int which) {
     * SharedPreferences.Editor e = prefs.edit(); e.putInt("ALERTS_FLAG",
     * Constants.RX_ALERT_NO_SHOW).commit(); Intent i = new Intent(activity,
     * PharmacyHistory.class); activity.startActivity(i); activity.finish(); }
     * }); alertbox.show(); }
     */


    public static String getProperString(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        str = str.toLowerCase();
        int strLen = str.length();
        StringBuffer buffer = new StringBuffer(strLen);
        boolean capitalizeNext = true;
        for (int i = 0; i < strLen; i++) {
            char ch = str.charAt(i);
            if (Character.isWhitespace(ch)) {
                buffer.append(ch);
                capitalizeNext = true;
            } else if (capitalizeNext) {
                buffer.append(Character.toTitleCase(ch));
                capitalizeNext = false;
            } else {
                buffer.append(ch);
            }
        }
        return buffer.toString();
    }

    public static String getPrice(String price) {
        if (price.contains("$")) {
            if (Common.DEBUG) {
                Log.e("Item Details", "In contains block");
            }
            return price;
        } else {
            boolean isNumber = false;
            try {
                Double.parseDouble(price);
                isNumber = true;
            } catch (Exception e) {
                if (Common.DEBUG) {
                    e.printStackTrace();
                }
                Log.e("Item Details", "Exception Occured in Price conversion");
            }
            if (isNumber) {
                return "$" + price;
            } else {
                return price;
            }
        }
    }

    public static String convertHtmlToString(String html) {
        String mTitle = Html.fromHtml(html).toString();
        mTitle = mTitle.replaceAll("\\<.*?>", " ");
        mTitle = Html.fromHtml(mTitle).toString().trim();
        return mTitle;

    }

    public static String hackText(String textToHack) {
        // Change HtmlEncoded text (&lt; to < and &gt; to >) so that the tag
        // handler will get called
        // for <pre> tags.
        // Also, fromHtml() handles the <em> and <strong> tag incorrectly. <em>
        // should be italic while <strong>
        // should be bold. The source of Html.java shows they have it reversed.
        // Replace all occurences of 'em>' with 'i>' and all occurences of
        // 'strong>' with 'b>'
        return textToHack.replaceAll("em>", "i>").replaceAll("strong>", "b>").replaceAll("<br>",
        "<br>\t").replaceAll("<br/>", "<br/>\t").replaceAll("<li>", "<br>\u2022\t");
    }


    public static void hideSoftKeyboard(Context context, IBinder ibinder) {
        ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE))
        .hideSoftInputFromWindow(ibinder, 0);
    }

    public static void showSoftKeyboard(Context context, View view) {
        ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE))
        .showSoftInput(view, 0);
    }

    
    public static DefaultHttpClient getSSLByPassedHttpClient(){
        DefaultHttpClient httpClient = null;
        try{
            // sets up parameters
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, "utf-8");
            params.setBooleanParameter("http.protocol.expect-continue", false);

            // registers schemes for both http and https
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", new EasySSLSocketFactory(), 443));

            ThreadSafeClientConnManager manager = new ThreadSafeClientConnManager(params,
                    registry);

            httpClient = new DefaultHttpClient(manager, params);
            httpClient.setKeepAliveStrategy(new ConnectionKeepAliveStrategy() {

                @Override
                public long getKeepAliveDuration(HttpResponse response, HttpContext context) {

                    HeaderElementIterator it = new BasicHeaderElementIterator(response
                            .headerIterator(HTTP.CONN_KEEP_ALIVE));
                    while (it.hasNext()) {
                        HeaderElement he = it.nextElement();
                        String param = he.getName();
                        String value = he.getValue();
                        if (Common.DEBUG) {
                            android.util.Log.i(TAG, "param,value :" + param + "," + value);
                        }
                        if (value != null && param.equalsIgnoreCase("timeout")) {
                            try {
                                if (Common.DEBUG) {
                                    Log.i(TAG, "KEEP-ALIVE :" + (Long.parseLong(value) * 1000));
                                }
                                return Long.parseLong(value) * 1000;
                            } catch (NumberFormatException ignore) {
                                if (Common.DEBUG) {
                                    ignore.printStackTrace();
                                }
                                Log.e("", ""+ignore
                                        .getMessage());
                            }
                        }
                    }
                    /*
                     * HttpHost target = (HttpHost) context.getAttribute(
                     * ExecutionContext.HTTP_TARGET_HOST); if(Common.DEBUG){
                     * android
                     * .util.Log.i(TAG,"Target Host :"+target.getHostName
                     * ()); }
                     */
                    // keep alive for 30 seconds
                    return 30 * 1000;
                }
            });
        }catch(Exception e){
            if (Common.DEBUG) {
                e.printStackTrace();
            }
            Log.e("", ""+e.getMessage());
        }
        return httpClient;
    }

    public static void killProcess(Context context) {
        ActivityManager lManager = (ActivityManager) context
        .getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningAppProcessInfo> list = lManager.getRunningAppProcesses();
        for (int i = 0; i < list.size(); i++) {
            String ProcName = list.get(i).processName;
            if (ProcName.equals("com.usablenet.mobile.walgreen:MapViewer")
                    || ProcName.equals("com.usablenet.mobile.walgreen:MapDirection")) {
                int lPid = list.get(i).pid;
                Process.killProcess(lPid);
            }
        }
    }




    public static void recycleBitmap(Bitmap bmp) {

        if (bmp != null) {
            bmp.recycle();
            bmp = null;
        }

    }


   
}
