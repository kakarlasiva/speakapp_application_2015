package com.androidapps.speakapp;

import android.app.Notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.log.MyDebug;
import com.androidapps.speakapp.parser.BasicInfoParser;
import com.androidapps.speakapp.parser.PushParser;
import com.androidapps.speakapp.pushnotification.CommonUtilities;
import com.androidapps.speakapp.util.MethodsHelper;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCMIntentService";
	Handler handler;

    public GCMIntentService() {
    	
        super(CommonUtilities.SENDER_ID);
        handler=new Handler();
    }

    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(final Context context, final String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
       
        DataEngine.GCM_REGID=registrationId;
        new Thread(new Runnable() {
    		
    		@Override
    		public void run() {
    			// TODO Auto-generated method stub
    			try {
    				Log.e("Hello","IIIIdd-----------"+registrationId);
    				String url = DataEngine.BASE_URL + DataEngine.REG_PUSHID
    						+ MethodsHelper.getDeviceId(context)+DataEngine.PUSH+registrationId;
    				PushParser parser_obj=new PushParser(context);
    				MyDebug.log_info(DataEngine.INFO_LOG,context.getClass().getSimpleName(),url);
    				Log.i("","------------"+url);
    				parser_obj.parse_basicjsonparser(url);

    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		}
    	}).start();
        //CommonUtilities.displayMessage(context, "Your device registred with GCM");
        
        //ServerUtilities.register(context,registrationId);
    }
   

    /**
     * Method called on device un registred
     * */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
       // CommonUtilities.displayMessage(context, getString(R.string.gcm_unregistered));
       // ServerUtilities.unregister(context, registrationId);
    }

    /**
     * Method called on Receiving a new message
     * */
    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message"+intent);
        String message = intent.getExtras().getString("message");
        String orderid=intent.getExtras().getString("OrderId");
        DataEngine.PUSH_ORDERID=orderid;
        CommonUtilities.displayMessage(context, message);
        // notifies user
        generateNotification(context, message);
    }

    /**
     * Method called on receiving a deleted message
     * */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        CommonUtilities.displayMessage(context, message);
        // notifies user
        generateNotification(context, message);
    }
    
    

    /**
     * Method called on Error
     * */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        CommonUtilities.displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        CommonUtilities.displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message) {
        int icon = R.drawable.appicon;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        
        String title = context.getString(R.string.app_name);
        
        Intent notificationIntent = new Intent(context, InteractiveCube.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        
        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;
        
        //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");
        
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);      

    }

}
