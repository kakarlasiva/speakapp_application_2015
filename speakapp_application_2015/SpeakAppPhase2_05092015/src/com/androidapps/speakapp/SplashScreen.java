package com.androidapps.speakapp;

import android.app.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.eforslag.EforseLogNewActivity;
import com.androidapps.speakapp.log.MyDebug;
import com.androidapps.speakapp.parser.BasicInfoParser;
import com.androidapps.speakapp.pushnotification.AlertDialogManager;
import com.androidapps.speakapp.pushnotification.CommonUtilities;
import com.androidapps.speakapp.pushnotification.ConnectionDetector;
import com.androidapps.speakapp.pushnotification.ServerUtilities;
import com.androidapps.speakapp.pushnotification.WakeLocker;
import com.androidapps.speakapp.util.BasicInfo;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;
import com.google.android.gcm.GCMRegistrar;

/**
 * This is the starting screen of the application. In this screen the device
 * will register with GCM Service and Check for that latest update of
 * application is availble in the playstore.
 */
public class SplashScreen extends Activity {
	// Splash Image View
	ImageView splash_image;
	// Screen height & width
	int height, width;
	// To check whether the screen is alive (or) not.
	boolean isAlive = false;
	// Handler to post the Runnable on UI thread.
	Handler handler;
	SharedPreferences settings;
	ProgressDialog progress;
	StringBuffer str_buffer;
	int versionCode;
	BasicInfoParser parser_obj;
	BasicInfo info_obj;
	Background background_task;

	// Asyntask
	AsyncTask<Void, Void, Void> mRegisterTask;

	// Alert dialog manager
	AlertDialogManager alert = new AlertDialogManager();

	// Connection detector
	ConnectionDetector cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_splash_screen);
		getinit();
		push_notifications();
	}

	/**
	 * This method is used for getting the register id
	 */
	private void push_notifications() {
		cd = new ConnectionDetector(getApplicationContext());

		// Check if Internet present
		if (!cd.isConnectingToInternet()) {
			showErrorDialog();
			return;
		}

		Intent i = getIntent();
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		if (!NetworkChecker.isNetworkOnline(this)) {
			showErrorDialog();
			return;
		}
		registerReceiver(mHandleMessageReceiver, new IntentFilter(
				CommonUtilities.DISPLAY_MESSAGE_ACTION));
		// Get GCM registration id
		DataEngine.GCM_REGID = GCMRegistrar.getRegistrationId(this);
		if (DataEngine.GCM_REGID.equals("")) {
			// Registration is not present, register now with GCM
			GCMRegistrar.register(this, CommonUtilities.SENDER_ID);
		} else {
			// Device is already registered on GCM
			if (GCMRegistrar.isRegisteredOnServer(this)) {
				// Skips registration.
			} else {
				final Context context = this;
				mRegisterTask = new AsyncTask<Void, Void, Void>() {

					@Override
					protected Void doInBackground(Void... params) {
						// Register on our server
						// On server creates a new user
						ServerUtilities.register(context, DataEngine.GCM_REGID);
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						mRegisterTask = null;
					}

				};
				mRegisterTask.execute(null, null, null);
			}
		}

		doBackground(true);
	}

	@Override
	protected void onStop() {

		if (mRegisterTask != null) {
			mRegisterTask.cancel(true);
		}
		try {
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(this);
		} catch (Exception e) {
			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
		super.onStop();

	}

	/**
	 * Receiving push messages
	 * */
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(
					CommonUtilities.EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getApplicationContext());
			WakeLocker.release();
		}
	};

	/**
	 * This dialog is shown when their is no network
	 */
	private void showErrorDialog() {

		/*AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.network_error));
		builder.setTitle("Varning!");
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			
				startActivity(new Intent(SplashScreen.this,
						InteractiveCube.class));
				finish();

			}
		});
		if (isAlive)
			builder.show();*/
		showCorrespondingDialogs(getString(R.string.network_error));
	}

	private void getinit() {
		splash_image = (ImageView) findViewById(R.id.splash_image);
		str_buffer = new StringBuffer();
		progress = new ProgressDialog(this);
		progress.setMessage(getResources().getString(R.string.loading));
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		// Height & width of the Screen.
		height = displaymetrics.heightPixels;
		width = displaymetrics.widthPixels;
		isAlive = true;
		handler = new Handler();
		setSplashImage();
		settings = getSharedPreferences(DataEngine.APP_REF, 0);

	}

	private void doBackground(boolean showProgress) {
		background_task = new Background(showProgress);
		if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
			background_task.execute();
		} else {
			MethodsHelper.showToast(getResources().getString(R.string.network_error),
					this);
		}
	}

	private void setSplashImage() {

		splash_image.setImageResource(R.drawable.splash_image);
	}

	Runnable runnable = new Runnable() {

		@Override
		public void run() {

			if (isAlive) {
				Intent intent = new Intent(SplashScreen.this,
						InteractiveCube.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(intent);
			}
			finish();
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash_screen, menu);
		return true;
	}

	/**
	 * 
	 * @return divice id
	 */
	private String getdeviceid() {
		String device_id = MethodsHelper.getDeviceId(SplashScreen.this);
		return device_id;
	}

	class Background extends AsyncTask<Void, Void, Void> {
		boolean showProgress = false;

		public Background(boolean progress) {

			this.showProgress = progress;
		}

		@Override
		protected void onPreExecute() {

			try {
				if (showProgress)
					progress.show();
			} catch (Exception e) {

				e.printStackTrace();
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				String url = DataEngine.BASE_URL + DataEngine.APP_INFO
						+ getdeviceid() + DataEngine.REG_ID
						+ DataEngine.GCM_REGID + DataEngine.DEVICE_INFO
						+ "android";
				Log.i("Gcm Register id","**************** device id********* "+getdeviceid());
				parser_obj = new BasicInfoParser(SplashScreen.this);
				MyDebug.log_info(DataEngine.INFO_LOG, SplashScreen.this
						.getClass().getSimpleName(), url);
				parser_obj.parse_basicjsonparser(url);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			try {
				versionCode = getPackageManager().getPackageInfo(
						getPackageName(), 0).versionCode;
			} catch (Exception e) {
				// TODO: handle exception
				versionCode = -1;
			}
			if (parser_obj.basic_data.size() > 0) {

				info_obj = parser_obj.basic_infoobj;
				/*if (Integer.parseInt(info_obj.getMin_version()) <= versionCode
						&& versionCode <= Integer.parseInt(info_obj
								.getMax_version())) {
					handler.postDelayed(runnable, 2000);
				} else {
					showForceUpdateMsg();
				}*/
				if(versionCode < Integer.parseInt(info_obj
								.getMax_version()))
				{
					//showCorrespondingDialogs("Din nuvarande version st�der inte l�ngre den h�r funktionen, v�nligen uppdatera till den senaste versionen.", true);
					showForceUpdateMsg();
				}
				else
					handler.postDelayed(runnable, 2000);	

			}

			try {
				if (showProgress)
					if (progress.isShowing())
						progress.dismiss();
			} catch (Exception e) {

				e.printStackTrace();
			}
			super.onPostExecute(result);
		}

	}

	/**
	 * shows the alert dialog for update application to latest version
	 */
	public void showForceUpdateMsg() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Din nuvarande version st�der inte l�ngre den h�r funktionen, v�nligen uppdatera till den senaste versionen.");
		builder.setTitle("Varning!");
		builder.setPositiveButton("Okej",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						/*
						 * Intent myIntent = new Intent( Intent.ACTION_VIEW,
						 * Uri.parse(
						 * "https://play.google.com/store/apps/details?id=com.androidapps.speakapp"
						 * )); startActivity(myIntent); finish();
						 */						
						final String my_package_name = SplashScreen.this
								.getPackageName(); // <- HERE YOUR PACKAGE
													// NAME!!
						String url = "";

						try {
							url = "market://details?id=" + my_package_name;
						} catch (final Exception e) {
							url = "https://play.google.com/store/apps/details?id="
									+ my_package_name;
						}
						Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri
								.parse(url));
						startActivity(myIntent);
						finish();
					}
				});
		if (isAlive)
			builder.show();

	}
	
	private void showCorrespondingDialogs(String error) {
		
		String errMsg = error.substring(0, 1).toUpperCase()
				+ error.substring(1).toLowerCase();
		final Dialog custom = new Dialog(SplashScreen.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.signtoregister_alert);

		custom.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);

		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextView dialogtext = (TextView) custom.findViewById(R.id.thanks_txt);
		dialogtext.setTypeface(DataEngine.gettypeface(this));

		dialogtext.setText(errMsg);

		Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
		GradientDrawable btnbg = (GradientDrawable) dialogButton
				.getBackground();
		dialogButton.setTypeface(DataEngine.gettypeface(this));
		btnbg.setColor(Color.parseColor("#474849"));
		dialogButton.setText("Okej");

		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {				
				
				startActivity(new Intent(SplashScreen.this,
						InteractiveCube.class));
				finish();
				
				
				

			}
		});
		custom.show();
	}

}
