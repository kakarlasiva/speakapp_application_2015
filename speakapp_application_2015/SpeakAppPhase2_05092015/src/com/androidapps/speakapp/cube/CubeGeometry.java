package com.androidapps.speakapp.cube;



public class CubeGeometry extends Mesh {
	/**
	 * Create a plane with a default with and height of 1 unit.
	 */
	public CubeGeometry() {
		this(1, 1, 1, "front", true);
	}

	/**
	 * Create a plane.
	 * 
	 * @param width
	 *            the width of the plane.
	 * @param height
	 *            the height of the plane.
	 */
	public CubeGeometry(float width, float height, float thickness, String faceName, boolean isPicking) {
		// Mapping coordinates for the vertices
		float textureCoordinates[] = {
				0.0f, 0.0f, //
				0.0f, 1.0f, //
				1.0f, 1.0f, //
				1.0f, 0.0f, //

				0.0f, 0.0f, //
				0.0f, 1.0f, //
				1.0f, 1.0f, //
				1.0f, 0.0f, //

				0.0f, 0.0f, //
				0.0f, 1.0f, //
				1.0f, 1.0f, //
				1.0f, 0.0f, //

				0.0f, 0.0f, //
				0.0f, 1.0f, //
				1.0f, 1.0f, //
				1.0f, 0.0f, //

				0.0f, 0.0f, //
				0.0f, 1.0f, //
				1.0f, 1.0f, //
				1.0f, 0.0f, //

				0.0f, 0.0f, //
				0.0f, 1.0f, //
				1.0f, 1.0f, //
				1.0f, 0.0f, //
		};

		short[] indices = new short[] { 
				0, 1, 2, 0, 2, 3,
				4, 5, 6, 4, 6, 7,
				8, 9, 10, 8, 10, 11,
				12, 13, 14, 12, 14, 15,
				16, 17, 18, 16, 18, 19,
				20, 21, 22, 20, 22, 23
		};

		float[] vertices = new float[] {
				// front
				-width / 2, height / 2, thickness / 2, 	// left top
				-width / 2, -height / 2, thickness / 2, // left bottom
				width / 2, -height / 2, thickness / 2, 	// right bottom
				width / 2, height / 2, thickness / 2, 	// right top

				// right face
				width / 2, height / 2, thickness / 2, 	// left top
				width / 2, -height / 2, thickness / 2, 	// left bottom
				width / 2, -height / 2, -thickness / 2, // right bottom
				width / 2, height / 2, -thickness / 2, 	// right top

				// back face
				width / 2, height / 2, -thickness / 2, 	// left top
				width / 2, -height / 2, -thickness / 2, // left bottom
				-width / 2, -height / 2, -thickness / 2,// right bottom
				-width / 2, height / 2, -thickness / 2, // right top

				// left face
				-width / 2, height / 2, -thickness / 2, // left top
				-width / 2, -height / 2, -thickness / 2,// left bottom
				-width / 2, -height / 2, thickness / 2, // right bottom
				-width / 2, height / 2, thickness / 2, 	// right top

				// bottom face
				-width / 2, -height / 2, thickness / 2, // left top
				-width / 2, -height / 2, -thickness / 2,// left bottom
				width / 2, -height / 2, -thickness / 2, // right bottom
				width / 2, -height / 2, thickness / 2, 	// right top

				// top face
				-width / 2, height / 2, -thickness / 2, // left top
				-width / 2, height / 2, thickness / 2, 	// left bottom
				width / 2, height / 2, thickness / 2, 	// right bottom
				width / 2, height / 2, -thickness / 2, 	// right top
		};

		setIndices(indices);
		setVertices(vertices);
		setTextureCoordinates(textureCoordinates);

		setFaceName(faceName);
		setPicking(isPicking);
	}
}
