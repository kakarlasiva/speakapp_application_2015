package com.androidapps.speakapp.cube;

public interface ICubeAnimationListner {
	
	public abstract void cubeOpened();
	public abstract void cubeClosed();

}
