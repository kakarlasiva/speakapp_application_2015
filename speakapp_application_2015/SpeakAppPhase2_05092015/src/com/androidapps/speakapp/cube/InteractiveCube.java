/**
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.androidapps.speakapp.cube;

import javax.microedition.khronos.opengles.GL;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class InteractiveCube extends Activity implements ICubeAnimationListner,
		OnClickListener {

	private ImageView hur_logo, han_logo, radda_logo, undo_logo,
			arvsfonden_logo;
	Handler handler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove the title bar from the window.
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.cube_lay);
		// Make the windows into full screen mode.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		handler = new Handler();
		mGLSurfaceView = (TouchSurfaceView) findViewById(R.id.touch_cube);
		cubeBottomLayout = (LinearLayout) findViewById(R.id.bottom_of_cube);
		cubeLayout = (LinearLayout) findViewById(R.id.cubelay);
		hur_logo = (ImageView) findViewById(R.id.logo);
		han_logo = (ImageView) findViewById(R.id.hanninge_logo);
		radda_logo = (ImageView) findViewById(R.id.radda_logo);
		undo_logo = (ImageView) findViewById(R.id.ungdoms_logo);
		arvsfonden_logo = (ImageView) findViewById(R.id.arvsfonden);

		hur_logo.setOnClickListener(this);
		han_logo.setOnClickListener(this);
		radda_logo.setOnClickListener(this);
		undo_logo.setOnClickListener(this);
		arvsfonden_logo.setOnClickListener(this);

		mGLSurfaceView.setZOrderOnTop(true);

		mGLSurfaceView.setGLWrapper(new GLSurfaceView.GLWrapper() {
			@Override
			public GL wrap(GL gl) {
				return new MatrixTrackingGL(gl);
			}
		});

		mGLSurfaceView.requestFocus();
		mGLSurfaceView.setFocusableInTouchMode(true);

	}

	@Override
	protected void onResume() {
		super.onResume();
		// mGLSurfaceView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		// mGLSurfaceView.onPause();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (DataEngine.disassemble)
			mGLSurfaceView.onTouchEvent(event);
		return true;
	}

	private GLSurfaceView mGLSurfaceView;

	private LinearLayout cubeBottomLayout, cubeLayout;

	@Override
	public void cubeOpened() {
		LayoutParams lp = new LayoutParams(LayoutParams.FILL_PARENT, 0, 0.8f);
		// cubeLayout.setLayoutParams(lp);
		// cubeBottomLayout.setVisibility(View.GONE);
		cubeLayout.setPadding(
				0,
				getResources().getDimensionPixelSize(
						R.dimen.space_cube_opened_top),
				0,
				getResources().getDimensionPixelSize(
						R.dimen.space_cube_opened_bottom));
		cubeBottomLayout.setVisibility(View.GONE);

	}

	@Override
	public void cubeClosed() {
		// cubeBottomLayout.setVisibility(View.VISIBLE);
		handler.post(close_run);

	}

	Runnable close_run = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			cubeLayout.setPadding(0, 0, 0, 0);
			cubeBottomLayout.setVisibility(View.VISIBLE);
		}
	};

	private void set_alertdialog(Context mtx) {

		final Dialog notify_dialog = new Dialog(this);
		notify_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		notify_dialog.setContentView(R.layout.call_alertdialog);
		notify_dialog.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		notify_dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		notify_dialog.setCanceledOnTouchOutside(true);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) notify_dialog.findViewById(R.id.number);
		text.setTypeface(DataEngine.gettypeface(this));
		text.setText("Vill du l�mna Speak app?");

		Button dialogButton = (Button) notify_dialog.findViewById(R.id.ring);
		dialogButton.setTypeface(DataEngine.gettypeface(this));
		dialogButton.setText("Ja");
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		Button cancelButton = (Button) notify_dialog.findViewById(R.id.cancel);
		cancelButton.setTypeface(DataEngine.gettypeface(this));
		cancelButton.setText("Avbryt");
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				notify_dialog.dismiss();
			}
		});

		notify_dialog.show();

	}
	
	@Override
	public void onBackPressed() {
		set_alertdialog(InteractiveCube.this);
	}

	public void call_dialog(final String url) {

		final Dialog notify_dialog = new Dialog(this);
		notify_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		notify_dialog.setContentView(R.layout.call_alertdialog);
		notify_dialog.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		notify_dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		notify_dialog.setCanceledOnTouchOutside(true);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) notify_dialog.findViewById(R.id.number);
		text.setTypeface(DataEngine.gettypeface(this));
		text.setText(" Vill du verkligen g� vidare till hemsidan?");

		Button dialogButton = (Button) notify_dialog.findViewById(R.id.ring);
		dialogButton.setTypeface(DataEngine.gettypeface(this));
		dialogButton.setText("Ja");
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				notify_dialog.dismiss();
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(intent);
			}
		});
		Button cancelButton = (Button) notify_dialog.findViewById(R.id.cancel);
		cancelButton.setTypeface(DataEngine.gettypeface(this));
		cancelButton.setText("Avbryt");
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				notify_dialog.dismiss();
			}
		});

		notify_dialog.show();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.logo:
			/*
			 * Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse());
			 * startActivity(intent);
			 */
			call_dialog("https://www.facebook.com/#!/haninge.ungodmsrad?fref=ts");
			break;

		case R.id.hanninge_logo:
			/*
			 * Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse());
			 * startActivity(intent1);
			 */
			call_dialog("http://www.haninge.se/");
			break;

		case R.id.radda_logo:
			/*
			 * Intent intent2 = new Intent(Intent.ACTION_VIEW, Uri.parse());
			 * startActivity(intent2);
			 */
			call_dialog("http://www.raddabarnen.se/lokalforeningar/ost/stockholms-lan/vasterhaninge/");
			break;

		case R.id.ungdoms_logo:
			/*
			 * Intent intent3 = new Intent(Intent.ACTION_VIEW, Uri.parse());
			 * startActivity(intent3);
			 */
			call_dialog("http://rbuf.se/");
			break;
		case R.id.arvsfonden:
			/*
			 * Intent intent4 = new Intent(Intent.ACTION_VIEW, Uri.parse());
			 * startActivity(intent4);
			 */
			call_dialog("http://www.arvsfonden.se/");
			break;

		default:
			break;
		}

	}
}