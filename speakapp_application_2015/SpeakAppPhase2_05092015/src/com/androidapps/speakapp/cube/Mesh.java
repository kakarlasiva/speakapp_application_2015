/**
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.androidapps.speakapp.cube;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import android.graphics.Bitmap;
import android.opengl.GLUtils;
import android.opengl.Matrix;

public class Mesh {
	private float vertices[];

	// Our vertex buffer.
	private FloatBuffer mVerticesBuffer = null;

	// Our index buffer.
	private ShortBuffer mIndicesBuffer = null;

	// Our UV texture buffer.
	private FloatBuffer mTextureBuffer; // New variable.

	// Our texture id.
	private int mTextureId = -1; // New variable.

	// The bitmap we want to load as a texture.
	private Bitmap mBitmap; // New variable.

	// Indicates if we need to load the texture.
	private boolean mShouldLoadTexture = false; // New variable.

	// The number of indices.
	private int mNumOfIndices = -1;

	// Flat Color
	private final float[] mRGBA = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };

	// Smooth Colors
	private FloatBuffer mColorBuffer = null;

	// can be pick
	private boolean isPicking = true;
	
	// name of the face
	private String faceName;

	// Translate parameters.
	private float translateX = 0;
	private float translateY = 0;
	private float translateZ = 0;

	// Rotate parameters.
	private float rotationX = 0;
	private float rotationY = 0;
	private float rotationZ = 0;

	/**
	 * Render the mesh.
	 * 
	 * @param gl : the OpenGL context to render to.
	 *            
	 */
	public void draw(GL10 gl, Ray ray) {
		// Enabled the vertices buffer for writing and to be used during rendering.
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVerticesBuffer);

		// Set flat color
		gl.glColor4f(mRGBA[0], mRGBA[1], mRGBA[2], mRGBA[3]);

		// Smooth color
		if (mColorBuffer != null) {
			// Enable the color array buffer to be used during rendering.
			gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
			gl.glColorPointer(4, GL10.GL_FLOAT, 0, mColorBuffer);
		}

		// New part...
		if (mShouldLoadTexture) {
			loadGLTexture(gl);
			mShouldLoadTexture = false;
		}
		
		if (mTextureId != -1 && mTextureBuffer != null) {
			gl.glEnable(GL10.GL_TEXTURE_2D);
			
			// Enable the texture state
			gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

			// Point to our buffers
			gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, mTextureBuffer);
			gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureId);
		}
		// ... end new part.
		
		gl.glPushMatrix();
			gl.glTranslatef(translateX, translateY, translateZ);
	
			gl.glRotatef(rotationX, 1, 0, 0);
			gl.glRotatef(rotationY, 0, 1, 0);
			gl.glRotatef(rotationZ, 0, 0, 1);

			if (isPicking) {
				// /////////////////////////////////////////////////////////////////////////
				if (ray != null) {
					MatrixGrabber matrixGrabber = new MatrixGrabber();
					matrixGrabber.getCurrentState(gl);
		
					int coordCount = vertices.length;
					float[] convertedSquare = new float[coordCount];
					float[] resultVector = new float[4];
					float[] inputVector = new float[4];

					for (int i = 0; i < coordCount; i = i + 3) {
						inputVector[0] = vertices[i];
						inputVector[1] = vertices[i + 1];
						inputVector[2] = vertices[i + 2];
						inputVector[3] = 1;
						Matrix.multiplyMV(resultVector, 0, matrixGrabber.mModelView, 0, inputVector, 0);
						convertedSquare[i] = resultVector[0] / resultVector[3];
						convertedSquare[i + 1] = resultVector[1] / resultVector[3];
						convertedSquare[i + 2] = resultVector[2] / resultVector[3];
					}
					matrixGrabber = null;
					
					Triangle t1 = new Triangle(
							new float[] { convertedSquare[0], convertedSquare[1], convertedSquare[2] },
							new float[] { convertedSquare[3], convertedSquare[4], convertedSquare[5] }, 
							new float[] { convertedSquare[6], convertedSquare[7], convertedSquare[8] });
					Triangle t2 = new Triangle(
							new float[] { convertedSquare[0], convertedSquare[1], convertedSquare[2] },
							new float[] { convertedSquare[6], convertedSquare[7], convertedSquare[8] }, 
							new float[] { convertedSquare[9], convertedSquare[10], convertedSquare[11] });
		
					float[] point1 = new float[3];
					int intersects1 = Triangle.intersectRayAndTriangle(ray, t1, point1);
					float[] point2 = new float[3];
					int intersects2 = Triangle.intersectRayAndTriangle(ray, t2, point2);
		
					if (intersects1 == 1 || intersects1 == 2) {
						// Log.d("test", "touch!: " + faceName + " : " + Math.abs(point1[2]));
						if (ray.getIntersectZPos() > Math.abs(point1[2])) {
							ray.setIntersectZPos(Math.abs(point1[2]));
							ray.setIntersectObj(this);

							/*Log.d("test", "Triangles : " + faceName + 
									" : " + convertedSquare[0] +
							 		" : " + convertedSquare[1] +
									" : " + convertedSquare[2] +
									" : " + convertedSquare[3] +
									" : " + convertedSquare[4] +
									" : " + convertedSquare[5] +
									" : " + convertedSquare[6] +
									" : " + convertedSquare[7] +
									" : " + convertedSquare[8] +
									" : " + convertedSquare[9] +
									" : " + convertedSquare[10] +
									" : " + convertedSquare[11]
							 		);

							Log.d("test", "Ray : " +
									" : " + ray.P0[0] +
									" : " + ray.P0[1] +
									" : " + ray.P0[2] +
									" : " + ray.P1[0] +
									" : " + ray.P1[1] +
									" : " + ray.P1[2]
									);*/
						}
					} else if (intersects2 == 1 || intersects2 == 2) {
						// Log.d("test", "touch!: " + faceName + " : " + Math.abs(point2[2]));
						if (ray.getIntersectZPos() > Math.abs(point2[2])) {
							ray.setIntersectZPos(Math.abs(point2[2]));
							ray.setIntersectObj(this);

							/*Log.d("test", "Triangles : " + faceName + 
									" : " + convertedSquare[0] +
							 		" : " + convertedSquare[1] +
									" : " + convertedSquare[2] +
									" : " + convertedSquare[3] +
									" : " + convertedSquare[4] +
									" : " + convertedSquare[5] +
									" : " + convertedSquare[6] +
									" : " + convertedSquare[7] +
									" : " + convertedSquare[8] +
									" : " + convertedSquare[9] +
									" : " + convertedSquare[10] +
									" : " + convertedSquare[11]
							 		);

							Log.d("test", "Ray : " +
									" : " + ray.P0[0] +
									" : " + ray.P0[1] +
									" : " + ray.P0[2] +
									" : " + ray.P1[0] +
									" : " + ray.P1[1] +
									" : " + ray.P1[2]
									);*/
						}
					}
				}
			}
			
			// Point out the where the color buffer is.
			gl.glDrawElements(GL10.GL_TRIANGLES, mNumOfIndices, GL10.GL_UNSIGNED_SHORT, mIndicesBuffer);
		gl.glPopMatrix();

		// Disable the vertices buffer.
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

		if (mTextureId != -1 && mTextureBuffer != null) {
			gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		}
		gl.glDisable(GL10.GL_TEXTURE_2D);
	}

	/**
	 * Set the vertices.
	 * 
	 * @param vertices
	 */
	protected void setVertices(float[] vertices) {
		this.vertices = vertices;

		// a float is 4 bytes, therefore we multiply the number if vertices with 4.
		ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
		vbb.order(ByteOrder.nativeOrder());
		mVerticesBuffer = vbb.asFloatBuffer();
		mVerticesBuffer.put(vertices);
		mVerticesBuffer.position(0);
	}

	/**
	 * Set the indices.
	 * 
	 * @param indices
	 */
	protected void setIndices(short[] indices) {
		// short is 2 bytes, therefore we multiply the number if
		// vertices with 2.
		ByteBuffer ibb = ByteBuffer.allocateDirect(indices.length * 2);
		ibb.order(ByteOrder.nativeOrder());
		mIndicesBuffer = ibb.asShortBuffer();
		mIndicesBuffer.put(indices);
		mIndicesBuffer.position(0);
		mNumOfIndices = indices.length;
	}

	/**
	 * Set the texture coordinates.
	 * 
	 * @param textureCoords
	 */
	protected void setTextureCoordinates(float[] textureCoords) {
		// float is 4 bytes, therefore we multiply the number if
		// vertices with 4.
		ByteBuffer byteBuf = ByteBuffer.allocateDirect(textureCoords.length * 4);
		byteBuf.order(ByteOrder.nativeOrder());
		mTextureBuffer = byteBuf.asFloatBuffer();
		mTextureBuffer.put(textureCoords);
		mTextureBuffer.position(0);
	}

	/**
	 * Set one flat color on the mesh.
	 * 
	 * @param red
	 * @param green
	 * @param blue
	 * @param alpha
	 */
	protected void setColor(float red, float green, float blue, float alpha) {
		mRGBA[0] = red;
		mRGBA[1] = green;
		mRGBA[2] = blue;
		mRGBA[3] = alpha;
	}

	/**
	 * Set the colors
	 * 
	 * @param colors
	 */
	protected void setColors(float[] colors) {
		// float has 4 bytes.
		ByteBuffer cbb = ByteBuffer.allocateDirect(colors.length * 4);
		cbb.order(ByteOrder.nativeOrder());
		mColorBuffer = cbb.asFloatBuffer();
		mColorBuffer.put(colors);
		mColorBuffer.position(0);
	}

	/**
	 * Set the face name
	 * 
	 * @param name
	 */
	protected void setFaceName(String name) {
		faceName = name;
	}

	public String getFaceName() {
		return faceName;
	}

	public boolean isPicking() {
		return isPicking;
	}

	public void setPicking(boolean isPicking) {
		this.isPicking = isPicking;
	}

	public float getTranslateX() {
		return translateX;
	}

	public void setTranslateX(float translateX) {
		this.translateX = translateX;
	}

	public float getTranslateY() {
		return translateY;
	}

	public void setTranslateY(float translateY) {
		this.translateY = translateY;
	}

	public float getTranslateZ() {
		return translateZ;
	}

	public void setTranslateZ(float translateZ) {
		this.translateZ = translateZ;
	}

	public float getRotationX() {
		return rotationX;
	}

	public void setRotationX(float rotationX) {
		this.rotationX = rotationX;
	}

	public float getRotationY() {
		return rotationY;
	}

	public void setRotationY(float rotationY) {
		this.rotationY = rotationY;
	}

	public float getRotationZ() {
		return rotationZ;
	}

	public void setRotationZ(float rotationZ) {
		this.rotationZ = rotationZ;
	}

	public void initModelMatrix() {
		translateX = 0.0f;
		translateY = 0.0f;
		translateZ = 0.0f;
		
		rotationX = 0.0f;
		rotationY = 0.0f;
		rotationZ = 0.0f;
	}
	
	/**
	 * Set the bitmap to load into a texture.
	 * 
	 * @param bitmap
	 */
	public void loadBitmap(Bitmap bitmap) {
		this.mBitmap = bitmap;
		mShouldLoadTexture = true;
	}

	/**
	 * Loads the texture.
	 * 
	 * @param gl
	 */
	private void loadGLTexture(GL10 gl) { // New function
		// Generate one texture pointer...
		int[] textures = new int[1];
		gl.glGenTextures(1, textures, 0);
		mTextureId = textures[0];

		// ...and bind it to our array
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureId);

		// Different possible texture parameters, e.g. GL10.GL_CLAMP_TO_EDGE
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);

		// Create Nearest Filtered Texture
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR_MIPMAP_NEAREST);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

		/*
		 * This is a change to the original tutorial, as buildMipMap does not exist anymore
		 * in the Android SDK.
		 * 
		 * We check if the GL context is version 1.1 and generate MipMaps by flag.
		 * Otherwise we call our own buildMipMap implementation
		 */
		if (gl instanceof GL11) {
			gl.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_GENERATE_MIPMAP, GL11.GL_TRUE);
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, mBitmap, 0);
		} else {
			buildMipmap(gl, mBitmap);
		}	
		
		mBitmap.recycle();
		mBitmap = null;
	}
	
	/**
	 * Our own MipMap generation implementation.
	 * Scale the original bitmap down, always by factor two,
	 * and set it as new mipmap level.
	 * 
	 * Thanks to Mike Miller (with minor changes)!
	 * 
	 * @param gl - The GL Context
	 * @param bitmap - The bitmap to mipmap
	 */
	private void buildMipmap(GL10 gl, Bitmap bitmap) {
		//
		int level = 0;
		//
		int height = bitmap.getHeight();
		int width = bitmap.getWidth();

		//
		while (height >= 1 || width >= 1) {
			//First of all, generate the texture from our bitmap and set it to the according level
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, level, bitmap, 0);
			
			//
			if (height == 1 || width == 1) {
				break;
			}

			//Increase the mipmap level
			level++;

			//
			height /= 4;
			width /= 4;
			Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, width, height, true);
			
			//Clean up
			bitmap.recycle();
			bitmap = bitmap2;
		}
	}	
}
