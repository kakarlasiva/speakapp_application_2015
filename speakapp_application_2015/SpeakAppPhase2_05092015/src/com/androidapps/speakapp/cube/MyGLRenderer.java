package com.androidapps.speakapp.cube;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;
import android.widget.Toast;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.eforslag.RecentPetitions;
import com.androidapps.speakapp.parser.QuestionReader;
import com.androidapps.speakapp.screens.AsiktarViewActivity;
import com.androidapps.speakapp.screens.EforslagViewActivity;
import com.androidapps.speakapp.screens.KontaktViewActivity;
import com.androidapps.speakapp.screens.PegangViewActivity;
import com.androidapps.speakapp.screens.PieChartActivity;
import com.androidapps.speakapp.screens.QuestionActivity;
import com.androidapps.speakapp.screens.QuestionViewActivity;
import com.androidapps.speakapp.screens.VideoplayerActivity;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;
import com.androidapps.speakapp.util.QuestionHelper;

public class MyGLRenderer implements GLSurfaceView.Renderer {
	Context ctx;

	GL10 gl = null;

	private Mesh intersectObj;

	private int width;
	private int height;

	private final Group root;

	// private float lookAngleX = 0.0f;
	// private float lookAngleY = 0.0f;
	private float lookAngleX = 0.47324f;
	private float lookAngleY = 0.74325f;

	private float distance;
	private float newDistance;

	private final float TOUCH_SCALE_FACTOR = 5.0f / 720;
	
	private final float TRACKBALL_SCALE_FACTOR = 36.0f;

	private float mPreviousX;
	private float mPreviousY;
	private float mDownX;
	private float mDownY;

	private boolean firstfalg = false;
	int rotationcount = 0;

	private float density;

	public boolean isAction = true;

	private final int zoomInOutStepCount = 20;
	private float zoomInOutStep;
	private float rotationStepX;
	private float rotationStepY;
	QuestionReader reader_obj;

	private boolean isDisassembled = false;
	private boolean isZoomInOut = false;
	private boolean isStartAnimation = false;
	private int animationAngle = 90;
	ProgressDialog p_dialog;
	BackgroundProcess background_process;

	private int animationStep = 0;
	private final int animationOrder[] = { 4, 3, 5, 1 }; // { 5, 4, 3, 1 };

	ICubeAnimationListner iCubeAnimationListner;
	
	public MyGLRenderer(Context context) {
		ctx = context;

		distance = 4.0f;

		// Initialize our root.
		Group group = new Group();
		root = group;

		init(context);
	}

	public void init(Context context) {
		p_dialog = new ProgressDialog(context);

		p_dialog.setCancelable(true);
	/*	((TextView) p_dialog.findViewById(ctx.getResources().getIdentifier(
                "alertTitle", "id", "android"))).setTypeface(DataEngine.gettypeface(ctx));
	*/	p_dialog.setMessage(context.getResources().getString(R.string.loading));
		root.clear();
		iCubeAnimationListner = (InteractiveCube) context;
		final DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(displayMetrics);
		density = displayMetrics.density;

		// Create a new plane.
		// Front Face

		PlaneGeometry frontFace = new PlaneGeometry(1, 1, "front", true);
		frontFace.setTranslateZ(0.5f);

		// Right Face
		PlaneGeometry rightFace = new PlaneGeometry(1, 1, "right", true);
		rightFace.setTranslateX(0.5f);
		rightFace.setRotationY(90);

		// Back Face
		PlaneGeometry backFace = new PlaneGeometry(1, 1, "back", true);
		backFace.setTranslateZ(-0.5f);
		backFace.setRotationY(180);

		// Left Face
		PlaneGeometry leftFace = new PlaneGeometry(1, 1, "left", true);
		leftFace.setTranslateX(-0.5f);
		leftFace.setRotationY(-90);

		// Top Face
		PlaneGeometry topFace = new PlaneGeometry(1, 1, "top", true);
		topFace.setTranslateY(0.5f);
		topFace.setRotationX(-90);

		// Bottom Face
		PlaneGeometry bottomFace = new PlaneGeometry(1, 1, "bottom", true);
		bottomFace.setTranslateY(-0.5f);
		bottomFace.setRotationX(90);

		// Load the texture.
		try {
			InputStream ims = context.getAssets().open("gfx/video_closed.png"); //gfx/video_closed.png
			frontFace.loadBitmap(BitmapFactory.decodeStream(ims));

			ims = context.getAssets().open("gfx/asikter_closed.png"); //gfx/asikter_closed.png
			topFace.loadBitmap(BitmapFactory.decodeStream(ims)); // rightFace

			ims = context.getAssets().open("gfx/pie_closed.png");  //gfx/pie_closed.png
			leftFace.loadBitmap(BitmapFactory.decodeStream(ims)); // backFace

			ims = context.getAssets().open("gfx/kontakta_closed.png"); //gfx/kontakta_closed.png
			backFace.loadBitmap(BitmapFactory.decodeStream(ims)); // left

			ims = context.getAssets().open("gfx/eforslag_closed.png");
			bottomFace.loadBitmap(BitmapFactory.decodeStream(ims)); // topFace

			ims = context.getAssets().open("gfx/pagang_closed.png"); //gfx/pagang_closed.png
			rightFace.loadBitmap(BitmapFactory.decodeStream(ims)); // bottomFace
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Add the plane to the renderer.
		root.add(frontFace);
		root.add(rightFace);
		root.add(backFace);
		root.add(leftFace);
		root.add(topFace);
		root.add(bottomFace);
		// root.setRotationZ(60);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig arg1) {
		// Set the background color to black ( rgba ).

		// gl.glClearColor(0.925f,0.925f,0.925f,0.0f);

		gl.glShadeModel(GL10.GL_SMOOTH);

		gl.glEnable(GL10.GL_DEPTH_TEST);
		gl.glDepthFunc(GL10.GL_LEQUAL);

		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);

		// Counter-clockwise winding.
		gl.glFrontFace(GL10.GL_CCW);

		// Enable face culling.
		gl.glEnable(GL10.GL_CULL_FACE);

		// What faces to remove with the face culling.
		gl.glCullFace(GL10.GL_BACK);
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		this.gl = gl;

		this.width = width;
		this.height = height;

		float ratio = (float) width / height;

		// Sets the current view port to the new size.

		gl.glViewport(0, 0, width, height);

		// Select the projection matrix
		gl.glMatrixMode(GL10.GL_PROJECTION);

		// Reset the projection matrix
		gl.glLoadIdentity();

		// Calculate the aspect ratio of the window
		if (sizeFlag)
			GLU.gluPerspective(gl, 25.0f, ratio, 0.1f, 1000.0f);
		else
			GLU.gluPerspective(gl, 43.0f, ratio, 0.1f, 1000.0f);

		// Select the model view matrix
		gl.glMatrixMode(GL10.GL_MODELVIEW);

		// Reset the model view matrix
		gl.glLoadIdentity();
	}

	boolean isSwipeLeft = false;
	boolean isSwipeRight = false;
	boolean isSwipeUp = false;
	boolean isSwipeDown = false;
	float floatx = -0.005f;

	@Override
	public void onDrawFrame(GL10 gl) {
		// Clears the screen and depth buffer.
		// lookAngleX+=0.034;
          DataEngine.disassemble = isDisassembled;
		// gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT );
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT
				| GL10.GL_STENCIL_BUFFER_BIT);
		gl.glClearStencil(GL10.GL_CLEAR);

		gl.glMatrixMode(GL10.GL_MODELVIEW);		

		// Replace the current matrix with the identity matrix
		gl.glLoadIdentity();

		float cameraX = (float) (-distance * Math.sin(lookAngleY) * Math
				.cos(lookAngleX));
		float cameraY = (float) (distance * Math.sin(lookAngleX)); 
		float cameraZ = (float) (distance * Math.cos(lookAngleY) * Math
				.cos(lookAngleX));

		/*gl.glRotatef(lookAngleX, 0, 1, 0);
		gl.glRotatef(lookAngleY, 1, 0, 0);

		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);*/

		GLU.gluLookAt(gl, cameraX, cameraY, cameraZ, 0, 0, 0, 0,
				(float) Math.cos(lookAngleX), 0);
	

		// Draw our scene.
		Ray ray = new Ray(gl, this.width, this.height, mPreviousX, mPreviousY);
		root.draw(gl, ray);

		intersectObj = ray.getIntersectObj();

		if (!isDisassembled) {
			if (firstfalg) {
				/*rotationcount = (rotationcount + 1) % 33;

				lookAngleY -= 0.1734 * (-1.0f);
				if (rotationcount == 20) {
					firstfalg = false;
					iCubeAnimationListner.cubeClosed();
					
					  lookAngleX = 0.47324f; lookAngleY = 0.74325f;
					 
				}*/
				
			}
			if (isSwipeLeft) {
				rotationcount = (rotationcount + 1) % 51;
				// This does the magic
				if (Math.cos(lookAngleX) < 0.0f) {
					floatx = -1.0f;
				} else {
					floatx = 1.0f;
				}

				lookAngleY -= 0.1734 * floatx;
				if (rotationcount == 50) {
					isSwipeLeft = false;
				}

			} else if (isSwipeRight) {
				rotationcount = (rotationcount + 1) % 51;
				if (Math.cos(lookAngleX) < 0.0f) {
					floatx = -1.0f;
				} else {
					floatx = 1.0f;
				}
				lookAngleY += 0.1734 * floatx;
				if (rotationcount == 50) {
					isSwipeRight = false;
				}

			} else if (isSwipeDown) {
				rotationcount = (rotationcount + 1) % 51;
				lookAngleX += 0.1734;
				if (rotationcount == 50) {
					isSwipeDown = false;
				}

			} else if (isSwipeUp) {
				rotationcount = (rotationcount + 1) % 51;
				lookAngleX -= 0.1734;
				if (rotationcount == 50) {
					isSwipeUp = false;
				}

			}
		}

		if (isStartAnimation) {

			if (!isDisassembled) {
				if (isZoomInOut) {
					float delta = newDistance - distance;
					if (Math.abs(delta) <= zoomInOutStep) {
						distance = newDistance;
						lookAngleX = 0.0f;
						lookAngleY = 0.0f;

						isZoomInOut = false;
					} else {
						lookAngleX += rotationStepX;
						lookAngleY += rotationStepY;
						distance += zoomInOutStep;
					}
				} else {
					disassembleCube();
				}
			} else {
				if (!isZoomInOut) {
					assembleCube();
				} else {
					float delta = newDistance - distance;
					if (Math.abs(delta) <= zoomInOutStep) {
						distance = newDistance;
						firstfalg = false;
						rotationcount = 0;
						isDisassembled = false;
						isStartAnimation = false;
						isAction = true;
					} else {
						distance -= zoomInOutStep;
					}
					
					lookAngleX = 0.47324f; 
				     lookAngleY = 0.74325f;
				     iCubeAnimationListner.cubeClosed();
				}
			}
		}
	}

	public void onActionMove(int x, int y) {
		if (isAction) {
			if (intersectObj != null) {
				
				String activity = intersectObj.getFaceName();

				float dx = (x - mPreviousX);
				float dy = (y - mPreviousY);
				if (Math.cos((double) lookAngleX) < 0.0f) {
					dx *= -1.0f;
				}
				lookAngleY += dx * TOUCH_SCALE_FACTOR;
				lookAngleX += dy * TOUCH_SCALE_FACTOR;
				/*float dx = x - mPreviousX;
				float dy = y - mPreviousY;
				lookAngleX += dx * TOUCH_SCALE_FACTOR;
				lookAngleY += dy * TOUCH_SCALE_FACTOR;*/
			}
		}

		mPreviousX = x;
		mPreviousY = y;
	}
   
	public void onActionDown(int x, int y) {
		mDownX = x;
		
		mDownY = y;

		mPreviousX = x;
		mPreviousY = y;
	}

	public void onActionUp(int x, int y) {
		int deltaX = (int) Math.abs(mDownX - x);
		int deltaY = (int) Math.abs(mDownY - y);
		if (deltaX < 5 && deltaY < 5 && !isStartAnimation) {
			if (intersectObj != null) {
				// Toast.makeText(ctx, intersectObj.getFaceName(),
				// Toast.LENGTH_SHORT).show();
				DataEngine.clickFlag = true;
				String click_activity = intersectObj.getFaceName();
				if (click_activity.equalsIgnoreCase("front")) {
					DataEngine.clickFlag = true;
					Intent video_intent = new Intent(ctx,
							VideoplayerActivity.class);
					ctx.startActivity(video_intent);
				} else if (click_activity.equalsIgnoreCase("left")) {
					DataEngine.clickFlag = true;
					doBackground(true);

				} else if (click_activity.equalsIgnoreCase("back")) {
					DataEngine.clickFlag = true;
					Intent contact_activity = new Intent(ctx,
							KontaktViewActivity.class);
					network_check(contact_activity);
					// ctx.startActivity(contact_activity);

				} else if (click_activity.equalsIgnoreCase("bottom")) {
					DataEngine.clickFlag = true;
					Intent eforslag_activity = new Intent(ctx,
							RecentPetitions.class);
					network_check(eforslag_activity);
					// ctx.startActivity(eforslag_activity);

				} else if (click_activity.equalsIgnoreCase("right")) {
					DataEngine.clickFlag = true;
					Intent pagang_activity = new Intent(ctx,
							PegangViewActivity.class);
					network_check(pagang_activity);
					// ctx.startActivity(pagang_activity);

				} else if (click_activity.equalsIgnoreCase("top")) {
					DataEngine.clickFlag = true;
					Intent asikt_activity = new Intent(ctx,
							AsiktarViewActivity.class);
					network_check(asikt_activity);
					// ctx.startActivity(asikt_activity);
				}
			}
		}

		mPreviousX = x;
		mPreviousY = y;
	}

	private String form_url() {
		String url = DataEngine.BASE_URL + DataEngine.QUESTION_INFO
				+ MethodsHelper.getDeviceId(ctx) + DataEngine.DEVICE_INFO
				+ "android";
		return url;
	}

	private void network_check(Intent intent) {
		if (NetworkChecker.isNetworkOnline(ctx)) {
			ctx.startActivity(intent);
		} else {
			showToast(ctx.getResources().getString(R.string.network_error));
			/*Toast.makeText(ctx,
					ctx.getResources().getString(R.string.network_error),
					Toast.LENGTH_SHORT).show();*/

		}
	}

	class BackgroundProcess extends AsyncTask<Void, Void, Void> {
		boolean showprogress = false;

		public BackgroundProcess(boolean progress) {
			// TODO Auto-generated constructor stub
			this.showprogress = progress;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			try {
				if (showprogress) {
					p_dialog.show();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			reader_obj = new QuestionReader(ctx);
			reader_obj.parse_json(form_url());
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			try {
				if (showprogress)
					if (p_dialog.isShowing())
						p_dialog.dismiss();
			} catch (Exception e) {
				// TODO: handle exception
			}
			check_status();
			super.onPostExecute(result);
		}

	}

	int val = 0;
	QuestionHelper helper_obj;

	private void check_status() {
		 if (reader_obj != null && reader_obj.questions_data.size() > val ) {
		if (!(reader_obj.questions_data.elementAt(val).isAns_status())) {
			helper_obj = reader_obj.helper_obj;
			Intent ques_activity = new Intent(ctx, QuestionViewActivity.class);
			ques_activity.putExtra(DataEngine.QUESTION_OBJECT, helper_obj);
			ctx.startActivity(ques_activity);
		} else {
			helper_obj = reader_obj.helper_obj;
			Intent piechart_intent = new Intent(ctx, PieChartActivity.class);
			piechart_intent.putExtra(DataEngine.PIE_OBJECT, helper_obj);
			ctx.startActivity(piechart_intent);
			
		}
	}
		/*
		 * }else{
		 * Toast.makeText(ctx,"Ingen fr�ga f�r att r�sta..",Toast.LENGTH_SHORT
		 * ).show(); }
		 */
	}

	private void doBackground(boolean showprogress) {
		background_process = new BackgroundProcess(showprogress);
		if (NetworkChecker.isNetworkOnline(ctx)) {
			background_process.execute();

		} else {
			showToast(ctx.getResources().getString(R.string.network_error));
			/*Toast.makeText(ctx,
					ctx.getResources().getString(R.string.network_error),
					Toast.LENGTH_SHORT).show();*/

		}
	}

	private void setFaceModelMatrix(int face, int angleStep,
			boolean isDisassemble) {
		float rotationAngle;
		float translateX;
		float translateY;
		float translateZ;

		switch (face) {
		case 5:
			Mesh bottomFace = root.get(5);
			bottomFace.initModelMatrix();
			rotationAngle = angleStep;
			bottomFace.setRotationX(rotationAngle);

			translateY = -(0.5f + 0.5f * (float) Math.cos(Math.PI
					* rotationAngle / 180));
			translateZ = (0.5f - 0.5f * (float) Math.sin(Math.PI
					* rotationAngle / 180));
			bottomFace.setTranslateY(translateY);
			bottomFace.setTranslateZ(translateZ);

			Mesh backFace = root.get(2);
			backFace.initModelMatrix();
			if (isDisassemble) {
				rotationAngle = 0;
				backFace.setRotationX(rotationAngle);

				translateY = bottomFace.getTranslateY()
						- (0.5f + 0.5f * (float) Math.cos(Math.PI
								* bottomFace.getRotationX() / 180));
				translateZ = bottomFace.getTranslateZ()
						- (0.5f * (float) Math.sin(Math.PI
								* bottomFace.getRotationX() / 180));
				backFace.setTranslateY(translateY);
				backFace.setTranslateZ(translateZ);
			} else {
				backFace.setRotationY(180);
				backFace.setTranslateZ(-0.5f);
			}
			break;
		case 4:
			Mesh topFace = root.get(4);
			topFace.initModelMatrix();
			rotationAngle = -angleStep;
			topFace.setRotationX(rotationAngle);

			translateY = (0.5f + 0.5f * (float) Math.cos(Math.PI
					* rotationAngle / 180));
			translateZ = (0.5f + 0.5f * (float) Math.sin(Math.PI
					* rotationAngle / 180));
			topFace.setTranslateY(translateY);
			topFace.setTranslateZ(translateZ);
			break;
		case 3:
			Mesh leftFace = root.get(3);
			leftFace.initModelMatrix();
			rotationAngle = -angleStep;
			leftFace.setRotationY(rotationAngle);

			translateX = -(0.5f + 0.5f * (float) Math.cos(Math.PI
					* rotationAngle / 180));
			translateZ = (0.5f + 0.5f * (float) Math.sin(Math.PI
					* rotationAngle / 180));
			leftFace.setTranslateX(translateX);
			leftFace.setTranslateZ(translateZ);
			break;
		case 1:
			Mesh rightFace = root.get(1);
			rightFace.initModelMatrix();
			rotationAngle = angleStep;
			rightFace.setRotationY(rotationAngle);

			translateX = (0.5f + 0.5f * (float) Math.cos(Math.PI
					* rotationAngle / 180));
			translateZ = (0.5f - 0.5f * (float) Math.sin(Math.PI
					* rotationAngle / 180));
			rightFace.setTranslateX(translateX);
			rightFace.setTranslateZ(translateZ);
			break;
		}
	}

	private void disassembleCube() {
		if (animationStep < animationOrder.length) {
			int face = animationOrder[animationStep];

			if (animationAngle < 0) {
				animationAngle = 0;
			} else {
				animationAngle -= 5;
			}

			setFaceModelMatrix(face, animationAngle, true);

			// next face
			if (animationAngle == 0) {
				animationAngle = 90;
				animationStep++;

				if (animationStep >= animationOrder.length) {
					isDisassembled = true;
					isStartAnimation = false;
				}
			}
		}
	}

	public void onDisassembleCube() {
		if (!isDisassembled && !isStartAnimation) {
			isAction = false;

			newDistance = 7.0f;
			zoomInOutStep = Math.abs(newDistance - distance)
					/ zoomInOutStepCount;
			rotationStepX = (0.0f - lookAngleX) / zoomInOutStepCount;
			rotationStepY = (0.0f - lookAngleY) / zoomInOutStepCount;

			animationStep = 0;
			animationAngle = 90;
			isStartAnimation = true;
			isZoomInOut = true;
		}
	}

	private void assembleCube() {
		if (animationStep < animationOrder.length) {
			int face = animationOrder[animationStep];

			if (animationAngle >= 90) {
				animationAngle = 90;
			} else {
				animationAngle += 5;
			}

			setFaceModelMatrix(face, animationAngle, false);

			// next face
			if (animationAngle == 90) {
				animationAngle = 0;
				animationStep--;

				if (animationStep < 0) {
					newDistance = 4.0f;
					zoomInOutStep = Math.abs(newDistance - distance)
							/ zoomInOutStepCount;

					isZoomInOut = true;
				}
			}
		}
	}

	public void onAssembleCube() {
		if (isDisassembled && !isStartAnimation) {
			isAction = false;

			lookAngleX = 0.0f;
			lookAngleY = 0.0f;
			/*
			 * lookAngleX = 0.47324f; lookAngleY = 0.74325f;
			 */

			animationStep = animationOrder.length - 1;
			animationAngle = 0;
			isStartAnimation = true;
		}
	}

	public void decreaseSizeOfCubeFace() {

		sizeFlag = false;
	}

	boolean sizeFlag = true;

	public void increaseSizeOfCubeFace() {

		sizeFlag = true;
	}

	public void setSwipeLeft(boolean isSwipeLeft) {
		this.isSwipeLeft = isSwipeLeft;
	}

	public void setSwipeRight(boolean isSwipeRight) {
		this.isSwipeRight = isSwipeRight;
	}

	public void setSwipeUp(boolean isSwipeUp) {
		this.isSwipeUp = isSwipeUp;
	}

	public void setSwipeDown(boolean isSwipeDown) {
		this.isSwipeDown = isSwipeDown;
	}
	public void showToast(String msg) {
		String fmsg = msg.substring(0, 1).toUpperCase()+msg.substring(1).toLowerCase();
		MethodsHelper.showToast(fmsg, ctx);
		//Toast.makeText(ctx, fmsg, Toast.LENGTH_SHORT).show();
	}
}
