/**
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.androidapps.speakapp.cube;

public class PlaneGeometry extends Mesh {
	/**
	 * Create a plane with a default with and height of 1 unit.
	 */
	public PlaneGeometry() {
		this(1, 1, "front", true);
	}

	/**
	 * Create a plane.
	 * 
	 * @param width
	 *            the width of the plane.
	 * @param height
	 *            the height of the plane.
	 */
	public PlaneGeometry(float width, float height, String faceName, boolean isPicking) {
		// Mapping coordinates for the vertices
		float textureCoordinates[] = { 
				0.0f, 0.0f, //
				0.0f, 1.0f, //
				1.0f, 1.0f, //
				1.0f, 0.0f, //
		};

		short[] indices = new short[] { 0, 1, 2, 0, 2, 3 };

		float[] vertices = new float[] { 
				-width / 2, height / 2, 0.0f,	// top left
				-width / 2, -height / 2, 0.0f, 	// bottom left
				width / 2, -height / 2, 0.0f,	// bottom right
				width / 2, height / 2, 0.0f,	// top right 
		}; 	

		setIndices(indices);
		setVertices(vertices);
		setTextureCoordinates(textureCoordinates);

		setFaceName(faceName);
		setPicking(isPicking);
	}
}
