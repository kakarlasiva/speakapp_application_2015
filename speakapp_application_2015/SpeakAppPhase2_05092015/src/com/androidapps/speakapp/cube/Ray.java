package com.androidapps.speakapp.cube;

import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLU;
import android.opengl.Matrix;

public class Ray {

	float[] P0;
	float[] P1;

	float intersectZPos = 1000.0f;
	Mesh intersectObj = null;
	
	public Ray(GL10 gl, int width, int height, float xTouch, float yTouch) {
		init(gl, width, height, xTouch, yTouch);
	}
	
	public void init(GL10 gl, int width, int height, float xTouch, float yTouch) {
		MatrixGrabber matrixGrabber = new MatrixGrabber();
		matrixGrabber.getCurrentState(gl);

		int[] viewport = { 0, 0, width, height };

		float[] nearCoords = new float[3];
		float[] farCoords = new float[3];
		float[] temp = new float[4];
		float[] temp2 = new float[4];

		// get the near and far coordinates for the click
		float winx = xTouch, winy = viewport[3] - yTouch;

		int result = GLU.gluUnProject(winx, winy, 1.0f, matrixGrabber.mModelView, 0, matrixGrabber.mProjection, 0, viewport, 0, temp, 0);

		Matrix.multiplyMV(temp2, 0, matrixGrabber.mModelView, 0, temp, 0);
		if (result == GL10.GL_TRUE) {
			nearCoords[0] = temp2[0] / temp2[3];
			nearCoords[1] = temp2[1] / temp2[3];
			nearCoords[2] = temp2[2] / temp2[3];
		}

		result = GLU.gluUnProject(winx, winy, 0, matrixGrabber.mModelView, 0, matrixGrabber.mProjection, 0, viewport, 0, temp, 0);
		Matrix.multiplyMV(temp2, 0, matrixGrabber.mModelView, 0, temp, 0);
		if (result == GL10.GL_TRUE) {
			farCoords[0] = temp2[0] / temp2[3];
			farCoords[1] = temp2[1] / temp2[3];
			farCoords[2] = temp2[2] / temp2[3];
		}
		this.P0 = farCoords;
		this.P1 = nearCoords;		
	}
	
	public float getIntersectZPos() {
		return intersectZPos;
	}

	public void setIntersectZPos(float intersectZPos) {
		this.intersectZPos = intersectZPos;
	}

	public Mesh getIntersectObj() {
		return intersectObj;
	}

	public void setIntersectObj(Mesh intersectObj) {
		this.intersectObj = intersectObj;
	}
}
