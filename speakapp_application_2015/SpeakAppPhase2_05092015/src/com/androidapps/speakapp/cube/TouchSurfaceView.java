/**
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.androidapps.speakapp.cube;

import com.androidapps.speakapp.cube.SimpleGestureFilter.SimpleGestureListener;
import com.androidapps.speakapp.data.DataEngine;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.graphics.PointF;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;


class TouchSurfaceView extends GLSurfaceView implements SimpleGestureListener{
	private final MyGLRenderer mRenderer;
	private final SparseArray<PointF> mActivePointers;

	private float prevScaleFactor = 1.0f;
	private float scaleFactor = 1.0f;
	private final ScaleGestureDetector scaleGestureDetector;
    Handler uiHandler;
	ICubeAnimationListner iCubeAnimationListner;
    SimpleGestureFilter detector;
    
    MotionEvent me1;
	public TouchSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setEGLContextClientVersion(1);
		mActivePointers = new SparseArray<PointF>();
		uiHandler = new Handler();
		
		scaleGestureDetector = new ScaleGestureDetector(context,
				new ScaleListener());		
		
		iCubeAnimationListner = (InteractiveCube) context;
		detector = new SimpleGestureFilter((Activity)context, this);
		mRenderer = new MyGLRenderer(context);
		setEGLConfigChooser(8, 8, 8, 8, 16, 0);
		//setEGLConfigChooser(new MultisampleConfigChooser());
		getHolder().setFormat(PixelFormat.TRANSLUCENT);
		
		setRenderer(mRenderer);
		// Render the view only when there is a change in the drawing data
		// setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}
   
	public TouchSurfaceView(InteractiveCube context) {
		super(context);

		setEGLContextClientVersion(1);

		mActivePointers = new SparseArray<PointF>();
		scaleGestureDetector = new ScaleGestureDetector(context,
				new ScaleListener());
		
		

		mRenderer = new MyGLRenderer(context);
		setRenderer(mRenderer);

		// Render the view only when there is a change in the drawing data
		// setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}

	@Override
	public void onResume() {

	}

	@Override
	public void onPause() {
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		scaleGestureDetector.onTouchEvent(event);
		// detector.onTouchEvent(event);
		 me1=event;
		// get pointer index from the event object
		int pointerIndex = event.getActionIndex();

		// get pointer ID
		int pointerID = event.getPointerId(pointerIndex);

		// get masked(not specific to a pointer) action
		int maskedAction = event.getActionMasked();

		switch (maskedAction) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_POINTER_DOWN: {
			PointF f = new PointF();
			f.x = event.getX(pointerIndex);
			f.y = event.getY(pointerIndex);
			mActivePointers.put(pointerID, f);

			if (maskedAction == MotionEvent.ACTION_DOWN) {
				mRenderer.onActionDown((int) f.x, (int) f.y);
				requestRender();
			}
		}
			break;
		case MotionEvent.ACTION_MOVE: {
			for (int size = event.getPointerCount(), i = 0; i < size; i++) {
				PointF point = mActivePointers.get(event.getPointerId(i));
				if (point != null) {
					point.x = event.getX(i);
					point.y = event.getY(i);
				}
			}

			float x = event.getX(pointerIndex);
			float y = event.getY(pointerIndex);
			//Log.i("ActionMove","---------on action move---------------"+(int) x+"---"+(int) y);
			mRenderer.onActionMove((int) x, (int) y);
			requestRender();
		}
			break;
		case MotionEvent.ACTION_UP: {			//DataEngine.clickFlag = false;
			float x = event.getX(pointerIndex);
			float y = event.getY(pointerIndex);
			

			mActivePointers.remove(pointerID);
			mRenderer.onActionUp((int) x, (int) y);
			
		}
			break;
		case MotionEvent.ACTION_POINTER_UP:
			Log.d("test", "Scale Factor : " + scaleFactor);
			if (scaleFactor >= 1.5f && (scaleFactor >= prevScaleFactor) && !DataEngine.clickFlag) {
				mRenderer.decreaseSizeOfCubeFace();
				iCubeAnimationListner.cubeOpened();				
				mRenderer.onDisassembleCube();
				
				
			} else if (scaleFactor < 1.5f && (scaleFactor <= prevScaleFactor) && !DataEngine.clickFlag) {
                mRenderer.increaseSizeOfCubeFace();
				mRenderer.onAssembleCube();
				
				//iCubeAnimationListner.cubeClosed();
				
			}
			break;
		case MotionEvent.ACTION_CANCEL:
			mActivePointers.remove(pointerID);
			break;
		}

		return true;
	}
    
	Runnable r = new Runnable() {
		
		@Override
		public void run() {
			
			
			// TODO Auto-generated method stub
			
		}
	};
	
	private class ScaleListener extends
			ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			prevScaleFactor = scaleFactor;

			// don't let the object get too small or too large.
			scaleFactor *= detector.getScaleFactor();
			scaleFactor = Math.max(1.0f, Math.min(scaleFactor, 2.0f));

			return true;
		}
	}

	@Override
	public void onSwipe(int direction) {
		switch (direction) {
		case SimpleGestureFilter.SWIPE_RIGHT:
			mRenderer.setSwipeRight(true);
			mRenderer.setSwipeDown(false);
			mRenderer.setSwipeLeft(false);
			mRenderer.setSwipeUp(false);
			break;
		case SimpleGestureFilter.SWIPE_LEFT:
			mRenderer.setSwipeRight(false);
			mRenderer.setSwipeDown(false);
			mRenderer.setSwipeLeft(true);
			mRenderer.setSwipeUp(false);
			break;
		case SimpleGestureFilter.SWIPE_DOWN:
			mRenderer.setSwipeRight(false);
			mRenderer.setSwipeDown(true);
			mRenderer.setSwipeLeft(false);
			mRenderer.setSwipeUp(false);
			break;
		case SimpleGestureFilter.SWIPE_UP:
			mRenderer.setSwipeRight(false);
			mRenderer.setSwipeDown(false);
			mRenderer.setSwipeLeft(false);
			mRenderer.setSwipeUp(true);
			break;
		}
		
	}

	@Override
	public void onDoubleTap() {
		
		// TODO Auto-generated method stub
		int pointerIndex = me1.getActionIndex();

	    // get pointer ID
	    int pointerID = me1.getPointerId(pointerIndex);
	    float x = me1.getX(pointerIndex);
	    float y = me1.getY(pointerIndex);

	    mActivePointers.remove(pointerID);
	    mRenderer.onActionUp((int) x, (int) y);
		
	}
}
