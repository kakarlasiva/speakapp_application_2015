 package com.androidapps.speakapp.data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;

import com.androidapps.speakapp.database.MyDbAdapter;
import com.androidapps.speakapp.eforslag.PetetionBean;

public class DataEngine {

	public static final String APP_REF = "SpeakApp";

	//public static final String BASE_URL = "http://dev-speakapp.solidpartner.com";
	public static final String BASE_URL = "http://speakapp.solidpartner.com";
	public static final String APP_INFO = "/API/CheckLatestVersion?deviceId=";
	public static final String QUESTION_INFO = "/API/GetQuestion?deviceId=";
	public static final String DEVICE_INFO = "&device=";
	public static final String SUBMIT_VOTE = "/API/SubmitVote?jsonstring=";
	public static final String NEWS_INFO = "/API/GetNews?size=500x300";
	public static final String REG_ID="&regId=";
	public static final String SUBJECTS="/API/GetSubjects";
	public static final String REG_PUSHID="/API/RegisterPushId?deviceId=";
	public static final String PUSH="&pushId=";
	public static final String SEND_MAIL="/API/SendEmail?jsonstring=";
	public static final String ASIK_SEND="/API/SubmitQuery?jsonstring=";

	public static String VOTE_URL = null;
	public static String NEWS_URL = null;
	public static String GCM_REGID = null;
	public static String PUSH_ORDERID = null;
	public static String SUBJECTS_URL=null;

	// The log type constants
	public static final String ERROR_LOG = "error";
	public static final String VERBOSE_LOG = "verbose";
	public static final String INFO_LOG = "info";
	public static final String DEBUG_LOG = "debug";

	// The log enabled/disabled status.
	public static final boolean ERROR_LOG_ENABLED = true;
	public static final boolean VERBOSE_LOG_ENABLED = true;
	public static final boolean INFO_LOG_ENABLED = true;
	public static final boolean DEBUG_LOG_ENABLED = true;

	// The helper object strings
	public static final String QUESTION_OBJECT="question_object";
	public static final String PIE_OBJECT = "pie_object";
	public static final String NEWS_OBJECT = "news_object";
	public static final String SELECTED_OBJECT="news_selected";
	public static final String INFO_OBJECT="info_txt";

	// The facebook objects
	public static final String FACEBOOK_APPID = "565962740166168";
	public static final String FB_ID = "fb_id";
	public static final String FB_NAME = "fb_name";
	public static final String FB_IMG_URL = "fb_imgurl";
	public static final String FB_SESSION_TOKEN = "fb_sessiontoken";

	// twitter logins
	public static final String CONSUMER_KEY = "dizVyUyWBrTeZcLOYwCT29DDs";
	public static final String SECRET_KEY = "3gUPkphqhawXIJL4JswG4K9UqlVR5gCYjgKx4yVUipA2QWXkFB";
	/*public static final String CONSUMER_KEY="gFUtrooktPrH1nkrvIzGQ";
	public static final String SECRET_KEY="g36tP2wnGyDvcCqtI0AF2svwk7tYMd3tCf5LZdKOOw";*/
	
	public static MyDbAdapter my_adapter = null;

	public static void open_database(Context mctx) {
		my_adapter = new MyDbAdapter(mctx);
	}

	public static Cursor mydbCursor;
	
	public static String fav_title=null;
	public static String fav_description=null;
	public static String fav_image=null;
	public static String fav_date=null;
	public static String fav_newsid=null;

	public static byte[] getBytes(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.JPEG, 0, stream);
		return stream.toByteArray();
	}

	/**
	 * 
	 * @param image -- in the form of byte array
	 * @return bitmap
	 */
	public static Bitmap getImage(byte[] image) {
		return BitmapFactory.decodeByteArray(image, 0, image.length);
	}
	/**
	 * 
	 * @param src -- url of the bitmap
	 * @return bitmap
	 */
	public static Bitmap getBitmapFromURL(String src) {
	    try {
	        URL url = new URL(src);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
	        return myBitmap;
	    } catch (IOException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
	
	public static Typeface gettypeface(Context ctx){
		Typeface typeface=Typeface.createFromAsset(ctx.getAssets(),"gothic_0.TTF");
		return typeface;
	}
   public static boolean clickFlag = false;
   
   public static boolean disassemble = false;
   
	public static boolean isRecentPetitionsDestroyed = false;
	
	public static boolean isLoginPetitionsDestroyed = false;
	
	public static boolean isDetailedPetitionsDestroyed = false;
	
	public static boolean isWritePetitionsDestroyed = false;
	
	public static ArrayList<PetetionBean> petetionsList =  new ArrayList<PetetionBean>();
	
	
}
