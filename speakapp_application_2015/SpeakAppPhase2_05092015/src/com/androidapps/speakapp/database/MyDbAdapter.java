package com.androidapps.speakapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDbAdapter {
	private MyDbHelper myDbhelper;
	private SQLiteDatabase mDb;
	private final static String TAG = "MyDbAdapter";
	private static final String DATABASE_CREATE = "create table favorites(_id integer primary key,title text not null,description text not null,date text not null,image text,newsid text not null);";

	private static final String DATABASE_TABLE = "favorites";	
	Cursor dateCursor = null;
	private final Context mCtx;
	private static final String DATABASE_NAME = "SpeakApp";
	private static final int DATABASE_VERSION = 1;
	public static final String TITLE = "title";
	public static final String ISLOGIN = "islogin";
	public static final String DESCRIPTION = "description";
	public static final String DATE = "date";
	public static final String IMAGE="image";
	public static final String NEWSID="newsid";
	private final String ID = "_id";
	
	private static class MyDbHelper extends SQLiteOpenHelper {

		public MyDbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			
		}

		@Override
		public void onCreate(SQLiteDatabase db) {			
			db.execSQL(DATABASE_CREATE);			
			
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {			
				Log.w(TAG, "Upgrading database from version " + oldVersion
						+ " to " + newVersion
						+ ", which will destroy all old data");
			
			onCreate(db);
		}

	}

	public MyDbAdapter(Context ctx) {
		this.mCtx = ctx;
	}

	public MyDbAdapter open() throws SQLException {
		try {
			myDbhelper = new MyDbHelper(mCtx);
			mDb = myDbhelper.getWritableDatabase();
			return this;
		} catch (Exception e) {
			// DebugLog.e(e);
			myDbhelper.close();
			mDb.close();
		}
		return null;
	}

	public void close() {
		try {
			if (null != myDbhelper) {
				myDbhelper.close();
				mDb.close();
			}
		} catch (Exception e) {
			// DebugLog.e(e);
		}

	}
	
	/**
	 * Add favorite details to the favorites table.
	 * @param title
	 * @param description
	 * @param date
	 * @param image
	 * @param newsid
	 * @return id --> inseted row id
	 */
	public long add_informationfav(String title, String description,String date,String image,String newsid) {
		Long return_value = null;
		ContentValues args_new = new ContentValues();

		ContentValues initialvalues = new ContentValues();
		initialvalues.put(TITLE,title);
		initialvalues.put(DESCRIPTION,description);
		initialvalues.put(DATE,date);
		initialvalues.put(IMAGE,image);
		initialvalues.put(NEWSID,newsid);
		
		args_new.put(TITLE,title);

		if (title.equals("")) {
			return -1;
		}

		if (mDb.update(DATABASE_TABLE, args_new, NEWSID + "=" + "\"" + newsid
				+ "\"", null) > 0) {
			return_value = 999L;
		} else {
			return_value = mDb.insert(DATABASE_TABLE, null, initialvalues);
		}
		return return_value;

	}		

	/**
	 * Get all the details of favorites table.
	 * @return cursor
	 */
	public Cursor get_allfavlist() {
		return mDb
				.query(DATABASE_TABLE, new String[] { ID,TITLE,DESCRIPTION,DATE,IMAGE,NEWSID}, null, null, null,
						null, null);

	}
  
	/**
	 * delete specific favorite item from favorites table
	 * @param text
	 * @return true if deleted els false
	 */
	public boolean delete_favinformation(String text) {
		boolean result2 = false;
		if (mDb.delete(DATABASE_TABLE, NEWSID + "=" + "\"" + text + "\"",
				null) > 0) {
			result2 = true;
		}
		return result2;
	}

	/**
	 * delete all the data from favorites table
	 * @return true if deleted else false
	 */
	public boolean delete_All_FavouriteChannel() {

		boolean result2 = false;
		if (mDb.delete(DATABASE_TABLE, "1", null) > 0) {
			result2 = true;
		}
		return result2;
	}

	
	

}
