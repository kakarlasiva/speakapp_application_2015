package com.androidapps.speakapp.eforslag;

import java.util.Vector;

/**
 * 
 * This interface gives the callbacks to the activity when we use async
 * task/calling webservices.
 * 
 */
public interface APIRespone {

	public void onSuccues(String response, Vector<String> errors);

	public void onStartExecution();
}
