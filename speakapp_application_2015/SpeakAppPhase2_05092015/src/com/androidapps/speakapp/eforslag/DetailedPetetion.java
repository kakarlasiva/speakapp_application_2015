package com.androidapps.speakapp.eforslag;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.log.MyDebug;
import com.androidapps.speakapp.screens.InfoActivity;
import com.androidapps.speakapp.twitter.Twitt_Sharing;
import com.androidapps.speakapp.util.MethodsHelper;
import com.facebook.samples.facebook.AsyncFacebookRunner;
import com.facebook.samples.facebook.AsyncFacebookRunner.RequestListener;
import com.facebook.samples.facebook.DialogError;
import com.facebook.samples.facebook.Facebook;
import com.facebook.samples.facebook.Facebook.DialogListener;
import com.facebook.samples.facebook.FacebookError;
import com.facebook.samples.facebook.SessionEvents;
import com.facebook.samples.facebook.SessionEvents.AuthListener;
import com.facebook.samples.facebook.SessionEvents.LogoutListener;
import com.facebook.samples.facebook.SessionStore;
import com.facebook.samples.facebook.Util;
import com.facebook.samples.fbconnect.FBConnectQueryProcessor;
import com.facebook.samples.friendpicker.Common;

/**
 * 
 * It shows the detailed petetion of selected specific petition
 * 
 */
public class DetailedPetetion extends Activity implements APIRespone,
		OnClickListener {
	protected static final int LOGGIN_CALLED = 1000;
	TextView summaryTv, summaryNotesTv, descTv, descNotesTv, signCountTv,
			titleTv;
	String summary, description, commentText;
	PetetionBean beanObj;
	SharedPreferences settings;
	MyAsyncTask background;
	EditText commentView;
	ImageView share_image, info;
	LinearLayout back;
	Dialog share_dialog;
	Facebook mFacebook;
	AsyncFacebookRunner mAsyncFacebookRunner;
	boolean fbisThere;
	Handler handler;
	protected static final int LOG_IN = 1000;
	TextView titleTxt;
	int Signature_count;
	Dialog custom;
	SessionManager sessionMgr;
	boolean isRelogin = false;

	/**
	 * 
	 * Facebook Authentication listener which gives callbacks for Authentication
	 * success/failure
	 * 
	 */
	public class FacebookAuthListener implements AuthListener {

		public void onAuthSucceed() {
			SessionStore.save(mFacebook, getApplicationContext());
		}

		public void onAuthFail(String error) {
		}
	}

	/**
	 * 
	 * Facebook logout listener which gives callbacks for logout start and
	 * finish
	 * 
	 */
	class FacebookLogoutListener implements LogoutListener {
		public void onLogoutBegin() {
			System.out.println(">....................>");

		}

		public void onLogoutFinish() {
			System.out.println(">....................>");

		}
	}

	/**
	 * check the facebook session is valid or not
	 * 
	 * @return true - session is valid , false - session is invalid
	 */
	public boolean checkStatus() {

		if (!Common.isInternetAvailable(getApplicationContext())
				|| Common.isAirplaneModeOn(getApplicationContext())) {
		} else {
			mFacebook = new Facebook(DataEngine.FACEBOOK_APPID);// new
			// Facebook("193498163996120");
			mAsyncFacebookRunner = new AsyncFacebookRunner(mFacebook);
			SessionStore.restore(mFacebook, getApplicationContext());
			SessionEvents.addAuthListener(new FacebookAuthListener());
			SessionEvents.addLogoutListener(new FacebookLogoutListener());

			if (mFacebook.isSessionValid()) {
				return true;
			}
		}
		return false;
	}

Runnable sr = new Runnable() {
		
		@Override
		public void run() {
			fb_share();
			
		}
	};
	private class UserDetailsRequestListener implements DialogListener,
			RequestListener {

		@Override
		public void onComplete(String response, Object state) {
			try {
				JSONObject json = Util.parseJson(response);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString(DataEngine.FB_ID, json.getString("id"));
				editor.putString(DataEngine.FB_IMG_URL,
						"http://graph.facebook.com/" + json.getString("id")
								+ "/picture?type=large");
				editor.putString(DataEngine.FB_NAME, json.getString("name"));
				String friedsresponse = new FBConnectQueryProcessor()
						.requestFriendsList(getApplicationContext(), mFacebook);
				JSONObject mainObj = new JSONObject(friedsresponse);
				
				editor.commit();
				if (mainObj.has("error")) {
					return;
				}				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FacebookError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onIOException(IOException e, Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {

		}

		@Override
		public void onComplete(Bundle values) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onError(DialogError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub

		}

	}

	class FacebookLoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			SessionEvents.onLoginSuccess();
			mAsyncFacebookRunner
					.request("me", new UserDetailsRequestListener());
			SessionStore.save(mFacebook, getApplicationContext());
			fb_share();
		}

		public void onFacebookError(FacebookError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onError(DialogError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onCancel() {
			SessionEvents.onLoginError("Action Canceled");
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {		
		Log.e("DetailedPetition","Requestcode----"+requestCode+"--Result code---"+resultCode);
		try {
			resultCode = data.getIntExtra("RESULT", 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.e("DetailedPetition","Requestcode--------"+requestCode+"--Result code-------"+resultCode);
		if (requestCode == 1000) {

			if (EforseLogConstants.httpClient == null) {
				sessionMgr.logoutUser();
			}
			if (!sessionMgr.isLoggedIn()) {
				setInvisibleCommentViews();
				return;
			}
			if (resultCode == EforseLogConstants.LOGIN_SUCCESS
					|| resultCode == EforseLogConstants.REGISTRATION_SUCCESS) {
				// signPetetion();
				setVisibleCommentViews();
			}
		}
		else if(requestCode == 4000)
		{
			if (resultCode == EforseLogConstants.LOGIN_SUCCESS)
			{
				startActivity(new Intent(DetailedPetetion.this,
						WritePetition.class));
				finish();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void setTypeface(TextView[] vs) {
		for (TextView v : vs) {
			v.setTypeface(customTypeface);

		}
	}

	CheckBox agreeTerms;
	RelativeLayout commentLayout;
	Button signPetion, signToRegister, addNewProposal;
	Typeface customTypeface;
	TextView logout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		background = new MyAsyncTask(this, this);
		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));
		/*settings = getSharedPreferences(EforseLogConstants.LOGIN_DETAILS,
				MODE_PRIVATE);*/
		sessionMgr = new SessionManager(getApplicationContext());
		if(sessionMgr.isLoggedIn())
		{
			sendLoginDataPreferences();
		}
		setContentView(R.layout.eforslag_kommentar_lay);
		sv = (ScrollView) findViewById(R.id.detailed_scroll);
		customTypeface = DataEngine.gettypeface(this);
		logout = (TextView) findViewById(R.id.logour);
		logout.setOnClickListener(this);
		logout.setTypeface(customTypeface);
		titleTxt = (TextView) findViewById(R.id.eforslag_title);
		titleTxt.setTypeface(customTypeface);
		summaryTv = (TextView) findViewById(R.id.pt_summary);
		summaryNotesTv = (TextView) findViewById(R.id.pt_summaryNotes);
		descTv = (TextView) findViewById(R.id.pt_description);
		commentView = (EditText) findViewById(R.id.komment);
		descNotesTv = (TextView) findViewById(R.id.pt_descNotes);
		signCountTv = (TextView) findViewById(R.id.pt_signerCount);
		agreeTerms = (CheckBox) findViewById(R.id.agreeTerms);
		if (MethodsHelper.getDensityName(this))
			agreeTerms.setPadding(
					(int) (agreeTerms.getPaddingLeft() + getResources()
							.getDimension(R.dimen.chkbox_padding)), agreeTerms
							.getPaddingTop(), agreeTerms.getPaddingRight(),
					agreeTerms.getPaddingBottom());
		signPetion = (Button) findViewById(R.id.signPetition);
		commentLayout = (RelativeLayout) findViewById(R.id.commentLay);
		back = (LinearLayout) findViewById(R.id.back_layout);
		share_image = (ImageView) findViewById(R.id.eforslag_share);
		info = (ImageView) findViewById(R.id.info);
		addNewProposal = (Button) findViewById(R.id.add_new_proposal);
		signToRegister = (Button) findViewById(R.id.signtoregister);
		addNewProposal.setTypeface(customTypeface);
		signToRegister.setTypeface(customTypeface);
		addNewProposal.setOnClickListener(this);
		signToRegister.setOnClickListener(this);
		back.setOnClickListener(this);
		share_image.setOnClickListener(this);
		info.setOnClickListener(this);
		titleTxt.setOnClickListener(this);
		handler = new Handler();
		beanObj = (PetetionBean) getIntent().getExtras().getSerializable("OBJ");

		summaryNotesTv.setText(beanObj.getSummary());
		String desc = beanObj.getDescription().trim();
		if(null != desc && desc.length()>0 && !desc.equals(" "))
			descTv.setVisibility(View.VISIBLE);
		else
			descTv.setVisibility(View.GONE);
		descNotesTv.setText(beanObj.getDescription()+"\n\n");
		// summaryNotesTv.setText(getResources().getString(R.string.write_petition_info)+"\n"+getResources().getString(R.string.write_petition_info));
		// descNotesTv.setText(getResources().getString(R.string.write_petition_info)+"\n"+getResources().getString(R.string.write_petition_info));

		titleTv = (TextView) findViewById(R.id.pt_title);
		titleTv.setText(beanObj.getTitle());
		signCountTv.setText("Signaturer " + beanObj.getSignatures()
				+ " h�ller med");
		GradientDrawable btnbg1 = (GradientDrawable) findViewById(R.id.komment)
				.getBackground();
		btnbg1.setColor(Color.parseColor("#ffffff"));
		btnbg1.setStroke(2, Color.parseColor("#000000"));
		commentView.setTypeface(customTypeface);
		/*
		 * commentView.setOnTouchListener(new OnTouchListener() {
		 * 
		 * @Override public boolean onTouch(View v, MotionEvent event) { // TODO
		 * Auto-generated method stub
		 * 
		 * commentView.setHint(""); commentView.setFocusable(true);
		 * 
		 * return false; } });
		 */

		agreeTerms.setTypeface(customTypeface);
		signPetion.setTypeface(customTypeface);
		int[] ids = { R.id.add_new_proposal, R.id.signtoregister,
				R.id.signPetition };
		setGradiantBackground(ids);
		TextView[] totalTextViews = { summaryTv, summaryNotesTv, descTv,
				descNotesTv, signCountTv, titleTv };
		setTypeface(totalTextViews);
		summaryTv.setTypeface(customTypeface, Typeface.BOLD);
		descTv.setTypeface(customTypeface, Typeface.BOLD);
		findViewById(R.id.signPetition).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {

						if (EforseLogConstants.httpClient == null) {
							sessionMgr.logoutUser();
						}
						if (sessionMgr.isLoggedIn()) {
							if (agreeTerms.isChecked()) {
								signPetetion();
							} else {
								showToast(getResources().getString(
										R.string.toast_for_detailedpetition));
								/*
								 * Toast.makeText( getApplicationContext(),
								 * getResources().getString(
								 * R.string.toast_for_detailedpetition),
								 * Toast.LENGTH_SHORT).show();
								 */
							}
						} else {
							final Dialog custom = new Dialog(
									DetailedPetetion.this);
							custom.requestWindowFeature(Window.FEATURE_NO_TITLE);

							custom.getWindow()
									.setBackgroundDrawable(
											new ColorDrawable(
													android.graphics.Color.TRANSPARENT));
							custom.getWindow().setLayout(
									LayoutParams.MATCH_PARENT,
									LayoutParams.WRAP_CONTENT);
							TextView dialogtext = (TextView) custom
									.findViewById(R.id.thanks_txt);
							dialogtext.setText(R.string.signtoregister);
							dialogtext.setTypeface(customTypeface);
							// TextView dialogtextcontent = (TextView)
							// custom.findViewById(R.id.thanks_txt_content);
							// dialogtextcontent.setVisibility(View.VISIBLE);
							// dialogtextcontent.setText(R.string.thank_msg_opinion_content);

							Button dialogButton = (Button) custom
									.findViewById(R.id.thanks_ok);
							dialogButton.setTypeface(customTypeface);
							GradientDrawable btnbg = (GradientDrawable) dialogButton
									.getBackground();
							btnbg.setColor(Color.parseColor("#ffc000"));
							dialogButton.setText(R.string.login_register);
							dialogButton.setTextColor(Color
									.parseColor("#000000"));
							// if button is clicked, close the custom dialog
							dialogButton
									.setOnClickListener(new OnClickListener() {
										@Override
										public void onClick(View v) {
											Intent intent = new Intent(
													DetailedPetetion.this,
													EforseLogNewActivity.class);
											startActivityForResult(intent,
													LOGGIN_CALLED);
											custom.dismiss();

										}
									});
							custom.show();

						}

					}

				});
		setInvisibleCommentViews();

	}
	
	private void sendLoginDataPreferences()
	{
		isRelogin = true;
		String LoginUser = "{ \"app_key\": \"" + EforseLogConstants.APP_KEY
				+ "\",\"login_name\":\""
				+ sessionMgr.getUserName() + "\""
				+ ",\"password\" : " + "\""
				+ sessionMgr.getPwd() + "\"}";
		//System.out.println(LoginUser);
		MyAsyncTask background;
		background = new MyAsyncTask(DetailedPetetion.this,
				DetailedPetetion.this);
		MyDebug.appendLog("Request Url : "+EforseLogConstants.LOGIN_URL +"\n"+LoginUser);
		background.execute(EforseLogConstants.LOGIN_URL, LoginUser);
	}

	@Override
	protected void onResume() {

		super.onResume();
		DataEngine.isDetailedPetitionsDestroyed = false;
		setLogoutButtonVisibility();
	}

	@Override
	protected void onDestroy() {

		super.onDestroy();
		DataEngine.isDetailedPetitionsDestroyed = true;
	}

	private void setGradiantBackground(int[] ids) {
		for (int id : ids) {
			GradientDrawable btnbgKomment = (GradientDrawable) findViewById(id)
					.getBackground();
			btnbgKomment.setColor(Color.parseColor("#000000"));
			btnbgKomment.setStroke(0, Color.parseColor("#00000000"));
		}
	}

	ProgressDialog progress;

	/**
	 * sign on the petition and starts async task for signing
	 */
	public void signPetetion() {
		if (agreeTerms.isChecked()) {
			String getLatestPetetions = "{ \"app_key\": \""
					+ EforseLogConstants.APP_KEY + "\",\"petition_id\":\""
					+ beanObj.getPetition_id() + "\"" + ",\"comment\" : "
					+ "\"" + commentView.getEditableText().toString() + "\""
					+ "}";
			//System.out.println("comment signature = " + getLatestPetetions);
			background = new MyAsyncTask(this, this);
			background.execute(EforseLogConstants.TO_SIGN_PETETION,
					getLatestPetetions);
			MyDebug.appendLog("Request Url : "+EforseLogConstants.TO_SIGN_PETETION+"\n"+getLatestPetetions);
			commentView.setText("");
			agreeTerms.setChecked(false);
		} else {
			showToast(getResources().getString(
					R.string.toast_for_detailedpetition));

		}

	}

	/**
	 * show the Dialog related to repsone of service calls
	 */
	private void showCorrespondingDialogs() {
		checkSignatures = false;
		custom = new Dialog(DetailedPetetion.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.alert_layout);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		custom.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				getResources().getDimensionPixelSize(
						R.dimen.thanks_dialog_height));

		TextView dialogtext = (TextView) custom.findViewById(R.id.thanks_txt);
		dialogtext.setTypeface(customTypeface);
		String errorText = getResources().getString(
				R.string.eforslag_comment_txt);
		String fmsg = errorText.substring(0, 1).toUpperCase()
				+ errorText.substring(1).toLowerCase();
		dialogtext.setText(fmsg);
		// TextView dialogtextcontent = (TextView)
		// custom.findViewById(R.id.thanks_txt_content);
		// dialogtextcontent.setVisibility(View.VISIBLE);
		// dialogtextcontent.setText(R.string.thank_msg_opinion_content);

		// signCountTv.setText(("Signaturer "+beanObj.getSignatures() + 1) +
		// " h�ller med");
		 Signature_count = beanObj.getSignatures();
		signCountTv.setText("Signaturer " + String.valueOf(Signature_count)
				+ " h�ller med");
		Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
		dialogButton.setTypeface(customTypeface);
		GradientDrawable btnbg = (GradientDrawable) dialogButton
				.getBackground();
		btnbg.setColor(Color.parseColor("#474849"));
		dialogButton.setText(R.string.close);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				custom.dismiss();
				if (signature_success) {
					Intent i = new Intent();
					i.putExtra("count",Signature_count );
					setResult(1000, i);
				}
				finish();
			}
		});
		custom.show();
	}

	boolean signature_success = false;

	@Override
	public void onBackPressed() {		
		if(custom != null && custom.isShowing())
			custom.dismiss();
		if (signature_success) {
			Intent i = new Intent();
			i.putExtra("count",Signature_count );
			setResult(1000, i);
		}
		finish();
		super.onBackPressed();
	}

	@Override
	public void onSuccues(String response, Vector<String> errors) {
		MyDebug.appendLog("Response : "+response);
		if(isRelogin)
		{
			isRelogin = false;
			dismissProgress();
			return;
		}
		if (!DataEngine.isDetailedPetitionsDestroyed) {
			if(checkSignatures)
			{
				if (errors != null) {
					if (errors.size() > 0) {
						if ("NOT_AUTHORIZED".equalsIgnoreCase(errors.get(0))) {
							sessionMgr.logoutUser();
						}

						String msg = EforseLogConstants.getErrorCode(errors
								.get(0));
						String fmsg = msg.substring(0, 1).toUpperCase()
								+ msg.substring(1).toLowerCase();
						showCorrespondingDialogs(fmsg);
						dismissProgress();

						return;
					}
					if (errors.size() == 0) {
						signature_success = true;
						try {
							JSONObject mainObj = new JSONObject(response);
							/*if(mainObj.has(ParseConstants.PETETIONS))
							{
								
								JSONObject petetionObj = mainObj.getJSONObject(ParseConstants.PETETIONS);
								if (petetionObj.has(ParseConstants.SIGNTURES)) {
									beanObj.setSignatures(petetionObj
											.getInt(ParseConstants.SIGNTURES));
								}
							}*/
							if (mainObj.has(ParseConstants.SIGNTURES)) {
								JSONArray petetionArray = mainObj.getJSONArray(ParseConstants.SIGNTURES);
								beanObj.setSignatures(petetionArray.length());
							}
						} catch (JSONException e) {							
							e.printStackTrace();
						}
						showCorrespondingDialogs();
						
					}
				}
				dismissProgress();
				return;
			}
			if (fromLogout) {
				if (errors != null) {
					if (errors.size() > 0) {
						if ("NOT_AUTHORIZED".equalsIgnoreCase(errors.get(0))) {
							sessionMgr.logoutUser();
						}

						String msg = EforseLogConstants.getErrorCode(errors
								.get(0));
						String fmsg = msg.substring(0, 1).toUpperCase()
								+ msg.substring(1).toLowerCase();
						showCorrespondingDialogs(fmsg);
						dismissProgress();

						return;
					}
				}
				dismissProgress();
				/*SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean(EforseLogConstants.LOGIN_STATUS_INFO, false);
				editor.commit();*/
				sessionMgr.logoutUser();
				finish();
			} else {
				if (errors.size() == 0) {
					//signature_success = true;
					//showCorrespondingDialogs();
					getSignatureCount();
					//getPetitionDetails();
					return;
				}
			}
			dismissProgress();
		}

	}

	private void getSignatureCount() {
		checkSignatures = true;
		String getSignaturs = "{ \"app_key\": \"" + EforseLogConstants.APP_KEY
				+ "\",\"petition_id\":\"" + beanObj.getPetition_id() + "\""
				+ "}";
		background = new MyAsyncTask(this, this);
		MyDebug.appendLog("Request Url : "+EforseLogConstants.GET_SIGNATURES+"\n"+getSignaturs);
		background.execute(EforseLogConstants.GET_SIGNATURES, getSignaturs);
	}
    private void getPetitionDetails()
    {
    	checkSignatures = true;
    	String getSignaturs = "{ \"app_key\": \"" + EforseLogConstants.APP_KEY
				+ "\",\"petition_id\":\"" + beanObj.getPetition_id() + "\""
				+ "}";
		background = new MyAsyncTask(this, this);
		background.execute(EforseLogConstants.GET_PETETION, getSignaturs);
    }
	private void dismissProgress() {
		if (progress != null) {
			if (progress.isShowing()) {
				progress.dismiss();
			}
		}
	}

	@Override
	public void onStartExecution() {		
		if (progress != null) {
			if (!progress.isShowing()) {
				progress.show();
			}
		}
	}

	@Override
	public void onClick(View v) {		
		switch (v.getId()) {
		case R.id.logour:
			logoutDialog();
			break;
		case R.id.back_layout:
			if (signature_success) {
				setResult(1000);
			}
			finish();
			break;
		case R.id.eforslag_title:
			if (signature_success) {
				setResult(1000);
			}
			finish();
			break;
		case R.id.info:
			Intent info_activity = new Intent(DetailedPetetion.this,
					InfoActivity.class);
			info_activity.putExtra(DataEngine.INFO_OBJECT,
					EforseLogConstants.INSTRUCTIONS);
			startActivity(info_activity);
			break;
		case R.id.eforslag_share:

			set_dialog();
			break;
		case R.id.twitter_image:
			if (share_dialog.isShowing())
				share_dialog.cancel();
			share_twitter();
			break;
		case R.id.fb_image:
			if (share_dialog.isShowing())
				share_dialog.cancel();
			fb_share();
			break;
		case R.id.signtoregister:
			if (EforseLogConstants.httpClient == null) {
				sessionMgr.logoutUser();
			}
			if (!sessionMgr.isLoggedIn()) {
				startActivityForResult(new Intent(DetailedPetetion.this,
						EforseLogNewActivity.class), LOG_IN);
			} else {
				// startActivity(new Intent(DetailedPetetion.this,
				// WritePetition.class));
				setVisibleCommentViews();

			}
			break;
		case R.id.add_new_proposal:
			if (EforseLogConstants.httpClient == null) {
				sessionMgr.logoutUser();
			}
			if (!sessionMgr.isLoggedIn()) {
				startActivityForResult(new Intent(DetailedPetetion.this,
						EforseLogNewActivity.class), 4000);
			} else {
				startActivityForResult(new Intent(DetailedPetetion.this,
						WritePetition.class), LOG_IN);

			}
			break;
		case R.id.cross_image:
			share_dialog.dismiss();
			break;
		case R.id.cross_dialog:
			share_dialog.dismiss();
			break;

		}
	}

	/**
	 * share on facebook wall
	 */
	private void fb_share() {
		showDialog();
	}

	public void showDialog() {		
		final Dialog sh_dialog = new Dialog(this);
		sh_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		sh_dialog.setContentView(R.layout.alert_layout);
		sh_dialog.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		sh_dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		TextView thanks = (TextView) sh_dialog.findViewById(R.id.thanks_txt);
		thanks.setText("Dela ett e-f�rslag");
		thanks.setTypeface(customTypeface);
		Button thanks_ok = (Button) sh_dialog.findViewById(R.id.thanks_ok);
		GradientDrawable background = (GradientDrawable) thanks_ok
				.getBackground();
		background.setColor(Color.parseColor("#404040"));
		thanks_ok.setTypeface(customTypeface);
		thanks_ok.setText("Okej");
		sh_dialog.show();
		thanks_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {				
				sh_dialog.dismiss();
				postOnMyWall();
			}
		});
		// onto

	}

	/**
	 * post on facebook wall
	 */
	private void postOnMyWall() {	

		try {
			mFacebook = new Facebook(DataEngine.FACEBOOK_APPID);
			mAsyncFacebookRunner = new AsyncFacebookRunner(mFacebook);
			SessionStore.restore(mFacebook, this);
			SessionEvents.addAuthListener(new FacebookAuthListener());
			SessionEvents.addLogoutListener(new FacebookLogoutListener());

			if (mFacebook.isSessionValid()) {
				Bundle params = new Bundle();
				params.putString("message", beanObj.getSummary());
				params.putString("link",
						"http://haninge.demokratiportalen.se/proposal?pid="
								+ beanObj.getPetition_id());
				params.putString("description", beanObj.getSummary());
				params.putString("name", beanObj.getTitle());

				mFacebook.dialog(this, "feed", params, new DialogListener() {

					@Override
					public void onFacebookError(FacebookError e) {
						Log.e("FacebookError","************** the error is = "+e.toString());
					}

					@Override
					public void onError(DialogError e) {
						// TODO Auto-generated method stub
						// showErrorMessage();
						Log.e("Facebookshare","onError ************ the value is "+e.toString());
					}

					@Override
					public void onComplete(Bundle values) {
						if(!values.isEmpty())
						handler.post(success_runnable);
					}
                    
					@Override
					public void onCancel() {
						// TODO Auto-generated method stub
						// showErrorMessage();
						Log.e("Facebookshare","onCancel *************** = ");
					}
				});

			} else {
				// no logged in, so relogin
				mFacebook.authorize(DetailedPetetion.this,
						Util.FACEBOOK_PERMISSIONS, Facebook.FORCE_DIALOG_AUTH,
						new FacebookLoginDialogListener());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * show dialog when facebook delivary failed
	 */
	Runnable success_runnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			showFbDeliveryFailed();
		}
	};

	/**
	 * show alert dialog
	 */
	private void showFbDeliveryFailed() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Du har nu delat detta p� Facebook");
		builder.setTitle("Klart!");

		builder.setNegativeButton("Okej",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

						dialog.dismiss();
						try {
							if (progress.isShowing())
								progress.dismiss();
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
				});

		builder.show();

	}

	/**
	 * show dialog for showin share options facebook/twitter
	 */
	private void set_dialog() {

		share_dialog = new Dialog(this);
		share_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		share_dialog.setContentView(R.layout.share_alertlay);
		share_dialog.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		share_dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		TextView share_txt = (TextView) share_dialog
				.findViewById(R.id.share_txt);
		share_txt.setTypeface(DataEngine.gettypeface(DetailedPetetion.this));
		share_txt.setText("Dela e-f�rslag");
		LinearLayout cross_lay = (LinearLayout) share_dialog
				.findViewById(R.id.cross_dialog);
		ImageView twitter = (ImageView) share_dialog
				.findViewById(R.id.twitter_image);
		ImageView facebook = (ImageView) share_dialog
				.findViewById(R.id.fb_image);
		ImageView crossImage = (ImageView) share_dialog
				.findViewById(R.id.cross_image);
		TextView shareTxt = (TextView) share_dialog
				.findViewById(R.id.share_txt);
		shareTxt.setTypeface(customTypeface);

		share_dialog.show();
		crossImage.setOnClickListener(this);
		twitter.setOnClickListener(this);
		facebook.setOnClickListener(this);
		cross_lay.setOnClickListener(this);

	}

	private String string_msg = null;
	File casted_image;
	String sharemsg_twitter = null;

	/**
	 * Share messages on Twitter
	 */
	private void share_twitter() {
		Twitt_Sharing twitt = new Twitt_Sharing(DetailedPetetion.this,
				DataEngine.CONSUMER_KEY, DataEngine.SECRET_KEY);
		String link = "http://haninge.demokratiportalen.se/proposal?pid="
				+ beanObj.getPetition_id();
		sharemsg_twitter = beanObj.getTitle() + "\n" + link;
		string_msg = sharemsg_twitter;

		String_to_File();

		twitt.shareToTwitter(string_msg, casted_image);

	}

	/**
	 * convert drawable of resource to file
	 * 
	 * @return file
	 */
	public File String_to_File() {

		try {
			File rootSdDirectory = Environment.getExternalStorageDirectory();

			casted_image = new File(rootSdDirectory, "attachment.jpg");
			if (casted_image.exists()) {
				casted_image.delete();
			}
			casted_image.createNewFile();

			FileOutputStream fos = new FileOutputStream(casted_image);
			try {

				BitmapFactory.Options options = new BitmapFactory.Options();

				options.inSampleSize = 2;

				BufferedOutputStream bos = new BufferedOutputStream(fos);
				// mBitmap = Bitmap.createBitmap(200, 200,
				// Bitmap.Config.RGB_565);
				Bitmap icon = BitmapFactory.decodeResource(
						DetailedPetetion.this.getResources(),
						R.drawable.speakapplogo);
				icon.compress(CompressFormat.JPEG, 100, bos);

				bos.flush();

				bos.close();

			} catch (Exception e) {
				casted_image = null;
				//return null;
			}

			return casted_image;

		} catch (Exception e) {
            casted_image = null;
			System.out.print(e);
			// e.printStackTrace();

		}
		return casted_image;

	}

	ScrollView sv;

	private void setVisibleCommentViews() {

		agreeTerms.setVisibility(View.VISIBLE);
		signPetion.setVisibility(View.VISIBLE);
		commentLayout.setVisibility(View.VISIBLE);
		// commentView.requestFocus();
		commentView.setText("");
		// sv.scrollTo(0, sv.getBottom());
		sv.fullScroll(ScrollView.FOCUS_UP);
	}

	private void setInvisibleCommentViews() {
		agreeTerms.setVisibility(View.GONE);
		signPetion.setVisibility(View.GONE);
		commentLayout.setVisibility(View.GONE);
	}

	private void setLogoutButtonVisibility() {
		if (EforseLogConstants.httpClient == null) {
			sessionMgr.logoutUser();
		}
		if (!sessionMgr.isLoggedIn()) {
			logout.setVisibility(View.GONE);
		} else {

			logout.setVisibility(View.VISIBLE);
		}
	}

	private void logoutDialog() {

		final Dialog dialog = new Dialog(DetailedPetetion.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.call_alertdialog);
		dialog.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setCanceledOnTouchOutside(true);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.number);
		text.setText(R.string.eforslag_logout);
		text.setTypeface(customTypeface);
		Button dialogButton = (Button) dialog.findViewById(R.id.ring);
		dialogButton.setTypeface(customTypeface);
		dialogButton.setText("Ja");
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				logout();
				dialog.dismiss();
			}
		});
		Button cancelButton = (Button) dialog.findViewById(R.id.cancel);
		cancelButton.setTypeface(customTypeface);
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});

		dialog.show();

	}

	boolean fromLogout = false;
	
	boolean checkSignatures = false;

	/**
	 * logout for eforslog user.
	 */
	public void logout() {
		fromLogout = true;
		String getLatestPetetions = "{ \"app_key\": \"ALGYENVHQMXPGJSTFMC\" }";
		// Log.i(TAG, getLatestPetetions);
		background = new MyAsyncTask(DetailedPetetion.this,
				DetailedPetetion.this);
		background.execute(EforseLogConstants.LOGOUT_URL, getLatestPetetions);
	}

	/**
	 * show the Dialog based on error
	 * 
	 * @param error
	 */
	private void showCorrespondingDialogs(final String error) {
		final Dialog custom = new Dialog(DetailedPetetion.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.signtoregister_alert);
		custom.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextView dialogtext = (TextView) custom.findViewById(R.id.thanks_txt);
		dialogtext.setTypeface(DataEngine.gettypeface(DetailedPetetion.this));
		dialogtext.setText(error);

		Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
		GradientDrawable btnbg = (GradientDrawable) dialogButton
				.getBackground();
		btnbg.setColor(Color.parseColor("#474849"));
		dialogButton.setTypeface(customTypeface);
		dialogButton.setText("Okej");
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if ("AUTENTISERINGEN INTE GODK�ND".equalsIgnoreCase(error)) {
					startActivityForResult(new Intent(DetailedPetetion.this,
							EforseLogNewActivity.class), LOG_IN);// EforslagLoginRegisterview
				}
				custom.dismiss();
			}
		});
		custom.show();
	}

	public void showToast(String msg) {
		String fmsg = msg.substring(0, 1).toUpperCase()
				+ msg.substring(1).toLowerCase();
		MethodsHelper.showToast(fmsg, this);
	}

}
