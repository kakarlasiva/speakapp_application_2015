package com.androidapps.speakapp.eforslag;

import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import android.util.Log;
 
public class EforseLogConstants {
	
	public static final String APP_KEY = "ALGYENVHQMXPGJSTFMC";
	//public static final String SERVER_URL ="http://democracyportal.dev.imcode.com/api/v0/";
	public static final String SERVER_URL="http://haninge.demokratiportalen.se/api/v0/";	
	public static final String LOGIN_URL =  SERVER_URL+"login"; 
	public static final String LOGOUT_URL =  SERVER_URL+"logout";
	public static final String GET_LATEST_PETETIONS =SERVER_URL+"get_latest_petitions";
	public static final String GET_INSTRUCTION_URL =SERVER_URL+"get_instructions";
	
	public static final String GET_SIGNED_PETETIONS =SERVER_URL+"get_signed_petitions";
	public static final String GET_PETETION = SERVER_URL+"get_petition"; 
	public static final String TO_SIGN_PETETION =SERVER_URL+"sign_petition";
	public static final String GET_SIGNATURES =SERVER_URL+"get_signatures"; 
	
	public static final String PASSWORD_REMINDER = SERVER_URL +"password_reminder"; 
	public static final String SAVE_PETETION = SERVER_URL +"save_petition";  
	protected static final String REGISTER_URL =  SERVER_URL +"register_user";
	public static final String CLASS_CATEGORY = "21";//Dev //"speakapp"
  //public static final String CLASS_CATEGORY = "17"; //Live 

	 public static final String SAVE_CHECK_PETETION = SERVER_URL
			   + "save_and_check_petition";
			 public static final String REGISTER_PETETION = SERVER_URL
			   + "register_petition";
	
	//Error Codes...
	public static final	String INCORRECT_FORMAT="INCORRECT_FORMAT";//(used by all functions) (if the function call does not have understandable json format)
	public static final String INVALID_APP_KEY="INVALID_APP_KEY"; // (used by all functions) 
	public static final String INVALID_PARAMETER="INVALID_PARAMETER"; // (used by all functions) 
	public static final String LOGIN_FAILED="LOGIN_FAILED";//(used by login)
	public static final String LOGOUT_FAILED="LOGOUT_FAILED";//(used by logout) 
	public static final String UNKNOWN_FUNCTION="UNKNOWN_FUNCTION"; 
	public static final String UNKNOWN_ERROR="UNKNOWN_ERROR";
	public static final String NOT_AUTHORIZED="NOT_AUTHORIZED";// (used by save_user)
	public static final String USERNAME_FORBIDDEN="USERNAME_FORBIDDEN";// (used by register_user and save_user) (there is a list in the systemsettings for names that are not allowed)
	public static final String NAME_FORBIDDEN = "NAME_FORBIDDEN";//(used by register_user and save_user) 
	public static final String EMAIL_IN_USE="EMAIL_IN_USE";//(used by register_user and save_user) 
	public static final String USER_EXISTS="USER_EXISTS";//(used by register_user) 
	public static final String SSN_IN_USE="SSN_IN_USE";// (used by register_user and save_user) 
	public static final String PETITION_DOES_NOT_EXIST="PETITION_DOES_NOT_EXIST";// (used by sign_petition, get_petition, subscribe, get_signatures, save_and_check_petition, register_petition and withdraw_petition)
	public static final String INVALID_TITLE="INVALID_TITLE";// (used by save_and_check_petition, register_petition)
	public static final String INVALID_SUMMARY="INVALID_SUMMARY";//(used by save_and_check_petition, register_petition)
	public static final String INVALID_DESCRIPTION="INVALID_DESCRIPTION";//(used by save_and_check_petition, register_petition)
	public static final String INVALID_STARTDATE="INVALID_STARTDATE";//(used by save_and_check_petition, register_petition)
	public static final String INVALID_ENDDATE="INVALID_ENDDATE";//(used by save_and_check_petition, register_petition)
	public static final String INVALID_LINKS="INVALID_LINKS";//(used by save_and_check_petition, register_petition)
	public static final String INVALID_SHORTURL="INVALID_SHORTURL";// (used by save_petition, save_and_check_petition, register_petition)
	public static final String SHORT_URL_ALREADY_USED="SHORT_URL_ALREADY_USED";//(used by save_petition, save_and_check_petition, register_petition)
	public static final String PETITION_LOCKED="PETITION_LOCKED";//(used by save_petition, save_and_check_petition, register_petition) (you cant edit a registered petition, unless you withdraw it first)
	public static final String TERMS_NOT_ACCEPTED="TERMS_NOT_ACCEPTED";// (used by save_and_check_petition)
	public static final String USER_DOES_NOT_EXIST="USER_DOES_NOT_EXIST";//(used by password_reminder)	
	public static final String LOGIN_DETAILS="login_details";
	public static final String LOGIN_STATUS_INFO="login";	
	public static final int LOGIN_SUCCESS=5000;
	public static final int REGISTRATION_SUCCESS=6000;
	
	public static  String INSTRUCTIONS = "S� h�r fungerar det"+"\n\n"+"Vem som helst kan l�mna eller skriva under ett e-f�rslag. Det spelar ingen roll var du bor eller hur gammal du �r. Alla �r v�lkomna att f�resl� f�rb�ttringar i Haninge och s�ga sin mening. "+"\n"+"F�r att l�mna ett e-f�rslag eller skriva under ett e-f�rslag m�ste du f�rst skaffa ett konto h�r i appen."+"\n\n"+"Vem  kan se mitt e-f�rslag?"+"\n\n"+"N�r du har l�mnat ett e-f�rslag �r det bara kommunens moderator som kan se e-f�rslaget. Moderatorn granskar e-f�rslaget och kontaktar dig om e-f�rslaget beh�ver kompletteras med mer information eller om det redan finns ett liknande e-f�rlag. Moderatorn publicerar sedan e-f�rslaget s� att det blir synligt f�r alla."+"\n\n"+"Vad h�nder med e-f�rslaget?"+"\n\n"+"Om ditt e-f�rslag har f�tt minst 25 underskrifter n�r slutdatumet f�r e-f�rslag har n�tts, l�mnas det �ver till politikerna i den n�mnd som ansvarar f�r de fr�gor som e-f�rslaget ber�r. Politikerna beslutar om och hur e-f�rslaget ska genomf�ras.";
	
	public static Map<String, String> errorcode = new HashMap<String, String>();

	public static HttpClient httpClient;

	public static HttpClient getHttpClient() {
		if (null == httpClient) {
			httpClient = new DefaultHttpClient();
		}
		return httpClient;
	}

	public static String getErrorCode(String error) {
		String data = "SPEAK APP KAN INTE KOPPLA UPP MOT SERVERN JUST NU";
		if (null == errorcode) {
			errordata();
		}
		try {
			if (errorcode.containsKey(error)) {
				Log.i("CONSTANT","DATA....."+errorcode.get(error.trim()));
				return errorcode.get(error.trim());
			} else {
				return data;
			}
		} catch (Exception e) {
			return data;
		}
	}

	public static void errordata() {		
		if(null==errorcode){
			errorcode=new HashMap<String, String>();
		}
		errorcode.put("LOGIN_FAILED", "INLOGGNINGEN MISSLYCKADES");
		errorcode.put("LOGOUT_FAILED", "UTLOGGNINGEN MISSLYCKADES");
		errorcode.put("NOT_AUTHORIZED", "AUTENTISERINGEN INTE GODK�ND");
		errorcode.put("USERNAME_FORBIDDEN","ANV�NDARNAMNET �R UPPTAGET");
		errorcode.put("NAME_FORBIDDEN", "NAMNET �R INTE TILL�TET");
		errorcode.put("EMAIL_IN_USE", "E-POSTADRESSEN FINNS REDAN");
		errorcode.put("USER_EXISTS", "ANV�NDAREN FINNS REDAN");
		errorcode.put("PETITION_DOES_NOT_EXIST", "F�RSLAGET FINNS INTE");
		errorcode.put("INVALID_TITLE", "OGILTIG RUBRIK");
		errorcode.put("INVALID_SUMMARY", "OGILTIG SAMMANFATTNING");
		errorcode.put("INVALID_DESCRIPTION", "OGILTIG BESKRIVNING");
		errorcode.put("INVALID_STARTDATE", "OGILTIGT STARTDATUM");
		errorcode.put("INVALID_ENDDATE", "OGILTIGT SLUTDATUM");
		errorcode.put("INVALID_LINKS", "OGILTIG L�NK");
		errorcode.put("INVALID_SHORTURL", "OGILTIG URL");
		errorcode.put("TERMS_NOT_ACCEPTED", "VILLKOREN M�STE GODK�NNAS");
		errorcode.put("USER_DOES_NOT_EXIST", "ANV�NDAREN FINNS INTE");
		errorcode.put("An Error Occured", "ETT FEL HAR UPPST�TT");
		errorcode.put("Alert", "VARNING");
		errorcode.put("Couldn't reach server at this time", "SPEAK APP KAN INTE KOPPLA UPP MOT SERVERN JUST NU");
		errorcode.put("Please enter Username", " FYLL I ANV�NDARNAMN");
		errorcode.put("Please enter Password", "FYLL I L�SENORD");
		errorcode.put("SERVER_ERROR", "SPEAK APP KAN INTE KOPPLA UPP MOT SERVERN JUST NU");

	}

}  