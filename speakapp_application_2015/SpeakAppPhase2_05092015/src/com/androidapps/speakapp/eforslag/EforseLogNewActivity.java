package com.androidapps.speakapp.eforslag;

import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.log.MyDebug;
import com.androidapps.speakapp.screens.InfoActivity;
import com.androidapps.speakapp.util.MethodsHelper;

public class EforseLogNewActivity extends Activity implements APIRespone,
		OnClickListener, OnTouchListener, OnKeyListener {
	boolean isLogin = true;
	boolean isResgisterLogin = false;
	boolean isFromLoguot = false;
  
	Button loginViewButton, registerViewButton;
	// RegisterView
	LinearLayout registerview, loginview;
	Button senddata;
	EditText username, password, repeat_password, eAddress, firstaname,
			lastname;
	
	ProgressDialog progress;
	private SharedPreferences settings;
	ImageView cross_image, info_button;
	Dialog custom;
	CheckBox check_agree;
	LinearLayout back;
	final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	// LoginViews
	EditText loginUsername, loginPassword;
	Button login;
	TextView forgotPwd;
	Typeface customTypeface;
	TextView title_txt;
	SessionManager sessionMgr;
	String userName,passWord;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.eforselag_new);
		//settings = getSharedPreferences(EforseLogConstants.LOGIN_DETAILS,
			//	MODE_PRIVATE);
		sessionMgr = new SessionManager(getApplicationContext());
		inputManager = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);		
		customTypeface = DataEngine.gettypeface(this);
		title_txt = (TextView) findViewById(R.id.eforslag_title);
		title_txt.setTypeface(DataEngine.gettypeface(this));
		registerview = (LinearLayout) findViewById(R.id.register);
		loginview = (LinearLayout) findViewById(R.id.login);
		loginViewButton = (Button) findViewById(R.id.loginViewButton);
		registerViewButton = (Button) findViewById(R.id.registerViewButton);
		loginViewButton.setTypeface(customTypeface);
		registerViewButton.setTypeface(customTypeface);
		back = (LinearLayout) findViewById(R.id.back_layout);
		back.setOnClickListener(this);
		loginViewButton.setOnClickListener(this);
		registerViewButton.setOnClickListener(this);
		title_txt.setOnClickListener(this);
		initRegisterViews();
		initLoginViews();
		setCurrentView(isLogin);

	}

	@Override
	protected void onResume() {
		super.onResume();
		DataEngine.isLoginPetitionsDestroyed = false;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		DataEngine.isLoginPetitionsDestroyed = true;
	}

	private void setCurrentView(boolean isLogin) {
		if (isLogin) {
			loginview.setVisibility(View.VISIBLE);
			registerview.setVisibility(View.GONE);
			loginViewButton.setBackground(getResources().getDrawable(
					R.drawable.logintab_selected));
			registerViewButton.setBackground(getResources().getDrawable(
					R.drawable.logintab));

		} else {
			loginview.setVisibility(View.GONE);
			registerview.setVisibility(View.VISIBLE);
			loginViewButton.setBackground(getResources().getDrawable(
					R.drawable.logintab));
			registerViewButton.setBackground(getResources().getDrawable(
					R.drawable.logintab_selected));

		}
		loginViewButton.setPadding(20, 0, 20, 0);
		registerViewButton.setPadding(20, 0, 20, 0);
	}

	private void initLoginViews() {

		loginUsername = (EditText) findViewById(R.id.loginusername);
		loginPassword = (EditText) findViewById(R.id.loginpassword);
		loginUsername.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {				
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						loginPassword.requestFocus();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});
		loginPassword
				.setOnEditorActionListener(new EditText.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_DONE
								|| event.getAction() == KeyEvent.ACTION_DOWN
								&& event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

							inputManager.hideSoftInputFromWindow(
									loginPassword.getWindowToken(), 0);
							sv.scrollTo(0, sv.getBottom());
							return true;
						}
						return false;
					}
				});
		forgotPwd = (TextView) findViewById(R.id.forgotPwd);
		login = (Button) findViewById(R.id.loginbtn);
		GradientDrawable btnbg = (GradientDrawable) login.getBackground();
		btnbg.setColor(Color.parseColor("#000000"));
		btnbg.setStroke(0, Color.parseColor("#00000000"));
		forgotPwd.setTypeface(customTypeface);
		login.setTypeface(customTypeface);
		// TextView loginMandatoryTxt = (TextView)
		// findViewById(R.id.loginMandatory);
		// loginMandatoryTxt.setTypeface(customTypeface);
		EditText[] totalEditText = { loginPassword, loginUsername };
		setTypeface(totalEditText);
		// loginUsername.setOnTouchListener(this);
		// loginPassword.setOnTouchListener(this);
		login.setOnClickListener(this);
		forgotPwd.setOnClickListener(this);

	}

	ScrollView sv;
	InputMethodManager inputManager;

	private void initRegisterViews() {

		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));
		sv = (ScrollView) findViewById(R.id.register_scroll);
		registerview = (LinearLayout) findViewById(R.id.register);
		info_button = (ImageView) findViewById(R.id.registerinfo);
		loginview = (LinearLayout) findViewById(R.id.login);
		registerview.setVisibility(View.VISIBLE);
		loginview.setVisibility(View.GONE);
		senddata = (Button) findViewById(R.id.senddata);
		GradientDrawable backgrounddesign = (GradientDrawable) senddata
				.getBackground();
		backgrounddesign.setColor(Color.parseColor("#000000"));
		backgrounddesign.setStroke(0, Color.parseColor("#00000000"));
		TextView textView2 = (TextView) findViewById(R.id.textView2);
		textView2.setTypeface(customTypeface);
		// TextView mandatoryTxt = (TextView) findViewById(R.id.mandatorytxt);
		// mandatoryTxt.setTypeface(customTypeface);
		info_button.setOnClickListener(this);
		senddata.setOnClickListener(this);
		senddata.setTypeface(customTypeface);
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);
		repeat_password = (EditText) findViewById(R.id.repeatpassword);
		eAddress = (EditText) findViewById(R.id.epostaddress);
		firstaname = (EditText) findViewById(R.id.firstaname);
		lastname = (EditText) findViewById(R.id.lastname);
		lastname.setOnEditorActionListener(new EditText.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE
						|| event.getAction() == KeyEvent.ACTION_DOWN
						&& event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

					// Show soft-keyboard:
					// getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
					// hide keyboard :
					// getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
					inputManager.hideSoftInputFromWindow(
							lastname.getWindowToken(), 0);
					sv.scrollTo(0, sv.getBottom());
					return true;
				}
				return false;
			}
		});
		check_agree = (CheckBox) findViewById(R.id.check_agree);
		if (MethodsHelper.getDensityName(this))
			check_agree.setPadding(
					(int) (check_agree.getPaddingLeft() + getResources()
							.getDimension(R.dimen.chkbox_padding)), check_agree
							.getPaddingTop(), check_agree.getPaddingRight(),
					check_agree.getPaddingBottom());
		check_agree.setTypeface(customTypeface);
		EditText[] totalEditText = { firstaname, lastname, password,
				repeat_password, eAddress, username };
		setTypeface(totalEditText);
		set_list();

	}

	private void set_list() {
		username.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						password.requestFocus();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});
		password.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						repeat_password.requestFocus();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});
		repeat_password.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						eAddress.requestFocus();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});
		eAddress.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						firstaname.requestFocus();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});
		firstaname.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						lastname.requestFocus();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});

	}

	private void setTypeface(EditText[] editTexts) {
		for (EditText editText : editTexts) {
			editText.setTypeface(DataEngine.gettypeface(this));
			GradientDrawable backgrounddesign = (GradientDrawable) editText
					.getBackground();
			backgrounddesign.setColor(Color.parseColor("#ffffff"));
			backgrounddesign.setStroke(2, Color.parseColor("#000000"));
			// editText.setOnTouchListener(this);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.senddata:
			sendRegisterPageData();
			break;
		case R.id.back_layout:
			finish();
			break;
		case R.id.eforslag_title:
			finish();
			break;
		case R.id.registerinfo:
			Intent info_activity = new Intent(this, InfoActivity.class);
			info_activity.putExtra(DataEngine.INFO_OBJECT,
					EforseLogConstants.INSTRUCTIONS);
			startActivity(info_activity);

			break;
		case R.id.loginbtn:
			sendLoginData();
			break;
		case R.id.forgotPwd:
			forgotPwd();
			break;
		case R.id.loginViewButton:
			isLogin = true;
			setCurrentView(isLogin);
			break;
		case R.id.registerViewButton:

			isLogin = false;
			setCurrentView(isLogin);
			break;

		}

	}

	private void sendLoginData() {
		if (loginValidate()) {
			userName = loginUsername.getText().toString();
			passWord = loginPassword.getEditableText().toString();
			String LoginUser = "{ \"app_key\": \"" + EforseLogConstants.APP_KEY
					+ "\",\"login_name\":\""
					+ userName + "\""
					+ ",\"password\" : " + "\""
					+ passWord + "\"}";
			//System.out.println(LoginUser);
			MyAsyncTask background;
			background = new MyAsyncTask(EforseLogNewActivity.this,
					EforseLogNewActivity.this);
			MyDebug.appendLog("Request Url : "+EforseLogConstants.LOGIN_URL +"\n"+LoginUser);
			background.execute(EforseLogConstants.LOGIN_URL, LoginUser);
		}

	}
	private void logout()
	{
		String getLatestPetetions = "{ \"app_key\": \"ALGYENVHQMXPGJSTFMC\" }";
		//Log.i("EforslagNew", getLatestPetetions);
		MyAsyncTask background;
		background = new MyAsyncTask(EforseLogNewActivity.this, EforseLogNewActivity.this);
		background.execute(EforseLogConstants.LOGOUT_URL, getLatestPetetions);
	}
    private void senRegisterLoginData()
    {
    	MyAsyncTask background;
    	isResgisterLogin = true;
    	//userName = username.getText().toString();
    	//passWord = password.getEditableText().toString();
    	String LoginUser = "{ \"app_key\": \"" + EforseLogConstants.APP_KEY
				+ "\",\"login_name\":\""
				+ userName + "\""
				+ ",\"password\" : " + "\""
				+ passWord + "\"}";
		//System.out.println(LoginUser);
		background = new MyAsyncTask(EforseLogNewActivity.this,
				EforseLogNewActivity.this);
		background.execute(EforseLogConstants.LOGIN_URL, LoginUser);
    }
	String email = "";
	boolean fromForgotPwd = false;

	/**
	 * shows the dialog when user forgot the password
	 */
	private void forgotPwd() {		
		final Dialog custom = new Dialog(EforseLogNewActivity.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.forgotpwd);
		custom.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);

		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		custom.setCancelable(true);
		TextView dialogtext = (TextView) custom
				.findViewById(R.id.thanks_txt_forgot);
		custom.setCanceledOnTouchOutside(true);
		dialogtext.setTypeface(customTypeface);
		Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
		GradientDrawable btnbg = (GradientDrawable) dialogButton
				.getBackground();
		btnbg.setColor(Color.parseColor("#474849"));
		dialogButton.setText("Okej");
		dialogButton.setTypeface(customTypeface);
		final EditText editBox = (EditText) custom.findViewById(R.id.editText1);
		GradientDrawable btnbgedit = (GradientDrawable) editBox.getBackground();
		btnbgedit.setColor(Color.parseColor("#ffffff"));
		btnbgedit.setStroke(2, Color.parseColor("#000000"));
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				email = editBox.getText().toString();
				if (null != email.trim()) {
					if (email.length() <= 0) {
						showToast("FYLL I e-post");
						return;
					}
					if (!isValidEmail(email.trim())) {
						showToast("ogiltig e-post");
						
						return;
					}
				}

				fromForgotPwd = true;
				String forgotPwd = "{ \"app_key\": \""
						+ EforseLogConstants.APP_KEY + "\",\"email\":\""
						+ email + "\"}";
				//System.out.println(forgotPwd);
				MyAsyncTask background;
				background = new MyAsyncTask(EforseLogNewActivity.this,
						EforseLogNewActivity.this);
				background.execute(EforseLogConstants.PASSWORD_REMINDER,
						forgotPwd);
				custom.dismiss();

			}
		});
		custom.show();

	}

	private void sendRegisterPageData() {
		if (validate()) {
			
			userName = username.getText().toString();
			passWord = password.getEditableText().toString();
			String registerUser = "{ \"app_key\": \""
					+ EforseLogConstants.APP_KEY + "\",\"login_name\":\""
					+ userName + "\""
					+ ",\"first_name\" : " + "\""
					+ firstaname.getEditableText().toString() + "\""
					+ ",\"last_name\" : " + "\""
					+ lastname.getEditableText().toString() + "\""
					+ ",\"password\" : " + "\""
					+ passWord + "\""
					+ ",\"ssn\" : " + "\"" + "" + "\"" + ",\"city\" : " + "\""
					+ "" + "\"" + ",\"address\" : " + "\"" + "" + "\""
					+ ",\"zip\" : " + "\"" + "" + "\"" + ",\"location\" : "
					+ "\"" + "" + "\"" + ",\"email\" : " + "\""
					+ eAddress.getEditableText().toString() + "\""
					+ ",\"mobile\" : " + "\"" + "" + "\"" + ",\"tel\" : "
					+ "\"" + "" + "\"}";
		//	System.out.println(registerUser);
			MyAsyncTask background;
			background = new MyAsyncTask(EforseLogNewActivity.this,
					EforseLogNewActivity.this);
			MyDebug.appendLog("Request Url : "+EforseLogConstants.REGISTER_URL +"\n"+registerUser+"\n");
			background.execute(EforseLogConstants.REGISTER_URL, registerUser);
		}
	}

	/***
	 * validate the email address
	 * 
	 * @param email
	 * @return true - valid , false - invalid
	 */
	private boolean isValidEmail(String email) {
		// TODO Auto-generated method stub
		//System.out.println("The email provided here is : " + email);
		if (email != null && !"".equalsIgnoreCase(email)) {
			// check and return true if it's ok..
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile(EMAIL_PATTERN);
			matcher = pattern.matcher(email);
			return matcher.matches();
		} else {
			return true;
		}
	}

	/**
	 * show Toast with given message
	 * 
	 * @param msg
	 */
	public void showToast(String msg) {
		String fmsg = msg.substring(0, 1).toUpperCase()
				+ msg.substring(1).toLowerCase();
		// Toast.makeText(this, fmsg, Toast.LENGTH_SHORT).show();
		LayoutInflater inflater = getLayoutInflater();
		View layout = inflater.inflate(R.layout.toast_layout, null);

		TextView text = (TextView) layout.findViewById(R.id.text);
		text.setTypeface(customTypeface);
		text.setText(fmsg);

		Toast toast = new Toast(getApplicationContext());
		toast.setGravity(
				Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL,
				0,
				getResources().getDimensionPixelOffset(
						R.dimen.padding_frm_bottom));
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(layout);
		toast.show();
	}

	/**
	 * validate the username and password
	 * 
	 * @return true both are valid , false ---> both are invalid or any one of
	 *         the invalid
	 */
	protected boolean loginValidate() {
		// TODO Auto-generated method stub
		if (("".equalsIgnoreCase(loginUsername.getText().toString()))
				|| (null == loginUsername.getText().toString())) {
			loginUsername.requestFocus();
			showToast("FYLL I ANV�NDARNAMN");
			return false;
		}
		if ("".equalsIgnoreCase(loginPassword.getText().toString())
				|| (null == loginPassword.getText().toString())) {
			loginPassword.requestFocus();
			showToast("FYLL I L�SENORD");
			return false;
		}

		return true;
	}

	/**
	 * validate the username, eaddress, first name,last name, repeat password,
	 * password and t&c checkbox
	 * 
	 * @return true both are valid , false- all are invalid/ any one of the
	 *         invalid
	 */
	public boolean validate() {
		if (username.getText().toString() == null
				|| "".equalsIgnoreCase(username.getText().toString())) {
			username.requestFocus();
			showToast("FYLL I ANV�NDARNAMN");
			return false;
		}
		if (password.getText().toString() == null
				|| password.getText().toString().trim().length()<4) {
			password.requestFocus();
			showToast("L�senord m�ste vara minst 4 tecken");
			return false;
		}
		if (password.getText().toString() == null
				|| "".equalsIgnoreCase(password.getText().toString())) {
			password.requestFocus();
			showToast("FYLL I L�SENORD");
			return false;
		}
		if (!repeat_password.getText().toString()
				.equals(password.getText().toString())) {
			repeat_password.requestFocus();
			showToast("L�SENORD matchar inte");
			return false;
		}
		if (eAddress.getText().toString() == null
				|| "".equalsIgnoreCase(eAddress.getText().toString())) {
			eAddress.requestFocus();
			showToast("FYLL I E-POSTADRESS");
			return false;
		} else {
			if (!isValidEmail(eAddress.getText().toString().trim())) {
				showToast("Ogiltig e-post");
				return false;
			}
		}

		if (firstaname.getText().toString() == null
				|| "".equalsIgnoreCase(firstaname.getText().toString())) {
			firstaname.requestFocus();
			showToast("FYLL I F�RNAMN");
			return false;
		}
		if (lastname.getText().toString() == null
				|| "".equalsIgnoreCase(lastname.getText().toString())) {
			showToast("FYLL I EFTERNAMN");
			lastname.requestFocus();
			return false;
		}

		if (!check_agree.isChecked()) {
			showToast(getResources().getString(
					R.string.toast_for_checkbox));// VILLKOREN M�STE
															// GODK�NNAS
			return false;
		}

		return true;

	}

	/**
	 * show the dialog based provided error
	 * 
	 * @param error
	 */
	private void showCorrespondingDialogs(String error) {
		String errMsg = error.substring(0, 1).toUpperCase()
				+ error.substring(1).toLowerCase();
		final Dialog custom = new Dialog(EforseLogNewActivity.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.signtoregister_alert);

		custom.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);

		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextView dialogtext = (TextView) custom.findViewById(R.id.thanks_txt);
		dialogtext.setTypeface(customTypeface);

		dialogtext.setText(errMsg);

		Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
		GradientDrawable btnbg = (GradientDrawable) dialogButton
				.getBackground();
		dialogButton.setTypeface(customTypeface);
		btnbg.setColor(Color.parseColor("#474849"));
		dialogButton.setText("Okej");

		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				custom.dismiss();

			}
		});
		custom.show();
	}

	@Override
	public void onSuccues(String response, Vector<String> errors) {
		MyDebug.appendLog("Response : "+response+"\n");
		if (!DataEngine.isLoginPetitionsDestroyed) {
			if (isLogin) {
				if (errors.size() > 0) {
					String msg = EforseLogConstants.getErrorCode(errors.get(0));
					String fmsg = msg.substring(0, 1).toUpperCase()
							+ msg.substring(1).toLowerCase();
					showCorrespondingDialogs(fmsg);
					username.setText("");
					password.setText("");
				} else {
					if (!fromForgotPwd) {
						/*SharedPreferences.Editor editor = settings.edit();
						editor.putBoolean(EforseLogConstants.LOGIN_STATUS_INFO,
								true);
						editor.commit();*/
						sessionMgr.createLoginSession(userName,passWord);
						Intent intent = new Intent();
						intent.putExtra("RESULT",
								EforseLogConstants.LOGIN_SUCCESS);
						if (null != getParent()) {
							getParent().setResult(RESULT_OK, intent);
						} else {
							setResult(RESULT_OK, intent);
						}
						// showToast("Tack f�r din signatur");
						finish();
					} else {
						fromForgotPwd = false;
					}
				}

			}else if(isResgisterLogin)
			{
				isResgisterLogin = false;
				/*SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean(EforseLogConstants.LOGIN_STATUS_INFO,
						true);
				editor.commit();*/
				if (errors.size() > 0) {
					showCorrespondingDialogs(EforseLogConstants
							.getErrorCode(errors.get(0)));
					
					if(progress != null && progress.isShowing())
					{
						progress.dismiss();
					}
					return;
				}
				sessionMgr.createLoginSession(userName,passWord);
				showToast("Registrerade framg�ngsrikt");
				setDefaultRegisterHintLables();
				
				 Intent intent = new Intent(); intent.putExtra("RESULT",
				  EforseLogConstants.LOGIN_SUCCESS);
				// getParent().setResult(RESULT_OK, intent);
				// setResult(EforseLogConstants.REGISTRATION_SUCCESS);
				 if (null != getParent()) {
					 Log.e("DetailedPetition","-----not null---");
						getParent().setResult(RESULT_OK, intent);
					} else {
						setResult(EforseLogConstants.REGISTRATION_SUCCESS);
					}					 
				finish();
			}
			else {
				if (errors.size() > 0) {
					showCorrespondingDialogs(EforseLogConstants
							.getErrorCode(errors.get(0)));
				} else {
					senRegisterLoginData();
					//setRegisterSuccess();
				}
			}
			if (progress != null) {
				if (progress.isShowing()) {
					progress.dismiss();
				}
			}
		}
	}	
	private void setRegisterSuccess()
	{
		//sessionMgr.createLoginSession();
		showToast("Registrerade framg�ngsrikt");
		setDefaultRegisterHintLables();
		
		 Intent intent = new Intent(); intent.putExtra("RESULT",
		  EforseLogConstants.LOGIN_SUCCESS);
		// getParent().setResult(RESULT_OK, intent);
		// setResult(EforseLogConstants.REGISTRATION_SUCCESS);
		 if (null != getParent()) {
				getParent().setResult(RESULT_OK, intent);
			} else {
				setResult(EforseLogConstants.REGISTRATION_SUCCESS);
			}					 
		finish();
	}

	@Override
	public void onStartExecution() {
		if (progress != null) {
			if (!progress.isShowing()) {
				progress.show();
			}
		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (v.getId()) {
		case R.id.username:
			setDefaultRegisterHintLables();
			username.setHint("");
			username.setFocusable(true);
			break;
		case R.id.loginusername:
			setDefaultEditTextLables();
			loginUsername.setHint("");
			loginUsername.setFocusable(true);
			break;
		case R.id.loginpassword:
			setDefaultEditTextLables();
			loginPassword.setHint("");
			loginPassword.setFocusable(true);
			break;
		case R.id.password:
			setDefaultRegisterHintLables();
			password.setHint("");
			password.setFocusable(true);
			break;
		case R.id.repeatpassword:
			setDefaultRegisterHintLables();
			repeat_password.setHint("");
			repeat_password.setFocusable(true);
			break;
		case R.id.epostaddress:
			setDefaultRegisterHintLables();
			eAddress.setHint("");
			eAddress.setFocusable(true);
			break;
		case R.id.firstaname:
			setDefaultRegisterHintLables();
			firstaname.setHint("");
			firstaname.setFocusable(true);
			break;
		case R.id.lastname:
			setDefaultRegisterHintLables();
			lastname.setHint("");
			lastname.setFocusable(true);
			break;

		}
		return false;
	}

	private void setDefaultEditTextLables() {
		if (loginUsername.getText().toString().length() == 0) {
			loginUsername.setHint(getString(R.string.hint_eforslag_username));
		}
		if (loginPassword.getText().toString().length() == 0) {
			loginPassword.setHint(getString(R.string.hint_eforslag_password));
		}

	}

	private void setDefaultRegisterHintLables() {
		if (username.getText().toString().length() == 0) {
			username.setHint(getString(R.string.hint_eforslag_username));
		}
		if (password.getText().toString().length() == 0) {
			password.setHint(getString(R.string.hint_eforslag_password));
		}
		if (repeat_password.getText().toString().length() == 0) {
			repeat_password
					.setHint(getString(R.string.hint_eforslag_repeat_password));
		}
		if (eAddress.getText().toString().length() == 0) {
			eAddress.setHint(getString(R.string.hint_eforslag_epostadress));
		}
		if (firstaname.getText().toString().length() == 0) {
			firstaname.setHint(getString(R.string.hint_eforslag_firstname));
		}
		if (lastname.getText().toString().length() == 0) {
			lastname.setHint(getString(R.string.hint_eforslag_lastname));
		}
	}

	/*
	 * Respond to soft keyboard events, look for the DONE press on the password
	 * field.
	 * 
	 * public boolean onKey(View v, int keyCode, KeyEvent event) { if
	 * ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode ==
	 * KeyEvent.KEYCODE_ENTER)) { sv.scrollTo(0, sv.getBottom()); } // Returning
	 * false allows other listeners to react to the press. return false; }
	 */

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {

		if ((event.getAction() == KeyEvent.ACTION_DOWN)
				&& (keyCode == KeyEvent.KEYCODE_ENTER)) {
			sv.scrollTo(0, sv.getBottom());
		}
		// Returning false allows other listeners to react to the press.
		return false;
	}
}
