package com.androidapps.speakapp.eforslag;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.screens.KontaktMailViewActivity;

/**
 * 
 * Eforslag login activity
 * 
 */
public class EforslagLoginActivity extends Activity implements APIRespone,
		OnClickListener {
	LinearLayout loginview, registerview;
	EditText username, password;
	Button login;
	MyAsyncTask background;
	ProgressDialog progress;
	private SharedPreferences settings;
	TextView forgotPwd;
	ImageView back;//, info_button;
	Dialog custom;
	final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.eforslag_login_register);
		settings = getSharedPreferences(EforseLogConstants.LOGIN_DETAILS,
				MODE_PRIVATE);
		background = new MyAsyncTask(this, this);
		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));
		registerview = (LinearLayout) findViewById(R.id.register);
		loginview = (LinearLayout) findViewById(R.id.login);
		loginview.setVisibility(View.VISIBLE);
		registerview.setVisibility(View.GONE);
		username = (EditText) findViewById(R.id.loginusername);
		password = (EditText) findViewById(R.id.loginpassword);
		forgotPwd = (TextView) findViewById(R.id.forgotPwd);
	//	info_button = (ImageView) findViewById(R.id.logininfo);
		// back = (ImageView) findViewById(R.id.back);
		// back.setOnClickListener(this);
	//	info_button.setOnClickListener(this);
		login = (Button) findViewById(R.id.loginbtn);
		GradientDrawable btnbg = (GradientDrawable) login.getBackground();
		btnbg.setColor(Color.parseColor("#97ff5e"));
		btnbg.setStroke(0, Color.parseColor("#00000000"));
		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (validate()) {
					String LoginUser = "{ \"app_key\": \""
							+ EforseLogConstants.APP_KEY
							+ "\",\"login_name\":\""
							+ username.getText().toString() + "\""
							+ ",\"password\" : " + "\""
							+ password.getEditableText().toString() + "\"}";
					System.out.println(LoginUser);
					background = new MyAsyncTask(EforslagLoginActivity.this,
							EforslagLoginActivity.this);
					background.execute(EforseLogConstants.LOGIN_URL, LoginUser);
				}
			}
		});
		forgotPwd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				forgotPwd();
			}
		});
	}

	String email = "";
	boolean fromForgotPwd = false;

	/**
	 * shows the dialog when user forgot the password
	 */
	private void forgotPwd() {
		// TODO Auto-generated method stub
		final Dialog custom = new Dialog(EforslagLoginActivity.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.forgotpwd);
		custom.getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextView dialogtext = (TextView) custom.findViewById(R.id.thanks_txt);

		Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
		GradientDrawable btnbg = (GradientDrawable) dialogButton
				.getBackground();
		btnbg.setColor(Color.parseColor("#97ff5e"));
		btnbg.setStroke(0, Color.parseColor("#00000000"));
		dialogButton.setText("Ok");
		final EditText editBox = (EditText) custom.findViewById(R.id.editText1);
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				email = editBox.getText().toString();
				if (null != email.trim()) {
					if (!isValidEmail(email.trim())) {
						Toast.makeText(EforslagLoginActivity.this,
								"ogiltig e-post", Toast.LENGTH_SHORT).show();
						return;
					}
				}

				fromForgotPwd = true;
				String forgotPwd = "{ \"app_key\": \""
						+ EforseLogConstants.APP_KEY + "\",\"email\":\""
						+ email + "\"}";
				System.out.println(forgotPwd);
				background = new MyAsyncTask(EforslagLoginActivity.this,
						EforslagLoginActivity.this);
				background.execute(EforseLogConstants.PASSWORD_REMINDER,
						forgotPwd);
				custom.dismiss();

			}
		});
		custom.show();

	}

	/**
	 * show tosat with msg value
	 * 
	 * @param msg
	 */
	public void showToast(String msg) {
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}

	/***
	 * checking the email is valid or not
	 * 
	 * @param email
	 * @return true - valid, false - not valid
	 */
	private boolean isValidEmail(String email) {
		// TODO Auto-generated method stub
		System.out.println("The email provided here is : " + email);
		if (email != null && !"".equalsIgnoreCase(email)) {
			// check and return true if it's ok..
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile(EMAIL_PATTERN);
			matcher = pattern.matcher(email);
			return matcher.matches();
		} else {
			return true;
		}
	}

	/**
	 * validate the username and password
	 * 
	 * @return true both are valid , false ---> both are invalid or any one of
	 *         the invalid
	 */
	protected boolean validate() {
		// TODO Auto-generated method stub
		if (("".equalsIgnoreCase(username.getText().toString()))
				|| (null == username.getText().toString())) {
			showToast("FYLL I ANVÄNDARNAMN");
			return false;
		}
		if ("".equalsIgnoreCase(password.getText().toString())
				|| (null == password.getText().toString())) {
			showToast("FYLL I LÖSENORD");
			return false;
		}

		return true;
	}

	/**
	 * show the dialogs based on provided error
	 * @param error
	 */
	private void showCorrespondingDialogs(String error) {
		// TODO Auto-generated method stub
		final Dialog custom = new Dialog(EforslagLoginActivity.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.signtoregister_alert);
		custom.getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LinearLayout laybg = (LinearLayout) custom.findViewById(R.id.lay);
		laybg.setBackgroundResource(R.drawable.toregister);
		TextView dialogtext = (TextView) custom.findViewById(R.id.thanks_txt);
		dialogtext.setText(error);

		Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
		GradientDrawable btnbg = (GradientDrawable) dialogButton
				.getBackground();
		btnbg.setColor(Color.parseColor("#97ff5e"));
		btnbg.setStroke(0, Color.parseColor("#00000000"));
		dialogButton.setText("OK");

		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				custom.dismiss();

			}
		});
		custom.show();
	}

	@Override
	public void onSuccues(String response, Vector<String> errors) {
		// TODO Auto-generated method stub
		if (errors.size() > 0) {
			showCorrespondingDialogs(EforseLogConstants.getErrorCode(errors
					.get(0)));
			username.setText("");
			password.setText("");
		} else {
			if (!fromForgotPwd) {
				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean(EforseLogConstants.LOGIN_STATUS_INFO, true);
				editor.commit();
				Intent intent = new Intent();
				intent.putExtra("RESULT", EforseLogConstants.LOGIN_SUCCESS);
				if (null != getParent()) {
					getParent().setResult(RESULT_OK, intent);
				} else {
					setResult(RESULT_OK, intent);
				}
				finish();
			} else {
				fromForgotPwd = false;
			}
		}

		if (progress != null) {
			if (progress.isShowing()) {
				progress.dismiss();
			}
		}
	}

	@Override
	public void onStartExecution() {
		// TODO Auto-generated method stub
		if (progress != null) {
			if (!progress.isShowing()) {
				progress.show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back:
			finish();
			break;
		/*case R.id.logininfo:
			custom = new Dialog(EforslagLoginActivity.this);
			custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
			custom.setContentView(R.layout.contact_info_alert);

			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.width = LayoutParams.FILL_PARENT;
			lp.height = LayoutParams.FILL_PARENT;
			custom.getWindow().setAttributes(lp);
			custom.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			Button ok_button = (Button) custom.findViewById(R.id.dialog_ok);
			GradientDrawable background = (GradientDrawable) ok_button
					.getBackground();
			background.setColor(Color.parseColor("#000000"));
			background.setStroke(0, Color.parseColor("#000000"));
			TextView info_txt = (TextView) custom.findViewById(R.id.info_txt);
			info_txt.setText(EforseLogConstants.INSTRUCTIONS);
			ok_button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					custom.cancel();
				}

			});
			custom.show();
			break;*/
		}
	}
}
