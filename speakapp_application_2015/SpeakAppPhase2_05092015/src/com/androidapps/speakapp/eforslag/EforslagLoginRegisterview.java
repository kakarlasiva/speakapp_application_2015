package com.androidapps.speakapp.eforslag;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.Toast;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.screens.PegangViewActivity;
import com.androidapps.speakapp.util.NetworkChecker;

/**
 * 
 * This is tabActivity shows two tabs  1) Register screen 2) Login screen
 *
 */

@SuppressWarnings("deprecation")
public class EforslagLoginRegisterview extends TabActivity implements OnTabChangeListener{
	TabHost tabHost;
	ImageView back;
	@SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.eforslag_login);
        if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			Toast.makeText(this,
					getResources().getString(R.string.network_error),
					Toast.LENGTH_SHORT).show();
			startActivity(new Intent(EforslagLoginRegisterview.this,InteractiveCube.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
         back = (ImageView) findViewById(R.id.back);
         back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
        tabHost = getTabHost();
         
        // Tab for Photos
        TabSpec loginspec = tabHost.newTabSpec("Logga in");
        // setting Title and Icon for the Tab
        loginspec.setIndicator("Logga in");
        Intent loginIntent = new Intent(this, EforslagLoginActivity.class);
        loginspec.setContent(loginIntent);
         
        // Tab for Songs
        TabSpec registerspec = tabHost.newTabSpec("Registrera dig");        
        registerspec.setIndicator("Registrera dig");
        Intent RegisterIntent = new Intent(this, EforslagRegisterActivity.class);
        registerspec.setContent(RegisterIntent);
         
        
         
     
        // Adding all TabSpec to TabHost
        tabHost.addTab(loginspec); // Adding photos tab
        tabHost.addTab(registerspec); // Adding songs tab
        
        tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.logintab_selected);//login_with_out_text
        tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.logintab);
        /*LayoutParams params = tabHost.getTabWidget().getChildAt(0).getLayoutParams();
        
        params.width =180;
        params.height = 100;
          tabHost.getTabWidget().getChildAt(0).setLayoutParams(params );
          tabHost.getTabWidget().getChildAt(1).setLayoutParams(params );*/
        tabHost.getTabWidget().getChildAt(0).getLayoutParams().width = 100;
        tabHost.getTabWidget().getChildAt(1).getLayoutParams().width = 180;
        tabHost.setOnTabChangedListener(this);
        TabWidget tabWidget = (TabWidget) findViewById(android.R.id.tabs);
        final int tabChildrenCount = tabWidget.getChildCount();
        View currentView;
        for (int i = 0; i < tabChildrenCount; i++) {
            currentView = tabWidget.getChildAt(i);
            LinearLayout.LayoutParams currentLayout =
                (LinearLayout.LayoutParams) currentView.getLayoutParams();
          if(i ==0)
            currentLayout.setMargins(0, 0, 10, 0);
        }
        tabWidget.requestLayout();
       
    }

	@Override
	public void onTabChanged(String tabId) {
		// TODO Auto-generated method stub
		if(tabId.equalsIgnoreCase("Logga in"))
	    {
	   tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.logintab_selected);;//login_with_out_text
	         tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.logintab); //blue
	  }else{
	   tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.logintab); //login
	         tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.logintab_selected); //registration_without_text
	        
	  }
	}
	
}
