package com.androidapps.speakapp.eforslag;

import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidapps.speakapp.R;

/**
 * 
 * EforcelgRegister view
 *
 */
public class EforslagRegisterActivity extends Activity implements APIRespone,
		OnClickListener {
	LinearLayout loginview, registerview;
	Button senddata;
	EditText username, password, repeat_password, eAddress, firstaname,
			lastname;
	MyAsyncTask background;
	ProgressDialog progress;
	private SharedPreferences settings;
	ImageView back, cross_image,info_button;
    Dialog custom;
	CheckBox check_agree;
	final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.eforslag_login_register);
		settings = getSharedPreferences(EforseLogConstants.LOGIN_DETAILS,
				MODE_PRIVATE);
		background = new MyAsyncTask(this, this);
		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));
		registerview = (LinearLayout) findViewById(R.id.register);
		info_button=(ImageView)findViewById(R.id.registerinfo);
		loginview = (LinearLayout) findViewById(R.id.login);
		registerview.setVisibility(View.VISIBLE);
		loginview.setVisibility(View.GONE);
		senddata = (Button) findViewById(R.id.senddata);
		GradientDrawable backgrounddesign = (GradientDrawable) senddata
				.getBackground();
		backgrounddesign.setColor(Color.parseColor("#5edbff"));
		backgrounddesign.setStroke(0, Color.parseColor("#00000000"));
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);
		repeat_password = (EditText) findViewById(R.id.repeatpassword);
		eAddress = (EditText) findViewById(R.id.epostaddress);
		firstaname = (EditText) findViewById(R.id.firstaname);
		lastname = (EditText) findViewById(R.id.lastname);
		check_agree=(CheckBox) findViewById(R.id.check_agree);


		info_button.setOnClickListener(this);
		senddata.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (validate()) {
					String registerUser = "{ \"app_key\": \""
							+ EforseLogConstants.APP_KEY
							+ "\",\"login_name\":\""
							+ username.getText().toString() + "\""
							+ ",\"first_name\" : " + "\""
							+ firstaname.getEditableText().toString() + "\""
							+ ",\"last_name\" : " + "\""
							+ lastname.getEditableText().toString() + "\""
							+ ",\"password\" : " + "\""
							+ password.getEditableText().toString() + "\""
							+ ",\"ssn\" : " + "\"" + "" + "\"" + ",\"city\" : "
							+ "\"" + "" + "\"" + ",\"address\" : " + "\"" + ""
							+ "\"" + ",\"zip\" : " + "\"" + "" + "\""
							+ ",\"location\" : " + "\"" + "" + "\""
							+ ",\"email\" : " + "\""
							+ eAddress.getEditableText().toString() + "\""
							+ ",\"mobile\" : " + "\"" + "" + "\""
							+ ",\"tel\" : " + "\"" + "" + "\"}";
					System.out.println(registerUser);
					background = new MyAsyncTask(EforslagRegisterActivity.this,
							EforslagRegisterActivity.this);
					background.execute(EforseLogConstants.REGISTER_URL,
							registerUser);
				}

			}
		});
	}
	/***
	 * validate the email address
	 * @param email
	 * @return true - valid , false  - invalid
	 */
	private boolean isValidEmail(String email) {
		// TODO Auto-generated method stub
		System.out.println("The email provided here is : " + email);
		if (email != null && !"".equalsIgnoreCase(email)) {
			// check and return true if it's ok..
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile(EMAIL_PATTERN);
			matcher = pattern.matcher(email);
			return matcher.matches();
		} else {
			return true;
		}
	}
	
   /**
    * show Toast with given message
    * @param msg
    */
	public void showToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * validate the username, eaddress, first name,last name, repeat password, password and t&c checkbox
	 * @return true both are valid  , false- all are invalid/ any one of the invalid
	 */
	public boolean validate() {
		if (username.getText().toString() == null
				|| "".equalsIgnoreCase(username.getText().toString())) {
			showToast("FYLL I ANV�NDARNAMN");
			return false;
		}
		if (eAddress.getText().toString() == null
				|| "".equalsIgnoreCase(eAddress.getText().toString())) {
			showToast("FYLL I E-POSTADRESS");
			return false;
		}else{
			if (!isValidEmail(eAddress.getText().toString().trim())) {
				showToast("ogiltig e-post");
				return false;
			}
		}
		if (password.getText().toString() == null
				|| "".equalsIgnoreCase(password.getText().toString())) {
			showToast("FYLL I L�SENORD");
			return false;
		}
		if (firstaname.getText().toString() == null
				|| "".equalsIgnoreCase(firstaname.getText().toString())) {
			showToast("FYLL I F�RNAMN");
			return false;
		}
		if (lastname.getText().toString() == null
				|| "".equalsIgnoreCase(lastname.getText().toString())) {
			showToast("FYLL I EFTERNAMN");
			return false;
		}
		if (!repeat_password.getText().toString()
				.equals(password.getText().toString())) {
			showToast("L�SENORD matchar inte");
			return false;
		}
		if(!check_agree.isChecked()){
			showToast("VILLKOREN M�STE GODK�NNAS");
			return false;
		}
		
		return true;

	}

	/**
	 * show the dialog based provided error
	 * @param error
	 */
	private void showCorrespondingDialogs(String error) {
		// TODO Auto-generated method stub
		final Dialog custom = new Dialog(EforslagRegisterActivity.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.signtoregister_alert);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LinearLayout laybg = (LinearLayout) custom.findViewById(R.id.lay);
		laybg.setBackgroundResource(R.drawable.toregister);

		TextView dialogtext = (TextView) custom.findViewById(R.id.thanks_txt);
		dialogtext.setText(error);

		Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
		GradientDrawable btnbg = (GradientDrawable) dialogButton
				.getBackground();
		btnbg.setColor(Color.parseColor("#ffc000"));
		dialogButton.setText("Ok");

		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				custom.dismiss();

			}
		});
		custom.show();
	}

	@Override
	public void onSuccues(String response, Vector<String> errors) {
		// TODO Auto-generated method stub
		if (errors.size() > 0) {
			showCorrespondingDialogs(EforseLogConstants.getErrorCode(errors.get(0)));
		} else {
			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean(EforseLogConstants.LOGIN_STATUS_INFO, true);
			editor.commit();
			showToast("Registrerade framg�ngsrikt");
			Intent intent = new Intent();
			intent.putExtra("RESULT", EforseLogConstants.LOGIN_SUCCESS);
			getParent().setResult(RESULT_OK, intent);
			setResult(EforseLogConstants.REGISTRATION_SUCCESS);
			finish();
		}
		if (progress != null) {
			if (progress.isShowing()) {
				progress.dismiss();
			}
		}
	}

	@Override
	public void onStartExecution() {
		// TODO Auto-generated method stub
		if (progress != null) {
			if (!progress.isShowing()) {
				progress.show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back:
			finish();
			break;
		case R.id.registerinfo:
			custom = new Dialog(EforslagRegisterActivity.this);
			custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
			custom.setContentView(R.layout.contact_info_alert);

			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.width = LayoutParams.FILL_PARENT;
			lp.height = LayoutParams.FILL_PARENT;
			custom.getWindow().setAttributes(lp);
			custom.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			Button ok_button = (Button) custom.findViewById(R.id.dialog_ok);
			GradientDrawable background = (GradientDrawable) ok_button
					.getBackground();
			background.setColor(Color.parseColor("#000000"));
			background.setStroke(0, Color.parseColor("#000000"));
			TextView info_txt = (TextView) custom.findViewById(R.id.info_txt);
			info_txt.setText(EforseLogConstants.INSTRUCTIONS);
			ok_button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					custom.cancel();
				}

			});
			custom.show();
			break;
		}
	}
}
