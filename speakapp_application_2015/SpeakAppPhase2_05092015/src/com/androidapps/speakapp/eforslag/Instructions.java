package com.androidapps.speakapp.eforslag;

import java.util.Vector;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.screens.KontaktMailViewActivity;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * Shows the dialog which shows the instructions getting from the service.
 *
 */
public class Instructions extends Activity implements APIRespone {
	private MyAsyncTask background;
	ProgressDialog progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getData();
		showAlertBox();
	}
    /**
     * get the instructions data from the service
     */
	
	private void getData() {
		// TODO Auto-generated method stub
		if (EforseLogConstants.INSTRUCTIONS == null
				|| EforseLogConstants.INSTRUCTIONS == "") {
			if(NetworkChecker.isNetworkOnline(this)){
			String instructions = "{ \"app_key\": \""
					+ EforseLogConstants.APP_KEY + "\"}";
			background = new MyAsyncTask(this, this);
			background.execute(EforseLogConstants.GET_INSTRUCTION_URL,
					instructions);
			}else{
				 showToast(EforseLogConstants.errorcode.get("Couldn't reach server at this time"));
				//Toast.makeText(this, EforseLogConstants.errorcode.get("Couldn't reach server at this time"), Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}
	public void showToast(String msg) {
		String fmsg = msg.substring(0, 1).toUpperCase()+msg.substring(1).toLowerCase();
		MethodsHelper.showToast(fmsg, this);
		//Toast.makeText(this, fmsg, Toast.LENGTH_SHORT).show();
	}
    /** 
     * show Dialog with instructions data
     */
	private void showAlertBox() {
		// TODO Auto-generated method stub
		final Dialog custom = new Dialog(Instructions.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.contact_info_alert);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.width = LayoutParams.FILL_PARENT;
		lp.height = LayoutParams.FILL_PARENT;
		custom.getWindow().setAttributes(lp);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		Button ok_button = (Button) custom.findViewById(R.id.dialog_ok);
		TextView info_txt = (TextView) custom.findViewById(R.id.info_txt);
		info_txt.setText(EforseLogConstants.INSTRUCTIONS);
		info_txt.setTypeface(DataEngine.gettypeface(this));
		ok_button.setTypeface(DataEngine.gettypeface(this));
		GradientDrawable btnbg = (GradientDrawable) ok_button
				.getBackground();
		btnbg.setColor(Color.parseColor("#474849"));
		
		ok_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				custom.cancel();
				finish();
			}

		});
		custom.show();
		
	}

	@Override
	public void onSuccues(String response, Vector<String> errors) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStartExecution() {
		// TODO Auto-generated method stub

	}
}
