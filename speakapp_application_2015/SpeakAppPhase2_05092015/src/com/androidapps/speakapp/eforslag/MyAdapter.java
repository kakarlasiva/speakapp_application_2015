package com.androidapps.speakapp.eforslag;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;

/**
 * Adapter for the Eforcelag listviews
 *
 */
public class MyAdapter extends BaseAdapter {
	ArrayList<PetetionBean> list;
	Context context;
	LayoutInflater inflater;

	public MyAdapter(Context cntxt, ArrayList<PetetionBean> petition_obj_list) {
		// TODO Auto-generated constructor stub
		this.context = cntxt;
		this.list = petition_obj_list;
		if (list == null)
			list = new ArrayList<PetetionBean>();
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = inflater.inflate(R.layout.eforslag_list_item, null);		
		TextView title = (TextView) convertView.findViewById(R.id.pt_title);
		TextView signCount = (TextView) convertView.findViewById(R.id.pt_sginCount);
		title.setText(list.get(position).getTitle());
		signCount.setText(""+ list.get(position).getSignatures() + " h�ller med");
		title.setTypeface(DataEngine.gettypeface(context));
		signCount.setTypeface(DataEngine.gettypeface(context));
		return convertView;
	}

}
