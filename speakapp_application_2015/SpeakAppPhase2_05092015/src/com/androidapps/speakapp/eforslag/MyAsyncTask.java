package com.androidapps.speakapp.eforslag;

import java.io.UnsupportedEncodingException;
import java.util.Vector;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * 
 * AsyncTask which gives the callbacks for pre-execute and post-execute. This
 * class is used for execute the sevice calls in the background.
 * 
 */
public class MyAsyncTask extends AsyncTask<String, String, String> {
	private static final String TAG = "BACKGROUND";
	Context ctx;
	int number;
	APIRespone response;
	Vector<String> errors;

	public MyAsyncTask(Context ctx, APIRespone response) {
		this.ctx = ctx;
		this.response = response;
		errors = new Vector<String>();
	}

	@Override
	protected void onPreExecute() {		
		super.onPreExecute();
		response.onStartExecution();
	}

	@Override
	protected String doInBackground(String... params) {
		return doTest(params[0], params[1]);
	}

	protected void onPostExecute(String result) {
		try {
		//	Log.i("onPostExecute", "********** json data ****************** "
			//		+ result);
			JSONObject mainObj = new JSONObject(result);
			if (mainObj.has(ParseConstants.ERROR_CODES)) {
				JSONArray errorArray = mainObj
						.getJSONArray(ParseConstants.ERROR_CODES);
				for (int i = 0; i <= errorArray.length(); i++) {
					JSONObject jsonObject = errorArray.getJSONObject(i);
					errors.add(jsonObject.getString("error"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.onSuccues(result, errors);
	}

	/**
	 * connect to the url and gives response in string format.
	 * 
	 * @param url
	 * @param datatoPost
	 * @return response string.
	 */
	public String doTest(String url, String datatoPost) {
		HttpPost postRequest = new HttpPost(url);
		StringEntity input = null;
		try {
			input = new StringEntity(datatoPost, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		input.setContentType("application/json;charset=UTF-8");
		postRequest.setEntity(input);
		input.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
				"application/json;charset=UTF-8"));
		postRequest.setHeader("Accept", "application/json");
		postRequest.setEntity(input);
		HttpResponse response = null;
		try {
			response = EforseLogConstants.getHttpClient().execute(postRequest);				
			String responseString = EntityUtils.toString(response.getEntity());
			Log.i(TAG, responseString);
			return responseString;
		} catch (Exception e) {
			Log.i(TAG, e.getLocalizedMessage());
			errors.add("Couldn't reach server at this time");
		}

		return null;
	}

}