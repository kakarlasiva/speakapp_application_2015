package com.androidapps.speakapp.eforslag;

/**
 * 
 * This class  contains all string constant which are used in parsing.
 *
 */
public class ParseConstants {
	public static final String ERROR_CODES = "errorcodes";
	public static final String PETETIONS = "petitions";
	public static final String SUMMARY = "summary";
	public static final String IS_ALLOW_PAPER = "allowpaper";
	public static final String CLASS_CATEGORY = "classcategory";
	public static final String PETETIONER = "petitioner";
	public static final String STATUS = "status";
	public static final String LINKS = "links";
	public static final String SIGNTURES = "signatures";
	public static final String SHORT_URL = "shorturl";
	public static final String START_DATE = "startdate";
	public static final String PETETION_ID = "petition_id";
	public static final String DESCRIPTION = "description";
	public static final String TARGET_GROUP = "targetgroup";
	public static final String END_DATE = "enddate";
	public static final String TITLE = "title";
	

}
