package com.androidapps.speakapp.eforslag;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * This class contains all the details of petition whichh implements Seriaizable interface.
 *
 */
public class PetetionBean implements Serializable {

	private String summary;
	private Boolean isAllowPaper;
	private String classCategory;
	private String petitioner;
	private String status;
	private ArrayList<String> links;
	private int signatures;
	private String shorturl;
	private String startdate;
	private int petition_id;
	private String description;
	private String targetgroup;
	private String endDate;
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Boolean isAllowPaper() {
		return isAllowPaper;
	}

	public void setIsAllowPaper(Boolean isAllowPaper) {
		this.isAllowPaper = isAllowPaper;
	}

	public String getClassCategory() {
		return classCategory;
	}

	public void setClassCategory(String classCategory) {
		this.classCategory = classCategory;
	}

	public String getPetitioner() {
		return petitioner;
	}

	public void setPetitioner(String petitioner) {
		this.petitioner = petitioner;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<String> getLinks() {
		return links;
	}

	public void setLinks(ArrayList<String> links) {
		this.links = links;
	}

	public int getSignatures() {
		return signatures;
	}

	public void setSignatures(int signatures) {
		this.signatures = signatures;
	}

	public String getShorturl() {
		return shorturl;
	}

	public void setShorturl(String shorturl) {
		this.shorturl = shorturl;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public int getPetition_id() {
		return petition_id;
	}

	public void setPetition_id(int petition_id) {
		this.petition_id = petition_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTargetgroup() {
		return targetgroup;
	}

	public void setTargetgroup(String targetgroup) {
		this.targetgroup = targetgroup;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
