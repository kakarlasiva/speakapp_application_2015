package com.androidapps.speakapp.eforslag;

import java.util.ArrayList;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.log.MyDebug;
import com.androidapps.speakapp.screens.InfoActivity;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;

/**
 * 
 * shows the list of recent petitions
 * 
 */

public class RecentPetitions extends Activity implements APIRespone,
		OnClickListener {
	private static final String TAG = "RECENT_PET";
	ProgressDialog progress;
	MyAsyncTask background;
	ListView petitionList;

	MyAdapter adapter;
	LinearLayout back;
	int position = -1;
	private SharedPreferences settings;
	protected static final int LOG_IN = 1000;
	TextView title_txt, logout;
	Typeface customTypeface;
	SessionManager sessionMgr;
	boolean isRelogin = false;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {		
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1000) {
			if (resultCode == 1000) {

				/*
				 * petetionsList.get(position).setSignatures(
				 * petetionsList.get(position).getSignatures() + 1);
				 */
				if (data != null && DataEngine.petetionsList != null
						&& DataEngine.petetionsList.size() > 0)
					DataEngine.petetionsList.get(position)
							.setSignatures(
									data.getIntExtra("count", DataEngine.petetionsList
											.get(position).getSignatures()));
				if (adapter != null)
					adapter.notifyDataSetChanged();

			}

		} else if (requestCode == 4000) {
			if (sessionMgr.isLoggedIn())
				startActivity(new Intent(RecentPetitions.this,
						WritePetition.class));
			// finish();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();		
		setLogoutButtonVisibility();
		DataEngine.isRecentPetitionsDestroyed = false;	

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		DataEngine.isRecentPetitionsDestroyed = true;
		DataEngine.clickFlag = false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.eforslag_list_lay);
		logout = (TextView) findViewById(R.id.logour);
		logout.setTypeface(customTypeface);
		logout.setOnClickListener(this);
		/*settings = getSharedPreferences(EforseLogConstants.LOGIN_DETAILS,
				MODE_PRIVATE);*/
		sessionMgr = new SessionManager(this);
		if(sessionMgr.isLoggedIn())
		{
			// sendLoginDataPreferences();
		}
		customTypeface = DataEngine.gettypeface(this);
		logout.setTypeface(customTypeface);
		EforseLogConstants.errordata();
		if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			showToast(getResources().getString(R.string.network_error));
			/*
			 * Toast.makeText(this,
			 * getResources().getString(R.string.network_error),
			 * Toast.LENGTH_SHORT).show();
			 */
			startActivity(new Intent(RecentPetitions.this,
					InteractiveCube.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}		
		title_txt = (TextView) findViewById(R.id.eforslag_title);
		title_txt.setTypeface(DataEngine.gettypeface(this));
		TextView textView1 = (TextView) findViewById(R.id.textView1);
		Button addNewProposal = (Button) findViewById(R.id.add_new_proposal);
		GradientDrawable btnbgKomment = (GradientDrawable) findViewById(
				R.id.add_new_proposal).getBackground();
		btnbgKomment.setColor(Color.parseColor("#000000"));

		addNewProposal.setTypeface(DataEngine.gettypeface(this));
		addNewProposal.setOnClickListener(this);
		textView1.setTypeface(DataEngine.gettypeface(this));
		if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			/*
			 * Toast.makeText(this,
			 * getResources().getString(R.string.network_error),
			 * Toast.LENGTH_SHORT).show();
			 */
			showToast(getResources().getString(R.string.network_error));
			startActivity(new Intent(RecentPetitions.this,
					InteractiveCube.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
		back = (LinearLayout) findViewById(R.id.back_layout);
		back.setOnClickListener(this);
		title_txt.setOnClickListener(this);
		ImageView top_info = (ImageView) findViewById(R.id.p_settings);
		top_info.setOnClickListener(this);
		petitionList = (ListView) findViewById(R.id.petitionlist);
		//DataEngine.petetionsList = new ArrayList<PetetionBean>();
		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage("Laddar...");
		
		String getLatestPetetions = "{ \"app_key\": \""
				+ EforseLogConstants.APP_KEY + "\",\"how_many\":\"100\" }";		

		background = new MyAsyncTask(RecentPetitions.this, this);
		background.execute(EforseLogConstants.GET_LATEST_PETETIONS,
				getLatestPetetions);
		
		MyDebug.appendLog("Reported Url : "+EforseLogConstants.GET_LATEST_PETETIONS+"\n"+getLatestPetetions);
		petitionList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				position = arg2;
				Intent intent = new Intent(RecentPetitions.this,
						DetailedPetetion.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable("OBJ", DataEngine.petetionsList.get(arg2));
				intent.putExtras(bundle);
				startActivityForResult(intent, 1000);
			}
		});
	}

	@Override
	public void onSuccues(String response, Vector<String> errocodes) {	
		/*if(isRelogin)
		{
			isRelogin = false;
			return;
		}*/
		
		if (!DataEngine.isRecentPetitionsDestroyed) {					

			if (fromLogout) {
				if (errocodes != null) {
					if (errocodes.size() > 0) {
						if ("NOT_AUTHORIZED".equalsIgnoreCase(errocodes.get(0))) {
							/*SharedPreferences.Editor editor = settings.edit();
							editor.putBoolean(
									EforseLogConstants.LOGIN_STATUS_INFO, false);
							editor.commit();*/
							sessionMgr.logoutUser();
						}
						String msg = EforseLogConstants.getErrorCode(errocodes
								.get(0));
						String fmsg = msg.substring(0, 1).toUpperCase()
								+ msg.substring(1).toLowerCase();
						showCorrespondingDialogs(fmsg);
						dismissProgress();
						return;
					}
				}
				dismissProgress();
				/*SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean(EforseLogConstants.LOGIN_STATUS_INFO, false);
				editor.commit();*/
				sessionMgr.logoutUser();
				logout.setVisibility(View.GONE);
			}
			if (errocodes != null) {
				/*
				 * if (errocodes.size() > 0) {
				 * showCorrespondingDialogs(EforseLogConstants
				 * .getErrorCode(errocodes.get(0))); } else { try {
				 * doParse(response); } catch (Exception e) { Log.i(TAG,
				 * "Exception here : " + e.getLocalizedMessage()); } }
				 */
				try {
					doParse(response);
					if (DataEngine.petetionsList.size() == 0) {
						showToast("Det finns inga inskickade e-f�rslag");
						
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					Log.i(TAG, "Exception here : " + e.getLocalizedMessage());
				}
			}
			adapter = new MyAdapter(this, DataEngine.petetionsList);
			petitionList.setAdapter(adapter);
			
			dismissProgress();
		}
	}

	private void dismissProgress() {
		if (progress != null) {
			if (progress.isShowing()) {
				progress.dismiss();
			}
		}
	}

	/**
	 * show the dialogs based on the provided error.
	 * 
	 * @param error
	 */
	@SuppressWarnings("deprecation")
	private void showCorrespondingDialogs(String error) {		
		final Dialog custom = new Dialog(RecentPetitions.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.signtoregister_alert);
		custom.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextView dialogtext = (TextView) custom.findViewById(R.id.thanks_txt);
		dialogtext.setTypeface(DataEngine.gettypeface(RecentPetitions.this));
		String errorText = EforseLogConstants.errorcode.get(error);
		if (null != errorText && errorText.length() > 3) {

		} else {
			errorText = EforseLogConstants.errorcode.get("An Error Occured");
		}

		String fmsg = errorText.substring(0, 1).toUpperCase()
				+ errorText.substring(1).toLowerCase();
		dialogtext.setText(fmsg);
		Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
		dialogButton.setTypeface(DataEngine.gettypeface(RecentPetitions.this));
		GradientDrawable btnbg = (GradientDrawable) dialogButton
				.getBackground();
		btnbg.setColor(Color.parseColor("#474849"));
		dialogButton.setText("Okej");
		dialogButton.setTypeface(customTypeface);
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				custom.dismiss();

			}
		});
		custom.show();
	}

	/**
	 * prepare petionbean object from jsondata string
	 * 
	 * @param jsonData
	 * @throws Exception
	 */
	private void doParse(String jsonData) throws Exception {
		
		//MyDebug.appendLog(jsonData);
		JSONObject mainObj = new JSONObject(jsonData);
		/*
		 * if (mainObj.has(ParseConstants.ERROR_CODES)) { JSONArray errorArray =
		 * mainObj .getJSONArray(ParseConstants.ERROR_CODES); }
		 */
		if (mainObj.has(ParseConstants.PETETIONS)) {
			JSONArray petetionArray = mainObj
					.getJSONArray(ParseConstants.PETETIONS);
		//	DataEngine.petetionsList = new ArrayList<PetetionBean>();
			DataEngine.petetionsList.clear();
			int le = petetionArray.length();
			for (int i = 0; i < petetionArray.length(); i++) {
				PetetionBean petetionBean = new PetetionBean();
				JSONObject petetionObj = petetionArray.getJSONObject(i);				

				if (petetionObj.has(ParseConstants.SUMMARY)) {
					petetionBean.setSummary(petetionObj
							.getString(ParseConstants.SUMMARY));
				}
				if (petetionObj.has(ParseConstants.TITLE)) {
					petetionBean.setTitle(petetionObj
							.getString(ParseConstants.TITLE));
				}
				if (petetionObj.has(ParseConstants.SUMMARY)) {
					petetionBean.setSummary(petetionObj
							.getString(ParseConstants.SUMMARY));
				}
				if (petetionObj.has(ParseConstants.IS_ALLOW_PAPER)) {
					petetionBean.setIsAllowPaper(petetionObj
							.getBoolean(ParseConstants.IS_ALLOW_PAPER));
				}
				if (petetionObj.has(ParseConstants.CLASS_CATEGORY)) {
					Log.i("doParse",
							"********** the class category value is ****************** = "
									+ petetionObj
											.getString(ParseConstants.CLASS_CATEGORY));
					petetionBean.setClassCategory(petetionObj
							.getString(ParseConstants.CLASS_CATEGORY));
				}
				if (petetionObj.has(ParseConstants.PETETIONER)) {
					petetionBean.setPetitioner(petetionObj
							.getString(ParseConstants.PETETIONER));
				}
				if (petetionObj.has(ParseConstants.STATUS)) {
					petetionBean.setStatus(petetionObj
							.getString(ParseConstants.STATUS));
				}
				if (petetionObj.has(ParseConstants.LINKS)) {
					JSONArray linkObj = petetionObj
							.getJSONArray(ParseConstants.LINKS);
					ArrayList<String> errors = new ArrayList<String>();
					for (int j = 0; j < linkObj.length(); j++) {

					}
					// petetionBean.setLinks(linkObj.);
				}
				if (petetionObj.has(ParseConstants.SIGNTURES)) {
					petetionBean.setSignatures(petetionObj
							.getInt(ParseConstants.SIGNTURES));
				}
				if (petetionObj.has(ParseConstants.SHORT_URL)) {
					Log.i("Bug",
							"---fb shorturl----------"
									+ petetionObj
											.getString(ParseConstants.SHORT_URL));

					petetionBean.setShorturl(petetionObj
							.getString(ParseConstants.SHORT_URL));
				}
				if (petetionObj.has(ParseConstants.START_DATE)) {
					petetionBean.setStartdate(petetionObj
							.getString(ParseConstants.START_DATE));
				}
				if (petetionObj.has(ParseConstants.PETETION_ID)) {
					petetionBean.setPetition_id(petetionObj
							.getInt(ParseConstants.PETETION_ID));
				}
				if (petetionObj.has(ParseConstants.DESCRIPTION)) {
					petetionBean.setDescription(petetionObj
							.getString(ParseConstants.DESCRIPTION));
				}
				if (petetionObj.has(ParseConstants.TARGET_GROUP)) {
					petetionBean.setTargetgroup(petetionObj
							.getString(ParseConstants.TARGET_GROUP));
				}

				if (petetionObj.has(ParseConstants.END_DATE)) {
					petetionBean.setEndDate(petetionObj
							.getString(ParseConstants.END_DATE));
				}

				Log.e("HMMMM", "DATA : " + petetionBean.getClassCategory());
				if (EforseLogConstants.CLASS_CATEGORY
						.equalsIgnoreCase(petetionBean.getClassCategory()
								.trim()))
					DataEngine.petetionsList.add(petetionBean);
			}
		}

	}

	@Override
	public void onStartExecution() {		
		if (progress != null) {
			if (!progress.isShowing()) {
				progress.show();
			}
		}
	}

	@Override
	public void onClick(View v) {		
		switch (v.getId()) {
		case R.id.back_layout:
			finish();
			break;
		case R.id.logour:
			logoutDialog();
			break;
		case R.id.eforslag_title:
			finish();
			break;
		case R.id.p_settings:
			Intent info_activity = new Intent(this, InfoActivity.class);
			info_activity.putExtra(DataEngine.INFO_OBJECT,
					EforseLogConstants.INSTRUCTIONS);
			startActivity(info_activity);
			// startActivity(new Intent(RecentPetitions.this,
			// Instructions.class));
			break;
		case R.id.add_new_proposal:
			if (EforseLogConstants.httpClient == null) {
				/*SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean(EforseLogConstants.LOGIN_STATUS_INFO, false);
				editor.commit();*/
				//sessionMgr.logoutUser();
			}
			if (!sessionMgr.isLoggedIn()) {
				startActivityForResult(new Intent(RecentPetitions.this,
						EforseLogNewActivity.class), 4000);
			} else {
				startActivity(new Intent(RecentPetitions.this,
						WritePetition.class));

			}
			break;
		}

	}

	private void setLogoutButtonVisibility() {
		/*if (EforseLogConstants.httpClient == null) {
			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean(EforseLogConstants.LOGIN_STATUS_INFO, false);
			editor.commit();
			sessionMgr.logoutUser();
		}*/
		if (!sessionMgr.isLoggedIn()) {
			logout.setVisibility(View.GONE);
		} else {           
			logout.setVisibility(View.VISIBLE);
			
		}
	}
	
	private void sendLoginDataPreferences()
	{
		isRelogin = true;
		String LoginUser = "{ \"app_key\": \"" + EforseLogConstants.APP_KEY
				+ "\",\"login_name\":\""
				+ sessionMgr.getUserName() + "\""
				+ ",\"password\" : " + "\""
				+ sessionMgr.getPwd() + "\"}";
		//System.out.println(LoginUser);
		MyAsyncTask background;
		background = new MyAsyncTask(RecentPetitions.this,
				RecentPetitions.this);
		MyDebug.appendLog("Request Url : "+EforseLogConstants.LOGIN_URL +"\n"+LoginUser);
		background.execute(EforseLogConstants.LOGIN_URL, LoginUser);
	}

	public void showToast(String msg) {
		String fmsg = msg.substring(0, 1).toUpperCase()
				+ msg.substring(1).toLowerCase();
		MethodsHelper.showToast(fmsg, this);
		// Toast.makeText(this, fmsg, Toast.LENGTH_SHORT).show();
	}

	@SuppressWarnings("deprecation")
	private void logoutDialog() {

		final Dialog dialog = new Dialog(RecentPetitions.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.call_alertdialog);
		dialog.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setCanceledOnTouchOutside(true);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) dialog.findViewById(R.id.number);
		text.setText(R.string.eforslag_logout);
		text.setTypeface(customTypeface);
		Button dialogButton = (Button) dialog.findViewById(R.id.ring);
		dialogButton.setTypeface(customTypeface);
		dialogButton.setText("Ja");
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				logout();
				dialog.dismiss();
			}
		});
		Button cancelButton = (Button) dialog.findViewById(R.id.cancel);
		cancelButton.setTypeface(customTypeface);
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});

		dialog.show();

	}

	boolean fromLogout = false;

	/**
	 * logout for eforslog user.
	 */
	public void logout() {
		fromLogout = true;
		String getLatestPetetions = "{ \"app_key\": \"ALGYENVHQMXPGJSTFMC\" }";
		//Log.i(TAG, getLatestPetetions);
		background = new MyAsyncTask(RecentPetitions.this, RecentPetitions.this);
		background.execute(EforseLogConstants.LOGOUT_URL, getLatestPetetions);
	}
}
