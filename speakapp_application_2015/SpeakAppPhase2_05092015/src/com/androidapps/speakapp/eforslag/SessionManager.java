package com.androidapps.speakapp.eforslag;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
 
public class SessionManager {
  
	// Shared Preferences
    SharedPreferences pref;
     
    // Editor for Shared preferences
    Editor editor;
     
    // Context
    Context _context;
     
    // Shared pref mode
    int PRIVATE_MODE = 0;
    
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    

    private static final String USER = "username";
    
 
    private static final String PWD = "pwd";
     
    // Sharedpref file name
    private static final String PREF_NAME = "SessionPref";     
  
     
    // Constructor
    public SessionManager(Context context){
    	
        this._context = context;       
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
       editor = pref.edit();
    }
    
    /**
     * Create login session
     * */
    public void createLoginSession(String user,String pwd){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);     
        editor.putString(USER, user);
        editor.putString(PWD, pwd);       
         
        // commit changes
        editor.commit();
        
        
    }   
    
    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
           
        }
    }
         
        /**
         * Quick check for login
         * **/
        // Get Login State
        public boolean isLoggedIn()
        {
            return pref.getBoolean(IS_LOGIN, false);
           
        }
        
        public String getUserName()
        {
        	 return pref.getString(USER, "");
        }
        public String getPwd()
        {
        	return pref.getString(PWD, "");
        }
        
        /**
         * Clear session details
         * */
        public void logoutUser(){
            // Clearing all data from Shared Preferences
           editor.clear();
            editor.commit();   
          
           
        }
    }
     

