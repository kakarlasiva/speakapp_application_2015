package com.androidapps.speakapp.eforslag;

import java.util.Calendar;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.log.MyDebug;
import com.androidapps.speakapp.screens.InfoActivity;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;

/**
 * 
 * Designed for write petition activity
 * 
 */
public class WritePetition extends Activity implements APIRespone,
		OnClickListener, OnTouchListener {
	public static final int FROM_LOGIN = 1000;
	private static final String TAG = "WRITE-PETETION";
	ProgressDialog progress;
	protected MyAsyncTask background;
	EditText title_ET, desc_ET, summary_ET;
	String title = "", description = "", summary = "", strtDate = "",
			endDate = "";
	ImageView info_button;
	boolean fromLogout = false;
	private SharedPreferences settings;
	Dialog custom;
	CheckBox checkagree;
	boolean isRelogin = false;

	TextView endDateTv, startDateTv;

	protected static final int LOG_IN = 1000;
	private static final int DATE_PICKER_ID = 0;
	private static final int END_DATE_PICKER = 1;
	boolean fromSavePetition = false, fromCheckPetition = false,
			fromRegisterPetition = false;
	private String petitionId = "";
	Typeface customTypeface;
	LinearLayout enddateLay, startdateLay, backLayout;
	TextView eforslagTitle;
	int year, month, day;
	ScrollView sv;
	InputMethodManager inputManager;
	SessionManager sessionMgr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.lagforslag_lay);
		sessionMgr = new SessionManager(getApplicationContext());
		if(sessionMgr.isLoggedIn())
		{
			sendLoginDataPreferences();
		}
		customTypeface = DataEngine.gettypeface(this);
		if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			/*
			 * Toast.makeText(this,
			 * getResources().getString(R.string.network_error),
			 * Toast.LENGTH_SHORT).show();
			 */
			showToast(getResources().getString(R.string.network_error));
			startActivity(new Intent(WritePetition.this, InteractiveCube.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
		// Get current date by calender

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		/*settings = getSharedPreferences(EforseLogConstants.LOGIN_DETAILS,
				MODE_PRIVATE);*/
		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));
		enddateLay = (LinearLayout) findViewById(R.id.enddate_lay);
		enddateLay.setOnClickListener(this);
		startdateLay = (LinearLayout) findViewById(R.id.startdateLay);
		startdateLay.setOnClickListener(this);
		title_ET = (EditText) findViewById(R.id.title);
		summary_ET = (EditText) findViewById(R.id.summary);
		inputManager = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		sv = (ScrollView) findViewById(R.id.writepetition_sv);
		desc_ET = (EditText) findViewById(R.id.description);
		desc_ET.setOnEditorActionListener(new EditText.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE
						|| event.getAction() == KeyEvent.ACTION_DOWN
						&& event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

					inputManager.hideSoftInputFromWindow(
							desc_ET.getWindowToken(), 0);
					sv.scrollTo(0, sv.getBottom());
					return true;
				}
				return false;
			}
		});
		TextView logoutTv = (TextView) findViewById(R.id.logour);
		logoutTv.setTypeface(customTypeface);
		backLayout = (LinearLayout) findViewById(R.id.back_layout);
		backLayout.setOnClickListener(this);
		// strtPicker = (ImageView) findViewById(R.id.strt_dt_picker);
		// endPicker = (ImageView) findViewById(R.id.end_dt_picker);
		checkagree = (CheckBox) findViewById(R.id.check_agree);
		if (MethodsHelper.getDensityName(this))
			checkagree.setPadding(
					(int) (checkagree.getPaddingLeft() + getResources()
							.getDimension(R.dimen.chkbox_padding)), checkagree
							.getPaddingTop(), checkagree.getPaddingRight(),
					checkagree.getPaddingBottom());

		checkagree.setTypeface(customTypeface);
		startDateTv = (TextView) findViewById(R.id.strt_date);
		endDateTv = (TextView) findViewById(R.id.end_date);
		endDateTv.setTypeface(customTypeface);
		startDateTv.setTypeface(customTypeface);
		EditText[] totalEditText = { title_ET, desc_ET, summary_ET };
		setTypeface(totalEditText);
		info_button = (ImageView) findViewById(R.id.logininform);
		eforslagTitle = (TextView) findViewById(R.id.eforslagTitle);
		eforslagTitle.setTypeface(customTypeface);
		TextView logoutText = (TextView) findViewById(R.id.logour);
		logoutText.setTypeface(customTypeface);
		ImageView eforslagInfoImg = (ImageView) findViewById(R.id.eforslaginform);
		eforslagInfoImg.setOnClickListener(this);

		GradientDrawable btnbg = (GradientDrawable) findViewById(R.id.senddata)
				.getBackground();
		btnbg.setColor(Color.parseColor("#000000"));
		btnbg.setStroke(2, Color.parseColor("#000000"));
		GradientDrawable btnbgendlay = (GradientDrawable) enddateLay
				.getBackground();
		Button senddataButton = (Button) findViewById(R.id.senddata);
		senddataButton.setTypeface(customTypeface);
		btnbgendlay.setColor(Color.parseColor("#ffffff"));
		btnbgendlay.setStroke(2, Color.parseColor("#000000"));
		GradientDrawable btnbgstartlay = (GradientDrawable) startdateLay
				.getBackground();

		btnbgstartlay.setColor(Color.parseColor("#ffffff"));
		btnbgstartlay.setStroke(2, Color.parseColor("#000000"));
		setListeners();
	}

	private void sendLoginDataPreferences()
	{
		isRelogin = true;
		String LoginUser = "{ \"app_key\": \"" + EforseLogConstants.APP_KEY
				+ "\",\"login_name\":\""
				+ sessionMgr.getUserName() + "\""
				+ ",\"password\" : " + "\""
				+ sessionMgr.getPwd() + "\"}";
		System.out.println(LoginUser);
		MyAsyncTask background;
		background = new MyAsyncTask(WritePetition.this,
				WritePetition.this);
		MyDebug.appendLog("Request Url : "+EforseLogConstants.LOGIN_URL +"\n"+LoginUser);
		background.execute(EforseLogConstants.LOGIN_URL, LoginUser);
	}

	private void setTypeface(EditText[] editTexts) {
		for (EditText editText : editTexts) {
			editText.setTypeface(DataEngine.gettypeface(this));
			GradientDrawable backgrounddesign = (GradientDrawable) editText
					.getBackground();
			backgrounddesign.setColor(Color.parseColor("#ffffff"));
			backgrounddesign.setStroke(2, Color.parseColor("#000000"));
			// editText.setOnTouchListener(this);
		}
	}

	/**
	 * set listeners for required ui components.
	 */
	private void setListeners() {
		// TODO Auto-generated method stub
		title_ET.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						summary_ET.requestFocus();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});
		summary_ET.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						desc_ET.requestFocus();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});

		findViewById(R.id.eforslagTitle).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						finish();
					}
				});

		findViewById(R.id.logininform).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent info_activity = new Intent(WritePetition.this,
								InfoActivity.class);
						info_activity.putExtra(DataEngine.INFO_OBJECT,
								EforseLogConstants.INSTRUCTIONS);
						startActivity(info_activity);

					}
				});

		findViewById(R.id.senddata).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				savePetetion();
			}
		});

		findViewById(R.id.logininform).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						Intent info_activity = new Intent(WritePetition.this,
								InfoActivity.class);
						info_activity.putExtra(DataEngine.INFO_OBJECT,
								EforseLogConstants.INSTRUCTIONS);
						startActivity(info_activity);
						// showInstructionsDialog(EforseLogConstants.INSTRUCTIONS);

					}

				});
		findViewById(R.id.logour).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Dialog dialog = new Dialog(WritePetition.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.call_alertdialog);
				dialog.getWindow().setLayout(
						android.view.WindowManager.LayoutParams.FILL_PARENT,
						android.view.WindowManager.LayoutParams.WRAP_CONTENT);
				dialog.getWindow().setBackgroundDrawable(
						new ColorDrawable(android.graphics.Color.TRANSPARENT));
				dialog.setCanceledOnTouchOutside(true);

				// set the custom dialog components - text, image and button
				TextView text = (TextView) dialog.findViewById(R.id.number);
				text.setText(R.string.eforslag_logout);
				text.setTypeface(customTypeface);
				Button dialogButton = (Button) dialog.findViewById(R.id.ring);
				dialogButton.setTypeface(customTypeface);
				dialogButton.setText("Ja");
				// if button is clicked, close the custom dialog
				dialogButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						logout();
					}
				});
				Button cancelButton = (Button) dialog.findViewById(R.id.cancel);
				cancelButton.setTypeface(customTypeface);
				cancelButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});

				dialog.show();

			}
		});
	}
    @Override
    protected void onResume() {
    
    	super.onResume();
    	DataEngine.isWritePetitionsDestroyed = false;
    }
    
    @Override
    protected void onDestroy() {
    	
    	super.onDestroy();
    	DataEngine.isWritePetitionsDestroyed = true;
    }
    
	/**
	 * logout for eforslog user.
	 */
	public void logout() {
		fromLogout = true;
		String getLatestPetetions = "{ \"app_key\": \"ALGYENVHQMXPGJSTFMC\" }";
		//Log.i(TAG, getLatestPetetions);
		background = new MyAsyncTask(WritePetition.this, WritePetition.this);
		background.execute(EforseLogConstants.LOGOUT_URL, getLatestPetetions);
	}

	/**
	 * save the entered petition.
	 */
	public void savePetetion() {
		if (validate()) {
			changeTheThings(true, false, false);			
			String getLatestPetetions = "{ \"app_key\":\"ALGYENVHQMXPGJSTFMC\",\"title\":\""
					+ title
					+ "\",\"petition_id\":\"\",\"classcategory\":\""+EforseLogConstants.CLASS_CATEGORY+"\",\"links\": [],\"summary\":\""
					+ summary
					+ "\",\"description\":\""
					+ description
					+ "\" ,\"shorturl\":\""
					+ ""
					+ "\",\"allowpaper\":\""
					+ "false"
					+ "\",\"startdate\":\""
					+ strtDate
					+ "\",\"enddate\":\"" + endDate + "\" }";
					
			//Log.i(TAG, getLatestPetetions);
				background = new MyAsyncTask(WritePetition.this, WritePetition.this);
				background.execute(EforseLogConstants.SAVE_PETETION,
						getLatestPetetions);
			
			
		}
		
		
	}
	 // convert from UTF-8 -> internal Java String format
    public  String convertFromUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("ISO-8859-1"), "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }
 
    // convert from internal Java String format -> UTF-8
    public String convertToUTF8(String s) {
        String out = "";
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return "";
    }
	/**
	 * shows the Toast with provided msg.
	 * 
	 * @param msg
	 */
	public void showToast(String msg) {
		String fmsg = msg.substring(0, 1).toUpperCase()
				+ msg.substring(1).toLowerCase();
		MethodsHelper.showToast(fmsg, this);
		// Toast.makeText(this, fmsg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * validate title, summary, startdate and enddate of the petition
	 * 
	 * @return true/false
	 */
	public boolean validate() {
		title = title_ET.getText().toString();
		title = title.replace("\"", "''");
		summary = summary_ET.getText().toString();
		summary = summary.replace("\"", "''");
		description = desc_ET.getText().toString();
		if (title == null || "".equalsIgnoreCase(title)) {
			title_ET.requestFocus();
			showToast("Fyll i rubrik");
			return false;
		}

		if (summary == null || "".equalsIgnoreCase(summary)) {
			summary_ET.requestFocus();
			summary_ET.requestFocus();
			showToast("Fyll i sammanfattning");
			return false;
		}
		if (desc_ET.getText().toString() != null
				&& desc_ET.getText().toString() != "") {
			desc_ET.requestFocus();
			description = desc_ET.getText().toString();
			if(null != description && description.length()>0)
			{
				description = description.replace("\"", "''");
			}
			else
				description=" ";
		}
		if (strtDate == null || "".equalsIgnoreCase(strtDate)) {
			showToast("Fyll i startdatum");
			return false;
		}
		if (endDate == null || "".equalsIgnoreCase(endDate)) {
			showToast("Fyll i slutdatum");
			return false;
		}

		if (!checkagree.isChecked()) {
			showToast(getResources().getString(
					R.string.toast_for_checkbox));
			return false;
		} else {
			return true;
		}

	}

	@Override
	public void onSuccues(String response, Vector<String> errocodes) {
		if(isRelogin)
		{
			isRelogin = false;
			return;
		}
		
		if(!DataEngine.isWritePetitionsDestroyed){
			Log.i(TAG, "Response : " + response);
            
			if (errocodes != null) {
				if (errocodes.size() > 0) {
					if ("NOT_AUTHORIZED".equalsIgnoreCase(errocodes.get(0))) {
						/*SharedPreferences.Editor editor = settings.edit();
						editor.putBoolean(EforseLogConstants.LOGIN_STATUS_INFO,
								false);
						editor.commit();*/
						sessionMgr.logoutUser();
					}
					String msg = EforseLogConstants.getErrorCode(errocodes
							.get(0));
					String fmsg = msg.substring(0, 1).toUpperCase()
							+ msg.substring(1).toLowerCase();
					showCorrespondingDialogs(fmsg);

					if (progress != null) {
						if (progress.isShowing()) {
							progress.dismiss();
						}
					}
					return;
				}
			}

			if (fromLogout) {
				sessionMgr.logoutUser();
				finish();
			} else {
				if (fromSavePetition) {
					getPetition(response);
					save_checkPetition();
					return;
				}
				if (fromCheckPetition) {
					registerPetition();
					return;
				} else
					showThankyouMessage();
			}
			if (progress != null) {
				if (progress.isShowing()) {
					progress.dismiss();
				}
			}
		}
	}

	/**
	 * get the petition id from the response.
	 * 
	 * @param response
	 */
	private void getPetition(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.has("petition_id")) {
				petitionId = jsonObject.getString("petition_id");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * register the enter petition.
	 */
	private void registerPetition() {
		// TODO Auto-generated method stub
		String getLatestPetetions = "{ \"app_key\":\"ALGYENVHQMXPGJSTFMC\",\"petition_id\":\""
				+ petitionId + "\"}";
		//Log.i(TAG, getLatestPetetions);
		changeTheThings(false, false, true);
		background = new MyAsyncTask(WritePetition.this, WritePetition.this);
		background.execute(EforseLogConstants.REGISTER_PETETION,
				getLatestPetetions);

	}

	/**
	 * intialize boolean values based on requirements.
	 * 
	 * @param _fromSave
	 * @param _fromSaveCheck
	 * @param _fromRegister
	 */
	private void changeTheThings(boolean _fromSave, boolean _fromSaveCheck,
			boolean _fromRegister) {
		fromCheckPetition = _fromSaveCheck;
		fromSavePetition = _fromSave;
		fromRegisterPetition = _fromRegister;
	}

	private void save_checkPetition() {
		// TODO Auto-generated method stub
		String getLatestPetetions = "{ \"app_key\":\"ALGYENVHQMXPGJSTFMC\",\"title\":\""
				+ title
				+ "\",\"petition_id\":\""
				+ petitionId
				+ "\",\"classcategory\":\""+EforseLogConstants.CLASS_CATEGORY+"\",\"links\": [],\"summary\":\""
				+ summary
				+ "\",\"description\":\""
				+ description
				+ "\" ,\"shorturl\":\""
				+ ""
				+ "\",\"allowpaper\":\""
				+ "true"
				+ "\",\"startdate\":\""
				+ strtDate
				+ "\",\"enddate\":\""
				+ endDate + "\",\"termsofservicechecked\" : \"true\"}";
		//Log.i(TAG, getLatestPetetions);
		changeTheThings(false, true, false);
		background = new MyAsyncTask(WritePetition.this, WritePetition.this);
		background.execute(EforseLogConstants.SAVE_CHECK_PETETION,
				getLatestPetetions);
	}

	/**
	 * show the Dialog based on error
	 * 
	 * @param error
	 */
	private void showCorrespondingDialogs(final String error) {
		String errMsg = error.substring(0, 1).toUpperCase()
				+ error.substring(1).toLowerCase();

		final Dialog custom = new Dialog(WritePetition.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.signtoregister_alert);
		custom.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		TextView dialogtext = (TextView) custom.findViewById(R.id.thanks_txt);
		dialogtext.setTypeface(DataEngine.gettypeface(WritePetition.this));
		dialogtext.setText(errMsg);

		Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
		GradientDrawable btnbg = (GradientDrawable) dialogButton
				.getBackground();
		btnbg.setColor(Color.parseColor("#474849"));
		dialogButton.setText("Okej");
		dialogButton.setTypeface(customTypeface);
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if ("AUTENTISERINGEN INTE GODK�ND".equalsIgnoreCase(error)) {
					startActivityForResult(new Intent(WritePetition.this,
							EforseLogNewActivity.class), LOG_IN);// EforslagLoginRegisterview

				}
				custom.dismiss();

			}
		});
		custom.show();
	}

	/**
	 * show the Dialog with thank you message
	 */
	private void showThankyouMessage() {
		// TODO Auto-generated method stub

		title_ET.setText("");
		summary_ET.setText("");
		desc_ET.setText("");
		endDateTv.setText("Slutdatum");

		checkagree.setChecked(false);
		setDefaultRegisterHintLables();
		final Dialog custom = new Dialog(WritePetition.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.alert_layout);
		custom.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		TextView dialogtext = (TextView) custom.findViewById(R.id.thanks_txt);
		dialogtext.setTypeface(DataEngine.gettypeface(this));
		dialogtext.setText(R.string.eforslag_thankyou);

		Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
		GradientDrawable btnbg = (GradientDrawable) dialogButton
				.getBackground();
		btnbg.setColor(Color.parseColor("#474849"));
		dialogButton.setTypeface(customTypeface);
		dialogButton.setText(R.string.close);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				custom.dismiss();
				finish();
				/*
				 * Intent new_intent=new
				 * Intent(WritePetition.this,RecentPetitions.class);
				 * startActivity(new_intent);
				 */

			}
		});
		custom.show();

	}

	/**
	 * show dialog with date picker.
	 * 
	 * @param error
	 * @param strt_end
	 */
	private void selectDate(String error, final boolean strt_end) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// builder.setMessage(error);
		View view = LayoutInflater.from(this).inflate(R.layout.date_picker,
				null);
		final DatePicker picker = (DatePicker) view
				.findViewById(R.id.datePicker);

		// picker.init(year, month, day, null);

		builder.setTitle(error);
		TextView tv = new TextView(this);
		LayoutParams lp = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, getResources()
						.getDimensionPixelSize(
								R.dimen.custom_title_textview_height));
		tv.setGravity(Gravity.CENTER_VERTICAL);
		tv.setTextSize(24);
		tv.setTextColor(android.R.color.holo_blue_bright);

		tv.setLayoutParams(lp);
		tv.setTypeface(customTypeface);
		tv.setText(error);
		// builder.setCustomTitle(tv);
		builder.setView(view);

		builder.setPositiveButton("Okej",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						String month = "", day = "";
						if ((picker.getMonth() + 1) <= 9) {
							month = "0" + (picker.getMonth() + 1);
						} else {
							month = String.valueOf((picker.getMonth() + 1));
						}
						if (picker.getDayOfMonth() <= 9) {
							day = "0" + picker.getDayOfMonth();
						} else {
							day = String.valueOf(picker.getDayOfMonth());
						}
						if (strt_end) {
							strtDate = picker.getYear() + "-" + month + "-"
									+ day;
							startDateTv.setText(strtDate);
						} else {
							endDate = picker.getYear() + "-" + month + "-"
									+ day;
							endDateTv.setText(endDate);
						}
						dialog.cancel();
					}
				});

		builder.setNegativeButton("Avbryt", null);

		AlertDialog dialog = builder.create();
		/*
		 * ((TextView) dialog.findViewById(getResources().getIdentifier(
		 * "alertTitle", "id", "android"))).setTypeface(customTypeface);
		 */
		/*
		 * ((Button) dialog.getButton(AlertDialog.BUTTON_POSITIVE))
		 * .setTypeface(customTypeface);
		 */
		dialog.show();

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_PICKER_ID:

			// open datepicker dialog.
			// set date picker for current date
			// add pickerListener listner to date picker
			return new DatePickerDialog(this, pickerListener, year, month, day) {
				@Override
				public void setTitle(CharSequence title) {
					// TODO Auto-generated method stub
					super.setTitle("Startdatum:");
				}

			};
		case END_DATE_PICKER:
			return new DatePickerDialog(this, endpickerListener, year, month,
					day) {
				@Override
				public void setTitle(CharSequence title) {
					// TODO Auto-generated method stub
					super.setTitle("Slutdatum:");
				}

			};
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			strtDate = year + "-" + month + "-" + day;
			startDateTv.setText(strtDate);

		}
	};
	private DatePickerDialog.OnDateSetListener endpickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			endDate = year + "-" + month + "-" + day;
			endDateTv.setText(endDate);

		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == 1000) {
			if (resultCode == EforseLogConstants.LOGIN_SUCCESS
					|| resultCode == EforseLogConstants.REGISTRATION_SUCCESS) {
				savePetetion();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onStartExecution() {
		// TODO Auto-generated method stub
		if (progress != null) {
			if (!progress.isShowing()) {
				progress.show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.enddate_lay:

			// showEndDatePicker();
			selectDate("Slutdatum:", false);
			break;
		case R.id.eforslaginform:
			Intent info_activity = new Intent(this, InfoActivity.class);
			info_activity.putExtra(DataEngine.INFO_OBJECT, getResources()
					.getString(R.string.write_petition_info));
			startActivity(info_activity);
			// showInstructionsDialog(EforseLogConstants.INSTRUCTIONS);
			break;
		case R.id.startdateLay:

			// showDatePickerDialog();
			selectDate("Startdatum:", true);
			break;
		case R.id.back_layout:
			finish();
			break;
		}

	}

	private void showInstructionsDialog(String txt) {
		custom = new Dialog(WritePetition.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.contact_info_alert);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.width = LayoutParams.FILL_PARENT;
		lp.height = LayoutParams.FILL_PARENT;
		custom.getWindow().setAttributes(lp);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		Button ok_button = (Button) custom.findViewById(R.id.dialog_ok);
		GradientDrawable background = (GradientDrawable) ok_button
				.getBackground();
		background.setColor(Color.parseColor("#474849"));
		background.setStroke(0, Color.parseColor("#000000"));
		TextView info_txt = (TextView) custom.findViewById(R.id.info_txt);
		info_txt.setText(txt);
		ok_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				custom.cancel();
			}

		});
		custom.show();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (v.getId()) {
		case R.id.title:
			setDefaultRegisterHintLables();
			title_ET.setHint("");
			title_ET.setFocusable(true);
			break;
		case R.id.summary:
			setDefaultRegisterHintLables();
			summary_ET.setHint("");
			summary_ET.setFocusable(true);
			break;
		case R.id.description:
			setDefaultRegisterHintLables();
			desc_ET.setHint("");
			desc_ET.setFocusable(true);
			break;
		}
		return false;
	}

	private void setDefaultRegisterHintLables() {
		if (desc_ET.getText().toString().length() == 0) {
			desc_ET.setHint(getString(R.string.hint_beskri));
		}
		if (summary_ET.getText().toString().length() == 0) {
			summary_ET.setHint(getString(R.string.hint_summ));
		}
		if (title_ET.getText().toString().length() == 0) {
			title_ET.setHint(getString(R.string.hint_rubrik));
		}

	}
}
