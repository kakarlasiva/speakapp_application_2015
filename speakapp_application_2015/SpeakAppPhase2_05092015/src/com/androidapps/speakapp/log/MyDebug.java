package com.androidapps.speakapp.log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.androidapps.speakapp.data.DataEngine;

/***
 * 
 * 
 * 
 *         This is the class Responsible for Log the Information on Screen;
 * 
 */
public class MyDebug {

	private static final String SPEAKAPP_FOLDER = "SpeakApp";
	private static final String SPEAKAPP_FILE = "eforslag_sign_count";

	boolean mExternalStorageAvailable = false;
	boolean mExternalStorageWriteable = false;

	static Context context;

	/**
	 * Writes the log onto the console.Here type defines the V/I/E/D as in case
	 * of Log. Tag - was the ActivityName which this call happens. Msg - the
	 * actual msg to be written.
	 * 
	 * @param type
	 * @param tag
	 * @param msg
	 */
	public static void log_info(String type, String tag, String msg) {
		if (DataEngine.INFO_LOG.equals(type)) {
			if (DataEngine.INFO_LOG_ENABLED) {
				//Log.i(tag, msg);
				//appendLog(tag + "...." + msg);
			}
			return;
		}
		if (DataEngine.DEBUG_LOG.equals(type)) {
			if (DataEngine.DEBUG_LOG_ENABLED) {
				//Log.d(tag, msg);
				//appendLog(tag + "...." + msg);
			}
			return;
		}
		if (DataEngine.ERROR_LOG.equals(type)) {
			if (DataEngine.ERROR_LOG_ENABLED) {
				//Log.e(tag, msg);
				//appendLog(tag + "...." + msg);
			}
			return;
		}
		if (DataEngine.VERBOSE_LOG.equals(type)) {
			if (DataEngine.VERBOSE_LOG_ENABLED) {
			//	Log.i(tag, msg);
				//appendLog(tag + "...." + msg);
			}
			return;
		}
	}

	public static void log_info(Context cntxt, String type, String tag,
			String msg) {
		context = cntxt;
		log_info(type, tag, msg);
	}
    
	/**
	 *  check the external storage availability of the device
	 * @return true/false
	 */
	public boolean check_externalStatus() {
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			// Something else is wrong. It may be one of many other states, but
			// all we need
			// to know is we can neither read nor write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		return mExternalStorageAvailable && mExternalStorageWriteable;
	}

	/**
	 * write the log to external file
	 * @param text
	 */
	public static void appendLog(String text) {
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File file = new File(filepath, SPEAKAPP_FOLDER);

		if (!file.exists()) {
			file.mkdirs();
		}
		File logFile = new File(file.getPath(),SPEAKAPP_FILE);
		File internalFile = new File(
				"/data/data/com.androidapp.speakapp/speakapp_logdev.txt");

		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (!internalFile.exists()) {
			try {
				internalFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		/*try {
			
			BufferedWriter buf = new BufferedWriter(new FileWriter(logFile,
					true));
			buf.append(text);
			buf.append("\n");
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		try {
			FileOutputStream fos = context.openFileOutput("Speakapp.txt",
					Context.MODE_APPEND);
			fos.write(text.getBytes());
			fos.close();
		} catch (Exception e) {
			
			e.printStackTrace();
		}*/
	}

	/**
	 * print the exception details
	 * @param e
	 */
	public static void exception_info(Exception e) {
		if (DataEngine.ERROR_LOG_ENABLED) {
			//e.printStackTrace();
		}
	}
}
