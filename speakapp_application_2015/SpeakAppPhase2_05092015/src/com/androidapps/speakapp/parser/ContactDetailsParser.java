package com.androidapps.speakapp.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.util.Contactdetailsetter;

public class ContactDetailsParser {
	String url;
	Contactdetailsetter contact_details;
	Context c;
	Dialog custom;

	public ContactDetailsParser(Context c) {
		super();
		this.c = c;
	}

	public String execute(String url, Contactdetailsetter details) {
		// TODO Auto-generated method stub

		InputStream inputStream = null;
		String result = "";
		try {

			// 1. create HttpClient
			HttpClient httpclient = new DefaultHttpClient();

			String json = "";

			// 3. build jsonObject
			// JSONObject headjsonobject = new JSONObject();
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("Text", details.getMessage());
			jsonObject.put("Name", details.getName());
			jsonObject.put("Telephone", details.getTelephone());
			jsonObject.put("Email", details.getEpost());
			// headjsonobject.put("Message", jsonObject);

			// 4. convert JSONObject to JSON to String
			json = jsonObject.toString();

			// ** Alternative way to convert Person object to JSON string usin
			// Jackson Lib
			// ObjectMapper mapper = new ObjectMapper();
			// json = mapper.writeValueAsString(person);

			// 5. set json to StringEntity

			String encodedUrl = URLEncoder.encode(json, "UTF-8");
			StringEntity se = new StringEntity(encodedUrl);
			// httpPost.setEntity(encodedUrl);

			System.out.println("Encoded URL " + encodedUrl);

			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!"
					+ se.getContent().toString());

			// 6. set httpPost Entity
			// httpPost.setEntity(se);
			// 2. make POST request to the given URL
			HttpPost httpPost = new HttpPost(url + encodedUrl);

			// 7. Set some headers to inform server about the type of the
			// content
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");

			// 8. Execute POST request to the given URL
			HttpResponse httpResponse = httpclient.execute(httpPost);

			// 9. receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();

			// 10. convert inputstream to string
			if (inputStream != null)

			{
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(inputStream, "iso-8859-1"), 8);
				String line = "";
				String result1 = "";
				while ((line = bufferedReader.readLine()) != null)
					result1 += line;
				Log.d("************", result1);
				inputStream.close();
				if(null != result1)
					return result1;
			} else
				result = "Did not work!";

		} catch (Exception e) {
			// Log.d("InputStream", e.getLocalizedMessage());
			e.printStackTrace();
		}

		// 11. return result
		return result;

	}

	// convert InputStream to String
	private static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}
}