package com.androidapps.speakapp.parser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Vector;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.androidapps.speakapp.util.NewsHelper;
import com.androidapps.speakapp.util.QuestionHelper;
import com.androidapps.speakapp.util.TagConstants;
import com.androidapps.speakapp.util.QuestionHelper.OptionsData;
import com.androidapps.speakapp.util.QuestionHelper.SurveyData;

public class NewsReader {
	public Vector<NewsHelper> news_data;
	public Vector<NewsHelper.OldNews> oldnews_data;
	public Vector<NewsHelper.CurrentNews> currentnews_data;
	public NewsHelper helper_obj;
	NewsHelper.OldNews oldnews_helperobj;
	NewsHelper.CurrentNews currentnews_helperobj;
	InputStream in = null;
	static JSONObject jobj = null;
	static String json = null;
	Context context;

	public NewsReader(Context mtx) {
		// TODO Auto-generated constructor stub
		this.context = mtx;
		news_data = new Vector<NewsHelper>();
		oldnews_data = new Vector<NewsHelper.OldNews>();
		currentnews_data = new Vector<NewsHelper.CurrentNews>();
	}

	public void clear() {
		news_data.clear();
		oldnews_data.clear();
		currentnews_data.clear();
	}

	/***
	 * always verify the host - dont check for certificate
	 */
	final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};

	/**
	 * Trust every server - dont check for any certificate
	 */
	private static void trustAllHosts() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[] {};
			}

			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub

			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain,
					String authType)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub

			}

		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public JSONObject getjsonfromurl(String menu_url) {

		HttpURLConnection http = null;
		URL url = null;
		try {
			url = new URL(menu_url);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (url.getProtocol().toLowerCase().equals("https")) {
			trustAllHosts();
			HttpsURLConnection https = null;
			try {
				https = (HttpsURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			https.setHostnameVerifier(DO_NOT_VERIFY);
			http = https;
		} else {
			try {
				http = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			in = new BufferedInputStream(http.getInputStream());
		} catch (Exception e) {
			// TODO: handle exception
		}
		// in=ctx.getResources().openRawResource(R.raw.menuview);
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					in, "utf-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			in.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			if (null != json)
				jobj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		return jobj;

	}

	public void parse_json(String url) {
		Log.i("JSON Parser", "--------url-------- " + url);
		JSONObject jmenus = getjsonfromurl(url);
		helper_obj = new NewsHelper();
		try {

			JSONArray currentnews_array = jmenus
					.getJSONArray(TagConstants.TAG_CURRENTNEWS);
			currentnews_data = new Vector<NewsHelper.CurrentNews>();
			for (int i = 0; i < currentnews_array.length(); i++) {
				JSONObject currentnews_object = currentnews_array
						.getJSONObject(i);
				currentnews_helperobj = helper_obj.new CurrentNews();
				int news_id = currentnews_object
						.getInt(TagConstants.TAG_NEWSID);
				currentnews_helperobj.setCurrentnews_id(news_id);
				String currentnews_name = currentnews_object
						.getString(TagConstants.TAG_NEWSTITLE);
				currentnews_helperobj.setCurrentnews_title(currentnews_name);
				String currentnews_startdate = currentnews_object
						.getString(TagConstants.TAG_NEWSSTARTDATE);
				currentnews_helperobj
						.setCurrentnews_startdate(currentnews_startdate);
				String currentnews_enddate = currentnews_object
						.getString(TagConstants.TAG_NEWSENDDATE);
				currentnews_helperobj
						.setCurrentnews_enddate(currentnews_enddate);
				String currentnews_text = currentnews_object
						.getString(TagConstants.TAG_NEWSTEXT);
				currentnews_helperobj.setCurrentnews_text(currentnews_text);
				String currentnews_image = currentnews_object
						.getString(TagConstants.TAG_NEWSIMAGE);
				currentnews_helperobj.setCurrentnews_image(currentnews_image);
				currentnews_data.add(currentnews_helperobj);
			}
			helper_obj.setCurrentnews_vec(currentnews_data);
			JSONArray oldnews_array = jmenus
					.getJSONArray(TagConstants.TAG_OLDNEWS);
			oldnews_data = new Vector<NewsHelper.OldNews>();
			for (int j = 0; j < oldnews_array.length(); j++) {
				JSONObject oldnews_obj = oldnews_array.getJSONObject(j);
				oldnews_helperobj = helper_obj.new OldNews();
				int news_id = oldnews_obj.getInt(TagConstants.TAG_NEWSID);
				oldnews_helperobj.setOldnews_id(news_id);
				String currentnews_name = oldnews_obj
						.getString(TagConstants.TAG_NEWSTITLE);
				oldnews_helperobj.setOldnews_title(currentnews_name);
				String currentnews_startdate = oldnews_obj
						.getString(TagConstants.TAG_NEWSSTARTDATE);
				oldnews_helperobj.setOldnews_startdate(currentnews_startdate);
				String currentnews_enddate = oldnews_obj
						.getString(TagConstants.TAG_NEWSENDDATE);
				oldnews_helperobj.setOldnews_enddate(currentnews_enddate);
				String currentnews_text = oldnews_obj
						.getString(TagConstants.TAG_NEWSTEXT);
				oldnews_helperobj.setOldnews_text(currentnews_text);
				String currentnews_image = oldnews_obj
						.getString(TagConstants.TAG_NEWSIMAGE);
				oldnews_helperobj.setOldnews_image(currentnews_image);
				oldnews_data.add(oldnews_helperobj);
			}
			helper_obj.setOldnews_vec(oldnews_data);

			news_data.add(helper_obj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
