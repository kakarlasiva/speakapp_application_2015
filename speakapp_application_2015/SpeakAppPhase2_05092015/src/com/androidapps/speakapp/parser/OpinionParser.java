package com.androidapps.speakapp.parser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.eforslag.EforseLogConstants;
import com.androidapps.speakapp.eforslag.EforslagLoginActivity;
import com.androidapps.speakapp.util.Contactdetailsetter;
import com.androidapps.speakapp.util.OpinionViewHelper;

public class OpinionParser {
	String url;
	Contactdetailsetter contact_details;
	Context c;
	InputStream in;
	String json,subjectid;
	JSONArray jobj,jmenus;
	Map<String, String> subjects_map;
	Dialog custom;
	ProgressDialog progress;
	
	public static OpinionViewHelper opinion_helper;
	
	

	public OpinionParser(Context applicationContext) {
		// TODO Auto-generated constructor stub
		this.c=applicationContext;
	}

	public String execute(String url, OpinionViewHelper opinion_obj) {
		
		 InputStream inputStream = null;
	        String result = "";
	        try {
	 
	            HttpClient httpclient = new DefaultHttpClient();
	             String json = "";
	         
	            JSONObject jsonObject = new JSONObject();
	            jsonObject.put("Email", opinion_obj.getEpost());
                jsonObject.put("Subject",opinion_obj.getSubject_whole_data().get(opinion_obj.getSelectedItem()));
	            jsonObject.put("Text", opinion_obj.getMessage());
	            jsonObject.put("Telephone", opinion_obj.getTelephone());
	            jsonObject.put("Name",opinion_obj.getName() );
	            jsonObject.put("DatePosted","");
	            JSONObject jsonnn = new JSONObject();
	            jsonnn.put("Message",jsonObject);
	            jsonnn.put("DeviceId", opinion_obj.getDeviceId());
	            json = jsonnn.toString();
	            String encodedUrl = URLEncoder.encode(json, "UTF-8");
	            
	            StringEntity se = new StringEntity(encodedUrl);
	            HttpPost httpPost = new HttpPost(url+encodedUrl);
	            // 7. Set some headers to inform server about the type of the content   
	            httpPost.setHeader("Accept", "application/json");
	            httpPost.setHeader("Content-type", "application/json");
	 
	            // 8. Execute POST request to the given URL
	            HttpResponse httpResponse = httpclient.execute(httpPost);
	 
	            // 9. receive response as inputStream
	            inputStream = httpResponse.getEntity().getContent();
	 
	            // 10. convert inputstream to string
	            if(inputStream != null)

	            {
	            	BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream, "UTF-8"), 8);
	                String line = "";
	                String result1 = "";
	                while((line = bufferedReader.readLine()) != null)
	                    result1 += line;
	                Log.d("************", result1);
	                inputStream.close();
	                
	                if(null != result1)
	                	return result1;
	              
	            }
	            else
	                result = "Did not work!";
	 
	        } catch (Exception e) {
//	            Log.d("InputStream", e.getLocalizedMessage());
	          e.printStackTrace();
	        }
	 
	        // 11. return result
	        return result;
	        
		
	}
	// convert InputStream to String
	 private static String getStringFromInputStream(InputStream is) {

	  BufferedReader br = null;
	  StringBuilder sb = new StringBuilder();

	  String line;
	  try {

	   br = new BufferedReader(new InputStreamReader(is));
	   while ((line = br.readLine()) != null) {
	    sb.append(line);
	   }

	  } catch (IOException e) {
	   e.printStackTrace();
	  } finally {
	   if (br != null) {
	    try {
	     br.close();
	    } catch (IOException e) {
	     e.printStackTrace();
	    }
	   }
	  }

	  return sb.toString();

	 }
	
	public JSONArray getjsonfromurl(String menu_url) {

		HttpURLConnection http = null;
		URL url = null;
		try {
			url = new URL(menu_url);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (url.getProtocol().toLowerCase().equals("https")) {
			
			HttpsURLConnection https = null;
			try {
				https = (HttpsURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			http = https;
		} else {
			try {
				http = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			in = new BufferedInputStream(http.getInputStream());
		} catch (Exception e) {
			// TODO: handle exception
		}
		// in=ctx.getResources().openRawResource(R.raw.menuview);
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					in, "utf-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			in.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			if (null != json)
				jobj = new JSONArray(json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		return jobj;

	}

	public void getsubjects(String url) {
		// TODO Auto-generated method stub

        jmenus = getjsonfromurl(url);
     	opinion_helper = new OpinionViewHelper();
     	subjects_map = new HashMap<String, String>();
		ArrayList<String> subjectsdata = new ArrayList<String>();
		
		try
		{
			for(int i=0;i<jmenus.length();i++)
			{
				JSONObject subjects = jmenus.getJSONObject(i);
				String subjects_values = subjects.getString("Text");
				String subjects_id = subjects.getString("Id");
				subjects_map.put(subjects_values,subjects_id);
				subjectsdata.add(subjects_values);
				
			}
			
			opinion_helper.setSubjects(subjectsdata);
			opinion_helper.setSubject_whole_data(subjects_map);
		}
		catch(Exception e)
		{
			
		}
	}
	}

