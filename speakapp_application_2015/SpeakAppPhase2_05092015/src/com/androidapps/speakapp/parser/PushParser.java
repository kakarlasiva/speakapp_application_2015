package com.androidapps.speakapp.parser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Vector;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.log.MyDebug;
import com.androidapps.speakapp.util.BasicInfo;
import com.androidapps.speakapp.util.TagConstants;

public class PushParser implements Serializable {

	InputStream in = null;
	static JSONObject basic_jobj = null;
	static String basic_json = null;
	static String basic_parse_url = null;
	Context ctx;

	public PushParser(Context ctx) {
		// TODO Auto-generated constructor stub
		this.ctx = ctx;
	}

	/***
	 * always verify the host - dont check for certificate
	 */
	final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};

	/**
	 * Trust every server - dont check for any certificate
	 */
	private static void trustAllHosts() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[] {};
			}

			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub

			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain,
					String authType)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub

			}

		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public JSONObject basic_getjsonfromurl(String bparse_url) {

		HttpURLConnection http = null;
		URL url = null;
		try {
			url = new URL(bparse_url);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (url.getProtocol().toLowerCase().equals("https")) {
			trustAllHosts();
			HttpsURLConnection https = null;
			try {
				https = (HttpsURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			https.setHostnameVerifier(DO_NOT_VERIFY);
			http = https;
		} else {
			try {
				http = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			in = new BufferedInputStream(http.getInputStream());
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					in, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {

				sb.append(line + "\n");
			}
			in.close();
			basic_json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			basic_jobj = new JSONObject(basic_json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		return basic_jobj;

	}

	public void parse_basicjsonparser(String p_basicurl) {

		JSONObject basic_obj = basic_getjsonfromurl(p_basicurl);

	}

}
