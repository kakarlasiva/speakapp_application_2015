package com.androidapps.speakapp.parser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Vector;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.androidapps.speakapp.util.QuestionHelper;
import com.androidapps.speakapp.util.TagConstants;

import android.content.Context;
import android.util.Log;

public class QuestionReader {

	public Vector<QuestionHelper> questions_data;
	public Vector<QuestionHelper.OptionsData> options_data;
	public Vector<QuestionHelper.SurveyData> survey_data;
	public QuestionHelper helper_obj;
	QuestionHelper.OptionsData opt_helperobj;
	QuestionHelper.SurveyData surv_helperobj;

	InputStream in = null;
	static JSONObject jobj = null;
	static String json = null;
	Context context;

	public QuestionReader(Context mtx) {
		// TODO Auto-generated constructor stub
		this.context = mtx;
		questions_data = new Vector<QuestionHelper>();
		options_data = new Vector<QuestionHelper.OptionsData>();
		survey_data = new Vector<QuestionHelper.SurveyData>();
	}

	public void clear() {
		questions_data.clear();
		options_data.clear();
		survey_data.clear();
	}

	/***
	 * always verify the host - dont check for certificate
	 */
	final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};

	/**
	 * Trust every server - dont check for any certificate
	 */
	private static void trustAllHosts() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[] {};
			}

			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub

			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain,
					String authType)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub

			}

		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public JSONObject getjsonfromurl(String menu_url) {

		HttpURLConnection http = null;
		URL url = null;
		try {
			url = new URL(menu_url);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (url.getProtocol().toLowerCase().equals("https")) {
			trustAllHosts();
			HttpsURLConnection https = null;
			try {
				https = (HttpsURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			https.setHostnameVerifier(DO_NOT_VERIFY);
			http = https;
		} else {
			try {
				http = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			in = new BufferedInputStream(http.getInputStream());
		} catch (Exception e) {
			// TODO: handle exception
		}
		// in=ctx.getResources().openRawResource(R.raw.menuview);
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					in, "utf-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			in.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			if (null != json)
				jobj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		return jobj;

	}
    //JSONObject question_array;
	public void parse_json(String url) {
		Log.i("JSON Parser", "--------url-------- " +url);
		JSONObject jmenus = getjsonfromurl(url);
		helper_obj=new QuestionHelper();
		try {
			//question_array=jmenus.getJSONObject(TagConstants.TAG_QUESTION);
			String error_code=jmenus.getString(TagConstants.TAG_ERROR_CODE);
			helper_obj.setError_code(error_code);
			
			int id=jmenus.getInt(TagConstants.TAG_QUESTIONID);
			helper_obj.setQuestion_id(id);
			String question=jmenus.getString(TagConstants.TAG_QUESTIONNAME);
			helper_obj.setQuestion(question);
			String ans_status=jmenus.getString(TagConstants.TAG_ANSWERSTATUS);
			helper_obj.setAns_status(ans_status.equalsIgnoreCase("true")?true:false);
			JSONArray option_array=jmenus.getJSONArray(TagConstants.TAG_OPTIONS);
			options_data=new Vector<QuestionHelper.OptionsData>();
			for(int i=0;i<option_array.length();i++){
				JSONObject opt_object=option_array.getJSONObject(i);
				opt_helperobj=helper_obj.new  OptionsData();
				int option_id=opt_object.getInt(TagConstants.TAG_OPTIONID);
				opt_helperobj.setOption_id(option_id);
				String option_name=opt_object.getString(TagConstants.TAG_OPTIONNAME);
				opt_helperobj.setOption_name(option_name);
				options_data.add(opt_helperobj);
			}
			Log.i("JSON Parser", "--------options-------- " +options_data.size());
			helper_obj.setOptions(options_data);
			JSONArray survey_array=jmenus.getJSONArray(TagConstants.TAG_SURVEY);
			survey_data=new Vector<QuestionHelper.SurveyData>();
			for(int j=0;j<survey_array.length();j++){
				JSONObject sur_obj=survey_array.getJSONObject(j);
				surv_helperobj=helper_obj.new SurveyData();
				String sur_name=sur_obj.getString(TagConstants.TAG_SURVEY_NAME);
				surv_helperobj.setSurveyoption_name(sur_name);
				int value=sur_obj.getInt(TagConstants.TAG_SURVEY_VALUE);
				surv_helperobj.setValue(value);
				String sur_color=sur_obj.getString(TagConstants.TAG_SURVEY_COLOR);
				surv_helperobj.setColor(sur_color);
				survey_data.add(surv_helperobj);
			}
			helper_obj.setSurvey(survey_data);
			
			questions_data.add(helper_obj);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
