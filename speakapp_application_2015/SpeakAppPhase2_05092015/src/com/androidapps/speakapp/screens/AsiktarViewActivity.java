package com.androidapps.speakapp.screens;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.parser.OpinionParser;
import com.androidapps.speakapp.util.JustifiedTextView;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;
import com.androidapps.speakapp.util.OpinionViewHelper;

public class AsiktarViewActivity extends Activity implements OnClickListener {
	// ImageView cross_image;
	// Spinner spinnerOsversions;
	BackgroundProcess background_process;
	ProgressDialog progress;
	OpinionViewHelper opinion_obj;
	String selState, telephonestatus;
	CheckBox agree_terms;
	Button sendData;
	EditText message, Name, telephone, epost;
	SpinnerAdapter adapter_obj;
	ImageView top_info, check_info, subjectimg;
	LinearLayout subjectLayout, back_layout;
	TextView subjectTxt, title_txt, header_txt;
	TextView text_hint, tel_hint, epost_hint;
	Dialog custom;
	// LinearLayout layout_text,layout_name,layout_tele,layout_epost;
	Calendar c;
	SimpleDateFormat format;
	final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	JustifiedTextView justify_textview;
	String[] subjectArray = new String[1];
	FrameLayout frame_info;
	ScrollView sv;
	InputMethodManager inputManager;

	/**
	 * This activity is used for displaying the Asikter view with all the
	 * functionalities.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		doBackground(true);
		setContentView(R.layout.asikter_lay);
		inputManager = 
    	        (InputMethodManager) this.
    	            getSystemService(Context.INPUT_METHOD_SERVICE); 
	 sv = (ScrollView)findViewById(R.id.asiktar_scroll);
		init();

	}

	/**
	 * This method is used for initializing the UI Components.
	 */
	private void init() {
		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage(getResources().getString(R.string.loading));
		back_layout = (LinearLayout) findViewById(R.id.back_layout);
		c = Calendar.getInstance();
		format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss aa");
		sendData = (Button) findViewById(R.id.senddata);
		sendData.setTypeface(DataEngine.gettypeface(this));
		GradientDrawable background = (GradientDrawable) sendData
				.getBackground();
		background.setColor(Color.parseColor("#000000"));
		background.setStroke(0, Color.parseColor("#000000"));
		// spinnerOsversions = (Spinner) findViewById(R.id.spinner1);
		title_txt = (TextView) findViewById(R.id.asiktar_title);
		title_txt.setTypeface(DataEngine.gettypeface(this));
		header_txt = (TextView) findViewById(R.id.asikter_header);
		frame_info = (FrameLayout) findViewById(R.id.title_layout);
		header_txt.setTypeface(DataEngine.gettypeface(this));
		
		message = (EditText) findViewById(R.id.Text);
		
		message.setHint(getString(R.string.hint_message));
		
		message.setTypeface(DataEngine.gettypeface(this));
		agree_terms = (CheckBox) findViewById(R.id.check_agree_opinion);
		if(MethodsHelper.getDensityName(this))
		agree_terms.setPadding((int) (agree_terms.getPaddingLeft() + getResources().getDimension(R.dimen.chkbox_padding)),
				agree_terms.getPaddingTop(), agree_terms.getPaddingRight(),
				agree_terms.getPaddingBottom());
		agree_terms.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Name.getText().toString().length() == 0) {
					Name.setHint(getString(R.string.hint_name)
							+" "+ getString(R.string.optional));
				}
				if (telephone.getText().toString().length() == 0) {
					telephone.setHint(getString(R.string.hint_telephone)
							+" "+ getString(R.string.optional));
				}
				if (message.getText().toString().length() == 0) {
					message.setHint(getString(R.string.hint_message));
				}
				if (epost.getText().toString().length() == 0) {
					epost.setHint(getString(R.string.hint_epost)
							+" "+ getString(R.string.optional));
				}

			}
		});
		agree_terms.setTypeface(DataEngine.gettypeface(this));
		Name = (EditText) findViewById(R.id.Name);
		Name.setHint(getString(R.string.hint_name) + " "
				+ getString(R.string.optional));
		

		Name.setTypeface(DataEngine.gettypeface(this));
		telephone = (EditText) findViewById(R.id.Telephone);
		telephone.setHint(getString(R.string.hint_telephone) + " "
				+ getString(R.string.optional));
		
		telephone.setTypeface(DataEngine.gettypeface(this));
		epost = (EditText) findViewById(R.id.Epost);
		epost.setHint(getString(R.string.hint_epost) + " "
				+ getString(R.string.optional));
		epost.setOnEditorActionListener(
		        new EditText.OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        if (actionId == EditorInfo.IME_ACTION_DONE ||
		                event.getAction() == KeyEvent.ACTION_DOWN &&
		                event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
		        
		        	inputManager.hideSoftInputFromWindow(epost.getWindowToken(), 0);
		        			sv.scrollTo(0, sv.getBottom());
		            return true;
		        }
		        return false;
		    }
		});
		epost.setTypeface(DataEngine.gettypeface(this));
		top_info = (ImageView) findViewById(R.id.p_settings);
		check_info = (ImageView) findViewById(R.id.info);
		subjectTxt = (TextView) findViewById(R.id.subject_text);
		subjectTxt.setTypeface(DataEngine.gettypeface(this));
		subjectTxt.setText("�mne:");
		subjectimg = (ImageView) findViewById(R.id.right_image);
		subjectimg.setVisibility(View.VISIBLE);
		subjectLayout = (LinearLayout) findViewById(R.id.subjectLay);
		set_listeners();
		if (null != opinion_obj) {
			int s = opinion_obj.getSubjects().size();
		}

		EditText[] totalEditTexts = { epost, telephone, epost, Name, message };
		setTypeface(totalEditTexts);
		frame_info.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				Intent info_activity = new Intent(AsiktarViewActivity.this,
						InfoActivity.class);
				info_activity.putExtra(DataEngine.INFO_OBJECT, getResources()
						.getString(R.string.asik_upinfo));
				startActivity(info_activity);
				return true;
			}
		});

	}

	private void setTypeface(EditText[] editTexts) {
		for (EditText editText : editTexts) {
			editText.setTypeface(DataEngine.gettypeface(this));
			GradientDrawable backgrounddesign = (GradientDrawable) editText
					.getBackground();
			backgrounddesign.setColor(Color.parseColor("#ffffff"));
			backgrounddesign.setStroke(2, Color.parseColor("#000000"));

		}
	}

	ArrayList<String> listSubContext = null;

	private void set_hints() {
		if (Name.getText().toString().length() == 0) {
			Name.setHint(getString(R.string.hint_name)
					+" "+ getString(R.string.optional));
		}
		if (telephone.getText().toString().length() == 0) {
			telephone.setHint(getString(R.string.hint_telephone)
					+" "+ getString(R.string.optional));
		}
		if (message.getText().toString().length() == 0) {
			message.setHint(getString(R.string.hint_message));
		}
		if (epost.getText().toString().length() == 0) {
			epost.setHint(getString(R.string.hint_epost)
					+" "+ getString(R.string.optional));
		}
	}

	// onClick of button perform this simplest code.
	private void showSubjectDilaog() {
		// set_hints();

		final Dialog dialog = new Dialog(AsiktarViewActivity.this);
		dialog.setContentView(R.layout.list_sub_context);
		dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		ListView list = (ListView) dialog.findViewById(R.id.listSubContext);
		dialog.setTitle("�mne");
		((TextView) dialog.findViewById(android.R.id.title))
				.setTypeface(DataEngine.gettypeface(this));
		dialog.setCancelable(true);
		dialog.show();

		if (null != opinion_obj && opinion_obj.getSubjects().size() > 0) {
			listSubContext = opinion_obj.getSubjects();
		} else {
			ArrayList<String> subjectList = new ArrayList<String>();
			subjectList.add("�mne");
			listSubContext = subjectList;
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				AsiktarViewActivity.this, android.R.layout.simple_list_item_1,
				listSubContext) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(DataEngine
						.gettypeface(AsiktarViewActivity.this));
				return v;

			}
		};
		if (null != adapter)
			list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long arg3) {
				// view.setSelected(true);

				setSelectedValue(position);
				dialog.dismiss();
			}
		});
	}

	private void setSelectedValue(int position) {
		selState = listSubContext.get(position);
		subjectTxt.setText(selState);
		subjectimg.setVisibility(View.GONE);
	}

	public boolean changedManually = false;
	public int temp = -1;
	private OnItemSelectedListener subject_listener = new AdapterView.OnItemSelectedListener() {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			Log.i("ontouch", "SELSTATIONL>>> " + pos);
			adapter_obj.notifyDataSetChanged();
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
			TextView txt = (TextView) view.findViewById(R.id.subject_text);
			txt.setTextColor(Color.WHITE);
			if (null != opinion_obj && null != opinion_obj.getSubjects()
					&& opinion_obj.getSubjects().size() > 0) {
				selState = String.valueOf(opinion_obj.getSubjects().get(pos));

			}

		}

		public void onNothingSelected(AdapterView<?> parent) {

		}
	};

	public boolean phone(String mobileNo) {
		// TODO Auto-generated method stub
		if (null != mobileNo && !"".equalsIgnoreCase(mobileNo)) {
			if (mobileNo.length() < 10 || mobileNo.length() > 18) {
				return false;
			} else {
				String countryCode = (String) mobileNo.subSequence(0, 4);
				String fiveDigitCode = (String) mobileNo.subSequence(0, 5);
				String twoDigitCode = (String) mobileNo.subSequence(0, 2);
				String temp = mobileNo;
				if (twoDigitCode.equalsIgnoreCase("07")
						|| fiveDigitCode.equalsIgnoreCase("00467")
						|| countryCode.equalsIgnoreCase("+467")
						|| countryCode.equalsIgnoreCase("0091")) {
					if (twoDigitCode.equalsIgnoreCase("07")) {
						if (mobileNo.length() > 10)
							return false;
						return isValidNumber(temp.substring(2, temp.length()));
					}
					if (fiveDigitCode.equalsIgnoreCase("00467")) {
						if (mobileNo.length() > 13)
							return false;
						return isValidNumber(temp.substring(5, temp.length()));
					}
					if (countryCode.equalsIgnoreCase("+467")
							|| countryCode.equalsIgnoreCase("0091")) {
						if (countryCode.equalsIgnoreCase("0091")
								&& mobileNo.length() > 14)
							return false;
						if (countryCode.equalsIgnoreCase("+467")
								&& mobileNo.length() > 12)
							return false;
						return isValidNumber(temp.substring(4, temp.length()));
					}

				} else {
					return false;
				}
			}
		}
		return false;

	}

	private boolean isValidNumber(String numb) {
		try {
			Long.parseLong(numb);
			// sendData.setEnabled(true);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			// sendData.setEnabled(false);
			return false;
		}
	}

	boolean isEmailValid(CharSequence email) {

		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	} // end of email matcher

	private void doBackground(boolean showprogress) {
		background_process = new BackgroundProcess(showprogress);
		if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
			background_process.execute();

		} else {
			showToast(getResources().getString(R.string.network_error));		}
	}

	/**
	 * 
	 * @author Speakapp This class is used for backgroundprocess for submitting
	 *         the form
	 * 
	 */
	class BackgroundProcess extends AsyncTask<Void, Void, Void> {
		boolean showprogress = false;

		public BackgroundProcess(boolean progress) {
			// TODO Auto-generated constructor stub
			this.showprogress = progress;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			try {
				if (showprogress) {
					progress.show();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			DataEngine.SUBJECTS_URL = DataEngine.BASE_URL + DataEngine.SUBJECTS;
			new OpinionParser(getApplicationContext())
					.getsubjects(DataEngine.SUBJECTS_URL);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			try {
				if (showprogress)
					if (progress.isShowing())
						progress.dismiss();
				opinion_obj = OpinionParser.opinion_helper;
				if (opinion_obj.getSubjects().size() > 0) {

					// set_subjectdata();
				} else {
					/*Toast.makeText(AsiktarViewActivity.this,
							"Server not responding", Toast.LENGTH_SHORT).show();*/
					showToast("Server not responding");
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void set_subjectdata() {
		adapter_obj = new SpinnerAdapter(this);

	}

	private void set_listeners() {
		subjectLayout.setOnClickListener(this);
		back_layout.setOnClickListener(this);
		sendData.setOnClickListener(this);
		top_info.setOnClickListener(this);
		check_info.setOnClickListener(this);
		title_txt.setOnClickListener(this);
		// spinnerOsversions.setOnItemSelectedListener(subject_listener);
		message.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						set_hints();
						Name.requestFocus();

						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});

		Name.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						telephone.requestFocus();
						set_hints();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});

		telephone.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						epost.requestFocus();
						set_hints();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});

	}

	class SpinnerAdapter extends BaseAdapter {
		LayoutInflater inflater;

		public SpinnerAdapter(Context mtx) {
			// TODO Auto-generated constructor stub
			inflater = LayoutInflater.from(mtx);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return subjectArray.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				final ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder holder;
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.spinner_adapter, null);
				holder = new ViewHolder();
				holder.txt = (TextView) convertView
						.findViewById(R.id.subject_text);
				holder.txt.setTypeface(DataEngine
						.gettypeface(AsiktarViewActivity.this));
				holder.right_image = (ImageView) convertView
						.findViewById(R.id.right_image);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.txt.setText(subjectArray[position]);
			if (subjectArray[position].equals("�mne"))
				holder.right_image.setVisibility(View.VISIBLE);
			else
				holder.right_image.setVisibility(View.GONE);

			return convertView;
		}

		class ViewHolder {
			TextView txt;
			ImageView right_image;
		}

	}

	/***
	 * 
	 * @param email
	 * @return
	 */
	private boolean isValidEmail(String email) {
		// TODO Auto-generated method stub
		System.out.println("The email provided here is : " + email);
		if (email != null && !"".equalsIgnoreCase(email)) {
			// check and return true if it's ok..
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile(EMAIL_PATTERN);
			matcher = pattern.matcher(email);
			return matcher.matches();
		} else {
			return true;
		}
	}

	private void sethints() {
		if (Name.getText().toString().length() == 0) {
			Name.setHint(getString(R.string.hint_name)
					+" "+ getString(R.string.optional));
		}
		if (telephone.getText().toString().length() == 0) {
			telephone.setHint(getString(R.string.hint_telephone)
					+" "+ getString(R.string.optional));
		}
		if (message.getText().toString().length() == 0) {
			message.setHint(getString(R.string.hint_message));
		}
		if (epost.getText().toString().length() == 0) {
			epost.setHint(getString(R.string.hint_epost)
					+" "+ getString(R.string.optional));
		}
	}
	
	@Override
	protected void onDestroy() {		
		super.onDestroy();
		DataEngine.clickFlag = false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back_layout:
			finish();
			break;
		case R.id.asiktar_title:
			finish();
			break;
		case R.id.p_settings:
			// sethints();
			Intent info_activity = new Intent(this, InfoActivity.class);
			info_activity.putExtra(DataEngine.INFO_OBJECT, getResources()
					.getString(R.string.asik_upinfo));
			startActivity(info_activity);
			
			break;
		case R.id.info:
			sethints();
			
			Intent info2_activity = new Intent(this, InfoActivity.class);
			info2_activity.putExtra(DataEngine.INFO_OBJECT, getResources()
					.getString(R.string.asik_lowerinfo));
			startActivity(info2_activity);
			break;
		case R.id.subjectLay:
			showSubjectDilaog();
			break;
		case R.id.senddata:
			if (null != epost.getText().toString().trim()) {
				if (!isValidEmail(epost.getText().toString().trim())) {
					MethodsHelper.showToast("ogiltig e-post", AsiktarViewActivity.this);
					return;
				}
			}

			if (!checkValidations())
				return;
			telephonestatus = telephone.getText().toString();
			String deviceId = MethodsHelper
					.getDeviceId(AsiktarViewActivity.this);
			opinion_obj.setDeviceId(deviceId);
			opinion_obj.setSelectedItem(selState);
			opinion_obj.setMessage(message.getText().toString());
			opinion_obj.setName(Name.getText().toString());
			opinion_obj.setTelephone(telephone.getText().toString());
			opinion_obj.setEpost(epost.getText().toString());
			String date1 = format.format(c.getTime());
			String date2 = format.format(c.getTimeInMillis());
			opinion_obj.setDate(date1);
			if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
				new AsyncOpinionExecute(true).execute();

			} else {
				showToast(getResources().getString(R.string.network_error));

			}

			break;

		default:
			break;
		}
	}

	class AsyncOpinionExecute extends AsyncTask<String, String, String> {
		boolean showprogress = false;

		public AsyncOpinionExecute(boolean progress) {
			// TODO Auto-generated constructor stub
			this.showprogress = progress;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			try {
				if (showprogress) {
					progress.show();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			// "http://speakapp.solidpartner.com/API/SubmitQuery?jsonstring=",
			String result = new OpinionParser(AsiktarViewActivity.this)
					.execute(DataEngine.BASE_URL + DataEngine.ASIK_SEND,
							opinion_obj);
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			try {
				if (showprogress)
					if (progress.isShowing())
						progress.dismiss();
			} catch (Exception e) {
				// TODO: handle exception
			}

			Name.setText(null);
			epost.setText(null);
			message.setText(null);
			telephone.setText(null);
			selState = "";
			subjectTxt.setText("�mne:");
			subjectimg.setVisibility(View.VISIBLE);
			agree_terms.setChecked(false);
			processResult(result);
			super.onPostExecute(result);
		}

	}

	private void processResult(String result1) {
		if (result1.contains("200")) {
			sethints();
			custom = new Dialog(this);
			custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
			custom.setContentView(R.layout.alert_layout);
			custom.getWindow().setLayout(android.view.WindowManager.LayoutParams.FILL_PARENT, 
					android.view.WindowManager.LayoutParams.WRAP_CONTENT);
			custom.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			TextView dialogtext = (TextView) custom
					.findViewById(R.id.thanks_txt);
			dialogtext.setTypeface(DataEngine
					.gettypeface(AsiktarViewActivity.this));
			dialogtext.setText(R.string.thank_msg_opinion);
			TextView dialogtextcontent = (TextView) custom
					.findViewById(R.id.thanks_txt_content);
			dialogtextcontent.setTypeface(DataEngine
					.gettypeface(AsiktarViewActivity.this));
			dialogtextcontent.setVisibility(View.VISIBLE);
			dialogtextcontent.setText(R.string.thank_msg_opinion_content);
			Button dialogButton = (Button) custom.findViewById(R.id.thanks_ok);
			dialogButton.setTypeface(DataEngine
					.gettypeface(AsiktarViewActivity.this));
			GradientDrawable btnbg = (GradientDrawable) dialogButton
					.getBackground();
			btnbg.setColor(Color.parseColor("#474849"));
			dialogButton.setText(R.string.close);
			// if button is clicked, close the custom dialog
			dialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					custom.dismiss();
					// finish();
				}
			});
			custom.show();
		} else {
			showToast("SPEAK APP KAN INTE KOPPLA UPP MOT SERVERN JUST NU");
		}
	}

	/**
	 * This method is written for the fields validation
	 * 
	 * @return true or false
	 */
	private boolean checkValidations() {
		boolean validate = false;
		try{
		if (null != opinion_obj && null != opinion_obj.getSubject_whole_data().get(selState)) {
			validate = true;
		} else {
			showToast(getString(R.string.select_sub));
			return false;
		}

		if (message.getText().toString().length() > 0) {
			validate = true;
		} else {
			message.requestFocus();
			showToast(getString(R.string.text_missing));
			return false;
		}
		if (agree_terms.isChecked()) {
			validate = true;

		} else {
			showToast(getString(R.string.toast_for_checkbox));
			return false;
		}
		} catch(Exception e)
		{
			
		}
		return validate;
	}

	private void showToast(String toastString) {
		String msg = toastString.substring(0, 1).toUpperCase()+toastString.substring(1).toLowerCase();
		MethodsHelper.showToast(msg, this);
	}

}
