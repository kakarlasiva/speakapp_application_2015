package com.androidapps.speakapp.screens;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.eforslag.EforseLogConstants;
import com.androidapps.speakapp.eforslag.EforslagLoginRegisterview;
import com.androidapps.speakapp.eforslag.Instructions;
import com.androidapps.speakapp.eforslag.RecentPetitions;
import com.androidapps.speakapp.eforslag.WritePetition;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;

public class EforslagViewActivity extends Activity implements OnClickListener {
	//ImageView cross_image;
	LinearLayout seeforslag, lagforslag, eforslag,back_layout;
	protected static final int LOG_IN = 1000;
	private SharedPreferences settings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.eforslag_lay);
		settings = getSharedPreferences(EforseLogConstants.LOGIN_DETAILS,
				MODE_PRIVATE);
		if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			/*Toast.makeText(this,
					getResources().getString(R.string.network_error),
					Toast.LENGTH_SHORT).show();*/
			MethodsHelper.showToast(getResources().getString(R.string.network_error), this);
			
			startActivity(new Intent(EforslagViewActivity.this,
					InteractiveCube.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
		EforseLogConstants.errordata();
		init();
		setListeners();
	}
	
	@Override
	protected void onDestroy() {		
		super.onDestroy();
		DataEngine.clickFlag = false;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (EforseLogConstants.httpClient == null) {
			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean(EforseLogConstants.LOGIN_STATUS_INFO, false);
			editor.commit();
		}
	}

	private void init() {
		back_layout = (LinearLayout) findViewById(R.id.back_layout);
		seeforslag = (LinearLayout) findViewById(R.id.eforlay1);
		lagforslag = (LinearLayout) findViewById(R.id.eforlay2);
		eforslag = (LinearLayout) findViewById(R.id.eforlay3);
		GradientDrawable background = (GradientDrawable) seeforslag
				.getBackground();
		background.setColor(Color.parseColor("#FFc05a"));
		background.setStroke(0, Color.parseColor("#00000000"));
		GradientDrawable l_background = (GradientDrawable) lagforslag
				.getBackground();
		l_background.setColor(Color.parseColor("#FFc05a"));
		l_background.setStroke(0, Color.parseColor("#00000000"));
		GradientDrawable e_background = (GradientDrawable) eforslag
				.getBackground();
		e_background.setColor(Color.parseColor("#FFc05a"));
		e_background.setStroke(0, Color.parseColor("#00000000"));

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		try {
			resultCode = data.getIntExtra("RESULT", 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (requestCode == LOG_IN
				&& (resultCode == EforseLogConstants.LOGIN_SUCCESS || resultCode == EforseLogConstants.REGISTRATION_SUCCESS)) {
			startActivity(new Intent(EforslagViewActivity.this,
					WritePetition.class));
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void setListeners() {
		// TODO Auto-generated method stub
		back_layout.setOnClickListener(this);
		findViewById(R.id.eforlay1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(EforslagViewActivity.this,
						RecentPetitions.class));
			}
		});
		findViewById(R.id.eforlay2).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (EforseLogConstants.httpClient == null) {
					SharedPreferences.Editor editor = settings.edit();
					editor.putBoolean(EforseLogConstants.LOGIN_STATUS_INFO,
							false);
					editor.commit();
				}
				if (!settings.getBoolean(EforseLogConstants.LOGIN_STATUS_INFO,
						false)) {
					startActivityForResult(new Intent(
							EforslagViewActivity.this,
							EforslagLoginRegisterview.class), LOG_IN);
				} else {
					startActivity(new Intent(EforslagViewActivity.this,
							WritePetition.class));

				}
			}
		});
		findViewById(R.id.eforlay3).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(EforslagViewActivity.this,
						Instructions.class));
			}
		});
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back_layout:
			finish();
			break;

		default:
			break;
		}
	}

}
