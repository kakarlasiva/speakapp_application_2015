package com.androidapps.speakapp.screens;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.util.QuestionHelper;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class InfoActivity extends Activity {
	Dialog custom;
	String info_txt;
	private DialogInterface.OnDismissListener ondismiss;
	LinearLayout dismiss;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 Bundle extras=getIntent().getExtras();
		    if (null != extras) {
				info_txt = extras.getString(DataEngine.INFO_OBJECT);
		    }
		    shoe_alertdialog(info_txt);
	}
	
	
	private void shoe_alertdialog(String txt){
		
		custom = new Dialog(InfoActivity.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.contact_info_alert);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.width = LayoutParams.FILL_PARENT;
		lp.height = LayoutParams.FILL_PARENT;
		custom.getWindow().setAttributes(lp);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		Button ok_button = (Button) custom.findViewById(R.id.dialog_ok);
		dismiss=(LinearLayout)custom.findViewById(R.id.dismiss);
		ok_button.setTypeface(DataEngine.gettypeface(this));
		GradientDrawable btnbg = (GradientDrawable) ok_button
				.getBackground();
		btnbg.setColor(Color.parseColor("#474849"));
		TextView txt1 = (TextView) custom.findViewById(R.id.info_txt);
		LinearLayout lay=(LinearLayout)custom.findViewById(R.id.dismiss);
		lay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				custom.cancel();
				finish();
			}
		});
		txt1.setTypeface(DataEngine.gettypeface(this));
		txt1.setText(txt);
		dismiss.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				custom.cancel();
				finish();
				return true;
			}
		});
		ok_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				custom.cancel();
				finish();
			}

		});
		ondismiss = new DialogInterface.OnDismissListener(){ // initializing variable
            @Override
            public void onDismiss(DialogInterface dialog) {
            	custom.cancel();
                finish();
            }
        };
        custom.setOnDismissListener(ondismiss);
		custom.show();
	}
	
	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
	Log.i("Back","-------keydown");
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Log.i("Back","-------back");
		custom.cancel();
		finish();
		//super.onBackPressed();
	}*/

}
