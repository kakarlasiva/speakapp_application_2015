package com.androidapps.speakapp.screens;

import java.util.jar.Attributes.Name;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.parser.ContactDetailsParser;
import com.androidapps.speakapp.util.Contactdetailsetter;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;

public class KontaktMailViewActivity extends Activity implements
		OnClickListener {
	ProgressDialog progress;
	EditText contact_message, conact_name, contact_telphone, contact_epost;
	Button send;
	CheckBox agree_terms;
	String message, name, tel, epost;
	String emailPattern;
	Contactdetailsetter details;
	ImageView info, hur_logo, han_logo, radda_logo, undo_logo;
	LinearLayout back_layout;
	Dialog custom;
	TextView title, header;
	//LinearLayout msg_layout, name_layout, tel_layout, post_layout;
	final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	ScrollView sv;
	InputMethodManager inputManager;
	/**
	 * @author Speakapp This Activity is used for displaying and submit the user
	 *         information regarding the events to the author.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.kontack_mail_lay);
		if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			/*Toast.makeText(this,
					getResources().getString(R.string.network_error),
					Toast.LENGTH_SHORT).show();*/
			showToast(getResources().getString(R.string.network_error));
			startActivity(new Intent(KontaktMailViewActivity.this,
					InteractiveCube.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
		 inputManager = 
	    	        (InputMethodManager) this.
	    	            getSystemService(Context.INPUT_METHOD_SERVICE); 
		 sv = (ScrollView) findViewById(R.id.kontact_mail_scrolls);
		details = new Contactdetailsetter();
		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage(getResources().getString(R.string.loading));

		contact_message = (EditText) findViewById(R.id.message);
		contact_message.setHint(getString(R.string.message));
		contact_message.setTypeface(DataEngine.gettypeface(this));
		
		conact_name = (EditText) findViewById(R.id.name);
		conact_name.setTypeface(DataEngine.gettypeface(this));
		

		contact_telphone = (EditText) findViewById(R.id.tel);
		contact_telphone.setHint(getString(R.string.telephone)+" "+getString(R.string.optional));
		contact_telphone.setTypeface(DataEngine.gettypeface(this));
		

		contact_epost = (EditText) findViewById(R.id.epost);
		contact_epost.setTypeface(DataEngine.gettypeface(this));
		contact_epost.setOnEditorActionListener(
		        new EditText.OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        if (actionId == EditorInfo.IME_ACTION_DONE ||
		                event.getAction() == KeyEvent.ACTION_DOWN &&
		                event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {        
		        	
		        	
		        	inputManager.hideSoftInputFromWindow(contact_epost.getWindowToken(), 0);
		        			sv.scrollTo(0, sv.getBottom());
		            return true;
		        }
		        return false;
		    }
		});
		send = (Button) findViewById(R.id.senddata);
		GradientDrawable btnbg = (GradientDrawable) send.getBackground();
		btnbg.setColor(Color.parseColor("#000000"));
		btnbg.setStroke(0, Color.parseColor("#00000000"));
		agree_terms = (CheckBox) findViewById(R.id.check_agree);
		if(MethodsHelper.getDensityName(this))
		agree_terms.setPadding((int) (agree_terms.getPaddingLeft() + getResources().getDimension(R.dimen.chkbox_padding)),
				agree_terms.getPaddingTop(), agree_terms.getPaddingRight(),
				agree_terms.getPaddingBottom());	
		agree_terms.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				set_hints();

			}
		});
		agree_terms.setTypeface(DataEngine.gettypeface(this));
		info = (ImageView) findViewById(R.id.info);
		back_layout = (LinearLayout) findViewById(R.id.back_layout);
		hur_logo = (ImageView) findViewById(R.id.logo);
		han_logo = (ImageView) findViewById(R.id.hanninge_logo);
		radda_logo = (ImageView) findViewById(R.id.radda_logo);
		undo_logo = (ImageView) findViewById(R.id.ungdoms_logo);
		title = (TextView) findViewById(R.id.kontactmail_title);
		
		title.setTypeface(DataEngine.gettypeface(this));
		header = (TextView) findViewById(R.id.kontact_header);
		header.setTypeface(DataEngine.gettypeface(this));
		info.setOnClickListener(this);
		send.setOnClickListener(this);
		send.setTypeface(DataEngine.gettypeface(this));
		back_layout.setOnClickListener(this);
		hur_logo.setOnClickListener(this);
		han_logo.setOnClickListener(this);
		radda_logo.setOnClickListener(this);
		undo_logo.setOnClickListener(this);
		title.setOnClickListener(this);

		set_listeners();
		
		 EditText[] totalEdittexts = {contact_message, conact_name, contact_telphone, contact_epost};
		  setTypeface(totalEdittexts);
	}
	@Override
	protected void onDestroy() {
	
		super.onDestroy();
		DataEngine.clickFlag = false;
	}
	private void setTypeface(EditText[] editTexts) {
		  for (EditText editText : editTexts) {
		   editText.setTypeface(DataEngine.gettypeface(this));
		   GradientDrawable backgrounddesign = (GradientDrawable) editText
		     .getBackground();
		   backgrounddesign.setColor(Color.parseColor("#ffffff"));
		   backgrounddesign.setStroke(2, Color.parseColor("#000000"));
		   
		  }
		 }

	/**
	 * This method is used for setting the listeners.
	 */
	private void set_listeners() {
		contact_message.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						conact_name.requestFocus();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});
		conact_name.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				try {
					if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						contact_telphone.requestFocus();
						return true;
					}
				} catch (Exception e) {

				}
				return false;
			}
		});
		contact_telphone
				.setOnEditorActionListener(new OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						// TODO Auto-generated method stub
						try {
							if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
								contact_epost.requestFocus();
								return true;
							}
						} catch (Exception e) {

						}
						return false;
					}
				});

	}

	// onClick of button perform this simplest code.

	/**
	 * This method is used validation of the phone number
	 * 
	 * @param contact_telphone2
	 * @return
	 */
	public boolean phone(EditText contact_telphone2) {
		String mobileNo = contact_telphone2.getText().toString();
		// TODO Auto-generated method stub
		if (null != mobileNo && !"".equalsIgnoreCase(mobileNo)) {
			if (mobileNo.length() < 10 || mobileNo.length() > 18) {
				contact_telphone2.setError("Invalid Phone Number");
				send.setEnabled(false);
				return false;
			} else {
				String countryCode = (String) mobileNo.subSequence(0, 4);
				String fiveDigitCode = (String) mobileNo.subSequence(0, 5);
				String twoDigitCode = (String) mobileNo.subSequence(0, 2);
				String temp = mobileNo;
				if (twoDigitCode.equalsIgnoreCase("07")
						|| fiveDigitCode.equalsIgnoreCase("00467")
						|| countryCode.equalsIgnoreCase("+467")
						|| countryCode.equalsIgnoreCase("0091")) {
					if (twoDigitCode.equalsIgnoreCase("07")) {
						if (mobileNo.length() > 10)
							return false;
						return isValidNumber(temp.substring(2, temp.length()));
					}
					if (fiveDigitCode.equalsIgnoreCase("00467")) {
						if (mobileNo.length() > 13)
							return false;
						return isValidNumber(temp.substring(5, temp.length()));
					}
					if (countryCode.equalsIgnoreCase("+467")
							|| countryCode.equalsIgnoreCase("0091")) {
						if (countryCode.equalsIgnoreCase("0091")
								&& mobileNo.length() > 14)
							return false;
						if (countryCode.equalsIgnoreCase("+467")
								&& mobileNo.length() > 12)
							return false;
						return isValidNumber(temp.substring(4, temp.length()));
					}
					/*
					 * if (countryCode.contains("91") ||
					 * countryCode.contains("46") || countryCode.contains("1")
					 * || countryCode.contains("44") ||
					 * countryCode.contains("49") || countryCode.charAt(o) ==
					 * '0' || countryCode.charAt(o) == '+' ||
					 * countryCode.charAt(o) == '7') { // then fine...it's valid
					 * return true; } else { return false; }
					 */
				} else {
					return false;
				}
			}
		}
		return false;

	}

	private boolean isValidNumber(String numb) {
		try {
			Long.parseLong(numb);
			send.setEnabled(true);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			send.setEnabled(false);
			return false;
		}
	}

	/***
	 * This method is used for email validation
	 * 
	 * @param email
	 * @return
	 */
	private boolean isValidEmail(String email) {
		// TODO Auto-generated method stub
		System.out.println("The email provided here is : " + email);
		if (email != null && !"".equalsIgnoreCase(email)) {
			// check and return true if it's ok..
			Pattern pattern;
			Matcher matcher;
			pattern = Pattern.compile(EMAIL_PATTERN);
			matcher = pattern.matcher(email);
			return matcher.matches();
		} else {
			return true;
		}
	}

	boolean isEmailValid(CharSequence email) {

		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	} // end of email matcher

	public void Is_Valid_Person_Name(EditText edt) throws NumberFormatException {
		Object valid_name;
		if (edt.getText().toString().length() <= 0) {

			send.setEnabled(false);
			valid_name = null;
		} else if (!edt.getText().toString().matches("[a-zA-Z ]+")) {
			edt.setError("Accept Alphabets Only.");
			send.setEnabled(false);
			valid_name = null;
		} else {

			valid_name = edt.getText().toString();
			send.setEnabled(true);
		}

	}

	public void thankyoupopup() {
		final Dialog custom = new Dialog(KontaktMailViewActivity.this);
		custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		custom.setContentView(R.layout.contact_ok);
		custom.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		Button dialogButton = (Button) custom.findViewById(R.id.ring);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				custom.dismiss();

			}
		});
		custom.show();

	}

	private void set_hints() {
		if (contact_epost.getText().toString().length() == 0) {
			contact_epost.setHint(getString(R.string.post));
		}

		if (contact_message.getText().toString().length() == 0) {
			contact_message.setHint(getString(R.string.message));
		}
		if (contact_telphone.getText().toString().length() == 0) {
			contact_telphone.setHint(getString(R.string.telephone)+" "+getString(R.string.optional));
		}
		if (conact_name.getText().toString().length() == 0) {
			conact_name.setHint(getString(R.string.name));
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.senddata: {
			set_hints();
			message = contact_message.getText().toString();

			if (!checkValidations())
				return;
			name = conact_name.getText().toString();
			tel = contact_telphone.getText().toString();
			epost = contact_epost.getText().toString().trim();
			details.setName(name);
			details.setMessage(message);
			details.setTelephone(tel);
			details.setEpost(epost);
			if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
				new AsyncOpinionExecute(true).execute();

			} else {
				showToast(getResources().getString(R.string.network_error));

			}

			break;
		}
		case R.id.info: {
			set_hints();
			Intent info_activity = new Intent(this,
					InfoActivity.class);
			info_activity.putExtra(DataEngine.INFO_OBJECT,getResources().getString(R.string.contact_info));
			startActivity(info_activity);			
			break;
		}
		case R.id.back_layout: {
			finish();
			break;
		}
		case R.id.kontactmail_title: {
			finish();
			break;
		}
		case R.id.logo:
			set_hints();
			call_dialog("https://www.facebook.com/#!/haninge.ungodmsrad?fref=ts");
			break;

		case R.id.hanninge_logo:
			set_hints();
			call_dialog("http://www.haninge.se/");
			break;

		case R.id.radda_logo:
			set_hints();
			call_dialog("http://www.raddabarnen.se/lokalforeningar/ost/stockholms-lan/vasterhaninge/");
			break;

		case R.id.ungdoms_logo:
			set_hints();
			call_dialog("http://rbuf.se/");
			break;
		}

	}

	public void call_dialog(final String url) {

		final Dialog notify_dialog = new Dialog(this);
		notify_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		notify_dialog.setContentView(R.layout.call_alertdialog);
		notify_dialog.getWindow().setLayout(android.view.WindowManager.LayoutParams.FILL_PARENT, 
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		notify_dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		notify_dialog.setCanceledOnTouchOutside(true);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) notify_dialog.findViewById(R.id.number);
		text.setTypeface(DataEngine.gettypeface(this));
		text.setText("Vill du verkligen g� vidare till hemsidan?");

		Button dialogButton = (Button) notify_dialog.findViewById(R.id.ring);
		dialogButton.setTypeface(DataEngine.gettypeface(this));
		dialogButton.setText("Ja");
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				notify_dialog.dismiss();
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(intent);
			}
		});
		Button cancelButton = (Button) notify_dialog.findViewById(R.id.cancel);
		cancelButton.setTypeface(DataEngine.gettypeface(this));
		cancelButton.setText("Avbryt");
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				notify_dialog.dismiss();
			}
		});

		notify_dialog.show();

	}

	/**
	 * 
	 * @author Speakapp This class is used for the background process for the
	 *         submission form.
	 * 
	 */
	class AsyncOpinionExecute extends AsyncTask<String, String, String> {
		boolean showprogress = false;

		public AsyncOpinionExecute(boolean progress) {
			// TODO Auto-generated constructor stub
			this.showprogress = progress;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			try {
				if (showprogress) {
					progress.show();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			//"http://speakapp.solidpartner.com/API/SendEmail?jsonstring="
			String result = new ContactDetailsParser(
					KontaktMailViewActivity.this)
					.execute(DataEngine.BASE_URL+DataEngine.SEND_MAIL,details);
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			try {
				if (showprogress)
					if (progress.isShowing())
						progress.dismiss();
			} catch (Exception e) {
				// TODO: handle exception
			}

			conact_name.setText("");
			contact_epost.setText("");
			contact_message.setText("");
			contact_telphone.setText("");
			agree_terms.setChecked(false);
			processResult(result);
			super.onPostExecute(result);
		}

	}

	/**
	 * This method is used for checking the response.
	 * 
	 * @param result1
	 */
	private void processResult(String result1) {
		if (result1.contains("200")) {
			set_hints();
			custom = new Dialog(this);
			custom.requestWindowFeature(Window.FEATURE_NO_TITLE);
			custom.setContentView(R.layout.contact_ok);
			custom.getWindow().setLayout(android.view.WindowManager.LayoutParams.FILL_PARENT, 
					android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		
			custom.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			Button dialogButton = (Button) custom.findViewById(R.id.ring);
			TextView thanks_txt=(TextView)custom.findViewById(R.id.Thankingtext);
			dialogButton.setText("Okej");
			thanks_txt.setTypeface(DataEngine.gettypeface(this));
			dialogButton.setTypeface(DataEngine.gettypeface(this));
			dialogButton.setTextColor(Color.WHITE);
			// if button is clicked, close the custom dialog
			GradientDrawable btnbg = (GradientDrawable) dialogButton
					.getBackground();
			btnbg.setColor(Color.parseColor("#474849"));
			dialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					custom.dismiss();

				}
			});
			custom.show();

		} else if (result1.contains("202")) {
			showToast("Server Busy.");
		} else if (result1.contains("203")) {
			showToast("Ogiltiga Ing�ngar.");
		} else {
			showToast("SPEAK APP KAN INTE KOPPLA UPP MOT SERVERN JUST NU.");
		}
	}

	/**
	 * This method is used for the checkbox validation
	 * 
	 * @return
	 */
	private boolean checkValidations() {
		boolean validate = false;
		if (null != contact_message.getText().toString()
				&& contact_message.getText().toString().length() > 0) {
			validate = true;
		} else {
			contact_message.requestFocus();
			showToast(getString(R.string.enter_msg));
			return false;
		}
		if (null != conact_name.getText().toString()
				&& conact_name.getText().toString().length() > 0) {
			validate = true;
		} else {
			conact_name.requestFocus();
			showToast(getString(R.string.enter_name));
			return false;
		}
		if (null != contact_epost.getText().toString().trim()
				&& contact_epost.getText().toString().length() > 0) {
			if (!isValidEmail(contact_epost.getText().toString().trim())) {
				Toast.makeText(this, "ogiltig e-post", Toast.LENGTH_SHORT)
						.show();
				return false;
			} else {
				validate = true;
			}
		} else {
			showToast(getString(R.string.enter_email));
			contact_epost.requestFocus();
			return false;
		}
		if (agree_terms.isChecked()) {
			validate = true;

		} else {
			showToast(getString(R.string.toast_for_checkbox));
			return false;
		}

		return validate;
	}

	private void showToast(String msg) {
		String fmsg = msg.substring(0, 1).toUpperCase()+msg.substring(1).toLowerCase();
		MethodsHelper.showToast(fmsg, this);
		//Toast.makeText(this, fmsg, Toast.LENGTH_SHORT).show();
	}

}