package com.androidapps.speakapp.screens;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;
import com.facebook.samples.facebook.DialogError;
import com.facebook.samples.facebook.Facebook;
import com.facebook.samples.facebook.Facebook.DialogListener;
import com.facebook.samples.facebook.FacebookError;

public class KontaktViewActivity extends Activity implements OnClickListener {
	ImageView call, fb, mail, talk, hur_logo, han_logo, radda_logo, undo_logo;
	final Context context = this;
	LinearLayout share_content, mail_content, back_layout;
	ScrollView scrollmail;
	Dialog dialog;
	EditText comment;
	TextView title, reb_txt;

	private Facebook facebook;
	private String messageToPost;
	// private static final String APP_ID = "565962740166168";
	private static final String[] PERMISSIONS = new String[] {
			"publish_stream", "manage_pages", "publish_actions" };
	private static final String TOKEN = "access_token";
	private static final String EXPIRES = "expires_in";
	private static final String KEY = "facebook-credentials";

	/**
	 * @author Speakapp This class is used for displaying the view of the
	 *         contact
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.kontack_lay);
		if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			/*
			 * Toast.makeText(this,
			 * getResources().getString(R.string.network_error),
			 * Toast.LENGTH_SHORT).show();
			 */
			showToast(getResources().getString(R.string.network_error));
			startActivity(new Intent(KontaktViewActivity.this,
					InteractiveCube.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
		if (android.os.Build.VERSION.SDK_INT > 9) {

			StrictMode.ThreadPolicy policy =

			new StrictMode.ThreadPolicy.Builder().permitAll().build();

			StrictMode.setThreadPolicy(policy);

		}
		call = (ImageView) findViewById(R.id.call);
		mail = (ImageView) findViewById(R.id.mail);
		fb = (ImageView) findViewById(R.id.facebook);
		talk = (ImageView) findViewById(R.id.talk);
		hur_logo = (ImageView) findViewById(R.id.logo);
		han_logo = (ImageView) findViewById(R.id.hanninge_logo);
		radda_logo = (ImageView) findViewById(R.id.radda_logo);
		undo_logo = (ImageView) findViewById(R.id.ungdoms_logo);
		title = (TextView) findViewById(R.id.kontact_title);
		title.setTypeface(DataEngine.gettypeface(this));
		// reb_txt=(TextView)findViewById(R.id.rebecka_txt);
		// reb_txt.setTypeface(DataEngine.gettypeface(this));

		share_content = (LinearLayout) findViewById(R.id.share);
		back_layout = (LinearLayout) findViewById(R.id.back_layout);

		call.setOnClickListener(this);
		mail.setOnClickListener(this);
		back_layout.setOnClickListener(this);
		fb.setOnClickListener(this);
		hur_logo.setOnClickListener(this);
		han_logo.setOnClickListener(this);
		radda_logo.setOnClickListener(this);
		undo_logo.setOnClickListener(this);
		title.setOnClickListener(this);

	}

	@Override
	protected void onDestroy() {

		super.onDestroy();
		DataEngine.clickFlag = false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.call: {
			final Dialog dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.call_alertdialog);
			dialog.getWindow().setLayout(
					android.view.WindowManager.LayoutParams.FILL_PARENT,
					android.view.WindowManager.LayoutParams.WRAP_CONTENT);
			dialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			dialog.setCanceledOnTouchOutside(true);
			// set the custom dialog components - text, image and button
			TextView text = (TextView) dialog.findViewById(R.id.number);
			text.setTypeface(DataEngine.gettypeface(this));
			// text.setText("+46 8 606 70 31");
			text.setText("+4670 606 08 09");

			Button dialogButton = (Button) dialog.findViewById(R.id.ring);
			dialogButton.setTypeface(DataEngine.gettypeface(this));
			// if button is clicked, close the custom dialog
			dialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					callToNumber();

				}
			});
			Button cancelButton = (Button) dialog.findViewById(R.id.cancel);
			cancelButton.setTypeface(DataEngine.gettypeface(this));
			cancelButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});

			dialog.show();
		}
			break;
		case R.id.mail: {

			Intent conatactdetails = new Intent(KontaktViewActivity.this,
					KontaktMailViewActivity.class);
			startActivity(conatactdetails);

		}
			break;
		case R.id.facebook: {
			/*
			 * Intent viewIntent = new Intent("android.intent.action.VIEW",
			 * Uri.parse("https://www.facebook.com/haninge.ungodmsrad"));
			 * startActivity(viewIntent);
			 */
			Intent facebookIntent = getOpenFacebookIntent(KontaktViewActivity.this);
			startActivity(facebookIntent);
		}
			break;

		case R.id.back_layout: {
			finish();
			break;
		}
		case R.id.kontact_title: {
			finish();
			break;
		}
		case R.id.logo:
			call_dialog("https://www.facebook.com/haninge.ungodmsrad?fref=ts");
			break;
		case R.id.hanninge_logo:
			call_dialog("http://www.haninge.se/");
			break;
		case R.id.radda_logo:
			call_dialog("http://www.raddabarnen.se/lokalforeningar/ost/stockholms-lan/vasterhaninge/");
			break;
		case R.id.ungdoms_logo:
			call_dialog("http://rbuf.se/");
			break;
		default:
			break;
		}

	}

	private void callToNumber() {
		try {
			PackageManager pm = getPackageManager();
			if (pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
				// has Telephony features.
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:+46706060809"));
				startActivity(callIntent);
			} else {
				MethodsHelper
						.showToast("Plattan har ingen ringfunktion!", this);
			}
		} catch (Exception e) {
			MethodsHelper.showToast("Plattan har ingen ringfunktion!", this);
		}
	}

	private Intent getOpenFacebookIntent(Context context) {
		String profileUrl = "https://www.facebook.com/haninge.ungodmsrad";
		try {
			context.getPackageManager()
					.getPackageInfo("com.facebook.katana", 0); // Checks if FB
																// is even
																// installed.
			return new Intent(Intent.ACTION_VIEW,
					Uri.parse("fb://profile/100006700713478")); // Trys to make
																// intent with
																// FB's URI
		} catch (Exception e) {
			return new Intent(Intent.ACTION_VIEW, Uri.parse(profileUrl)); // catches
																			// and
																			// opens
																			// a
																			// url
																			// to
																			// the
																			// desired
																			// page
		}
	}

	public void call_dialog(final String url) {

		final Dialog notify_dialog = new Dialog(context);
		notify_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		notify_dialog.setContentView(R.layout.call_alertdialog);
		notify_dialog.getWindow().setLayout(
				android.view.WindowManager.LayoutParams.FILL_PARENT,
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		notify_dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		notify_dialog.setCanceledOnTouchOutside(true);

		// set the custom dialog components - text, image and button
		TextView text = (TextView) notify_dialog.findViewById(R.id.number);
		text.setTypeface(DataEngine.gettypeface(this));
		text.setText("Vill du verkligen g� vidare till hemsidan? ");
		// text.setText("Vill du verkligen g� ");
		Button dialogButton = (Button) notify_dialog.findViewById(R.id.ring);
		dialogButton.setTypeface(DataEngine.gettypeface(this));
		dialogButton.setText("Ja");
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				notify_dialog.dismiss();
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(intent);
			}
		});
		Button cancelButton = (Button) notify_dialog.findViewById(R.id.cancel);
		cancelButton.setTypeface(DataEngine.gettypeface(this));
		cancelButton.setText("Avbryt");
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				notify_dialog.dismiss();
			}
		});

		notify_dialog.show();

	}

	public boolean saveCredentials(Facebook facebook) {
		Editor editor = getApplicationContext().getSharedPreferences(KEY,
				Context.MODE_PRIVATE).edit();
		editor.putString(TOKEN, facebook.getAccessToken());
		editor.putLong(EXPIRES, facebook.getAccessExpires());
		return editor.commit();
	}

	public boolean restoreCredentials(Facebook facebook) {
		SharedPreferences sharedPreferences = getApplicationContext()
				.getSharedPreferences(KEY, Context.MODE_PRIVATE);
		facebook.setAccessToken(sharedPreferences.getString(TOKEN, null));
		facebook.setAccessExpires(sharedPreferences.getLong(EXPIRES, 0));
		return facebook.isSessionValid();
	}

	public void loginAndPostToWall() {
		facebook.authorize(this, PERMISSIONS, Facebook.FORCE_DIALOG_AUTH,
				new LoginDialogListener());
	}

	public void postToWall(String message) {
		Bundle parameters = new Bundle();
		parameters.putString("message", message);
		parameters.putString("description", "topic share");
		// call_dialog();

		try {
			facebook.request("me");
			String response = facebook.request("100006578712936/feed",
					parameters, "POST");
			Log.d("Tests", "got response: " + response);
			if (response == null || response.equals("")
					|| response.equals("false")) {
				showToast("Blank response.");
			} else {
				showToast("Message posted to speak app wall!");
				dialog.dismiss();
			}
		} catch (Exception e) {
			showToast("Failed to post to wall!");
			e.printStackTrace();
		}
	}

	class LoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			saveCredentials(facebook);
			if (messageToPost != null) {
				postToWall(messageToPost);

			}
		}

		public void onCancel() {
			showToast("Authentication with Facebook cancelled!");
		}

		@Override
		public void onFacebookError(FacebookError e) {
			// TODO Auto-generated method stub
			showToast("Authentication with Facebook failed!");
		}

		@Override
		public void onError(DialogError e) {
			// TODO Auto-generated method stub
			showToast("Authentication with Facebook failed!");
		}
	}

	private void showToast(String msg) {
		String fmsg = msg.substring(0, 1).toUpperCase()
				+ msg.substring(1).toLowerCase();
		MethodsHelper.showToast(fmsg, this);
		// Toast.makeText(this, fmsg, Toast.LENGTH_SHORT).show();
	}

}