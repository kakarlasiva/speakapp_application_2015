package com.androidapps.speakapp.screens;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.database.MyDbAdapter;
import com.androidapps.speakapp.util.FavouritesUtil;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;
import com.androidapps.speakapp.util.SelectedNewsHelper;
import com.androidapps.speakapp.util.FavouritesUtil.Favouritesdata;

public class NewsFavourites extends Activity implements OnClickListener{
	final Handler my_Favourite_UI_Handler = new Handler();
	private static final int EVENTS_NULL = 4444;
	private static final int DIALOG_WORK_PROG = 1111;
	ProgressDialog workProgress_UP;
	FavouritesUtil helper_obj;
	TextView Data_null,header_txt;
	//ImageView cross_image;
	LinearLayout back_layout;
	ListView favlist;
	int j = 0;
	Handler myUiHandler = new Handler();
	FavouriteAdapter favadapter;
	public static int pos = 0;
	String[] months_names={ "januari", "februari", "mars", "april", "maj", "juni", "juli",
			"augusti", "september", "oktober", "november", "december" };
	String[] months={"01","02","03","04","05","06","07","08","09","10","11","12"};
	HashMap<String, String> map;
	Dialog dialog;
	SelectedNewsHelper sel_helperobj;
	FavouritesUtil.Favouritesdata f_vec_obj;
	Vector<FavouritesUtil.Favouritesdata> full_datavec;
	/**
	 * @author Speakapp
	 * This activity is used for showing the favorite list
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		try{
		setContentView(R.layout.news_fav);
		if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			/*Toast.makeText(this,
					getResources().getString(R.string.network_error),
					Toast.LENGTH_SHORT).show();*/
			MethodsHelper.showToast(getResources().getString(R.string.network_error), this);
			startActivity(new Intent(NewsFavourites.this,InteractiveCube.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		init();
		get_favlist();
		set_listeners();
	}

	/**
	 * This method is used for initializing the UI Components
	 */
	private void init() {
		favlist = (ListView) findViewById(R.id.fav_news_list);
		back_layout = (LinearLayout) findViewById(R.id.back_layout);
		header_txt=(TextView)findViewById(R.id.header_fav);
		header_txt.setTypeface(DataEngine.gettypeface(this));
		Data_null=(TextView)findViewById(R.id.null_data);
		Data_null.setTypeface(DataEngine.gettypeface(this));
		map=new HashMap<String, String>();
		for(int i=0;i<months.length;i++){
			map.put(months[i],months_names[i]);
		}
		helper_obj=new FavouritesUtil();
		favadapter=new FavouriteAdapter(NewsFavourites.this);
	}
	
	/**
	 * This method is used for setting the listeners.
	 */
	private void set_listeners() {
		back_layout.setOnClickListener(this);
		header_txt.setOnClickListener(this);
		favlist.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				set_selecteditems(position);
				Intent full_intent = new Intent(NewsFavourites.this,
						NewsViewActivity.class);
				full_intent.putExtra(DataEngine.SELECTED_OBJECT,sel_helperobj);
				full_intent.putExtra("info", position);
				startActivity(full_intent);
			}
			
		});
	}
	
	/**
     * This method is used for storing the selected one data into a particular object.
     * @param position Defines the particular position choosen by the user
     */
	private void set_selecteditems(int position) {
		sel_helperobj = new SelectedNewsHelper();
		int id = Integer.parseInt(helper_obj.getFav_fullvec().elementAt(position)
				.getF_id());
		sel_helperobj.setSelectednews_id(id);
		String title=helper_obj.getFav_fullvec().elementAt(position).getF_title();
		sel_helperobj.setSelectednews_title(title);
		String text=helper_obj.getFav_fullvec().elementAt(position).getF_description();
		sel_helperobj.setSelectednews_text(text);
		String image=helper_obj.getFav_fullvec().elementAt(position).getF_image();
		sel_helperobj.setSelectednews_image(image);
		String start_date=helper_obj.getFav_fullvec().elementAt(position).getF_date();
		sel_helperobj.setSelectednews_startdate(start_date);
		String end_date="";
		sel_helperobj.setSelectednews_enddate(end_date);
	}


	/**
	 * This method is used for getting the data from the database which is stored in the database
	 */
	private void get_favlist() {
		my_Favourite_UI_Handler.post(Show_Progress_Bar_F);
		if (DataEngine.my_adapter == null)
			DataEngine.open_database(this);
		if (null != DataEngine.my_adapter) {
			DataEngine.my_adapter.open();
			DataEngine.mydbCursor = DataEngine.my_adapter.get_allfavlist();
		}
		if ((DataEngine.my_adapter == null)
				|| (DataEngine.mydbCursor.isClosed())) {
			Log.i("DATABASE", "***Database is closed****");
		} else {

			if (null != DataEngine.mydbCursor
					&& DataEngine.mydbCursor.getCount() > 0) {
				
				full_datavec=new Vector<FavouritesUtil.Favouritesdata>();
				clear_vectors();
								if (DataEngine.mydbCursor.moveToFirst()) {
					while (DataEngine.mydbCursor.isAfterLast() == false) {
						f_vec_obj=helper_obj.new Favouritesdata();
						f_vec_obj.setF_id(DataEngine.mydbCursor
								.getString(DataEngine.mydbCursor
										.getColumnIndex(MyDbAdapter.NEWSID)));
						f_vec_obj.setF_title(DataEngine.mydbCursor
								.getString(DataEngine.mydbCursor
										.getColumnIndex(MyDbAdapter.TITLE)));
						
						f_vec_obj.setF_description(DataEngine.mydbCursor.getString(DataEngine.mydbCursor
								.getColumnIndex(MyDbAdapter.DESCRIPTION)));
						
						f_vec_obj.setF_date(DataEngine.mydbCursor
								.getString(DataEngine.mydbCursor
										.getColumnIndex(MyDbAdapter.DATE)));
					
						f_vec_obj.setF_image(DataEngine.mydbCursor
								.getString(DataEngine.mydbCursor
										.getColumnIndex(MyDbAdapter.IMAGE)));
						f_vec_obj.setF_newsid(DataEngine.mydbCursor
								.getString(DataEngine.mydbCursor
										.getColumnIndex(MyDbAdapter.NEWSID)));
						full_datavec.add(f_vec_obj);
						DataEngine.mydbCursor.moveToNext();
						j++;
					}
					helper_obj.setFav_fullvec(full_datavec);
				}
			} else {
				//showDialog(EVENTS_NULL);
				favlist.setVisibility(View.GONE);
				Data_null.setVisibility(View.VISIBLE);
			}
		}
		DataEngine.mydbCursor.close();
		DataEngine.my_adapter.close();
		try {
			my_Favourite_UI_Handler.post(Cancel_Progress_Bar_F);
			// if(data_engine.Channel_Name.size()>0)
			if ((null != helper_obj.getFav_fullvec()) && (helper_obj.getFav_fullvec().size() > 0)) {
				// if(myChannelCursor_Favourite.getCount()>0)
				my_Favourite_UI_Handler.post(Show_Favourite_List_channel_View);
				// else
				// setListAdapter(new EfficientAdapter(this));
			} else {
				fill_set_Favourite_channel_ListItems();
			}
		} catch (Exception e) {
			Log.i("fill-set-favorate-listitems", "Exception Occured");
		}

	}

	final Runnable Show_Favourite_List_channel_View = new Runnable() {
		public void run() {
			fill_set_Favourite_channel_ListItems();
		}
	};

	public void fill_set_Favourite_channel_ListItems() {
		for(int i=0;i<helper_obj.getFav_fullvec().size();i++){
		Log.i("fill-set-favorate-listitems", "in UserFavorate Channel Class"+helper_obj.getFav_fullvec().elementAt(i).getF_title());
		}

		try {
			Collections.sort(helper_obj.getFav_fullvec(), new Comparator<Favouritesdata>() {

			    @Override
			    public int compare(Favouritesdata p1, Favouritesdata p2) {
			     return p1.getF_date().compareToIgnoreCase(p2.getF_date());
			    }
			   });
			favlist.setAdapter(favadapter);
		} catch (Exception e) {
			System.out.println("In Setting List Items" + e);
		}
		// getListView().setSelector(R.drawable.list_item_selector);
	}

	final Runnable Show_Progress_Bar_F = new Runnable() {
		public void run() {
			progress_Show();
		}
	};
	final Runnable Cancel_Progress_Bar_F = new Runnable() {
		public void run() {
			progress_Stop();
		}
	};

	private void progress_Show() {
		showDialog(DIALOG_WORK_PROG);
	}

	private void progress_Stop() {
		workProgress_UP.dismiss();
	}

	public void clear_vectors() {
		
        full_datavec.clear();
	}

	/**
	 * @author Speakapp
	 * This class is the adapter class for listview
	 *
	 */
	class FavouriteAdapter extends BaseAdapter {
		LayoutInflater inflater;
		Context context;
		public FavouriteAdapter(Context ctx) {
			// TODO Auto-generated constructor stub
			inflater = LayoutInflater.from(ctx);
			context=ctx;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return helper_obj.getFav_fullvec().size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}
		String label;
		String preLabel;
		int pos;

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View view = (View)inflater.inflate(R.layout.news_adapter, null);
			LinearLayout header = (LinearLayout) view
					.findViewById(R.id.section);
			GradientDrawable fbackground = (GradientDrawable) header
					.getBackground();
			fbackground.setColor(Color.parseColor("#497286"));
			fbackground.setStroke(0, Color.parseColor("#000000"));
			LinearLayout list_layout = (LinearLayout) view
					.findViewById(R.id.news_lay);
			list_layout.setTag(position);

			if (position < helper_obj.getFav_fullvec().size()) {
				label = helper_obj.getFav_fullvec().elementAt(position).getF_date();
			}

			if (position == 0) {
				setSection(header, label);
			} else {
				if (position < helper_obj.getFav_fullvec().size())
					preLabel =  helper_obj.getFav_fullvec().elementAt(position-1).getF_date();
				if (!label.equalsIgnoreCase(preLabel)) {
					setSection(header, label);
				} else {
					header.setVisibility(View.GONE);
				}
			}
			try {
				TextView part_locationname = (TextView) view
						.findViewById(R.id.news_title);
				part_locationname.setTypeface(DataEngine.gettypeface(NewsFavourites.this));
				part_locationname.setText(helper_obj.getFav_fullvec().elementAt(position).getF_title());
				TextView ordertime = (TextView) view
						.findViewById(R.id.text);
				ordertime.setTypeface(DataEngine.gettypeface(NewsFavourites.this));
				ImageView fav_image=(ImageView)view.findViewById(R.id.fav_image);
				fav_image.setImageResource(R.drawable.fav_enabled);
				ordertime.setText(helper_obj.getFav_fullvec().elementAt(position).getF_description());
				fav_image.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						delete_Favourite_item(position);
					}
				});
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			

			return view;
		}
		private void setSection(LinearLayout header, String label) {
			LayoutInflater inflate = ((Activity) context).getLayoutInflater();
			View date_view = (View)inflate.inflate(R.layout.date_lay, null);
			TextView month=(TextView)date_view.findViewById(R.id.month_title);
			
			TextView month_date=(TextView)date_view.findViewById(R.id.month_date);
			month.setTypeface(DataEngine.gettypeface(NewsFavourites.this));
			month_date.setTypeface(DataEngine.gettypeface(NewsFavourites.this));
			TextView month_year = (TextView) date_view
					.findViewById(R.id.month_year);
			month_year.setTypeface(DataEngine.gettypeface(NewsFavourites.this));
			String[] s = label.split("T");
			
			
			month_date.setText(s[0].split("-")[2]);
			month.setText(map.get(s[0].split("-")[1]));
			month_year.setText(s[0].split("-")[0]);
			
			header.addView(date_view);
		}
		

	}
	
	/**
	 * This method is for unfavorite
	 * @param arg_pos
	 */
	private void delete_Favourite_item(int arg_pos) {
		if ((null != helper_obj.getFav_fullvec()) && (helper_obj.getFav_fullvec().size() > arg_pos)) {
			String item_urlToDel = helper_obj.getFav_fullvec().elementAt(arg_pos).getF_newsid();
			if (null != DataEngine.my_adapter) {
				if (!item_urlToDel.equals("")) {
					if (null != DataEngine.my_adapter) {
						DataEngine.my_adapter.open();

					}
					DataEngine.my_adapter
							.delete_favinformation(item_urlToDel);
					DataEngine.my_adapter.close();
					clear_vectors();

					get_favlist();

				}
			}
		}
	}

	@Override
	protected Dialog onCreateDialog(int dialogID) {
		// if(myDebug.debug_Log)Log.i("onCreateDialog","Dialog Create called...!");
		switch (dialogID) {
		case DIALOG_WORK_PROG: {
			workProgress_UP = new ProgressDialog(NewsFavourites.this);
			workProgress_UP.setMessage(getResources().getString(
					R.string.loading));
			workProgress_UP.setIndeterminate(true);
			workProgress_UP.setCancelable(false);// true);
			return workProgress_UP;
		}
		case EVENTS_NULL:
			showDialog();
		}

		return null;
	}
	
	/**
	 * This method is for showing the dialog when there are no events favorited.
	 */
	public void showDialog() {
		// TODO Auto-generated method stub
		dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alert_layout);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		TextView thanks=(TextView)dialog.findViewById(R.id.thanks_txt);
		thanks.setText("Du har inga sparade h�ndelser.");
		Button thanks_ok = (Button) dialog.findViewById(R.id.thanks_ok);
		GradientDrawable background = (GradientDrawable) thanks_ok
				.getBackground();
		background.setColor(Color.parseColor("#404040"));
		thanks_ok.setText("OK");
		dialog.show();
		thanks_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				NewsFavourites.this.finish();
			}
		});
		// onto
      
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back_layout:
			finish();
			break;
		case R.id.header_fav:
			finish();
			break;

		default:
			break;
		}
	}
}
