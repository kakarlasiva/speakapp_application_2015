package com.androidapps.speakapp.screens;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.database.MyDbAdapter;

import com.androidapps.speakapp.screens.PegangViewActivity.BackgroundProcess;
import com.androidapps.speakapp.twitter.Twitt_Sharing;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;
import com.androidapps.speakapp.util.NewsHelper;
import com.androidapps.speakapp.util.SelectedNewsHelper;
import com.facebook.samples.facebook.AsyncFacebookRunner;
import com.facebook.samples.facebook.AsyncFacebookRunner.RequestListener;
import com.facebook.samples.facebook.DialogError;
import com.facebook.samples.facebook.Facebook;
import com.facebook.samples.facebook.Facebook.DialogListener;
import com.facebook.samples.facebook.FacebookError;
import com.facebook.samples.facebook.SessionEvents;
import com.facebook.samples.facebook.SessionEvents.AuthListener;
import com.facebook.samples.facebook.SessionEvents.LogoutListener;
import com.facebook.samples.facebook.SessionStore;
import com.facebook.samples.facebook.Util;
import com.facebook.samples.fbconnect.FBConnectQueryProcessor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class NewsViewActivity extends Activity implements OnClickListener {
	ProgressDialog progress;
	BackgroundProcess background_process;
	Facebook mFacebook;
	private String fbId;

	AsyncFacebookRunner mAsyncFacebookRunner;
	ImageView news_image, share, fav_image;
	LinearLayout back_layout,pagang_layout;
	TextView news_header, news_body, month, month_date,header_txt,month_year;
	SelectedNewsHelper news_obj;
	HashMap<String, String> map;
	int position;
	Bitmap bi;
	String[] months_names = { "januari", "februari", "mars", "april", "maj", "juni", "juli",
			"augusti", "september", "oktober", "november", "december" };
	String[] months = { "01", "02", "03", "04", "05", "06", "07", "08", "09",
			"10", "11", "12" };
	File casted_image;
	Dialog share_dialog;
	Vector<Integer> newsid_vec;
	Vector<String> newstitle_vec;
	Handler handler;
	int j = 0;
	int id;
	LinearLayout setting_lay;
	/**
	 * @author Speakapp
	 * This activity is for displaying the full view when the user selects a particular event with full information.
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newsview_lay);
		Bundle extras = getIntent().getExtras();
		map = new HashMap<String, String>();
		if (null != extras) {
			news_obj = (SelectedNewsHelper) extras
					.getSerializable(DataEngine.SELECTED_OBJECT);
			position = extras.getInt("info");
			for (int i = 0; i < months.length; i++) {
				map.put(months[i], months_names[i]);
			}
		}
		init();

		doBackground(true);
	}

	/**
	 * This method is for initializing the UI Components
	 */
	private void init() {
		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		handler = new Handler();
		progress.setMessage(getResources().getString(R.string.loading));
		if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			/*Toast.makeText(this,
					getResources().getString(R.string.network_error),
					Toast.LENGTH_SHORT).show();*/
			MethodsHelper.showToast(getResources().getString(R.string.network_error), this);
			startActivity(new Intent(NewsViewActivity.this,InteractiveCube.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
		news_image = (ImageView) findViewById(R.id.newsimage);
		fav_image = (ImageView) findViewById(R.id.fav_image);
		news_header = (TextView) findViewById(R.id.newsheading);
		setting_lay=(LinearLayout)findViewById(R.id.setting_lay);
		news_header.setTypeface(DataEngine.gettypeface(this));
		news_body = (TextView) findViewById(R.id.newsbody);
		pagang_layout=(LinearLayout)findViewById(R.id.pagang_desclay);
		GradientDrawable fbackground = (GradientDrawable) pagang_layout
				.getBackground();
		fbackground.setColor(Color.parseColor("#497286"));
		fbackground.setStroke(0, Color.parseColor("#000000"));
		news_body.setTypeface(DataEngine.gettypeface(this));
		header_txt=(TextView)findViewById(R.id.pagang_detailheader);
		header_txt.setTypeface(DataEngine.gettypeface(this));
		share = (ImageView) findViewById(R.id.p_settings);
		month = (TextView) findViewById(R.id.month_title);
		month.setTypeface(DataEngine.gettypeface(this));
		month_date = (TextView) findViewById(R.id.month_date);
		month_date.setTypeface(DataEngine.gettypeface(this));
		month_year = (TextView) findViewById(R.id.month_year);
		month_year.setTypeface(DataEngine.gettypeface(this));
		
		back_layout = (LinearLayout) findViewById(R.id.back_layout);
		get_favlist();
		back_layout.setOnClickListener(this);
		share.setOnClickListener(this);
		id = news_obj.getSelectednews_id();
		if (newsid_vec.contains(id)) {
			fav_image.setImageResource(R.drawable.fav_enabled);
		} else {
			fav_image.setImageResource(R.drawable.fav_disabled);
		}
		fav_image.setOnClickListener(this);
		setting_lay.setOnClickListener(this);
		header_txt.setOnClickListener(this);
	}

	/**
	 * This method is used to get the data of the events which are stored in the database.
	 */
	private void get_favlist() {
		if (DataEngine.my_adapter == null)
			DataEngine.open_database(this);
		if (null != DataEngine.my_adapter) {
			DataEngine.my_adapter.open();
			DataEngine.mydbCursor = DataEngine.my_adapter.get_allfavlist();
		}
		if ((DataEngine.my_adapter == null)
				|| (DataEngine.mydbCursor.isClosed())) {
			Log.i("DATABASE", "***Database is closed****");
		} else {
			newsid_vec = new Vector<Integer>();
			newstitle_vec = new Vector<String>();
			clear_vectors();

			if (null != DataEngine.mydbCursor
					&& DataEngine.mydbCursor.getCount() > 0) {

				if (DataEngine.mydbCursor.moveToFirst()) {
					while (DataEngine.mydbCursor.isAfterLast() == false) {
						newsid_vec.add(DataEngine.mydbCursor
								.getInt(DataEngine.mydbCursor
										.getColumnIndex(MyDbAdapter.NEWSID)));
						newstitle_vec.add(DataEngine.mydbCursor
								.getString(DataEngine.mydbCursor
										.getColumnIndex(MyDbAdapter.TITLE)));
						DataEngine.mydbCursor.moveToNext();
						j++;
					}

				}
			} else {
				// showDialog(EVENTS_NULL);
			}
		}
		DataEngine.mydbCursor.close();
		DataEngine.my_adapter.close();

	}

	/**
	 * This method is used to clear all the vectors.
	 */
	public void clear_vectors() {
		newstitle_vec.clear();
		newsid_vec.clear();
	}

	/**
	 * This method is used for the unfavorite
	 * @param arg_pos
	 * @param id
	 */
	private void delete_Favourite_item(int arg_pos, int id) {

		if (id != 0) {
			final String item_urlToDel = String.valueOf(id);
			newsid_vec.remove(newsid_vec.indexOf(id));
			new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (null != DataEngine.my_adapter) {
						if (!item_urlToDel.equals("")) {
							if (null != DataEngine.my_adapter) {
								DataEngine.my_adapter.open();
							}
							DataEngine.my_adapter
									.delete_favinformation(item_urlToDel);
							DataEngine.my_adapter.close();
							// new Thread(del_runnable).start();

						}
					}

				}
			}).start();

		}
	}

	/**
	 * This method is used for add the event data into the database when the user favorite a particular event.
	 * @param position
	 */
	private void add_favourites(int position) {
		DataEngine.fav_title = news_obj.getSelectednews_title();
		DataEngine.fav_description = news_obj.getSelectednews_text();
		DataEngine.fav_date = news_obj.getSelectednews_startdate();
		DataEngine.fav_newsid = String.valueOf(news_obj.getSelectednews_id());
		DataEngine.fav_image = news_obj.getSelectednews_image();
		newsid_vec.add(Integer.parseInt(DataEngine.fav_newsid));
		new Thread(runnable).start();

	}

	Runnable runnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			handler.post(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					DataEngine.open_database(NewsViewActivity.this);
					DataEngine.my_adapter.open();
					DataEngine.my_adapter.add_informationfav(
							DataEngine.fav_title, DataEngine.fav_description,
							DataEngine.fav_date, DataEngine.fav_image,
							DataEngine.fav_newsid);
					DataEngine.my_adapter.close();
				}
			});

		}
	};

	/**
	 * To chack the Network
	 * @param showprogress
	 */
	private void doBackground(boolean showprogress) {
		background_process = new BackgroundProcess(showprogress);
		if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
			background_process.execute();

		} else {
			MethodsHelper.showToast(getResources().getString(R.string.network_error),
					this);

		}
	}

	/**
	 * This class is used for the bitmap background process.
	 * @author SPdesign
	 *
	 */
	class BackgroundProcess extends AsyncTask<Void, Void, Void> {
		boolean showprogress = false;

		public BackgroundProcess(boolean progress) {
			// TODO Auto-generated constructor stub
			this.showprogress = progress;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			try {
				if (showprogress) {
					progress.show();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			changebitmap(news_obj.getSelectednews_image());
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			try {
				if (showprogress)
					if (progress.isShowing())
						progress.dismiss();

				news_header.setText(news_obj.getSelectednews_title());
				news_body.setText(news_obj.getSelectednews_text());
				if(null!=bi){
					news_image.setImageBitmap(bi);
				}else{
					news_image.setVisibility(View.GONE);
				}
				String label = news_obj.getSelectednews_startdate();
				String[] s = label.split("T");
				month_date.setText(s[0].split("-")[2]);
				month.setText(map.get(s[0].split("-")[1]));
				month_year.setText(s[0].split("-")[0]);
				// news_image.setImageURI(news_obj.getCurrentnews_vec().get(position).getCurrentnews_image());
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

		private void changebitmap(String newsImage) {
			// TODO Auto-generated method stub
			URL imageURL;
			Bitmap bitmap;
			try {

				imageURL = new URL(newsImage);
				System.out.println(imageURL);

				try {
					HttpURLConnection connection = (HttpURLConnection) imageURL
							.openConnection();
					connection.setDoInput(true);
					connection.connect();
					InputStream inputStream = connection.getInputStream();

					bitmap = BitmapFactory.decodeStream(inputStream);// Convert
																		// to
																		// bitmap
					System.out.println(bitmap);
					bi = bitmap;

					// image_view.setImageBitmap(bitmap);
				} catch (IOException e) {
                     bi = null;
					e.printStackTrace();
				}

			}

			catch (MalformedURLException e) {
				e.printStackTrace();
			}

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back_layout:
			finish();
			break;
		case R.id.pagang_detailheader:
			finish();
			break;
		case R.id.p_settings:
			doShare();
			break;
		case R.id.setting_lay:
			doShare();
			break;
		case R.id.cross_image:
			share_dialog.dismiss();
			break;
		case R.id.cross_dialog:
			share_dialog.dismiss();
			break;
		case R.id.twitter_image:
			if (share_dialog.isShowing())
				share_dialog.cancel();
			doTweetShare();
			break;
		case R.id.fb_image: 
			if (share_dialog.isShowing())
				share_dialog.cancel();
			doFbShare();
			break;
		case R.id.fav_image:
			if (newsid_vec.contains(id)) {
						fav_image.setImageResource(R.drawable.fav_disabled);
						delete_Favourite_item(position, id);

					} else {
						fav_image.setImageResource(R.drawable.fav_enabled);
						add_favourites(position);
					}

			

		}

	}

	/**
	 * This method is called when the user selects to share .
	 */
	public void doShare() {
		share_dialog = new Dialog(this);
		share_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		share_dialog.setContentView(R.layout.share_alertlay);
		share_dialog.getWindow().setLayout(android.view.WindowManager.LayoutParams.FILL_PARENT, 
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
	
		share_dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		TextView shareText = (TextView) share_dialog.findViewById(R.id.share_txt);
		shareText.setTypeface(DataEngine.gettypeface(NewsViewActivity.this));
		ImageView cross = (ImageView) share_dialog
				.findViewById(R.id.cross_image);
		LinearLayout cross_layout=(LinearLayout)share_dialog
				.findViewById(R.id.cross_dialog);
		shareText.setText(R.string.share_favtxt);
		ImageView twitter = (ImageView) share_dialog
				.findViewById(R.id.twitter_image);
		ImageView facebook = (ImageView) share_dialog
				.findViewById(R.id.fb_image);
		share_dialog.show();

		twitter.setOnClickListener(this);
		facebook.setOnClickListener(this);
		cross.setOnClickListener(this);
		cross_layout.setOnClickListener(this);

	}

	/**
	 * This method is used for facebook sharing.
	 */
	private void doFbShare() {
		mFacebook = new Facebook(DataEngine.FACEBOOK_APPID);// new
		// Facebook("193498163996120");
		mAsyncFacebookRunner = new AsyncFacebookRunner(mFacebook);
		SessionStore.restore(mFacebook, getApplicationContext());
		SessionEvents.addAuthListener(new FacebookAuthListener());
		SessionEvents.addLogoutListener(new FacebookLogoutListener());
		if (mFacebook.isSessionValid()) {

			Bundle params = new Bundle();
			// params.putString("message",
			// news_obj.getCurrentnews_vec().get(position).getCurrentnews_text());
			// params.putString("link", restaurant.getWebsite());
			// params.putString("caption", "{*actor*} just posted this!");
			params.putString("description", news_obj.getSelectednews_text());
			params.putString("name", news_obj.getSelectednews_title());
			params.putString("picture", news_obj.getSelectednews_image());
			params.putString("to", fbId);
			mFacebook.dialog(this, "feed", params, new DialogListener() {

				@Override
				public void onComplete(Bundle values) {					
					if(share_dialog.isShowing())
						share_dialog.cancel();
					if(!values.isEmpty())
					handler.post(success_runnable);
				}

				@Override
				public void onCancel() {
					// TODO Auto-generated method stub
				}

				@Override
				public void onFacebookError(FacebookError e) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onError(DialogError e) {
					// TODO Auto-generated method stub

				}
			});

		} else {
			// TODO Auto-generated method stub
			mFacebook.authorize(NewsViewActivity.this,
					Util.FACEBOOK_PERMISSIONS, Facebook.FORCE_DIALOG_AUTH,
					new FacebookLoginDialogListener());
		}

	}
	
	Runnable success_runnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			showFbDeliveryFailed();
		}
	};

	/**
	 * This dialog is shown for facebook posting status.
	 */
	private void showFbDeliveryFailed() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Du har nu delat detta p� Facebook");
		builder.setTitle("Klart!");

		builder.setNegativeButton("Okej", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

				dialog.dismiss();
				try {
					if (progress.isShowing())
						progress.dismiss();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});

		builder.show();

	}

	/**
	 * This method is used for sharing in the twitter.
	 */
	public void doTweetShare() {
		
		Twitt_Sharing twitt = new Twitt_Sharing(NewsViewActivity.this,
				DataEngine.CONSUMER_KEY, DataEngine.SECRET_KEY);
		// string_img_url =
		// "http://3.bp.blogspot.com/_Y8u09A7q7DU/S-o0pf4EqwI/AAAAAAAAFHI/PdRKv8iaq70/s1600/id-do-anything-logo.jpg";
		String string_msg = news_obj.getSelectednews_title();
		// here we have web url image so we have to make it
		// as file to
		// upload
		if(string_msg.length()>140){
			string_msg=string_msg.substring(0,134)+"...";
		}
		String_to_File();
		
		twitt.shareToTwitter(string_msg, casted_image);

	}

	public File String_to_File() {

		Bitmap mBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.ic_launcher);
		try {
			File rootSdDirectory = Environment.getExternalStorageDirectory();

			casted_image = new File(rootSdDirectory, "attachment.jpg");
			if (casted_image.exists()) {
				casted_image.delete();
			}
			casted_image.createNewFile();

			FileOutputStream fos = new FileOutputStream(casted_image);
			try {

				BitmapFactory.Options options = new BitmapFactory.Options();

				options.inSampleSize = 2;

				BufferedOutputStream bos = new BufferedOutputStream(fos);
				mBitmap = Bitmap.createBitmap(200, 200, Bitmap.Config.RGB_565);

				bi.compress(CompressFormat.JPEG, 100, bos);

				bos.flush();

				bos.close();

			} catch (Exception e) {
				casted_image = null;
				//return null;
			}

			return casted_image;

		} catch (Exception e) {
            casted_image = null;
			System.out.print(e);
			// e.printStackTrace();

		}
		return casted_image;

	}

	public class FacebookAuthListener implements AuthListener {

		public void onAuthSucceed() {
			SessionStore.save(mFacebook, getApplicationContext());
		}

		public void onAuthFail(String error) {
		}
	}

	class FacebookLogoutListener implements LogoutListener {
		public void onLogoutBegin() {
		}

		public void onLogoutFinish() {
		}
	}
	
	Runnable sr = new Runnable() {
		
		@Override
		public void run() {
			doFbShare();
			
		}
	};

	private class UserDetailsRequestListener implements DialogListener,
			RequestListener {

		@Override
		public void onComplete(String response, Object state) {
			try {
				JSONObject json = Util.parseJson(response);
				String friedsresponse = new FBConnectQueryProcessor()
						.requestFriendsList(getApplicationContext(), mFacebook);
				JSONObject mainObj = new JSONObject(friedsresponse);
				fbId = json.getString("id");
				if (mainObj.has("error")) {
					return;
				} else {
					handler.post(sr);
					
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FacebookError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onIOException(IOException e, Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {

		}

		@Override
		public void onComplete(Bundle values) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onError(DialogError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {
			// TODO Auto-generated method stub

		}

	}

	class FacebookLoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			SessionEvents.onLoginSuccess();
			mAsyncFacebookRunner
					.request("me", new UserDetailsRequestListener());
			SessionStore.save(mFacebook, getApplicationContext());
		}

		public void onFacebookError(FacebookError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onError(DialogError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onCancel() {
			SessionEvents.onLoginError("Action Canceled");
		}
	}

}
