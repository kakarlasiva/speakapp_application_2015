package com.androidapps.speakapp.screens;

import java.util.HashMap;
import java.util.Vector;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.database.MyDbAdapter;
import com.androidapps.speakapp.screens.ActiveNewsView.MainAdapter;
import com.androidapps.speakapp.util.FavouritesUtil;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;
import com.androidapps.speakapp.util.NewsHelper;
import com.androidapps.speakapp.util.SelectedNewsHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class OldNewsView extends Activity implements OnClickListener {
	// ImageView cross_image;
	ListView news_list;
	NewsHelper helper_obj;
	FavouritesUtil favhelper_obj;
	MainAdapter adapter_obj;
	String[] months_names = { "januari", "februari", "mars", "april", "maj", "juni", "juli",
			"augusti", "september", "oktober", "november", "december" };
	String[] months = { "01", "02", "03", "04", "05", "06", "07", "08", "09",
			"10", "11", "12" };
	HashMap<String, String> map;
	Handler handler;
	Vector<Integer> newsid_vec;
	Vector<String> newstitle_vec;
	int j = 0;
	SelectedNewsHelper sel_helperobj;
	LinearLayout back_layout;
	TextView null_txt,header_txt;
	ImageView back_image;

	/**
	 * @author Speakapp This activity is used for displaying the old events
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		try{
		setContentView(R.layout.active_newslay);
		Bundle extras = getIntent().getExtras();
		map = new HashMap<String, String>();
		if (null != extras) {
			helper_obj = (NewsHelper) extras
					.getSerializable(DataEngine.NEWS_OBJECT);
			for (int i = 0; i < months.length; i++) {
				map.put(months[i], months_names[i]);
			}
		}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		init();
		set_listeners();
	}

	/**
	 * This method is used for getting the events which are stored in the
	 * database.
	 */
	private void get_favlist() {
		if (DataEngine.my_adapter == null)
			DataEngine.open_database(this);
		if (null != DataEngine.my_adapter) {
			DataEngine.my_adapter.open();
			DataEngine.mydbCursor = DataEngine.my_adapter.get_allfavlist();
		}
		if ((DataEngine.my_adapter == null)
				|| (DataEngine.mydbCursor.isClosed())) {
		} else {
			newsid_vec = new Vector<Integer>();
			newstitle_vec = new Vector<String>();
			clear_vectors();

			if (null != DataEngine.mydbCursor
					&& DataEngine.mydbCursor.getCount() > 0) {

				if (DataEngine.mydbCursor.moveToFirst()) {
					while (DataEngine.mydbCursor.isAfterLast() == false) {
						newsid_vec.add(DataEngine.mydbCursor
								.getInt(DataEngine.mydbCursor
										.getColumnIndex(MyDbAdapter.NEWSID)));
						newstitle_vec.add(DataEngine.mydbCursor
								.getString(DataEngine.mydbCursor
										.getColumnIndex(MyDbAdapter.TITLE)));
						DataEngine.mydbCursor.moveToNext();
						j++;
					}

				}
			} else {
				// showDialog(EVENTS_NULL);
			}
		}
		DataEngine.mydbCursor.close();
		DataEngine.my_adapter.close();

	}

	/**
	 * This method is used for clearing the vectors
	 */
	public void clear_vectors() {
		newstitle_vec.clear();
		newsid_vec.clear();
	}

	/**
	 * This method is used for initializing the UI Components
	 */
	private void init() {
		if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			MethodsHelper.showToast(getResources().getString(R.string.network_error),
					this);
			startActivity(new Intent(OldNewsView.this, InteractiveCube.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
		handler = new Handler();
		back_layout = (LinearLayout) findViewById(R.id.back_layout);
		news_list = (ListView) findViewById(R.id.news_list);
		header_txt=(TextView)findViewById(R.id.header_prevtxt);
		header_txt.setTypeface(DataEngine.gettypeface(this));
		null_txt = (TextView) findViewById(R.id.null_activedata);
		back_image=(ImageView)findViewById(R.id.back_image);
		null_txt.setTypeface(DataEngine.gettypeface(this));
		if (helper_obj.getOldnews_vec().size() > 0) {

			adapter_obj = new MainAdapter(this);
			get_favlist();
			news_list.setAdapter(adapter_obj);
			news_list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					set_selecteditems(position);
					Intent full_intent = new Intent(OldNewsView.this,
							NewsViewActivity.class);
					full_intent.putExtra(DataEngine.SELECTED_OBJECT,
							sel_helperobj);
					full_intent.putExtra("info", position);
					startActivity(full_intent);
				}

			});
		} else {
              news_list.setVisibility(View.GONE);
              null_txt.setVisibility(View.VISIBLE);
              null_txt.setText(getResources().getString(R.string.no_newdata));
		}
	}

	/**
	 * This method is used setting the listeners
	 */
	private void set_listeners() {
		back_layout.setOnClickListener(this);
		back_image.setOnClickListener(this);
		header_txt.setOnClickListener(this);
	}

	/**
	 * This method is used for storing the selected one data into a particular
	 * object.
	 * 
	 * @param position
	 *            Defines the particular position choosen by the user
	 */
	private void set_selecteditems(int position) {
		sel_helperobj = new SelectedNewsHelper();
		int id = helper_obj.getOldnews_vec().get(position).getOldnews_id();
		sel_helperobj.setSelectednews_id(id);
		String title = helper_obj.getOldnews_vec().get(position)
				.getOldnews_title();
		sel_helperobj.setSelectednews_title(title);
		String text = helper_obj.getOldnews_vec().get(position)
				.getOldnews_text();
		sel_helperobj.setSelectednews_text(text);
		String image = helper_obj.getOldnews_vec().get(position)
				.getOldnews_image();
		sel_helperobj.setSelectednews_image(image);
		String start_date = helper_obj.getOldnews_vec().get(position)
				.getOldnews_startdate();
		sel_helperobj.setSelectednews_startdate(start_date);
		String end_date = helper_obj.getOldnews_vec().get(position)
				.getOldnews_enddate();
		sel_helperobj.setSelectednews_enddate(end_date);
	}

	/**
	 * 
	 * @author Speakapp This is the Adapter class for the listview
	 * 
	 */
	class MainAdapter extends BaseAdapter {
		LayoutInflater inflater;
		Context context;

		public MainAdapter(Context ctx) {
			// TODO Auto-generated constructor stub
			inflater = LayoutInflater.from(ctx);
			context = ctx;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return helper_obj.getOldnews_vec().size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		String label;
		String preLabel;
		int pos;

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View view = (View) inflater.inflate(R.layout.news_adapter, null);
			LinearLayout header = (LinearLayout) view
					.findViewById(R.id.section);
			GradientDrawable fbackground = (GradientDrawable) header
					.getBackground();
			fbackground.setColor(Color.parseColor("#497286"));
			fbackground.setStroke(0, Color.parseColor("#000000"));
			LinearLayout list_layout = (LinearLayout) view
					.findViewById(R.id.news_lay);
			list_layout.setTag(position);

			if (position < helper_obj.getOldnews_vec().size()) {
				label = helper_obj.getOldnews_vec().get(position)
						.getOldnews_startdate();
			}

			if (position == 0) {
				setSection(header, label);
			} else {
				if (position < helper_obj.getOldnews_vec().size())
					preLabel = helper_obj.getOldnews_vec().get(position - 1)
							.getOldnews_startdate();
				if (!label.equalsIgnoreCase(preLabel)) {
					setSection(header, label);
				} else {
					header.setVisibility(View.GONE);
				}
			}
			try {
				TextView part_locationname = (TextView) view
						.findViewById(R.id.news_title);
				part_locationname.setTypeface(DataEngine.gettypeface(OldNewsView.this));
				part_locationname.setText(helper_obj.getOldnews_vec()
						.get(position).getOldnews_title());
				TextView ordertime = (TextView) view.findViewById(R.id.text);
				ordertime.setTypeface(DataEngine.gettypeface(OldNewsView.this));
				ordertime.setText(helper_obj.getOldnews_vec().get(position)
						.getOldnews_text());

				final int id = (helper_obj.getOldnews_vec().get(position)
						.getOldnews_id());
				final ImageView fav_image = (ImageView) view
						.findViewById(R.id.fav_image);
				if (newsid_vec.contains(id)) {
					fav_image.setImageResource(R.drawable.fav_enabled);
				} else {
					fav_image.setImageResource(R.drawable.fav_disabled);
				}

				fav_image.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (newsid_vec.contains(id)) {
							fav_image.setImageResource(R.drawable.fav_disabled);
							delete_Favourite_item(position, id);

						} else {
							fav_image.setImageResource(R.drawable.fav_enabled);
							add_favourites(position);
						}

					}
				});

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return view;
		}

		private void setSection(LinearLayout header, String label) {
			LayoutInflater inflate = ((Activity) context).getLayoutInflater();
			View date_view = (View) inflate.inflate(R.layout.date_lay, null);
			TextView month = (TextView) date_view
					.findViewById(R.id.month_title);
			month.setTypeface(DataEngine.gettypeface(OldNewsView.this));
			TextView month_date = (TextView) date_view
					.findViewById(R.id.month_date);
			month_date.setTypeface(DataEngine.gettypeface(OldNewsView.this));
			TextView month_year = (TextView) date_view
					.findViewById(R.id.month_year);
			month_year.setTypeface(DataEngine.gettypeface(OldNewsView.this));
			String[] s = label.split("T");

			month_date.setText(s[0].split("-")[2]);
			month.setText(map.get(s[0].split("-")[1]));
			month_year.setText(s[0].split("-")[0]);

			header.addView(date_view);
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back_layout:
			finish();
			break;
		case R.id.header_prevtxt:
			finish();
			break;
		case R.id.back_image:
			finish();
			break;

		default:
			break;
		}
	}

	String image;

	/**
	 * This method is for adding the data for which the user had been favorited.
	 * 
	 * @param position
	 */
	private void add_favourites(int position) {
		DataEngine.fav_title = helper_obj.getOldnews_vec().get(position)
				.getOldnews_title();
		DataEngine.fav_description = helper_obj.getOldnews_vec().get(position)
				.getOldnews_text();
		DataEngine.fav_date = helper_obj.getOldnews_vec().get(position)
				.getOldnews_startdate();
		DataEngine.fav_newsid = String.valueOf(helper_obj.getOldnews_vec()
				.get(position).getOldnews_id());
		DataEngine.fav_image = helper_obj.getOldnews_vec().get(position)
				.getOldnews_image();
		newsid_vec.add(Integer.parseInt(DataEngine.fav_newsid));
		new Thread(runnable).start();

	}

	/**
	 * This method is used for unfavorite the selected one which is favorited
	 * 
	 * @param arg_pos
	 *            position
	 * @param id
	 *            This param is the selected one event id.
	 */
	private void delete_Favourite_item(int arg_pos, int id) {

		if (id != 0) {
			final String item_urlToDel = String.valueOf(id);
			newsid_vec.remove(newsid_vec.indexOf(id));
			new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (null != DataEngine.my_adapter) {
						if (!item_urlToDel.equals("")) {
							if (null != DataEngine.my_adapter) {
								DataEngine.my_adapter.open();
							}
							DataEngine.my_adapter
									.delete_favinformation(item_urlToDel);
							DataEngine.my_adapter.close();
							// new Thread(del_runnable).start();

						}
					}

				}
			}).start();

		}
	}

	Runnable runnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			handler.post(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					DataEngine.open_database(OldNewsView.this);
					DataEngine.my_adapter.open();
					DataEngine.my_adapter.add_informationfav(
							DataEngine.fav_title, DataEngine.fav_description,
							DataEngine.fav_date, DataEngine.fav_image,
							DataEngine.fav_newsid);
					DataEngine.my_adapter.close();
					clear_vectors();

					get_favlist();
				}
			});

		}
	};

}
