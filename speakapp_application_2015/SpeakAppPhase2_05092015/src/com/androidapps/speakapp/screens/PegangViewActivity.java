package com.androidapps.speakapp.screens;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.parser.NewsReader;
import com.androidapps.speakapp.screens.QuestionActivity.BackgroundProcess;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;
import com.androidapps.speakapp.util.NewsHelper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author SpeakApp This activity is used for just displaying the different
 *         types of events like old,current and user favorite and the data is
 *         retrieved from server in this class itself.
 * 
 */

public class PegangViewActivity extends Activity implements OnClickListener {
	LinearLayout prev_lay, current_lay, fav_lay, back_lay;
	// ImageView cross_image;
	ProgressDialog progress;
	BackgroundProcess background_process;
	NewsReader reader_obj;
	NewsHelper helper_obj;
	TextView header_txt, prev_txt, curr_txt, fa_txt, pagang_txt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		try {
			setContentView(R.layout.pegang_lay);
			init();
			set_listeners();
			doBackground(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		DataEngine.clickFlag = false;
	}

	/**
	 * This method is used for initialization
	 */

	private void init() {
		if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			MethodsHelper.showToast(
					getResources().getString(R.string.network_error), this);
			startActivity(new Intent(PegangViewActivity.this,
					InteractiveCube.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage(getResources().getString(R.string.loading));
		prev_lay = (LinearLayout) findViewById(R.id.prev_lay);
		header_txt = (TextView) findViewById(R.id.header_pagang);
		header_txt.setTypeface(DataEngine.gettypeface(this));
		prev_txt = (TextView) findViewById(R.id.prev_txt);
		prev_txt.setTypeface(DataEngine.gettypeface(this));
		curr_txt = (TextView) findViewById(R.id.current_txt);
		curr_txt.setTypeface(DataEngine.gettypeface(this));
		fa_txt = (TextView) findViewById(R.id.fav_txt);
		fa_txt.setTypeface(DataEngine.gettypeface(this));
		pagang_txt = (TextView) findViewById(R.id.pagang_txt);
		pagang_txt.setTypeface(DataEngine.gettypeface(this));
		current_lay = (LinearLayout) findViewById(R.id.current_lay);
		fav_lay = (LinearLayout) findViewById(R.id.fav_lay);
		back_lay = (LinearLayout) findViewById(R.id.back_layout);
		GradientDrawable background = (GradientDrawable) prev_lay
				.getBackground();
		background.setColor(Color.parseColor("#497286"));
		background.setStroke(0, Color.parseColor("#000000"));
		GradientDrawable cbackground = (GradientDrawable) current_lay
				.getBackground();
		cbackground.setColor(Color.parseColor("#497286"));
		cbackground.setStroke(0, Color.parseColor("#000000"));
		GradientDrawable fbackground = (GradientDrawable) fav_lay
				.getBackground();
		fbackground.setColor(Color.parseColor("#497286"));
		fbackground.setStroke(0, Color.parseColor("#000000"));
		reader_obj = new NewsReader(this);
	}

	/**
	 * This method is for listeners to be set
	 */
	private void set_listeners() {
		prev_lay.setOnClickListener(this);
		current_lay.setOnClickListener(this);
		fav_lay.setOnClickListener(this);
		back_lay.setOnClickListener(this);
		header_txt.setOnClickListener(this);
	}

	/**
	 * checking network for background process
	 * 
	 * @param showprogress
	 */
	private void doBackground(boolean showprogress) {
		background_process = new BackgroundProcess(showprogress);
		if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
			background_process.execute();

		} else {
			MethodsHelper.showToast(
					getResources().getString(R.string.network_error), this);

		}
	}

	/**
	 * Background Process for data
	 * 
	 * @author SPdesign
	 * 
	 */
	class BackgroundProcess extends AsyncTask<Void, Void, Void> {
		boolean showprogress = false;

		public BackgroundProcess(boolean progress) {
			// TODO Auto-generated constructor stub
			this.showprogress = progress;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			try {
				if (showprogress) {
					progress.show();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				DataEngine.NEWS_URL = DataEngine.BASE_URL
						+ DataEngine.NEWS_INFO;
				reader_obj.parse_json(DataEngine.NEWS_URL);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			try {
				if (showprogress)
					if (progress.isShowing())
						progress.dismiss();
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (reader_obj.news_data.size() > 0) {
				helper_obj = reader_obj.helper_obj;
			}
			super.onPostExecute(result);
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.prev_lay:
			if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
				Intent old_intent = new Intent(PegangViewActivity.this,
						OldNewsView.class);
				old_intent.putExtra(DataEngine.NEWS_OBJECT, helper_obj);
				startActivity(old_intent);
			} else {
				MethodsHelper.showToast(
						getResources().getString(R.string.network_error), this);

			}
			break;
		case R.id.current_lay:
			if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
				Intent current_intent = new Intent(PegangViewActivity.this,
						ActiveNewsView.class);
				current_intent.putExtra(DataEngine.NEWS_OBJECT, helper_obj);
				startActivity(current_intent);

			} else {
				MethodsHelper.showToast(
						getResources().getString(R.string.network_error), this);

			}

			break;
		case R.id.fav_lay:
			if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
				Intent fav_intent = new Intent(PegangViewActivity.this,
						NewsFavourites.class);
				startActivity(fav_intent);
			} else {
				MethodsHelper.showToast(
						getResources().getString(R.string.network_error), this);

			}
			break;
		case R.id.back_layout:
			finish();
			break;
		case R.id.header_pagang:
			finish();
			break;

		default:
			break;
		}

	}

}
