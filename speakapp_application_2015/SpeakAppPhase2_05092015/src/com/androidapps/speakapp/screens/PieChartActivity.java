package com.androidapps.speakapp.screens;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.twitter.Twitt_Sharing;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;
import com.androidapps.speakapp.util.QuestionHelper;
import com.facebook.samples.facebook.AsyncFacebookRunner;
import com.facebook.samples.facebook.AsyncFacebookRunner.RequestListener;
import com.facebook.samples.facebook.DialogError;
import com.facebook.samples.facebook.Facebook;
import com.facebook.samples.facebook.Facebook.DialogListener;
import com.facebook.samples.facebook.FacebookError;
import com.facebook.samples.facebook.SessionEvents;
import com.facebook.samples.facebook.SessionEvents.AuthListener;
import com.facebook.samples.facebook.SessionEvents.LogoutListener;
import com.facebook.samples.facebook.SessionStore;
import com.facebook.samples.facebook.Util;
import com.facebook.samples.fbconnect.FBConnectQueryProcessor;
import com.facebook.samples.friendpicker.Common;
import com.nostra13.socialsharing.common.PostListener;

public class PieChartActivity extends Activity implements OnClickListener {
	// Layout for piechart
	LinearLayout lv1, picture_lay,back_layout;
	// Bitmap for squarebox
	Bitmap bitmap;
	ImageView setting_image;
	GridView grid;
	Paint paint;
	Canvas canvas;
	QuestionHelper helper_obj;
	float values[];
	ImageAdapter adapter;
	TextView question_txt, total_txt,header_txt;
	MyGraphview graphview;
	int xcord, ycord;
	float radius;
	boolean set_flag = true;
	Dialog share_dialog;
	// progress dialog to complete background process
	ProgressDialog progress;
	private boolean pendingAnnounce = false;
	SharedPreferences settings;
	Bitmap fb_shareimage;
	String fbId, fbvoucher, fbTreatUrl, fbResLocation = "", fbMessage = "";
	int total_voted = 0;
	Facebook mFacebook;
	AsyncFacebookRunner mAsyncFacebookRunner;
	boolean fbisThere;
	Handler handler;
	FrameLayout share_lay;
	/**
	 * 
	 * @author Speakapp
	 * This class is for showing the pie chart graph for the users for the question they voted.
	 *
	 */

	public class FacebookAuthListener implements AuthListener {

		public void onAuthSucceed() {
			SessionStore.save(mFacebook, getApplicationContext());
		}

		public void onAuthFail(String error) {
		}
	}

	class FacebookLogoutListener implements LogoutListener {
		public void onLogoutBegin() {

		}

		public void onLogoutFinish() {

		}
	}

	public boolean checkStatus() {

		if (!Common.isInternetAvailable(getApplicationContext())
				|| Common.isAirplaneModeOn(getApplicationContext())) {
		} else {
			mFacebook = new Facebook(DataEngine.FACEBOOK_APPID);// new
			// Facebook("193498163996120");
			mAsyncFacebookRunner = new AsyncFacebookRunner(mFacebook);
			SessionStore.restore(mFacebook, getApplicationContext());
			SessionEvents.addAuthListener(new FacebookAuthListener());
			SessionEvents.addLogoutListener(new FacebookLogoutListener());

			if (mFacebook.isSessionValid()) {
				return true;
			}
		}
		return false;
	}
Runnable sr = new Runnable() {
		
		@Override
		public void run() {
			fb_share();
			
		}
	};
	
	private class UserDetailsRequestListener implements DialogListener,
			RequestListener {

		@Override
		public void onComplete(String response, Object state) {
			try {
				JSONObject json = Util.parseJson(response);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString(DataEngine.FB_ID, json.getString("id"));
				editor.putString(DataEngine.FB_IMG_URL,
						"http://graph.facebook.com/" + json.getString("id")
								+ "/picture?type=large");
				editor.putString(DataEngine.FB_NAME, json.getString("name"));
				String friedsresponse = new FBConnectQueryProcessor()
						.requestFriendsList(getApplicationContext(), mFacebook);
				JSONObject mainObj = new JSONObject(friedsresponse);
				if (mainObj.has("error")) {
					return;
				}
				editor.commit();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FacebookError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onIOException(IOException e, Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {

		}

		@Override
		public void onComplete(Bundle values) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onError(DialogError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub

		}

	}

	class FacebookLoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			SessionEvents.onLoginSuccess();
			mAsyncFacebookRunner
					.request("me", new UserDetailsRequestListener());
			SessionStore.save(mFacebook, getApplicationContext());
			fb_share();
		}

		public void onFacebookError(FacebookError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onError(DialogError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onCancel() {
			SessionEvents.onLoginError("Action Canceled");
		}
	}

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.piechart_layout);
		Bundle extras = getIntent().getExtras();
		if (null != extras) {
			helper_obj = (QuestionHelper) extras
					.getSerializable(DataEngine.PIE_OBJECT);

			values = new float[helper_obj.getSurvey().size()];
			for (int i = 0; i < helper_obj.getSurvey().size(); i++) {
				int v = helper_obj.getSurvey().elementAt(i).getValue();
				values[i] = v;
				total_voted = total_voted + v;
			}
		}
		if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
			MethodsHelper.showToast(getResources().getString(R.string.network_error),
					this);
			startActivity(new Intent(PieChartActivity.this,
					InteractiveCube.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
		handler = new Handler();
		settings = getSharedPreferences(DataEngine.APP_REF, 0);
		share_lay=(FrameLayout)findViewById(R.id.title_layout);

		init();

		lv1 = (LinearLayout) findViewById(R.id.pie_chart);
		
		values = calculateData(values);
		graphview = new MyGraphview(this);
		graphview.set(values);
		lv1.addView(graphview);

		set_values();
		lv1.getViewTreeObserver().addOnPreDrawListener(
				new ViewTreeObserver.OnPreDrawListener() {
					@SuppressLint("NewApi")
					public boolean onPreDraw() {
						int finalHeight = lv1.getMeasuredHeight();
						int finalWidth = lv1.getMeasuredWidth();
						if (set_flag) {
							graphview.setcolorview(finalHeight, finalWidth,
									lv1.getX(), lv1.getY());
							set_flag = false;
							// Do your work here
						}
						return true;
					}
				});

	}

	/**
	 * This method is used for initializing the UI Components.
	 */
	private void init() {
		grid = (GridView) findViewById(R.id.color_gridView);
		setting_image = (ImageView) findViewById(R.id.p_settings);
		back_layout = (LinearLayout) findViewById(R.id.back_layout);
		picture_lay = (LinearLayout) findViewById(R.id.picture_lay);
		header_txt=(TextView)findViewById(R.id.header_piechart);
		header_txt.setTypeface(DataEngine.gettypeface(this));
		total_txt = (TextView) findViewById(R.id.totalvote_msg);
		total_txt.setTypeface(DataEngine.gettypeface(this));
		question_txt = (TextView) findViewById(R.id.ques_msg);
		question_txt.setTypeface(DataEngine.gettypeface(this));
		if (null != helper_obj.getQuestion()) {
			question_txt.setText(helper_obj.getQuestion());
			total_txt.setText("" + total_voted + " har r�stat i fr�gan");
		}
		back_layout.setOnClickListener(this);
		share_lay.setOnClickListener(this);
		header_txt.setOnClickListener(this);
	}

	/**
	 * This method is used for setting the options with the color code.
	 */
	private void set_values() {
		adapter = new ImageAdapter(PieChartActivity.this);
		grid.setAdapter(adapter);
		setting_image.setOnClickListener(this);
	}

	private void getbitmap(ImageView img, String color) {
		float ld = 30;
		bitmap = Bitmap.createBitmap((int) ld, (int) ld,
				Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bitmap);
		paint = new Paint();
		Random r = new Random();

		paint.setColor(Color.parseColor(color));

		img.setImageBitmap(bitmap);
		canvas.drawRect(0, 0, (float) ld, (float) ld, paint);
	}

	private float[] calculateData(float[] data) {
		float total = 0;
		for (int i = 0; i < data.length; i++) {
			total += data[i];
		}
		for (int i = 0; i < data.length; i++) {
			data[i] = 360 * (data[i] / total);
		}
		return data;
	}
	
	@Override
	protected void onDestroy() {		
		super.onDestroy();
		DataEngine.clickFlag = false;
	}

	/**
	 * This class is for the adapter
	 * @author SPdesign
	 *
	 */
	class ImageAdapter extends BaseAdapter {
		private Context context;

		public ImageAdapter(Context context) {
			this.context = context;
		}

		@Override
		public int getCount() {
			return helper_obj.getSurvey().size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {			

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.pie_gridadapter, null);

			// set value into textview
			TextView textView = (TextView) convertView
					.findViewById(R.id.color_txt);
			textView.setTypeface(DataEngine.gettypeface(PieChartActivity.this));
			textView.setText(helper_obj.getSurvey().elementAt(position)
					.getSurveyoption_name());
           textView.setFocusable(true);
           textView.setMarqueeRepeatLimit(-1);
			ImageView imageView = (ImageView) convertView
					.findViewById(R.id.color_box);
			/*if (position % 2 == 0) {
				imageView.setPadding(
						getResources().getDimensionPixelOffset(
								R.dimen.gridview_margin_left_item), 0, 0, 0);
			} else {
				imageView.setPadding(
						getResources().getDimensionPixelOffset(
								R.dimen.gridview_margin_right_item), 0, 0, 0);
			}*/
			String color_value = helper_obj.getSurvey().elementAt(position)
					.getColor();
			getbitmap(imageView, color_value);

			return convertView;

		}

	}

	public class MyGraphview extends View {

		public MyGraphview(Context context, AttributeSet attrs, int defStyle) {
			super(context);			
		}

		public MyGraphview(Context context) {
			super(context);
		}		

		private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		private float[] value_degree;
		RectF rectf = new RectF();
		RectF white_rect=new RectF();

		public void set(float[] values) {
			value_degree = new float[values.length];
			for (int i = 0; i < values.length; i++) {
				value_degree[i] = values[i];
			}
		}

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			rectf.set(getWidth() / 2 - radius, getHeight() / 2 - radius,
					getWidth() / 2 + radius, getHeight() / 2 + radius);		
			
			
			float temp = 0;
			int mcenterX = (int) ((rectf.left + rectf.right) / 2);
	        int mcenterY = (int) ((rectf.top + rectf.bottom) / 2);
	        int mradius = (int) ((rectf.right - rectf.left) / 2);
			for (int i = 0; i < value_degree.length; i++) {
				if (i == 0) {
					String color_value = helper_obj.getSurvey().elementAt(i)
							.getColor();
					paint.setColor(Color.parseColor(color_value));
					Paint p=new Paint();
					p.setShader(null);
					p.setColor(Color.WHITE);
					p.setAntiAlias(true);
					p.setStyle(Paint.Style.STROKE);
			        p.setStrokeWidth(8);
					//canvas.drawArc(rectf, 0,360,true, p);
					canvas.drawCircle(getWidth()/2,getHeight()/2,radius, p);
					canvas.drawArc(rectf, 0, value_degree[i], true, paint);					
					
				} else {
					temp += value_degree[i - 1];
					String color_value = helper_obj.getSurvey().elementAt(i)
							.getColor();
					paint.setColor(Color.parseColor(color_value));					
					canvas.drawArc(rectf, temp, value_degree[i], true, paint);
					// float medianAngle = (temp + (value_degree[i] / 2f)) * (float)Math.PI / 180f; // this angle will place the text in the center of the arc.
					// float medianAngle = temp  * (float)Math.PI / 180f; // this angle will place the text in the center of the arc.
				        
				//	canvas.drawText("28%", (float)(mcenterX + (radius * Math.cos(medianAngle))), (float)(mcenterY + (mradius * Math.sin(medianAngle))), tp);
			     
				}
			}
		}

		public void setcolorview(int height, int width, float xdis, float ydis) {
			xcord = (int) (xdis + width / 2);
			ycord = (int) (ydis + height / 2);
			radius = (height / (float) 2.2);
			invalidate();
		} 
	}

	/**
	 * This method is used for sharing
	 */
	@SuppressWarnings("deprecation")
	public void showDialog() {	

		share_dialog = new Dialog(this);
		share_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		share_dialog.setContentView(R.layout.share_alertlay);
		share_dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		share_dialog.getWindow().setLayout(android.view.WindowManager.LayoutParams.FILL_PARENT, 
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		
		TextView share_txt=(TextView)share_dialog.findViewById(R.id.share_txt);
		share_txt.setTypeface(DataEngine.gettypeface(this));
		ImageView twitter = (ImageView) share_dialog
				.findViewById(R.id.twitter_image);
		LinearLayout cross_dialog=(LinearLayout)share_dialog.findViewById(R.id.cross_dialog);
		ImageView cross = (ImageView) share_dialog
				.findViewById(R.id.cross_image);
		ImageView facebook = (ImageView) share_dialog
				.findViewById(R.id.fb_image);
		
		share_dialog.show();

		twitter.setOnClickListener(this);
		facebook.setOnClickListener(this);
		cross.setOnClickListener(this);
		cross_dialog.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {		
		switch (v.getId()) {
		case R.id.twitter_image:
			share_dialog.dismiss();
			share_twitter();
			break;
		case R.id.fb_image:
			share_dialog.dismiss();
			fb_share();
			break;
		case R.id.p_settings:
			captureWV();
			showDialog();
			break;
		case R.id.title_layout:
			captureWV();
			showDialog();
			break;
		case R.id.back_layout:
			finish();
			break;
		case R.id.header_piechart:
			finish();
			break;
		case R.id.cross_image:
			share_dialog.dismiss();
			break;
		case R.id.cross_dialog:
			share_dialog.dismiss();
			break;
		default:
			break;
		}
	}

	/**
	 * This method is used for the layout change to bitmap.
	 */
	private void captureWV() {
		picture_lay.setDrawingCacheEnabled(true);
		picture_lay.buildDrawingCache();
		fb_shareimage = picture_lay.getDrawingCache().copy(Config.RGB_565,
				false);
		picture_lay.setDrawingCacheEnabled(false);
	}

	/**
	 * This method is used for the facebook share.
	 */
	private void fb_share() {
		try {
			mFacebook = new Facebook(DataEngine.FACEBOOK_APPID);// new
			// Facebook("193498163996120");
			mAsyncFacebookRunner = new AsyncFacebookRunner(mFacebook);
			SessionStore.restore(mFacebook, this);
			SessionEvents.addAuthListener(new FacebookAuthListener());
			SessionEvents.addLogoutListener(new FacebookLogoutListener());

			if (mFacebook.isSessionValid()) {

				progress = ProgressDialog.show(this, "", getResources()
						.getString(R.string.loading), true);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				fb_shareimage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
				data = baos.toByteArray();

				Bundle params = new Bundle();
				params.putString("method", "photos.upload");
				params.putByteArray("picture", data);

				AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(
						mFacebook);
				mAsyncRunner.request(null, params, "POST",
						new SampleUploadListener(), null);

			} else {
				// no logged in, so relogin
				mFacebook.authorize(PieChartActivity.this,
						Util.FACEBOOK_PERMISSIONS, Facebook.FORCE_DIALOG_AUTH,
						new FacebookLoginDialogListener());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	class SampleUploadListener implements RequestListener {

		@Override
		public void onComplete(String response, Object state) {
			// TODO Auto-generated method stub
			try {
				if (progress.isShowing())
					progress.dismiss();
			} catch (Exception e) {
				// TODO: handle exception
			}
			handler.post(success_runnable);
		}

		@Override
		public void onIOException(IOException e, Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {
			// TODO Auto-generated method stub

		}

	}

	byte[] data = null;

	private void saveImageToExternalStorage(Bitmap finalBitmap) {
		String root = Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_PICTURES).toString();
		File myDir = new File(root + "/SpeakApp_images");
		myDir.mkdirs();

		String fname = "SpeakImage.jpg";
		File file = new File(myDir, fname);
		if (file.exists())
			file.delete();
		file = new File(myDir, fname);

		try {
			FileOutputStream out = new FileOutputStream(file);
			finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Runnable success_runnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			showFbDeliveryFailed();
		}
	};

	private void showFbDeliveryFailed() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Du har nu delat detta p� Facebook");
		builder.setTitle("Klart!");

		builder.setNegativeButton("Okej", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

				dialog.dismiss();
				try {
					if (progress.isShowing())
						progress.dismiss();
				} catch (Exception e) {
					
				}
			}
		});
		builder.show();

	}

	private String string_msg = null;
	File casted_image;
	String sharemsg_twitter = "SpeakApp";

	/**
	 * This method is used for the twitter sharing.
	 */
	private void share_twitter() {
		Twitt_Sharing twitt = new Twitt_Sharing(PieChartActivity.this,
				DataEngine.CONSUMER_KEY, DataEngine.SECRET_KEY);

		string_msg = sharemsg_twitter;

		String_to_File();

		twitt.shareToTwitter(string_msg, casted_image);

	}

	public File String_to_File() {

		try {
			File rootSdDirectory = Environment.getExternalStorageDirectory();

			casted_image = new File(rootSdDirectory, "attachment.jpg");
			if (casted_image.exists()) {
				casted_image.delete();
			}
			casted_image.createNewFile();

			FileOutputStream fos = new FileOutputStream(casted_image);
			try {

				BitmapFactory.Options options = new BitmapFactory.Options();

				options.inSampleSize = 2;

				BufferedOutputStream bos = new BufferedOutputStream(fos);
				// mBitmap = Bitmap.createBitmap(200, 200,
				// Bitmap.Config.RGB_565);

				fb_shareimage.compress(CompressFormat.JPEG, 100, bos);

				bos.flush();

				bos.close();

			} catch (Exception e) {
				casted_image = null;
				//return null;
			}

			return casted_image;

		} catch (Exception e) {
            casted_image = null;
			System.out.print(e);
			// e.printStackTrace();

		}
		return casted_image;

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == -1) {
			try {

			} catch (Exception e) {
				// TODO: handle exception8730
			}
			pendingAnnounce = false;
			fb_share();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
