package com.androidapps.speakapp.screens;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.parser.QuestionReader;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;
import com.androidapps.speakapp.util.QuestionHelper;

public class QuestionActivity extends Activity implements OnClickListener {
	// Textview for Question
	TextView ques_txt;
	// Textview for all the options
	TextView opt_txt;
	// Checkboxes for the selection
	ImageView tick_image;
	LinearLayout back_layout;
	// Button to submit
	Button sub_btn;
	// progress dialog to complete background process
	ProgressDialog p_dialog;
	// Layout for options
	LinearLayout option_lay;
	BackgroundProcess background_process;
	String[] names = { "sport", "Natur", "L�xor", "Spel", "Chilla" };
	private HashMap<String, Integer> map_id;
	int sel_nameid = 0, val = 0, question_id = 0;
	QuestionReader reader_obj;
	FrameLayout f_layout;
	QuestionHelper helper_obj;
	Dialog thanks_view;
	boolean set_flag = false;
	LinearLayout touch_lay;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    setContentView(R.layout.activity_question);
		touch_lay=(LinearLayout)findViewById(R.id.ontouch);
		init();
	    map_id = new HashMap<String, Integer>(); 
  	    set_listeners();
		doBackground(true);
		//showDialog();
	}

	private void init() {
		p_dialog = new ProgressDialog(this);
		p_dialog.setCancelable(true);
		p_dialog.setMessage(getResources().getString(R.string.loading));
		ques_txt = (TextView) findViewById(R.id.question_txt);
		back_layout=(LinearLayout)findViewById(R.id.back_layout);
		option_lay = (LinearLayout) findViewById(R.id.options_lay);
		sub_btn = (Button) findViewById(R.id.sub_btn);
		reader_obj = new QuestionReader(this);
	}

	private void doBackground(boolean showprogress) {
		background_process = new BackgroundProcess(showprogress);
		if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
			background_process.execute();

		} else {
			MethodsHelper.showToast(getResources().getString(R.string.network_error),
					this);

		}
	}

	private void set_listeners() {

	  sub_btn.setOnClickListener(this);
	  back_layout.setOnClickListener(this);

	}

	private void set_values() {

		for (int i = 0; i < reader_obj.options_data.size(); i++) {
			String name = reader_obj.options_data.elementAt(i).getOption_name();
			set_options(option_lay, name);
		}
	}

	private void set_options(LinearLayout lay, String name) {
		LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
		View option_view = (View) inflater.inflate(R.layout.qoption_lay, null);
		f_layout = (FrameLayout) option_view.findViewById(R.id.opt_lay);

		opt_txt = (TextView) option_view.findViewById(R.id.qoption);
		opt_txt.setText(name);
		tick_image = (ImageView) option_view.findViewById(R.id.tick_image);
		lay.addView(option_view);
		f_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				for (int i = 0; i < option_lay.getChildCount(); i++) {
					if (option_lay.getChildAt(i).findViewById(R.id.opt_lay)
							.equals(v)) {
						String name = ((TextView) v.findViewById(R.id.qoption))
								.getText().toString();
						Log.i("", "--------name-----" + name);
						sel_nameid = map_id.get(name);
						Log.i("", "--------click-----" + sel_nameid);
						FrameLayout flay = (FrameLayout) option_lay.getChildAt(
								i).findViewById(R.id.opt_lay);
						ImageView t_image = (ImageView) option_lay
								.getChildAt(i).findViewById(R.id.tick_image);
						t_image.setVisibility(View.VISIBLE);
						flay.setBackgroundResource(R.drawable.box_selected);

						// f_layout.setBackgroundResource(R.drawable.rectangle_box);
					} else {
						FrameLayout flay = (FrameLayout) option_lay.getChildAt(
								i).findViewById(R.id.opt_lay);
						ImageView t_image = (ImageView) option_lay
								.getChildAt(i).findViewById(R.id.tick_image);
						t_image.setVisibility(View.GONE);
						flay.setBackgroundResource(R.drawable.rectangle_box);
					}
				}

			}
		});

	}

	/*
	 * @Override public void onCheckedChanged(CompoundButton buttonView, boolean
	 * isChecked) { // TODO Auto-generated method stub View v = (View)
	 * buttonView.getParent();
	 * f_layout=(FrameLayout)v.findViewById(R.id.opt_lay); if (isChecked) {
	 * String name = ((TextView) v.findViewById(R.id.qoption)).getText()
	 * .toString(); f_layout.setBackgroundResource(R.drawable.box_selected);
	 * sel_nameid = map_id.get(name); Log.i("", "-------------------" +
	 * sel_nameid + "+++++++++++" + name); for (int i = 0; i <
	 * option_lay.getChildCount(); i++) { if
	 * (!(option_lay.getChildAt(i).findViewById(R.id.opt_checkBox)
	 * .equals(buttonView))) { CheckBox check = (CheckBox)
	 * option_lay.getChildAt(i) .findViewById(R.id.opt_checkBox);
	 * check.setChecked(false);
	 * //f_layout.setBackgroundResource(R.drawable.rectangle_box); } }
	 * 
	 * // CheckBox check } else { String s_name = ((TextView)
	 * v.findViewById(R.id.qoption)).getText() .toString();
	 * f_layout.setBackgroundResource(R.drawable.rectangle_box); sel_nameid = 0;
	 * Log.i("", "---------else----------" + sel_nameid ); } }
	 */
	private void form_json() {
		Log.i("", "---------json----------" + sel_nameid);
		set_flag = true;
		JSONObject object = new JSONObject();
		try {
			object.put("DeviceId", getdeviceid());
			object.put("QuestionId", question_id);
			object.put("OptionId", sel_nameid);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		DataEngine.VOTE_URL = DataEngine.BASE_URL + DataEngine.SUBMIT_VOTE
				+ object.toString();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.sub_btn:
			form_json();
			new SubmitBackgroundProcess(true).execute();
			break;
		case R.id.opt_lay:

			break;
		case R.id.back_layout:
			finish();
			break;
		default:
			break;
		}
	}

	private String getdeviceid() {
		String device_id = MethodsHelper.getDeviceId(QuestionActivity.this);
		return device_id;
	}

	private String form_url() {
		String url = DataEngine.BASE_URL + DataEngine.QUESTION_INFO
				+ getdeviceid() + DataEngine.DEVICE_INFO + "android";
		return url;
	}

	private void preparedata() {
		if (reader_obj.questions_data.size() > 0) {
			Log.i("", "-------------"
					+ reader_obj.questions_data.elementAt(val).isAns_status());
			question_id = reader_obj.questions_data.elementAt(val)
					.getQuestion_id();
			// helper_obj=reader_obj.helper_obj;
			if (!(reader_obj.questions_data.elementAt(val).isAns_status())) {
				ques_txt.setText(reader_obj.questions_data.elementAt(val)
						.getQuestion());
				for (int i = 0; i < reader_obj.options_data.size(); i++) {
					String name = reader_obj.options_data.elementAt(i)
							.getOption_name();
					int id = reader_obj.options_data.elementAt(i)
							.getOption_id();
					map_id.put(name, id);
				}
			} else {
				check_status();
				/*
				 * Intent pie_intent=new
				 * Intent(QuestionActivity.this,PieChartActivity.class);
				 * pie_intent.putExtra(DataEngine.PIE_OBJECT,helper_obj);
				 * startActivity(pie_intent); finish();
				 */
			}
		}

		set_values();
	}

	class SubmitBackgroundProcess extends AsyncTask<Void, Void, Void> {
		boolean showprogress = false;

		public SubmitBackgroundProcess(boolean progress) {
			// TODO Auto-generated constructor stub
			this.showprogress = progress;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			try {
				if (showprogress) {
					p_dialog.show();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			reader_obj.clear();
			reader_obj.parse_json(DataEngine.VOTE_URL);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			check_status();
			try {
				if (showprogress)
					if (p_dialog.isShowing())
						p_dialog.dismiss();
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void check_status() {
		if (reader_obj.questions_data.size() > 0
				&& reader_obj.survey_data.size() > 0) {
			String error_code = reader_obj.questions_data.elementAt(val)
					.getError_code();
			if (error_code.equalsIgnoreCase("200")
					|| error_code.equalsIgnoreCase("0")) {
				if (set_flag) {
					set_flag = false;
					showDialog();
				} else {
					helper_obj = reader_obj.helper_obj;
					Intent piechart_intent = new Intent(QuestionActivity.this,
							PieChartActivity.class);
					piechart_intent.putExtra(DataEngine.PIE_OBJECT, helper_obj);
					startActivity(piechart_intent);
					finish();
				}
			}
		}
	}

	class BackgroundProcess extends AsyncTask<Void, Void, Void> {
		boolean showprogress = false;

		public BackgroundProcess(boolean progress) {
			// TODO Auto-generated constructor stub
			this.showprogress = progress;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			try {
				if (showprogress) {
					p_dialog.show();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			reader_obj.parse_json(form_url());
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			preparedata();
			try {
				if (showprogress)
					if (p_dialog.isShowing())
						p_dialog.dismiss();
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	public void showDialog() {
		// TODO Auto-generated method stub

		thanks_view = new Dialog(this);
		thanks_view.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		thanks_view.setContentView(R.layout.alert_layout);
		thanks_view.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		Button thanks_ok = (Button) thanks_view.findViewById(R.id.thanks_ok);

		thanks_view.show();

		thanks_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				thanks_view.dismiss();
				helper_obj = reader_obj.helper_obj;
				Intent piechart_intent = new Intent(QuestionActivity.this,
						PieChartActivity.class);
				piechart_intent.putExtra(DataEngine.PIE_OBJECT, helper_obj);
				startActivity(piechart_intent);
				finish();

			}
		});
		// onto
      
	}

	

}
