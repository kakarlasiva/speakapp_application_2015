package com.androidapps.speakapp.screens;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import com.androidapps.speakapp.R;
import com.androidapps.speakapp.cube.InteractiveCube;
import com.androidapps.speakapp.data.DataEngine;
import com.androidapps.speakapp.parser.QuestionReader;
import com.androidapps.speakapp.screens.QuestionActivity.BackgroundProcess;
import com.androidapps.speakapp.screens.QuestionActivity.SubmitBackgroundProcess;
import com.androidapps.speakapp.util.MethodsHelper;
import com.androidapps.speakapp.util.NetworkChecker;
import com.androidapps.speakapp.util.QuestionHelper;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class QuestionViewActivity extends Activity implements OnClickListener {

	// Textview for Question
	TextView ques_txt,header_txt;
	// Textview for all the options
	TextView opt_txt;
	// Checkboxes for the selection
	ImageView tick_image;
	// Button to submit
	Button sub_btn;
	// progress dialog to complete background process
	ProgressDialog p_dialog;
	// Layout for options
	LinearLayout option_lay,back_layout;
	BackgroundProcess background_process;
	String[] names = { "sport", "Natur", "L�xor", "Spel", "Chilla" };
	private HashMap<String, Integer> map_id;
	int sel_nameid = 0, val = 0, question_id = 0;
	QuestionReader reader_obj;
	FrameLayout f_layout;
	QuestionHelper helper_obj;
	Dialog thanks_view;
	boolean set_flag = false;
	LinearLayout touch_lay;
	/**
	 * @author Speakapp
	 * This Activity is for displaying the question with the options for the user to vote.
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    setContentView(R.layout.activity_question);
	    if (!NetworkChecker.isNetworkOnline(getApplicationContext())) {
	    	MethodsHelper.showToast(getResources().getString(R.string.network_error),
					this);
			startActivity(new Intent(QuestionViewActivity.this,InteractiveCube.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP));
			return;
		}
	    Bundle extras=getIntent().getExtras();
	    if (null != extras) {
			helper_obj = (QuestionHelper) extras
					.getSerializable(DataEngine.QUESTION_OBJECT);
	    }

		touch_lay=(LinearLayout)findViewById(R.id.ontouch);
		init();
	    map_id = new HashMap<String, Integer>(); 
  	    set_listeners();
		//doBackground(true);
		//showDialog();
  	    preparedata();
	}

	/**
	 * This method is used for initializing the UI Components.
	 */
	private void init() {
		p_dialog = new ProgressDialog(this);
		p_dialog.setCancelable(true);
		p_dialog.setMessage(getResources().getString(R.string.loading));
		ques_txt = (TextView) findViewById(R.id.question_txt);
		ques_txt.setTypeface(DataEngine.gettypeface(this));
		back_layout=(LinearLayout)findViewById(R.id.back_layout);
		option_lay = (LinearLayout) findViewById(R.id.options_lay);
		sub_btn = (Button) findViewById(R.id.sub_btn);
		sub_btn.setTypeface(DataEngine.gettypeface(this));
		header_txt=(TextView)findViewById(R.id.header_question);
		header_txt.setTypeface(DataEngine.gettypeface(this));
		GradientDrawable background = (GradientDrawable) sub_btn
				.getBackground();
		background.setColor(Color.parseColor("#000000"));
		background.setStroke(0, Color.parseColor("#000000"));
		reader_obj = new QuestionReader(this);
	}

	/**
	 * This method is used for the backgroundprocess.
	 * @param showprogress
	 */
	private void doBackground(boolean showprogress) {
		background_process = new BackgroundProcess(showprogress);
		if (NetworkChecker.isNetworkOnline(getApplicationContext())) {
			background_process.execute();

		} else {
			MethodsHelper.showToast(getResources().getString(R.string.network_error),
					this);

		}
	}

	/**
	 * This method is used for setting up the listeners.
	 */
	private void set_listeners() {

	  sub_btn.setOnClickListener(this);
	  back_layout.setOnClickListener(this);
	  header_txt.setOnClickListener(this);

	}

	/**
	 * This method is used for adding the options for the question dynamically.
	 */
	private void set_values() {

		for (int i = 0; i < helper_obj.getOptions().size(); i++) {
			String name = helper_obj.getOptions().elementAt(i).getOption_name();
			set_options(option_lay, name);
		}
	}

	private void set_options(LinearLayout lay, String name) {
		LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
		View option_view = (View) inflater.inflate(R.layout.qoption_lay, null);
		f_layout = (FrameLayout) option_view.findViewById(R.id.opt_lay);
		GradientDrawable backgrounddesign = (GradientDrawable) f_layout
				.getBackground();
		backgrounddesign.setColor(Color.parseColor("#e3e3e3"));
		backgrounddesign.setStroke(2, Color.parseColor("#000000"));
		opt_txt = (TextView) option_view.findViewById(R.id.qoption);
		opt_txt.setTypeface(DataEngine.gettypeface(QuestionViewActivity.this));
		opt_txt.setText(name);
		tick_image = (ImageView) option_view.findViewById(R.id.tick_image);
		lay.addView(option_view);
		f_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				for (int i = 0; i < option_lay.getChildCount(); i++) {
					FrameLayout flay;
					ImageView t_image;
					if (option_lay.getChildAt(i).findViewById(R.id.opt_lay)
							.equals(v)) {
						String name = ((TextView) v.findViewById(R.id.qoption))
								.getText().toString();
						sel_nameid = map_id.get(name);
						 flay = (FrameLayout) option_lay.getChildAt(
								i).findViewById(R.id.opt_lay);
						 t_image = (ImageView) option_lay
								.getChildAt(i).findViewById(R.id.tick_image);
						t_image.setVisibility(View.VISIBLE);
						flay.setBackgroundResource(R.drawable.ans_selected);

						// f_layout.setBackgroundResource(R.drawable.rectangle_box);
					} else {
						 flay = (FrameLayout) option_lay.getChildAt(
								i).findViewById(R.id.opt_lay);
						 t_image = (ImageView) option_lay
								.getChildAt(i).findViewById(R.id.tick_image);
						t_image.setVisibility(View.GONE);
						//flay.setBackgroundResource(R.drawable.rectangle_box);
						GradientDrawable backgrounddesigns = (GradientDrawable) f_layout
								.getBackground();
						backgrounddesigns.setColor(Color.parseColor("#e3e3e3"));
						backgrounddesigns.setStroke(2, Color.parseColor("#000000"));
					}
				}

			}
		});

	}
     @Override
    protected void onDestroy() {
        	super.onDestroy();
        	DataEngine.clickFlag = false;
    }
	/**
	 * This method is used for formation of the json structure
	 */
	private void form_json() {
		set_flag = true;
		JSONObject object = new JSONObject();
		try {
			object.put("DeviceId", getdeviceid());
			object.put("QuestionId", question_id);
			object.put("OptionId", sel_nameid);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		DataEngine.VOTE_URL = DataEngine.BASE_URL + DataEngine.SUBMIT_VOTE
				+ object.toString();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.sub_btn:
			
			form_json();
			new SubmitBackgroundProcess(true).execute();			
			break;
		case R.id.opt_lay:

			break;
		case R.id.back_layout:
			finish();
			break;
		case R.id.header_question:
			finish();
			break;
		default:
			break;
		}
	}

	/**
	 * This method is used for getting the device id.
	 * @return
	 */
	private String getdeviceid() {
		String device_id = MethodsHelper.getDeviceId(QuestionViewActivity.this);
		return device_id;
	}

	/**
	 * This method is used for the url formation
	 * @return url
	 */
	private String form_url() {
		String url = DataEngine.BASE_URL + DataEngine.QUESTION_INFO
				+ getdeviceid() + DataEngine.DEVICE_INFO + "android";
		return url;
	}

	private void preparedata() {
		if (helper_obj.getQuestion().length() > 0) {
			
			question_id = helper_obj.getQuestion_id();
			//if ((helper_obj.isAns_status())) {
				ques_txt.setText(helper_obj.getQuestion());
				for (int i = 0; i < helper_obj.getOptions().size(); i++) {
					String name = helper_obj.getOptions().elementAt(i)
							.getOption_name();
					int id = helper_obj.getOptions().elementAt(i)
							.getOption_id();
					map_id.put(name, id);
				}
			/*} else {
				check_status();*/
				/*
				 * Intent pie_intent=new
				 * Intent(QuestionActivity.this,PieChartActivity.class);
				 * pie_intent.putExtra(DataEngine.PIE_OBJECT,helper_obj);
				 * startActivity(pie_intent); finish();
				 */
			//}
		}

		set_values();
	}

	class SubmitBackgroundProcess extends AsyncTask<Void, Void, Void> {
		boolean showprogress = false;

		public SubmitBackgroundProcess(boolean progress) {
			// TODO Auto-generated constructor stub
			this.showprogress = progress;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			try {
				if (showprogress) {
					p_dialog.show();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			reader_obj.clear();
			Log.i("URL","-------------------------"+DataEngine.VOTE_URL);
			reader_obj.parse_json(DataEngine.VOTE_URL);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			check_status();
			try {
				if (showprogress)
					if (p_dialog.isShowing())
						p_dialog.dismiss();
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	/**
	 * This method is used to check the response after submitting the vote.
	 */
	private void check_status() {
		if (reader_obj.questions_data.size() > 0
				&& reader_obj.survey_data.size() > 0) {
			String error_code = reader_obj.questions_data.elementAt(val)
					.getError_code();
			Log.i("Errorcode999", "++++++++++++++++"+error_code);
			if (error_code.equalsIgnoreCase("200")
					|| error_code.equalsIgnoreCase("0")) {
				if (set_flag) {
					set_flag = false;
					showDialog();
				} else {
					helper_obj = reader_obj.helper_obj;
					Intent piechart_intent = new Intent(QuestionViewActivity.this,
							PieChartActivity.class);
					piechart_intent.putExtra(DataEngine.PIE_OBJECT, helper_obj);
					startActivity(piechart_intent);
					finish();
				}
			}else if(error_code.equalsIgnoreCase("203")){
				showDialog_voted();
			}
		}
	}

	class BackgroundProcess extends AsyncTask<Void, Void, Void> {
		boolean showprogress = false;

		public BackgroundProcess(boolean progress) {
			// TODO Auto-generated constructor stub
			this.showprogress = progress;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			try {
				if (showprogress) {
					p_dialog.show();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			reader_obj.parse_json(form_url());
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			preparedata();
			try {
				if (showprogress)
					if (p_dialog.isShowing())
						p_dialog.dismiss();
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	/**
	 * This dialog is for the thank you msg after the voting.
	 */
	public void showDialog() {
		// TODO Auto-generated method stub

		thanks_view = new Dialog(this);
		thanks_view.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		thanks_view.setContentView(R.layout.question_alert_layout);
		thanks_view.getWindow().setLayout(android.view.WindowManager.LayoutParams.FILL_PARENT, 
				getResources().getDimensionPixelSize(R.dimen.thanks_dialog_height));
		thanks_view.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		
		Button thanks_ok = (Button) thanks_view.findViewById(R.id.thanks_ok);
        TextView thanks_txt=(TextView)thanks_view.findViewById(R.id.thanks_txt);
        thanks_txt.setTypeface(DataEngine.gettypeface(this));
        GradientDrawable background = (GradientDrawable) thanks_ok
				.getBackground();
		background.setColor(Color.parseColor("#474849"));
        thanks_ok.setText(getString(R.string.graph_result));
        thanks_ok.setTypeface(DataEngine.gettypeface(this));
		thanks_view.show();

		thanks_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				thanks_view.dismiss();
				helper_obj = reader_obj.helper_obj;
				Intent piechart_intent = new Intent(QuestionViewActivity.this,
						PieChartActivity.class);
				piechart_intent.putExtra(DataEngine.PIE_OBJECT, helper_obj);
				startActivity(piechart_intent);
				finish();

			}
		});
      
	}
	
	
	public void showDialog_voted() {
				thanks_view = new Dialog(this);
		thanks_view.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		thanks_view.setContentView(R.layout.question_alert_layout);
		thanks_view.getWindow().setLayout(android.view.WindowManager.LayoutParams.FILL_PARENT, 
				android.view.WindowManager.LayoutParams.WRAP_CONTENT);
		thanks_view.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		Button thanks_ok = (Button) thanks_view.findViewById(R.id.thanks_ok);
        TextView thanks_txt=(TextView)thanks_view.findViewById(R.id.thanks_txt);
        thanks_txt.setText("Du har redan r�stat");
        thanks_txt.setTypeface(DataEngine.gettypeface(this));
        GradientDrawable background = (GradientDrawable) thanks_ok
				.getBackground();
		background.setColor(Color.parseColor("#474849"));
        thanks_ok.setText("Visa resultat");
        thanks_ok.setTypeface(DataEngine.gettypeface(this));
		thanks_view.show();

		thanks_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				thanks_view.dismiss();
				helper_obj = reader_obj.helper_obj;
				Intent piechart_intent = new Intent(QuestionViewActivity.this,
						PieChartActivity.class);
				piechart_intent.putExtra(DataEngine.PIE_OBJECT, helper_obj);
				startActivity(piechart_intent);
				finish();

			}
		});
      
	}

	

	



}
