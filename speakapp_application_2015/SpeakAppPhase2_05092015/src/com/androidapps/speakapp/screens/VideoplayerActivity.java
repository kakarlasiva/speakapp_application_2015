package com.androidapps.speakapp.screens;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class VideoplayerActivity extends Activity implements OnClickListener,
		OnPreparedListener {
	VideoView video_player_view;
	DisplayMetrics dm;
	SurfaceView sur_View;
	MediaController media_Controller;
	ImageView playPauseImageView;
	LinearLayout back_layout,top_layout;
	TextView title;
	boolean music_was_playing;
	/**
	 * @author Speakapp
	 * This Activity is for playing the video which is stored in the raw folder.
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.video_lay);
		getInit();
	}

	/**
	 * This method is used for initializing the UI Components
	 */
	public void getInit() {
		music_was_playing = ((AudioManager) getSystemService(Context.AUDIO_SERVICE)).isMusicActive();
		back_layout = (LinearLayout) findViewById(R.id.back_layout);
		top_layout=(LinearLayout)findViewById(R.id.lay_one);
		playPauseImageView = (ImageView) findViewById(R.id.play_pause);
		title=(TextView)findViewById(R.id.video_title);
		title.setTypeface(DataEngine.gettypeface(this));
		playPauseImageView.setOnClickListener(this);
		back_layout.setOnClickListener(this);
		title.setOnClickListener(this);
		//top_layout.setOnClickListener(this);
		back_layout.setOnTouchListener(new OnTouchListener(){

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				finish();
				return false;
			}
			
		});

		// video_player_view.start();
	}
  
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.back_layout:
			finish();
			break;
		case R.id.video_title:
			finish();
			break;
		case R.id.play_pause:
			//playVideo();
          initVidoPlayerComponet();
			break;
		default:
			break;
		}
	}

	/**
	 * This method is used for playing the vvideo
	 */
	private void playVideo() {

		playPauseImageView.setVisibility(View.GONE);

		video_player_view.start();
	}
	/**
	 * This method is written for initializing the video components
	 */

	private void initVidoPlayerComponet() {

		video_player_view = (VideoView) findViewById(R.id.video_player_view);
		media_Controller = new MediaController(this);

		video_player_view.setMediaController(media_Controller);
		// video_player_view.setZOrderOnTop(true);
		String UrlPath = "android.resource://" + getPackageName() + "/"
				+ R.raw.speakapp;
		video_player_view.setVideoURI(Uri.parse(UrlPath));
		video_player_view.setOnPreparedListener(this);
		// video_player_view.setDrawingCacheEnabled(true);
		// video_player_view.setZOrderOnTop(true);
		video_player_view.setOnErrorListener(new OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				// TODO Auto-generated method stub
				return false;
			}

		});
		video_player_view.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
			}
		});
		
		video_player_view.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				playPauseImageView.setVisibility(View.VISIBLE);
				media_Controller = null;
				video_player_view = null;				
				System.gc();
				
				set_playaudio();
			}
		});
		playPauseImageView.setVisibility(View.GONE);

		video_player_view.start();
	}
	
	private void set_playaudio(){
		if (music_was_playing) {
            Intent i = new Intent("com.android.music.musicservicecommand");
            i.putExtra("command", "play");
            sendBroadcast(i);
        }
	}
	
	@Override
	protected void onDestroy() {
		
		DataEngine.clickFlag = false;
		set_playaudio();
		super.onDestroy();
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
	}

}
