package com.androidapps.speakapp.twitter;

import java.io.File;
import com.androidapps.speakapp.twitter.Twitter_Handler.TwDialogListener;
import com.androidapps.speakapp.util.MethodsHelper;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class Twitt_Sharing {

	private final Twitter_Handler mTwitter;
	private final Activity activity;
	private String twitt_msg;
	private File image_path;

	public Twitt_Sharing(Activity act, String consumer_key,
			String consumer_secret) {
		this.activity = act;
		mTwitter = new Twitter_Handler(activity, consumer_key, consumer_secret);
	}

	/**
	 * share message and image
	 * @param msg
	 * @param Image_url
	 */
	public void shareToTwitter(String msg, File Image_url) {
		this.twitt_msg = msg;
		this.image_path = Image_url;
		mTwitter.setListener(mTwLoginDialogListener);

		if (mTwitter.hasAccessToken()) {
			// this will post data in asyn background thread
			showTwittDialog();
		} else {
			mTwitter.authorize();
		}
	}

	/**
	 * start asynctask which post msg on twitter
	 */
	private void showTwittDialog() {

		new PostTwittTask().execute(twitt_msg);

	}

	private final TwDialogListener mTwLoginDialogListener = new TwDialogListener() {

		@Override
		public void onError(String value) {
			showToast("INLOGGNINGEN MISSLYCKADES");//Login Failed
			mTwitter.resetAccessToken();
		}

		@Override
		public void onComplete(String value) {
			showTwittDialog();
		}
	};

	/**
	 * show toast with provided msg
	 * @param msg
	 */
	void showToast(final String msg) {
		activity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				//Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
				String fmsg = msg.substring(0, 1).toUpperCase()+msg.substring(1).toLowerCase();
				//Toast.makeText(activity, fmsg, Toast.LENGTH_SHORT).show();
				MethodsHelper.showToast(fmsg, activity);

			}
		});

	}

	class PostTwittTask extends AsyncTask<String, Void, String> {
		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(activity);
			pDialog.setMessage("Inl�gget Twitt..."); //Posting Twitt
			pDialog.setCancelable(false);
			pDialog.show();
			super.onPreExecute();
		}

		@SuppressWarnings("static-access")
		@Override
		protected String doInBackground(String... twitt) {
			try {
				Share_Pic_Text_Titter(image_path, twitt_msg,
						mTwitter.twitterObj);
				return "success";

			} catch (Exception e) {
				if (e.getMessage().toString().contains("duplicate")) {
					return "Inl�gget har redan delats..."; //Posting Failed because of Duplicate message
				}
				e.printStackTrace();
				return "Inl�gget delades inte !!!";//Posting Failed
			}

		}

		@Override
		protected void onPostExecute(String result) {
			pDialog.dismiss();

			if (null != result && result.equals("success")) {
				showToast("Inl�gget delades"); // Posted Successfully

			} else {
				showToast(result);
			}

			super.onPostExecute(result);
		}
	}

	/**
	 * Share image and message
	 * @param image_path
	 * @param message
	 * @param twitter
	 * @throws Exception
	 */
	public void Share_Pic_Text_Titter(File image_path, String message,
			Twitter twitter) throws Exception {
		try {
			StatusUpdate st = new StatusUpdate(message);
			try{
			if(null != image_path)
			st.setMedia(image_path);
			}catch(Exception e)
			{
				
			}
			
			twitter.updateStatus(st);
		} catch (TwitterException e) {
			Log.d("TAG", "Pic Upload error" + e.getErrorMessage());
			throw e;
		}
	}

	public void Authorize_UserDetail() {

	}
}
