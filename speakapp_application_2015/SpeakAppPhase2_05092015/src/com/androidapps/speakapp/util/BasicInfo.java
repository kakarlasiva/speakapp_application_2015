package com.androidapps.speakapp.util;

import java.io.Serializable;


public class BasicInfo implements Serializable{
	
	static BasicInfo app_data;
	
	public static BasicInfo basic_info_obj(){
		if (null == app_data) {
			app_data = new BasicInfo();
		}
		return app_data;
	}
	
	private String min_version;
	private String max_version;
	
	public String getMin_version() {
		return min_version;
	}
	public void setMin_version(String min_version) {
		this.min_version = min_version;
	}
	public String getMax_version() {
		return max_version;
	}
	public void setMax_version(String max_version) {
		this.max_version = max_version;
	}

}
