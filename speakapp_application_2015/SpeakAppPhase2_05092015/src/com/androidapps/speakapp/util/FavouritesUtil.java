package com.androidapps.speakapp.util;

import java.util.Vector;

public class FavouritesUtil {
	private Vector<Favouritesdata> fav_fullvec;
	public FavouritesUtil() {
		// TODO Auto-generated constructor stub
		fav_fullvec=new Vector<FavouritesUtil.Favouritesdata>();
	}
	
	public Vector<Favouritesdata> getFav_fullvec() {
		return fav_fullvec;
	}

	public void setFav_fullvec(Vector<Favouritesdata> fav_fullvec) {
		this.fav_fullvec = fav_fullvec;
	}

	public class Favouritesdata{
		private String F_id;
		
		
		private String F_title;
		private String F_description ;
		private String F_date;
		private String F_image;
		private String F_newsid;
		public String getF_newsid() {
			return F_newsid;
		}
		public String getF_id() {
			return F_id;
		}
		public void setF_id(String f_id) {
			F_id = f_id;
		}
		public void setF_newsid(String f_newsid) {
			F_newsid = f_newsid;
		}
		
		public String getF_image() {
			return F_image;
		}
		public void setF_image(String f_image) {
			F_image = f_image;
		}
		public String getF_title() {
			return F_title;
		}
		public void setF_title(String f_title) {
			F_title = f_title;
		}
		public String getF_description() {
			return F_description;
		}
		public void setF_description(String f_description) {
			F_description = f_description;
		}
		public String getF_date() {
			return F_date;
		}
		public void setF_date(String f_date) {
			F_date = f_date;
		}
		
		
	}

}
