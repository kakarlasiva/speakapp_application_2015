package com.androidapps.speakapp.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

public class JustifiedTextView extends WebView{
    private String core      = "<html><body style='text-align:justify;color:rgba(%s);font-size:%dpx;margin: 10px 10px 10px 10px;'>%s</body></html>";
	//private String core="<html><body>%s</body></html>";
    private String textColor = "0,0,0,255";
    private String text      = "";
    private int textSize     = 16;
    //private int backgroundColor=Color.WHITE;

    public JustifiedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setWebChromeClient(new WebChromeClient(){});
    }

    public void setText(String s){
        this.text = s;
        reloadData();
    }

    @SuppressLint("NewApi")
    private void reloadData(){

        // loadData(...) has a bug showing utf-8 correctly. That's why we need to set it first.
        this.getSettings().setDefaultTextEncodingName("utf-8");
        this.getSettings().setJavaScriptEnabled(true);
        //String header = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
        this.loadData( String.format(core,textColor,textSize,text), "text/html; charset=UTF-8", null);
        //this.loadData(String.format(core,textColor,textSize,text), "text/html","charset=UTF-8");

        // set WebView's background color *after* data was loaded.
      //  super.setBackgroundColor(backgroundColor);
        

        // Hardware rendering breaks background color to work as expected.
        // Need to use software renderer in that case.
       /* if(android.os.Build.VERSION.SDK_INT >= 11)
            this.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);*/
    }
    
    

    public void setTextColor(int hex){
        String h = Integer.toHexString(hex);
        int a = Integer.parseInt(h.substring(0, 2),16);
        int r = Integer.parseInt(h.substring(2, 4),16);
        int g = Integer.parseInt(h.substring(4, 6),16);
        int b = Integer.parseInt(h.substring(6, 8),16);
        textColor = String.format("%d,%d,%d,%d", r, g, b, a); 
        reloadData();
    }
    
    private int titleHeight;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
       super.onMeasure(widthMeasureSpec, heightMeasureSpec);
       // determine height of title bar
       View title = getChildAt(0);
       titleHeight = title==null ? 0 : title.getMeasuredHeight();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev){
       return true;   // don't pass our touch events to children (title bar), we send these in dispatchTouchEvent
    }

    private boolean touchInTitleBar;
    @Override
    public boolean dispatchTouchEvent(MotionEvent me){

       boolean wasInTitle = false;
       switch(me.getActionMasked()){
       case MotionEvent.ACTION_DOWN:
          touchInTitleBar = (me.getY() <= visibleTitleHeight());
          break;

       case MotionEvent.ACTION_UP:
       case MotionEvent.ACTION_CANCEL:
          wasInTitle = touchInTitleBar;
          touchInTitleBar = false;
          break;
       }
       if(touchInTitleBar || wasInTitle) {
          View title = getChildAt(0);
          if(title!=null) {
             // this touch belongs to title bar, dispatch it here
             me.offsetLocation(0, getScrollY());
             return title.dispatchTouchEvent(me);
          }
       }
       // this is our touch, offset and process
       me.offsetLocation(0, -titleHeight);
       return super.dispatchTouchEvent(me);
    }

    /**
     * @return visible height of title (may return negative values)
     */
    private int visibleTitleHeight(){
       return titleHeight-getScrollY();
    }       

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt){
       super.onScrollChanged(l, t, oldl, oldt);
       View title = getChildAt(0);
       if(title!=null)   // undo horizontal scroll, so that title scrolls only vertically
          title.offsetLeftAndRight(l - title.getLeft());
    }

    @Override
    protected void onDraw(Canvas c){

       c.save();
       int tH = visibleTitleHeight();
       if(tH>0) {
          // clip so that it doesn't clear background under title bar
          int sx = getScrollX(), sy = getScrollY();
          c.clipRect(sx, sy+tH, sx+getWidth(), sy+getHeight());
       }
       c.translate(0, titleHeight);
       super.onDraw(c);
       c.restore();
    }

    public void setBackgroundColor(int hex){
       // backgroundColor = hex;
        reloadData();
    }

    public void setTextSize(int textSize){
        this.textSize = textSize;
        reloadData();
    }
   

	public void setMaxLines(int i) {
		// TODO Auto-generated method stub
		text.substring(0, i);
	}
}
