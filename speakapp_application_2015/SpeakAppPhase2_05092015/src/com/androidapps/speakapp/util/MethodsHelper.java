package com.androidapps.speakapp.util;

import com.androidapps.speakapp.R;
import com.androidapps.speakapp.data.DataEngine;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MethodsHelper {

	public static String getDeviceId(Context cntxt) {
		// TODO Auto-generated method stub
		String devId = null;
		devId = ((TelephonyManager) cntxt
				.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		if (null == devId) {
			devId = android.provider.Settings.Secure.getString(
					cntxt.getContentResolver(),
					android.provider.Settings.Secure.ANDROID_ID);
		}
		return devId;
	}

	public static void showToast(String msg, Context ctx) {
		String fmsg = msg.substring(0, 1).toUpperCase()
				+ msg.substring(1).toLowerCase();
		// Toast.makeText(ctx, fmsg, Toast.LENGTH_SHORT).show();
		LayoutInflater inflater = LayoutInflater.from(ctx);
		View layout = inflater.inflate(R.layout.toast_layout, null);

		TextView text = (TextView) layout.findViewById(R.id.text);
		text.setTypeface(DataEngine.gettypeface(ctx));
		text.setText(fmsg);

		Toast toast = new Toast(ctx);
		toast.setGravity(
				Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL,
				0,
				ctx.getResources().getDimensionPixelOffset(
						R.dimen.padding_frm_bottom));
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(layout);
		toast.show();
	}

	public static String encodeToBase64(String string) {
		String encodedString = "";
		try {
			byte[] byteData = null;
			if (Build.VERSION.SDK_INT >= 8) // Build.VERSION_CODES.FROYO --> 8
			{
				byteData = android.util.Base64.encode(string.getBytes(),
						android.util.Base64.DEFAULT);
			} else {
				//byteData = Base64Utility.encode(string.getBytes(),
						//Base64Utility.DEFAULT);
			}
			encodedString = new String(byteData);
		} catch (Exception e) {
		}
		return encodedString;
	}

	public static boolean getDensityName(Context context) {
		float density = context.getResources().getDisplayMetrics().density;
		if (density >= 4.0) {
			return true;// "xxxhdpi";
		}
		if (density >= 3.0) {
			return true;// "xxhdpi";
		}
		if (density >= 2.0) {
			return true;// "xhdpi";
		}
		if (density >= 1.5) {
			return false;// "hdpi";
		}
		if (density >= 1.0) {
			return false;// "mdpi";
		}
		return false;// "ldpi";
	}
}
