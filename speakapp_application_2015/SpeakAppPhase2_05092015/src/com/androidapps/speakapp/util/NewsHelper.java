package com.androidapps.speakapp.util;

import java.io.Serializable;
import java.util.Vector;


public class NewsHelper implements Serializable{
	private Vector<OldNews> oldnews_vec;
	private Vector<CurrentNews> currentnews_vec;
	
	public NewsHelper() {
		// TODO Auto-generated constructor stub
		oldnews_vec=new Vector<NewsHelper.OldNews>();
		currentnews_vec=new Vector<NewsHelper.CurrentNews>();
	}
	
	
	public Vector<OldNews> getOldnews_vec() {
		return oldnews_vec;
	}
	public void setOldnews_vec(Vector<OldNews> oldnews_vec) {
		this.oldnews_vec = oldnews_vec;
	}
	public Vector<CurrentNews> getCurrentnews_vec() {
		return currentnews_vec;
	}
	public void setCurrentnews_vec(Vector<CurrentNews> currentnews_vec) {
		this.currentnews_vec = currentnews_vec;
	}


	public class OldNews implements Serializable{
		private int oldnews_id;
		private String oldnews_startdate;
		private String oldnews_enddate;
		private String oldnews_title;
		private String oldnews_text;
		private String oldnews_image;
		public int getOldnews_id() {
			return oldnews_id;
		}
		public void setOldnews_id(int oldnews_id) {
			this.oldnews_id = oldnews_id;
		}
		public String getOldnews_startdate() {
			return oldnews_startdate;
		}
		public void setOldnews_startdate(String oldnews_startdate) {
			this.oldnews_startdate = oldnews_startdate;
		}
		public String getOldnews_enddate() {
			return oldnews_enddate;
		}
		public void setOldnews_enddate(String oldnews_enddate) {
			this.oldnews_enddate = oldnews_enddate;
		}
		public String getOldnews_title() {
			return oldnews_title;
		}
		public void setOldnews_title(String oldnews_title) {
			this.oldnews_title = oldnews_title;
		}
		public String getOldnews_text() {
			return oldnews_text;
		}
		public void setOldnews_text(String oldnews_text) {
			this.oldnews_text = oldnews_text;
		}
		public String getOldnews_image() {
			return oldnews_image;
		}
		public void setOldnews_image(String oldnews_image) {
			this.oldnews_image = oldnews_image;
		}
		
	}
	public class CurrentNews implements Serializable{
		private int currentnews_id;
		private String currentnews_startdate;
		private String currentnews_enddate;
		private String currentnews_title;
		private String currentnews_text;
		private String currentnews_image;
		public int getCurrentnews_id() {
			return currentnews_id;
		}
		public void setCurrentnews_id(int currentnews_id) {
			this.currentnews_id = currentnews_id;
		}
		public String getCurrentnews_startdate() {
			return currentnews_startdate;
		}
		public void setCurrentnews_startdate(String currentnews_startdate) {
			this.currentnews_startdate = currentnews_startdate;
		}
		public String getCurrentnews_enddate() {
			return currentnews_enddate;
		}
		public void setCurrentnews_enddate(String currentnews_enddate) {
			this.currentnews_enddate = currentnews_enddate;
		}
		public String getCurrentnews_title() {
			return currentnews_title;
		}
		public void setCurrentnews_title(String currentnews_title) {
			this.currentnews_title = currentnews_title;
		}
		public String getCurrentnews_text() {
			return currentnews_text;
		}
		public void setCurrentnews_text(String currentnews_text) {
			this.currentnews_text = currentnews_text;
		}
		public String getCurrentnews_image() {
			return currentnews_image;
		}
		public void setCurrentnews_image(String currentnews_image) {
			this.currentnews_image = currentnews_image;
		}
		
	}

}
