package com.androidapps.speakapp.util;

import java.util.ArrayList;
import java.util.Map;

public class OpinionViewHelper {
	String Name, Telephone, Epost, Message, deviceId, selectedItem,date;
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	ArrayList<String> subjects;
	Map subject_whole_data;

	public String getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(String selectedItem) {
		this.selectedItem = selectedItem;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Map getSubject_whole_data() {
		return subject_whole_data;
	}

	public void setSubject_whole_data(Map subject_whole_data) {
		this.subject_whole_data = subject_whole_data;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getTelephone() {
		return Telephone;
	}

	public void setTelephone(String telephone) {
		Telephone = telephone;
	}

	public String getEpost() {
		return Epost;
	}

	public void setEpost(String epost) {
		Epost = epost;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public ArrayList<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(ArrayList<String> subjects) {
		this.subjects = subjects;
	}

}
