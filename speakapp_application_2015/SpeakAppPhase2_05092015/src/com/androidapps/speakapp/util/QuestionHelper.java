package com.androidapps.speakapp.util;

import java.io.Serializable;
import java.util.Vector;

import org.apache.http.entity.SerializableEntity;

public class QuestionHelper implements Serializable{
	private int question_id;
	private String question;
	private String error_code;
	
	private boolean ans_status;
	
	private Vector<OptionsData> options;
	private Vector<SurveyData> survey;
    public QuestionHelper() {
		// TODO Auto-generated constructor stub
    	options=new Vector<QuestionHelper.OptionsData>();
    	survey=new Vector<QuestionHelper.SurveyData>();
	}
    public boolean isAns_status() {
		return ans_status;
	}
	public void setAns_status(boolean ans_status) {
		this.ans_status = ans_status;
	}
	public String getError_code() {
		return error_code;
	}
	public void setError_code(String error_code) {
		this.error_code = error_code;
	}
	public int getQuestion_id() {
		return question_id;
	}
	public void setQuestion_id(int question_id) {
		this.question_id = question_id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public Vector<OptionsData> getOptions() {
		return options;
	}
	public void setOptions(Vector<OptionsData> options) {
		this.options = options;
	}
	public Vector<SurveyData> getSurvey() {
		return survey;
	}
	public void setSurvey(Vector<SurveyData> survey) {
		this.survey = survey;
	}
	public class OptionsData implements Serializable {
		private int option_id;
		private String option_name;
		public int getOption_id() {
			return option_id;
		}
		public void setOption_id(int option_id) {
			this.option_id = option_id;
		}
		public String getOption_name() {
			return option_name;
		}
		public void setOption_name(String option_name) {
			this.option_name = option_name;
		}
		
		
	}
	public class SurveyData implements Serializable{
		private String surveyoption_name;
		private int value;
		private String color;
		public String getSurveyoption_name() {
			return surveyoption_name;
		}
		public void setSurveyoption_name(String surveyoption_name) {
			this.surveyoption_name = surveyoption_name;
		}
		public int getValue() {
			return value;
		}
		public void setValue(int value) {
			this.value = value;
		}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		
	}
}
