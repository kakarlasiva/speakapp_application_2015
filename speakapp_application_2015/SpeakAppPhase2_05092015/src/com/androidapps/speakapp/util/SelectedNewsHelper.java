package com.androidapps.speakapp.util;

import java.io.Serializable;

public class SelectedNewsHelper implements Serializable{
	
	private int selectednews_id;
	private String selectednews_startdate;
	private String selectednews_enddate;
	private String selectednews_title;
	private String selectednews_text;
	private String selectednews_image;
	public int getSelectednews_id() {
		return selectednews_id;
	}
	public void setSelectednews_id(int selectednews_id) {
		this.selectednews_id = selectednews_id;
	}
	public String getSelectednews_startdate() {
		return selectednews_startdate;
	}
	public void setSelectednews_startdate(String selectednews_startdate) {
		this.selectednews_startdate = selectednews_startdate;
	}
	public String getSelectednews_enddate() {
		return selectednews_enddate;
	}
	public void setSelectednews_enddate(String selectednews_enddate) {
		this.selectednews_enddate = selectednews_enddate;
	}
	public String getSelectednews_title() {
		return selectednews_title;
	}
	public void setSelectednews_title(String selectednews_title) {
		this.selectednews_title = selectednews_title;
	}
	public String getSelectednews_text() {
		return selectednews_text;
	}
	public void setSelectednews_text(String selectednews_text) {
		this.selectednews_text = selectednews_text;
	}
	public String getSelectednews_image() {
		return selectednews_image;
	}
	public void setSelectednews_image(String selectednews_image) {
		this.selectednews_image = selectednews_image;
	}
	

}
