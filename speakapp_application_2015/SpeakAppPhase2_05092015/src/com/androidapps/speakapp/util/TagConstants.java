package com.androidapps.speakapp.util;

public class TagConstants {
	
	 //  JSON Basic Info Node Names
    public static final String TAG_MINVERSION="MinVersion";
    public static final String TAG_MAXVERSION="MaxVersion";
   
    
    //  Question tags
    public static final String TAG_QUESTION="";
    public static final String TAG_QUESTIONID="Id";
    public static final String TAG_QUESTIONNAME="Text";
    public static final String TAG_OPTIONS="Options";
    public static final String TAG_OPTIONID="Id";
    public static final String TAG_OPTIONNAME="Text";
    public static final String TAG_ANSWERSTATUS="Answered";
    public static final String TAG_SURVEY="Survey";
    public static final String TAG_SURVEY_NAME="Option";
    public static final String TAG_SURVEY_COLOR="color";
    public static final String TAG_SURVEY_VALUE="Value";
    public static final String TAG_ERROR_CODE="ErrorCode";
    
    
    //  news Tags
    public static final String TAG_OLDNEWS="OldNews";
    public static final String TAG_CURRENTNEWS="ActiveNews";
    public static final String TAG_NEWSID="Id";
    public static final String TAG_NEWSSTARTDATE="StartDate";
    public static final String TAG_NEWSENDDATE="EndDate";
    public static final String TAG_NEWSTEXT="Text";
    public static final String TAG_NEWSIMAGE="Image";
    public static final String TAG_NEWSTITLE="Title";
    
    
}
